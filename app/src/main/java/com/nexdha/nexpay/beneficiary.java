package com.nexdha.nexpay;

public class beneficiary {
    private String amount;
    private String name;
    private String created_at;
    private String id;
    private String nickname;

    public beneficiary(String amount, String name, String created_at, String id, String nickname) {
        this.amount = amount;
        this.name = name;
        this.created_at = created_at;
        this.id = id;
        this.nickname = nickname;
    }

    public String getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getid() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }
    /*   public beneficiary(String amount, String name, String created_at) {
        this.amount = amount;
        this.name = name;
        this.created_at = created_at;
    }

    public String getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }
    public String getCreateddate() {
        return created_at;
    }

    */
}
