package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class resendotp extends AppCompatActivity {
    Button resend,resend2button;
    EditText resendotpbox;
    String ResendOtpverify;
    public static final String WELCOMEPREFERENCE = "welpref";
    SharedPreferences welcomepreference;
    TextView resend2otpcoundown;
    String timeRestriction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_resendotp);

        otp2coundown();
        resendotpbox = findViewById(R.id.editText4);
        resend = (Button) findViewById(R.id.button14);
        resend2button = (Button) findViewById(R.id.button18);
        resend2button.setOnClickListener(view -> {
            resend2otpmethod();
            //  otp2coundown();
            resend2otpcoundown.setText("OTP Sent");
        });
        resend.setOnClickListener(view -> {
            if (!ValidationresendOTP()) {
                resend.setEnabled(true);
                resend.setBackgroundResource(R.drawable.roundedbutton);

                return;
            } else {
                resend.setEnabled(false);
                resend.setBackgroundResource(R.drawable.pageoneconstraintthree);
            }


            final String resendotpinput = resendotpbox.getText().toString().trim();
            welcomepreference = getSharedPreferences(WELCOMEPREFERENCE, Context.MODE_PRIVATE);


            SharedPreferences sharedPreferences = resendotp.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            final String token1 = sharedPreferences.getString("token", "");
            System.out.println("----------------------------------------------------------------------");
            System.out.println(token1);
            final String token200 = token1;


            StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/verify_otp",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                System.out.println("***********Verify Otp****************");
                                System.out.println(response);
                                JSONObject otpresponse = new JSONObject(response);
                                ResendOtpverify = otpresponse.getString("Details");
                                System.out.println("***********Details****************");
                                System.out.println(ResendOtpverify);
                                if (ResendOtpverify.equals("OTP Matched")) {

                                    if (!welcomepreference.contains("verify")) {
                                        SharedPreferences.Editor editor = welcomepreference.edit();
                                        editor.putString("verify", "{'verify':'true'}");
                                        editor.apply();
                                        Intent intent = new Intent(resendotp.this, Dashboard.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        SharedPreferences.Editor editor = welcomepreference.edit();
                                        editor.putString("verify", "{'verify':'true'}");
                                        editor.apply();
                                        Intent intent = new Intent(resendotp.this, Dashboard.class);
                                        startActivity(intent);
                                        finish();
                                    }


                                } else {
                                    // System.out.println("this is else part");
                                    Toast.makeText(resendotp.this, "Otp Incorrect", Toast.LENGTH_SHORT).show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    parseVolleyError(error);
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ServerError) {
                        message = "";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ParseError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection Timedout......please check your internet connection";
                    }
                    if (!message.isEmpty()) {
                        Toast.makeText(resendotp.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                //This is for Headers If You Needed

                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("otp", resendotpinput);


                    return params;

                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String T = "";
                    Map<String, String> params = new HashMap<>();


                    // params.put("Content-Type", "application/json; charset=UTF-8");
                    try {
                        JSONObject response1 = new JSONObject(token200);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        // do your work with response object
                        T = response1.getString("token");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("44444444444444444444444444");
                    System.out.println(token200);
                    ///check t!blank

                    String TokenS = "token " + T;
                    System.out.println(T);

                    params.put("Authorization", TokenS);

                    return params;
                }
            };


            RequestQueue requestQueue = Volley.newRequestQueue(resendotp.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);


        });
    }
    public void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            String otperror = data.getString("error");

            System.out.println("OTP");
            System.out.println(responseBody);
            System.out.println(data);
            System.out.println(otperror);
            if (otperror.equals("OTP does not match")){
                Toast.makeText(resendotp.this,"Invalid OTP. Please Enter valid OTP",Toast.LENGTH_LONG).show();
                resend.setEnabled(true);
                resend.setBackgroundResource(R.drawable.roundedbutton);


            }

        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }
    private boolean ValidationresendOTP() {

        final String resendotpinput = resendotpbox.getText().toString().trim();

        if (resendotpinput.length() == 0) {
            Toast.makeText(resendotp.this, "Please Enter OTP ", Toast.LENGTH_LONG).show();
            return false;
        }

        if ((resendotpinput.length() == 6) || (resendotpinput.length() == 4)) {
            System.out.println(resendotpinput.length());
            return true;
        } else {
            Toast.makeText(this, "Please Enter Correct OTP", Toast.LENGTH_LONG).show();
            return false;
        }

    }
    public void otp2coundown(){
        resend2otpcoundown=(TextView) findViewById(R.id.textView129);
        new CountDownTimer(90000, 1000) {

            public void onTick(long millisUntilFinished) {
                resend2button.setEnabled(false);
                resend2otpcoundown.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                resend2otpcoundown.setText("Click To send OTP");
                resend2button.setTextColor(Color.parseColor("#ffffff"));
                resend2button.setEnabled(true);
                resend2button.setBackgroundResource(R.drawable.roundedbutton);

            }
        }.start();

    }
    private void resend2otpmethod(){
        SharedPreferences sharedPreferences = resendotp.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserid",
                response -> {
                    System.out.println("***************openotppage*******************");
                    System.out.println(response);
                    JSONArray jsonarray = null;
                    try {
                        JSONObject otpresponse = new JSONObject(response);
                        timeRestriction = otpresponse.getString("status");

                        if (timeRestriction.equals("otp have time restriction")) {
                            Toast.makeText(resendotp.this, "Next OTP will sent after 60 seconds", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(resendotp.this, "OTP sent", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {
            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(resendotp.this, message, Toast.LENGTH_SHORT).show();
            }
        })

        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(resendotp.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
}
