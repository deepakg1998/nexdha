package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SECRET_KEY;
import static com.nexdha.nexpay.constant.SERVER;
import static com.nexdha.nexpay.constant.url;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class SignUp extends AppCompatActivity implements View.OnClickListener {

    EditText name, email, phone, promo;
    Button buttonsignup;
    TextView button6;
    ProgressBar progress;
    View Mysnackview;
    public static final String MYPREFERENCES = "MyPrefs";
    SharedPreferences sharedPreferences;
    String TAG = MainActivity.class.getSimpleName();
    RequestQueue queue;
    Dialog loadingDialog;
    LayoutInflater inflater;
    String promoCode;
    String saltKey = MainActivity.getNativeKey1(), getHashKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_sign_up);
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.loading_layout, null));
        loadingDialog.setCancelable(false);


        progress = findViewById(R.id.progressBar2);
        progress.setVisibility(View.GONE);
        name = findViewById(R.id.editText8);
        email = findViewById(R.id.editText11);
        phone = findViewById(R.id.editText12);
        promo = findViewById(R.id.promo_code);
        queue = Volley.newRequestQueue(getApplicationContext());


        sharedPreferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);

        buttonsignup = findViewById(R.id.button4);
        buttonsignup.setOnClickListener(this);

        button6 = findViewById(R.id.button6);
        button6.setOnClickListener(v -> openLogin());
    }

    @Override
    public void onClick(View view) {

        if (Validationsignup()) {
            Mysnackview = view;

            promoCode = promo.getText().toString().trim().toUpperCase(Locale.ROOT);
            if (promoCode.isEmpty()) {
                promoCode = "EMPTY";
            } else {
                promoCode = promo.getText().toString().trim().toUpperCase(Locale.ROOT);
            }

            try {
                final String username = name.getText().toString().trim();
                final String useremail = email.getText().toString().trim();
                final String userphone = phone.getText().toString().trim();
                final String result = userphone.substring(Math.max(phone.length() - 10, 0)).replaceAll("-", "");

                getHashKey = result + promoCode + useremail + username + saltKey;
                MessageDigest md = MessageDigest.getInstance("SHA-512");
                byte[] digest = md.digest(getHashKey.getBytes());
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < digest.length; i++) {
                    sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                }
                System.out.println(sb);
                openSignUpApi(sb.toString());

            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
            loadingDialog.show();

         /*   SafetyNet.getClient(SignUp.this).verifyWithRecaptcha(SITE_KEY.trim())
                    .addOnSuccessListener(this, response -> {
                        if (!Objects.requireNonNull(response.getTokenResult()).isEmpty()) {
                            handleSiteVerify(response.getTokenResult());
                        }
                    })
                    .addOnFailureListener(this, e -> {
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            int status = apiException.getStatusCode();
                            System.out.println("Error code: " + status);
                            Log.d(TAG, "Error message: " +
                                    CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                        } else {
                            Log.d(TAG, "Unknown type of error: " + e.getMessage());
                        }
                    });  */
        }


    }

    private void handleSiteVerify(String responseToken) {
        StringRequest request = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("success")) {
                            promoCode = promo.getText().toString().trim().toUpperCase(Locale.ROOT);
                            if (promoCode.isEmpty()) {
                                promoCode = "EMPTY";
                            } else {
                                promoCode = promo.getText().toString().trim().toUpperCase(Locale.ROOT);
                            }

                            try {
                                final String username = name.getText().toString().trim();
                                final String useremail = email.getText().toString().trim();
                                final String userphone = phone.getText().toString().trim();
                                final String result = userphone.substring(Math.max(phone.length() - 10, 0)).replaceAll("-", "");

                                getHashKey = result + promoCode + useremail + username + saltKey;
                                MessageDigest md = MessageDigest.getInstance("SHA-512");
                                byte[] digest = md.digest(getHashKey.getBytes());
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < digest.length; i++) {
                                    sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                                }
                                System.out.println(sb);
                                openSignUpApi(sb.toString());

                            } catch (NoSuchAlgorithmException e) {
                                throw new RuntimeException(e);
                            }
                            loadingDialog.show();
                            //Toast.makeText(SignUp.this, "You are not a robot", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignUp.this, "Human verification failed. Please try again later ", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "JSON exception: " + ex.getMessage());

                    }

                },
                error -> {
                    Toast.makeText(SignUp.this, "Server error, Please try again later", Toast.LENGTH_SHORT).show();
                    finish();


                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("secret", SECRET_KEY.trim());
                params.put("response", responseToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);


    }

    private void openSignUpApi(String sb) {

        progress.setVisibility(View.GONE);
        final String username = name.getText().toString().trim();
        final String useremail = email.getText().toString().trim();
        final String userphone = phone.getText().toString().trim();
        final String result = userphone.substring(Math.max(phone.length() - 10, 0)).replaceAll("-", "");
        System.out.println(result);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/save_users_2022",
                response -> {

                    try {
                        JSONObject response1 = new JSONObject(response);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        if (response1.getString("token").equals("Number Exists")) {
                            loadingDialog.dismiss();
                            Toast.makeText(SignUp.this, "Mobile Number Exists, Please Login", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(SignUp.this, LoginPage.class);
                            startActivity(intent);
                            progress.setVisibility(View.GONE);
                        } else if (response1.getString("token").equals("Promo Code not Valid")) {
                            loadingDialog.dismiss();
                            promo.setError("Invalid Promo Code");
                            progress.setVisibility(View.GONE);
                        } else if (response1.getString("token").equals("success")) {
                            loadingDialog.dismiss();
                            Intent intent = new Intent(SignUp.this, OTP.class);
                            intent.putExtra("FROM_ACTIVITY", "Signup");
                            intent.putExtra("PHONE", result);
                            startActivity(intent);
                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }, error -> {
            loadingDialog.dismiss();
            parseVolleyError(error);
            progress.setVisibility(View.GONE);

            if (error instanceof NoConnectionError) {
                Snackbar.make(Mysnackview, "Cannot connect to internet, please check your internet connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet, please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet, please check your internet connection";
            }
            else if (error instanceof ParseError) {
                message = "Cannot connect to internet, please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout, please check your internet connection";
            }
            if (!message.isEmpty()) {
                //  Toast.makeText(SignUp.this, message, Toast.LENGTH_SHORT).show();
                Snackbar.make(Mysnackview, message, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", username);
                params.put("email", useremail);
                params.put("phone", result);
                params.put("oss", "Android");
                params.put("promo", promoCode);
                params.put("hash_signup", sb);

                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(SignUp.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }


    public void parseVolleyError(VolleyError error) {
        try {
            if (error.networkResponse != null && error.networkResponse.data != null) {
                String responseBody = new String(error.networkResponse.data, "utf-8");
                JSONObject data = new JSONObject(responseBody);
                if (responseBody.contains("email")) {
                    System.out.println("Email");
                    String storeemail = data.getString("email");
                    if (storeemail.contains("Enter a valid email address.")) {
                        Toast.makeText(SignUp.this, "Enter a valid Email Address", Toast.LENGTH_LONG).show();
                        progress.setVisibility(View.GONE);
                    }
                }
            } else {
                Toast.makeText(SignUp.this, "Oops! something went wrong", Toast.LENGTH_LONG).show();
                progress.setVisibility(View.GONE);
            }


        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        progress.setVisibility(View.GONE);
    }



    public void openLogin() {
        Intent intent = new Intent(this, LoginPage.class);
        startActivity(intent);
        finish();
       // Toast.makeText(SignUp.this,"Availabe in Next Update",Toast.LENGTH_LONG).show();
    }

    private void openotppage(){
        SharedPreferences sharedPreferences = SignUp.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserid",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("***************openotppage*******************");
                        System.out.println(response);
                       // Intent intent = new Intent(SignUp.this, Payment.class);
                       // startActivity(intent);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(SignUp.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(SignUp.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
    private boolean Validationsignup(){
        final String username = name.getText().toString().trim();
        final String useremail = email.getText().toString().trim();
        final String userphone = phone.getText().toString().trim();


        if (username.isEmpty() || username.length() == 0 || username.equals("") || username == null){
            Toast.makeText(SignUp.this,"Please Enter Name ",Toast.LENGTH_LONG).show();
            return false;
        }
        if (username.length() >= 30){
            Toast.makeText(SignUp.this,"Please Enter Valid Name ",Toast.LENGTH_LONG).show();
            return false;
        }
        if (useremail.isEmpty() || useremail.length() == 0 || useremail.equals("") || useremail == null){
            Toast.makeText(SignUp.this,"Please Enter Email ",Toast.LENGTH_LONG).show();
            return false;
        }
        if (useremail.length() >= 50){
            Toast.makeText(SignUp.this, "Please Enter Valid Email", Toast.LENGTH_LONG).show();
            return false;
        }
        if (userphone.isEmpty() || userphone.length() == 0 || userphone.equals("") || userphone == null){
            Toast.makeText(SignUp.this,"Please Enter Phone Number ",Toast.LENGTH_LONG).show();
            return false;
        }
        if (userphone.isEmpty() || userphone.length() <= 9){
            Toast.makeText(SignUp.this,"Please Enter Correct Phone Number ",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;

    }


}
