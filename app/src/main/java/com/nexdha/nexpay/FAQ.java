package com.nexdha.nexpay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nexdha.nexpay.constant.SERVER;

public class FAQ extends AppCompatActivity {
    ScrollView faqscrollview;
    TextView whatisnexdhaquestion,whatisnexdhaanswer,whatisnexdhasubanswer,
            howtousenexdhaquestion,howtousenexdhaanswer,howtousenexdhasubanswer,
            viewprofiledetailsquestion,viewprofiledetailsanswer,viewprofiledetailssubanswer,
            howtoaddbeneficiaryquestion,howtoaddbeneficiaryanswer,howtoaddbeneficiarysubanswer,
            conveniencefeequestion,conveniencefeeanswer,conveniencefeesubanswer,
    dashboarddetailsquestion,dashboarddetailsanswer,dashboarddetailsubanswer,
    transactionfailsquestion,transactionfailsanswer,transactionfailssubanswer,
            gatewayacceptancequestion,gatewayacceptanceanswer,gatewayacceptancesubanswer,
    gatewayloadingquestion,gatewayloadinganswer,gatewayloadingsubanswer,
    amountdebitquestion,amountdebitanswer,amountdebitsubanswer,
    debitnotreachedquestion,debitnotreachedanswer,debitnotreachedsubanswer,
    amounttimedurationquestion,amounttimedurationanswer,amounttimedurationsubanswer,
    appsecurityquestion,appsecurityanswer,appsecuritysubanswer,
    issuenotlistedquestion,issuenotlistedanswer,issuenotlistedsubanswer;
    String emailto,emailsubject,emailissuecontent = "",sendername,sendernumber;
    EditText emailcontent;
    Button emailbutton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_faq);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        mailprofile();
        activity_log_faq();

        faqscrollview=(ScrollView)findViewById(R.id.faqscrollview);


     /*   nsv.setOnScrollChangeListener(new ScrollView().OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    fab.hide();
                } else {
                    fab.show();
                }
            }
        });
*/

        whatisnexdhaquestion=(TextView)findViewById(R.id.textView136);
        whatisnexdhaanswer=(TextView)findViewById(R.id.textView151);
        whatisnexdhasubanswer=(TextView)findViewById(R.id.textView153);

        whatisnexdhaquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonwhatisnexdhaanswer();
            }
        });
        howtousenexdhaquestion=(TextView)findViewById(R.id.textView137);
        howtousenexdhaanswer=(TextView)findViewById(R.id.textView154);
        howtousenexdhasubanswer=(TextView)findViewById(R.id.textView155);
        howtousenexdhaquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonhowtousenexdhaanswer();
            }
        });
        viewprofiledetailsquestion=(TextView)findViewById(R.id.textView138);
        viewprofiledetailsanswer=(TextView)findViewById(R.id.textView156);
        viewprofiledetailssubanswer=(TextView)findViewById(R.id.textView157);
        viewprofiledetailsquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonviewprofiledetailsanswer();
            }
        });
        howtoaddbeneficiaryquestion=(TextView)findViewById(R.id.textView139);
        howtoaddbeneficiaryanswer=(TextView)findViewById(R.id.textView158);
        howtoaddbeneficiarysubanswer=(TextView)findViewById(R.id.textView159);
        howtoaddbeneficiaryquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonaddbeneficiaryanswer();
            }
        });
        conveniencefeequestion=(TextView)findViewById(R.id.textView150);
        conveniencefeeanswer=(TextView)findViewById(R.id.textView160);
        conveniencefeesubanswer=(TextView)findViewById(R.id.textView161);
        conveniencefeequestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonconveniencefee();
            }
        });

        dashboarddetailsquestion=(TextView)findViewById(R.id.textView141);
        dashboarddetailsanswer=(TextView)findViewById(R.id.textView164);
        dashboarddetailsubanswer=(TextView)findViewById(R.id.textView167);
        dashboarddetailsquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusondashboardetails();
            }
        });
        transactionfailsquestion=(TextView)findViewById(R.id.textView142);
        transactionfailsanswer=(TextView)findViewById(R.id.textView168);
        transactionfailssubanswer=(TextView)findViewById(R.id.textView170);
        transactionfailsquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusontransactionfails();
            }
        });
        gatewayacceptancequestion=(TextView)findViewById(R.id.textView143);
        gatewayacceptanceanswer=(TextView)findViewById(R.id.textView171);
        gatewayacceptancesubanswer=(TextView)findViewById(R.id.textView172);
        gatewayacceptancequestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusongatewayacceptance();
            }
        });
        gatewayloadingquestion=(TextView)findViewById(R.id.textView144);
        gatewayloadinganswer=(TextView)findViewById(R.id.textView173);
        gatewayloadingsubanswer=(TextView)findViewById(R.id.textView174);
        gatewayloadingquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusongatewayloading();
            }
        });
        amountdebitquestion=(TextView)findViewById(R.id.textView145);
        amountdebitanswer=(TextView)findViewById(R.id.textView166);
        amountdebitsubanswer=(TextView)findViewById(R.id.textView169);
        amountdebitquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonamountdebit();
            }
        });
        debitnotreachedquestion=(TextView)findViewById(R.id.textView146);
        debitnotreachedanswer=(TextView)findViewById(R.id.textView175);
        debitnotreachedsubanswer=(TextView)findViewById(R.id.textView176);
        debitnotreachedquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonbenenotreached();
            }
        });
        amounttimedurationquestion=(TextView)findViewById(R.id.textView147);
        amounttimedurationanswer=(TextView)findViewById(R.id.textView177);
        amounttimedurationsubanswer=(TextView)findViewById(R.id.textView178);
        amounttimedurationquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonamounttimeduration();
            }
        });
        appsecurityquestion=(TextView)findViewById(R.id.textView148);
        appsecurityanswer=(TextView)findViewById(R.id.textView179);
        appsecuritysubanswer=(TextView)findViewById(R.id.textView180);
        appsecurityquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonappsecurity();
            }
        });
        issuenotlistedquestion=(TextView)findViewById(R.id.textView149);
        issuenotlistedanswer=(TextView)findViewById(R.id.textView181);
        issuenotlistedsubanswer=(TextView)findViewById(R.id.textView182);
        issuenotlistedquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusonissuenotlisted();
            }
        });

        emailbutton=findViewById(R.id.button20);
        emailbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (issuevalidation() == false){
                    return;
                }

                emailto = "support@nexdha.com";
                emailsubject = "Issue from " + sendername + " with number " + sendernumber;
                emailcontent= (EditText)findViewById(R.id.editText9);
                emailissuecontent = emailcontent.getText().toString().trim();
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{ emailto});
                //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
                //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
                email.putExtra(Intent.EXTRA_SUBJECT, emailsubject);
                email.putExtra(Intent.EXTRA_TEXT, "My name is " + sendername + ", my mobile number is " + sendernumber + "." + "\r\n \r\nIssue description : " + emailissuecontent);

                //need this to prompts email client only
                email.setType("message/rfc822");

                startActivity(Intent.createChooser(email, "Choose:"));

            }
        });

    }

    private void focusonwhatisnexdhaanswer(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, whatisnexdhaanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        whatisnexdhaanswer.setTextColor(Color.parseColor("#3EF611"));
                        whatisnexdhasubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        whatisnexdhaanswer.setTextColor(Color.parseColor("#122c3d"));
                        whatisnexdhasubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonhowtousenexdhaanswer(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, howtousenexdhaanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        howtousenexdhaanswer.setTextColor(Color.parseColor("#3EF611"));
                        howtousenexdhasubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        howtousenexdhaanswer.setTextColor(Color.parseColor("#122c3d"));
                        howtousenexdhasubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonviewprofiledetailsanswer(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, viewprofiledetailsanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        viewprofiledetailsanswer.setTextColor(Color.parseColor("#3EF611"));
                        viewprofiledetailssubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        viewprofiledetailsanswer.setTextColor(Color.parseColor("#122c3d"));
                        viewprofiledetailssubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonaddbeneficiaryanswer(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, howtoaddbeneficiaryanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        howtoaddbeneficiaryanswer.setTextColor(Color.parseColor("#3EF611"));
                        howtoaddbeneficiarysubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        howtoaddbeneficiaryanswer.setTextColor(Color.parseColor("#122c3d"));
                        howtoaddbeneficiarysubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonconveniencefee(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, conveniencefeeanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        conveniencefeeanswer.setTextColor(Color.parseColor("#3EF611"));
                        conveniencefeesubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        conveniencefeeanswer.setTextColor(Color.parseColor("#122c3d"));
                        conveniencefeesubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusondashboardetails(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, dashboarddetailsanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        dashboarddetailsanswer.setTextColor(Color.parseColor("#3EF611"));
                        dashboarddetailsubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        dashboarddetailsanswer.setTextColor(Color.parseColor("#122c3d"));
                        dashboarddetailsubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusontransactionfails(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, transactionfailsanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        transactionfailsanswer.setTextColor(Color.parseColor("#3EF611"));
                        transactionfailssubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        transactionfailsanswer.setTextColor(Color.parseColor("#122c3d"));
                        transactionfailssubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusongatewayacceptance(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, gatewayacceptanceanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        gatewayacceptanceanswer.setTextColor(Color.parseColor("#3EF611"));
                        gatewayacceptancesubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        gatewayacceptanceanswer.setTextColor(Color.parseColor("#122c3d"));
                        gatewayacceptancesubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusongatewayloading(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, gatewayloadinganswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        gatewayloadinganswer.setTextColor(Color.parseColor("#3EF611"));
                        gatewayloadingsubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        gatewayloadinganswer.setTextColor(Color.parseColor("#122c3d"));
                        gatewayloadingsubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonamountdebit(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, amountdebitanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        amountdebitanswer.setTextColor(Color.parseColor("#3EF611"));
                        amountdebitsubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        amountdebitanswer.setTextColor(Color.parseColor("#122c3d"));
                        amountdebitsubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonbenenotreached(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, debitnotreachedanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        debitnotreachedanswer.setTextColor(Color.parseColor("#3EF611"));
                        debitnotreachedsubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        debitnotreachedanswer.setTextColor(Color.parseColor("#122c3d"));
                        debitnotreachedsubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonamounttimeduration(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, amounttimedurationanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        amounttimedurationanswer.setTextColor(Color.parseColor("#3EF611"));
                        amounttimedurationsubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        amounttimedurationanswer.setTextColor(Color.parseColor("#122c3d"));
                        amounttimedurationsubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonappsecurity(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, appsecurityanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        appsecurityanswer.setTextColor(Color.parseColor("#3EF611"));
                        appsecuritysubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        appsecurityanswer.setTextColor(Color.parseColor("#122c3d"));
                        appsecuritysubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void focusonissuenotlisted(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                faqscrollview.scrollTo(0, issuenotlistedanswer.getTop());
                new CountDownTimer(1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        issuenotlistedanswer.setTextColor(Color.parseColor("#3EF611"));
                        issuenotlistedsubanswer.setTextColor(Color.parseColor("#3EF611"));
                    }

                    public void onFinish() {
                        issuenotlistedanswer.setTextColor(Color.parseColor("#122c3d"));
                        issuenotlistedsubanswer.setTextColor(Color.parseColor("#245C80"));
                    }
                }.start();
            }
        });
    }
    private void mailprofile(){
        SharedPreferences sharedPreferences = FAQ.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            sendername = profileresponse.getString("name");
                            sendernumber = profileresponse.getString("phone");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //     message = "cannot connect";
                    new MaterialStyledDialog.Builder(FAQ.this)
                            .setTitle("Internet Error")
                            .setDescription("There will be issue displaying your info,Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(FAQ.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(FAQ.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }
    private boolean issuevalidation(){

        emailcontent= (EditText)findViewById(R.id.editText9);
        emailissuecontent = emailcontent.getText().toString().trim();

        if (emailissuecontent.isEmpty() || emailissuecontent.length() == 0 || emailissuecontent.equals("") || emailissuecontent == null){
            Toast.makeText(FAQ.this,"Please describe your issue ",Toast.LENGTH_LONG).show();
            return false;
        }


        return true;


    }
    @Override
    public void onResume(){
        super.onResume();
        emailcontent= (EditText)findViewById(R.id.editText9);
        emailcontent.setText("");
        emailissuecontent = "";
    }
    private void activity_log_faq(){

        SharedPreferences sharedPreferences = FAQ.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(FAQ.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(FAQ.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","118");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(FAQ.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
}
