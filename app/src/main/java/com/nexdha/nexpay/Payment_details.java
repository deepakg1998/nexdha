package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.ncorti.slidetoact.SlideToActView;
import com.nexdha.nexpay.activity.PayUGatewayActivity;
import com.nexdha.nexpay.activity.TransactionCancelledActivity;
import com.nexdha.nexpay.activity.TransactionSuccessActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class Payment_details extends AppCompatActivity {
    TextView beneficiaryname, beneficiary_nick_name, beneficiary_account_number, beneficiary_ifsc, beneficiary_phone_display, transaction_type_display, card_type_display, beneficiary_receives_amt, final_amount, transac_eta, transact_cf, date_reminder;
    DecimalFormat formatter, formatter_two;
    String final_calculated_amount;
    CheckBox termsandcond;
    boolean check_terms;

    String final_percentage;
    TextView terms_and_condtions_text;
    ProgressBar gateway_progress;
    String[] beneficiary_array;

    ImageView imageview_71;


    String retrying_tran;
    Dialog loadingDialog;
    LayoutInflater inflater;
    String saltKey = MainActivity.getNativeKey1(), getHashKey;
    StringBuilder hashString;


    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n", "DefaultLocale"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_payment_details);

        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.loading_layout, null));
        loadingDialog.setCancelable(false);

        retrying_tran = "";

        terms_and_condtions_text = findViewById(R.id.textView303);
        terms_and_condtions_text.setOnClickListener(view -> new MaterialStyledDialog.Builder(Payment_details.this)
                .setTitle("Terms and Conditions")
                .setDescription("I agree that Nexdha Fintech LLP (NFL) allows me to transfer money using Payment Gateways. I also certify that I have received the services satisfactory and am making the payment for the same. I further agree that NFL is not liable for any refund request. All refund request to be taken with the vendor/seller. Any chargeback request will be summarily rejected.")
                .setHeaderColor(R.color.md_divider_white)
                .setIcon(R.drawable.aboutus1)
                .withDarkerOverlay(true)
                .withDialogAnimation(true)
                .setCancelable(false)
                .withDivider(true)
                .setPositiveText("Close")
                .onPositive((dialog, which) -> dialog.dismiss())
                .show());
        activity_log();
        sessionCheck();

        imageview_71 = findViewById(R.id.imageView71);
        imageview_71.setOnClickListener(view -> finish());
        gateway_progress = findViewById(R.id.progressBar24);
        gateway_progress.setVisibility(View.GONE);
        loadingDialog.dismiss();

        formatter = new DecimalFormat("##,##,##,##,##,##,##,##,##,###.##");
        formatter_two = new DecimalFormat("##,##,##,##,##,##,##,##,##,###.00");


        final_amount = findViewById(R.id.textView299);
        final_amount.setText("₹ " + new_payment.final_amt);
        final_calculated_amount = new_payment.final_amt;


        beneficiaryname = findViewById(R.id.textView279);
        beneficiary_array = new_payment.beneficiary_name.split("\\(");
        beneficiaryname.setText(String.valueOf(beneficiary_array[0]));

        beneficiary_nick_name = findViewById(R.id.textView281);
        String[] beneficiary_nickname_array = String.valueOf(beneficiary_array[1]).split("\\)");
        beneficiary_nick_name.setText(String.valueOf(beneficiary_nickname_array[0]));

        date_reminder = findViewById(R.id.bill_reminder_date);
        billreminder();

        beneficiary_account_number = findViewById(R.id.textView283);
        beneficiary_account_number.setText(new_payment.account_number);

        beneficiary_ifsc = findViewById(R.id.textView285);
        beneficiary_ifsc.setText(new_payment.ifsc);

        beneficiary_phone_display = findViewById(R.id.textView287);
        beneficiary_phone_display.setText(new_payment.beneficiary_phone);

        transaction_type_display = findViewById(R.id.textView289);
        String[] display_trans_type = new_payment.transaction_display.split("\\(");
        transaction_type_display.setText(String.valueOf(display_trans_type[0]));

        card_type_display = findViewById(R.id.textView291);
        card_type_display.setText(new_payment.card_type);

        transact_cf = findViewById(R.id.textView293);
        transact_cf.setText(new_payment.percentage + " %");

        transac_eta = findViewById(R.id.textView301);
        transac_eta.setText(new_payment.eta_tran);

        beneficiary_receives_amt = findViewById(R.id.textView295);
        beneficiary_receives_amt.setText("₹ " + new_payment.beneficiary_amount);

        final SlideToActView gateway = findViewById(R.id.example);
        gateway.setLocked(true);
        termsandcond = findViewById(R.id.checkBox12);
        termsandcond.setOnCheckedChangeListener((compoundButton, b) -> {
            if (termsandcond.isChecked()) {
                check_terms = true;
                gateway.setLocked(false);

            } else {
                check_terms = false;
                gateway.setLocked(true);
            }
        });
        gateway.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                if (!termsandcond.isChecked()) {
                    Toast.makeText(Payment_details.this, "Agree to Terms and conditions", Toast.LENGTH_SHORT).show();
                }
            }
            return false;
        });

        gateway.setBumpVibration(100);

        gateway.setOnSlideCompleteListener(view -> {
            System.out.println("printed");
            if (check_terms) {
                payUGateway();
            }

        });

    }

    private void payUGateway() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/M20d2KKm",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println(response);
                        String getResponse = jsonObject.getString("statuss");
                        if (getResponse.equals("success")) {
                            Intent intent = new Intent(this, PayUGatewayActivity.class);
                            intent.putExtra("URL", jsonObject.getString("gateway"));
                            startActivityForResult(intent, 101);
                        } else {
                            Toast.makeText(this, jsonObject.getString("gateway"), Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            protected Map<String, String> getParams() {
                try {
                    getHashKey = final_calculated_amount + new_payment.purpose_of_payment + "Android" + "udf2" + String.valueOf(beneficiary_array[0]) + new_payment.vendor_code + new_payment.eta_tran + new_payment.beneficiary_amount + new_payment.card_type + new_payment.transaction_type + saltKey;
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    byte[] digest = md.digest(getHashKey.getBytes());
                    hashString = new StringBuilder();
                    for (int i = 0; i < digest.length; i++) {
                        hashString.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                    }

                } catch (NoSuchAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                Map<String, String> params = new HashMap<>();
                params.put("amount", final_calculated_amount);
                params.put("country", new_payment.purpose_of_payment);
                params.put("udf2", "udf2");
                params.put("udf3", String.valueOf(beneficiary_array[0]));
                params.put("udf4", new_payment.vendor_code);
                params.put("udf6", "Android");
                params.put("udf11", hashString.toString());
                params.put("address_line_1", new_payment.eta_tran);
                params.put("address_line_2", new_payment.beneficiary_amount);
                params.put("city", new_payment.card_type);
                params.put("state", new_payment.transaction_type);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("Authorization", "token " + TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    private void sessionCheck() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/counter_check",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println(response);
                        String getResponse = jsonObject.getString("detail");

                        if (getResponse.equals("1")) {
                            String allowUser = jsonObject.getString("allow_user");
                            if (allowUser.equals("0")) {
                                final LayoutInflater layoutInflaterBlock = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog blockUserDialog = new Dialog(this);
                                blockUserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                blockUserDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                blockUserDialog.setContentView(layoutInflaterBlock.inflate(R.layout.block_user_layout, null));
                                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                blockUserDialog.getWindow().setLayout(width, height);
                                blockUserDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                blockUserDialog.setCancelable(false);
                                blockUserDialog.show();
                            } else {
                                //Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            }
                        } else if (getResponse.equals("Invalid token")) {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            });
                        } else {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("auth", TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }





    @Override
    protected void onResume() {
        super.onResume();
        sessionCheck();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(requestCode);
        System.out.println(requestCode);
        if (requestCode == 101) {
            String paymentResponse = data.getStringExtra("payuResponse");
            System.out.println("paymentResponse: " + paymentResponse);
            if (paymentResponse.equals("null")) {
                System.out.println("Transaction Error!");
                new MaterialStyledDialog.Builder(this)

                        .setTitle("Error")
                        .setDescription("There was an error in your transaction, if the amount has been debited from your account it will be credited to your beneficiary bank account within T+2 bank working days.")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.transactionfailed)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(true)
                        .setCancelable(false)
                        .withDivider(true)
                        .setPositiveText("Got it")
                        .onPositive((dialog, which) -> finish())
                        .show();


            } else if (paymentResponse.equals("Transaction Success")) {
                Intent intent = new Intent(Payment_details.this, TransactionSuccessActivity.class);
                startActivity(intent);
                finish();

            } else if (paymentResponse.equals("Transaction Failure")) {
                Intent intent = new Intent(Payment_details.this, TransactionCancelledActivity.class);
                startActivity(intent);
                finish();
            }

        }
    }




    private void activity_log(){

        SharedPreferences sharedPreferences = Payment_details.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                response -> {
                    //Nothing

                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                //   message = "cannot profile connect";
                new MaterialStyledDialog.Builder(Payment_details.this)
                        .setTitle("Internet Error")
                        .setDescription("Check Your Interenet Connection")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.warninginternet)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .setNegativeText("Cancel")
                        .onNegative((dialog, which) -> {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            dialog.dismiss();
                        })
                        .setPositiveText("Open Settings")
                        .onPositive((dialog, which) -> {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            // dialog.dismiss();
                        })
                        .show();
            } else if (error instanceof AuthFailureError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof NoConnectionError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout. Please try after sometime";
            }
            if (!message.isEmpty()) {
                Toast.makeText(Payment_details.this, message, Toast.LENGTH_SHORT).show();
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();




                params.put("AcitvityID","121");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment_details.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


    private void billreminder() {

        SharedPreferences sharedPreferences = Payment_details.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/ccdate_reminder",
                response -> {
                    try {
                        JSONObject json = new JSONObject(response);
                        System.out.println("*******************************************");
                        System.out.println(response);
                        String name = json.optString("status");
                        System.out.println(name);
                        if (!name.equals("False")) {
                            date_reminder.setText(name);
                            date_reminder.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
        }) {
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


            //This is for Headers If You Needed

        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment_details.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }
    @Override
    public void onBackPressed() {

        return;
    }



}
