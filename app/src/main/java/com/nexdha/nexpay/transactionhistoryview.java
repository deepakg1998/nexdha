package com.nexdha.nexpay;

import com.google.gson.annotations.SerializedName;

public class transactionhistoryview {
    private String amount;

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    private final String udf5;
    private String pg_tran_id;
    private String response_message;
    private String address_line_1;
    private String state;
    private final String id;
    private String udf3;

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }


    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    @SerializedName("order_id")
    String order_id;

    public transactionhistoryview(String amount, String udf3, String udf5, String pg_tran_id, String response_message, String address_line_1, String state, String id, String sendername, String order_id) {
        this.amount = amount;
        this.udf3 = udf3;
        this.order_id = order_id;
        this.udf5 = udf5;
        this.pg_tran_id = pg_tran_id;
        this.response_message = response_message;
        this.address_line_1 = address_line_1;
        this.state = state;
        this.id = id;
        this.udf3 = udf3;
    }

    public String getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public String getCreateddate() {
        return udf5;
    }

    public String getpg_tran_id() {
        return pg_tran_id;
    }

    public String getresponse_message() {
        return response_message;
    }

    public String getaddress_line_1() {
        return address_line_1;
    }

    public String getState() {
        return state;
    }

    public String getid() {
        return id;
    }

    public String getSendername() {
        return udf3;
    }
}
