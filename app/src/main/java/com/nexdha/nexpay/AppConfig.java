package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppConfig {
    static Retrofit getRetrofit() {
        return new Retrofit.Builder().baseUrl(SERVER).addConverterFactory(GsonConverterFactory.create()).build();
    }
}
