package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class
UpdateApp extends AppCompatActivity {
    Button updatebutton;
    TextView content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_update_app);

        content = findViewById(R.id.textView152);
        updatebutton = findViewById(R.id.button19);
        updatebutton.setOnClickListener(view -> {
            Intent viewupdate =
                    new Intent("android.intent.action.VIEW",
                            Uri.parse("https://play.google.com/store/search?q=nexdha"));
            startActivity(viewupdate);
        });


        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/up_app",
                response -> {
                    try {
                        JSONArray jsonarray = new JSONArray(response);
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                            content.setText(jsonobject.getString("content"));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                new MaterialStyledDialog.Builder(UpdateApp.this)
                        .setTitle("Internet Error")
                        .setDescription("There will be issue displaying your info,Check Your Interenet Connection")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.warninginternet)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .setNegativeText("Cancel")
                        .onNegative((dialog, which) -> {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            dialog.dismiss();
                        })
                        .setPositiveText("Open Settings")
                        .onPositive((dialog, which) -> {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            // dialog.dismiss();
                        })
                        .show();
            } else if (error instanceof AuthFailureError) {
                message = "";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout";
            }
            if (!message.isEmpty()) {
                Toast.makeText(UpdateApp.this, message, Toast.LENGTH_SHORT).show();
            }
        });



        RequestQueue requestQueue = Volley.newRequestQueue(UpdateApp.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }
    @Override
    public void onBackPressed() {
        finishAffinity();

    }
}
