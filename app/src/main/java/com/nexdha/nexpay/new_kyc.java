package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;


public class new_kyc extends AppCompatActivity {
    Button pan, af, ab, selfie, upload;
    Button display_pan, display_ab, display_af, display_selfie;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    Uri selfie_path;
    Uri test2_path;
    Uri pan_path = null;
    Uri af_path = null;
    Uri ab_path = null;

    String numberWithoutSpace;

    EditText creditNumber;

    String kyc_uid = "";
    String kyc_uname = "";
    String kyc_uphone = "";
    String kyc_uemail = "";
    CheckBox checkBox10;

    int timer_count = 1;

    Bitmap selfie_bitmap = null;
    TextView p_image_name,af_image_name,ab_image_name,s_image_name;
    ProgressBar progress_bar11;

    Date date_img_name = new Date();
    //This method returns the time in millis
    long timeMilli = date_img_name.getTime();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_new_kyc);
        activity_log_KYC();
        profileinfo();
        System.out.println("This is date");
        System.out.println(timeMilli);
        creditNumber = findViewById(R.id.enter_credit_number);
        progress_bar11 = findViewById(R.id.progressBar11);
        progress_bar11.setVisibility(View.GONE);
        pan = findViewById(R.id.button45);
        p_image_name = findViewById(R.id.textView239);
        p_image_name.setVisibility(View.GONE);
        af = findViewById(R.id.button46);
        af_image_name = findViewById(R.id.textView240);
        af_image_name.setVisibility(View.GONE);
        ab = findViewById(R.id.button47);
        ab_image_name = findViewById(R.id.textView241);
        ab_image_name.setVisibility(View.GONE);
        selfie = findViewById(R.id.button48);
        s_image_name = findViewById(R.id.textView242);
        s_image_name.setVisibility(View.GONE);

        display_pan = findViewById(R.id.button49);
        display_pan.setBackgroundResource(R.drawable.pageoneconstraintthree);
        display_pan.setEnabled(false);

        display_af = findViewById(R.id.button50);
        display_af.setBackgroundResource(R.drawable.pageoneconstraintthree);
        display_af.setEnabled(false);
        checkBox10 =findViewById(R.id.checkBox10);
        display_ab = findViewById(R.id.button51);
        display_ab.setBackgroundResource(R.drawable.pageoneconstraintthree);
        display_ab.setEnabled(false);

        display_selfie = findViewById(R.id.button52);
        display_selfie.setBackgroundResource(R.drawable.pageoneconstraintthree);
        display_selfie.setEnabled(false);

        upload = findViewById(R.id.button53);


        pan.setOnClickListener(view -> {
            System.out.println("PAN");
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PAN_IMAGE);
        });
        display_pan.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(new_kyc.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.new_kyc_image, null);
            ImageView pan_dis = dialogLayout.findViewById(R.id.imageView59);
            pan_dis.setImageURI(pan_path);
            builder.setPositiveButton("OK", null);
            builder.setView(dialogLayout);
            builder.show();
        });

        af.setOnClickListener(view -> {
            System.out.println("af");
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), AF_IMAGE);
        });

        display_af.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(new_kyc.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.new_kyc_image, null);
            ImageView pan_dis = dialogLayout.findViewById(R.id.imageView59);
            pan_dis.setImageURI(af_path);
            builder.setPositiveButton("OK", null);
            builder.setView(dialogLayout);
            builder.show();
        });

        ab.setOnClickListener(view -> {
            System.out.println("ab");
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), AB_IMAGE);
        });
        display_ab.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(new_kyc.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.new_kyc_image, null);
            ImageView pan_dis = dialogLayout.findViewById(R.id.imageView59);
            pan_dis.setImageURI(ab_path);
            builder.setPositiveButton("OK", null);
            builder.setView(dialogLayout);
            builder.show();
        });

        selfie.setOnClickListener(view -> {
            System.out.println("selfie");
            if (ContextCompat.checkSelfPermission(new_kyc.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                //ask for permission
                System.out.println("camera permission initiated");
                ActivityCompat.requestPermissions(new_kyc.this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_PERMISSION_CODE);
            } else {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                } catch (ActivityNotFoundException e) {
                    // display error state to the user
                }
            }


        });
        display_selfie.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(new_kyc.this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.new_kyc_image, null);
            ImageView pan_dis = dialogLayout.findViewById(R.id.imageView59);
            pan_dis.setImageURI(selfie_path);
            builder.setPositiveButton("OK", null);
            builder.setView(dialogLayout);
            builder.show();
        });


        upload.setOnClickListener(view -> {
            if (!Validationacc()) {
                return;
            } else {
                progress_bar11.setVisibility(View.VISIBLE);
                Toast.makeText(new_kyc.this, "Please wait while we upload your image....", Toast.LENGTH_LONG).show();
                upload();
                //otpcoundown();
            }

        });
    }

    private boolean Validationacc() {

        final String number = creditNumber.getText().toString().trim();
        numberWithoutSpace = number.replaceAll(" ", "");

        if (numberWithoutSpace.length() != 6) {
            Toast.makeText(new_kyc.this, "Enter first 6 digits of the Credit Card number", Toast.LENGTH_LONG).show();
            return false;
        }

        if (pan_path == null) {
            Toast.makeText(new_kyc.this, "Upload your PAN card image", Toast.LENGTH_LONG).show();
            return false;

        }
        if (af_path == null) {
            Toast.makeText(new_kyc.this, "Upload your Aadhaar Front image", Toast.LENGTH_LONG).show();
            return false;

        }
        if (ab_path == null) {
            Toast.makeText(new_kyc.this, "Upload your Aadhaar Back image", Toast.LENGTH_LONG).show();
            return false;

        }
        if (selfie_path == null) {
            Toast.makeText(new_kyc.this, "Upload your Selfie image", Toast.LENGTH_LONG).show();
            return false;

        }
        return true;
    }


    private void upload(){
        progress_bar11.setVisibility(View.VISIBLE);
        // Map is used to multipart the file using okhttp3.RequestBody
        File retrofit_file = new File(Objects.requireNonNull(FileUtil.getPath(selfie_path, this)));
        File retrofit_file1 = new File(FileUtil.getPath(pan_path, this));
        File retrofit_file2 = new File(FileUtil.getPath(af_path, this));
        File retrofit_file3 = new File(FileUtil.getPath(ab_path, this));
        // Parsing any Media type file
        RequestBody requestBody1 = RequestBody.create(MediaType.parse("image/jpeg"), retrofit_file);
        RequestBody requestBody2 = RequestBody.create(MediaType.parse("image/jpeg"), retrofit_file1);
        RequestBody requestBody3 = RequestBody.create(MediaType.parse("image/jpeg"), retrofit_file2);
        RequestBody requestBody4 = RequestBody.create(MediaType.parse("image/jpeg"), retrofit_file3);
        MultipartBody.Part fileToUpload1 = MultipartBody.Part.createFormData("file1", kyc_uphone + "_s.jpeg", requestBody1);
        MultipartBody.Part fileToUpload2 = MultipartBody.Part.createFormData("file2", kyc_uphone + "_p.jpeg", requestBody2);
        MultipartBody.Part fileToUpload3 = MultipartBody.Part.createFormData("file3", kyc_uphone + "_af.jpeg", requestBody3);
        MultipartBody.Part fileToUpload4 = MultipartBody.Part.createFormData("file4", kyc_uphone + "_ab.jpeg", requestBody4);
        RequestBody numberConverted = RequestBody.create(numberWithoutSpace, MediaType.parse("text/plain"));

        ApiConfig getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        Call<ServerResponse> call = getResponse.uploadMulFile(Integer.parseInt(kyc_uid), numberConverted, fileToUpload1, fileToUpload2, fileToUpload3, fileToUpload4);
        call.enqueue(new Callback<ServerResponse>() {

            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {
                System.out.println("This one");
                ServerResponse serverResponse = response.body();
                System.out.println("This two " + serverResponse);
                if (serverResponse != null) {
                    System.out.println("this");
                    System.out.println(response);
                    Toast.makeText(getApplicationContext(), "Upload Success", Toast.LENGTH_LONG).show();
                    progress_bar11.setVisibility(View.GONE);
                    timer_count = 100;
                    Intent intent = new Intent(new_kyc.this,Moreoptions.class);
                    startActivity(intent);
                    finish();


                    if (serverResponse.getSuccess()) {
                        System.out.println("This four");
                        //Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        System.out.println("This five");
                       // Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    System.out.println("This six");
                    assert false;
                    Log.v("Response", serverResponse.toString());
                    System.out.println("This seven "+ serverResponse.toString());
                }
                progress_bar11.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call< ServerResponse > call, Throwable t) {
                System.out.println(t);
                System.out.println(call);
            }
        });
    }




/*
private void upload(){
    Multipart multipart = new Multipart(new_kyc.this);


    Map<String,String> params = new HashMap<>();
    params.put("userid",kyc_uid);
    multipart.addParams(params);
    multipart.addFile("image/jpeg", "file1", kyc_uphone+"_s.jpeg", selfie_path);
    multipart.addFile("image/jpeg", "file2", kyc_uphone+"_p.jpeg", pan_path);
    multipart.addFile("image/jpeg", "file3", kyc_uphone+"_af.jpeg", af_path);
    multipart.addFile("image/jpeg", "file4", kyc_uphone+"_ab.jpeg", ab_path);
    multipart.launchRequest(SERVER + "/api/android_FileUploadView", new Response.Listener<NetworkResponse>() {
        @Override
        public void onResponse(NetworkResponse response) {
            System.out.println("this");
            System.out.println(response);
            Toast.makeText(getApplicationContext(), "Upload Success", Toast.LENGTH_LONG).show();
            progress_bar11.setVisibility(View.GONE);
            timer_count = 100;
            Intent intent = new Intent(new_kyc.this,Moreoptions.class);
            startActivity(intent);
            finish();
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
           // Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
            progress_bar11.setVisibility(View.GONE);
            timer_count = 100;
            Intent intent = new Intent(new_kyc.this,Moreoptions.class);
            startActivity(intent);
            finish();
        }


    });

}
*/

    public void otpcoundown(){
        new CountDownTimer(8500, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                if (String.valueOf(timer_count).equals("1")){
                    Toast.makeText(getApplicationContext(), "50% Uploaded.....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }
                else if (String.valueOf(timer_count).equals("2")){
                    Toast.makeText(getApplicationContext(), "Slow internet detected.....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (String.valueOf(timer_count).equals("3")){
                    Toast.makeText(getApplicationContext(), "65% Uploaded....slow internet detected...Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (String.valueOf(timer_count).equals("4")){
                    Toast.makeText(getApplicationContext(), "75% Uploaded (Slow Internet detected).....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (String.valueOf(timer_count).equals("5")){
                    Toast.makeText(getApplicationContext(), "80% Uploaded.....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (String.valueOf(timer_count).equals("6")){
                    Toast.makeText(getApplicationContext(), "90% Uploaded.....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (String.valueOf(timer_count).equals("7")){
                    Toast.makeText(getApplicationContext(), "92% Uploaded.....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (String.valueOf(timer_count).equals("8")){
                    Toast.makeText(getApplicationContext(), "94% Uploaded.....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (String.valueOf(timer_count).equals("9")){
                    Toast.makeText(getApplicationContext(), "96% Uploaded.....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (String.valueOf(timer_count).equals("10")){
                    Toast.makeText(getApplicationContext(), "98% Uploaded.....Please Wait", Toast.LENGTH_SHORT).show();
                    timer_count = timer_count+1;
                }else if (timer_count > 10 && timer_count < 99){
                    Toast.makeText(getApplicationContext(), "Do not close the app....your file will be uploaded", Toast.LENGTH_SHORT).show();
                }else{
                    this.cancel();
                }

                otpcoundown();
            }
        }.start();


    }

    private void profileinfo(){

        SharedPreferences sharedPreferences = new_kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            kyc_uname = profileresponse.getString("name");
                            kyc_uid = profileresponse.getString("id");
                            kyc_uphone = profileresponse.getString("phone");
                            kyc_uemail = profileresponse.getString("email");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(new_kyc.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new_kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(new_kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


    static final int REQUEST_IMAGE_CAPTURE = 4;
    public static final int PAN_IMAGE = 1;
    public static final int AF_IMAGE = 2;
    public static final int AB_IMAGE = 3;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            System.out.println("PAN_Selected");
            if (data == null) {
                Toast.makeText(getApplicationContext(), "No image Selected", Toast.LENGTH_LONG).show();
            } else {

                Uri selectedImg = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImg);
                    pan_path = getImageUri(getApplicationContext(), bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                //pan_path = data.getData();
                p_image_name.setVisibility(View.VISIBLE);

                display_pan.setBackgroundResource(R.drawable.kyc_border_display);
                display_pan.setEnabled(true);
            }

        } else if (requestCode == 2) {
            System.out.println("AF SELECTED");
            if (data == null) {
                Toast.makeText(getApplicationContext(), "No image Selected", Toast.LENGTH_LONG).show();
            } else {

                Uri selectedImg = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImg);
                    af_path = getImageUri(getApplicationContext(), bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //af_path = data.getData();
                af_image_name.setVisibility(View.VISIBLE);

                display_af.setBackgroundResource(R.drawable.kyc_border_display);
                display_af.setEnabled(true);
            }

        } else if (requestCode == 3) {
            System.out.println("AB SELECTED");
            if (data == null) {
                Toast.makeText(getApplicationContext(), "No image Selected", Toast.LENGTH_LONG).show();
            } else {

                Uri selectedImg = data.getData();
                if (selectedImg != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImg);
                        ab_path = getImageUri(getApplicationContext(), bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();
                }

                //ab_path = data.getData();
                ab_image_name.setVisibility(View.VISIBLE);

                display_ab.setBackgroundResource(R.drawable.kyc_border_display);
                display_ab.setEnabled(true);
            }

        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            System.out.println("this is camera result");
            Bundle extras = data.getExtras();
            selfie_bitmap = (Bitmap) extras.get("data");
            selfie_path = getImageUri(getApplicationContext(), selfie_bitmap);
            s_image_name.setVisibility(View.VISIBLE);

            display_selfie.setBackgroundResource(R.drawable.kyc_border_display);
            display_selfie.setEnabled(true);


        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 0, bytes);
        String test_path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, String.valueOf(timeMilli), null);
        return Uri.parse(test_path);
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED){
            System.out.println("Granted");
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } catch (ActivityNotFoundException e) {
                // display error state to the user
            }
        }else  {
            System.out.println("Denied");
            Intent intent = new Intent(getApplicationContext(), permissiondenied.class);
            intent.putExtra("FROM_ACTIVITY", "new_kyc");
            startActivity(intent);
            finish();
        }
    }

    private void activity_log_KYC(){

        SharedPreferences sharedPreferences = new_kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(new_kyc.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new_kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","115");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(new_kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }



}