package com.nexdha.nexpay;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import static com.nexdha.nexpay.constant.SERVER;

public class KYC extends AppCompatActivity {
    Button uploadkyc;
    Button takeselfie;
    private static final int SELECT_PICTURE = 100;
    public static Uri selectedImageUri;
    private static final int CAMERA_REQUEST = 1888;
    private static final int EMAIL_INTENT = 77;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    String comp_path = "null";
    String path = "null";
    String camerap = "null";
    String comp__rap = "null";
    ConstraintLayout mobileuploadlayout;
    ConstraintLayout selfieupload;
    ImageView imageview52;
    Bitmap photo;
    Button sendemailprogress;
    Uri tempUri;
    //FloatingActionButton sendmailback;
    ImageView sendmailback;
    Button cancel;
    String mailname,mailphone,mailemail;
    ConstraintLayout otp_layout;
    EditText enterotp;
    EditText enterpan;
    Button send_otp;
    Button resend_otp;
    String verifyotp;
    String otp_method_verification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_kyc);
        activity_log_KYC();
        getprofiledetails();
        mobileuploadlayout = findViewById(R.id.mobileuploadlayouts);
        selfieupload = findViewById(R.id.selfieuploads);
        sendemailprogress = findViewById(R.id.button388);
        // sendemailprogress.setTranslationX(2000);
        imageview52 = findViewById(R.id.imageView522);
        selfieupload.setTranslationX(2000);
        //sendmailback = findViewById(R.id.floatingActionButton11);
        sendmailback = findViewById(R.id.imageView588);
        // sendmailback.setTranslationX(2000);
        otp_layout = findViewById(R.id.otplayouts);
        otp_layout.setTranslationX(2000);
        send_otp = findViewById(R.id.button43);
        send_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send_otp.setEnabled(false);
                send_otp.setBackground(getDrawable(R.drawable.pageoneconstraintthree));
                sendotp();
            }
        });
        resend_otp = findViewById(R.id.button44);
        resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resend_otp.setEnabled(false);
                resend_otp.setBackground(getDrawable(R.drawable.pageoneconstrainttwo));
                sendotp();
            }
        });
        resend_otp = findViewById(R.id.button44);
        enterotp = findViewById(R.id.editText188);
        enterpan = findViewById(R.id.editText200);
        cancel=findViewById(R.id.button401);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        handlePermission();
        // handlePermissioncamera();
        takeselfie = findViewById(R.id.button377);
        takeselfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            /*    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);

             */
                handlePermissioncamera();
                // dispatchTakePictureIntent();

            }
        });
        uploadkyc = findViewById(R.id.button366);
        uploadkyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);

            }
        });
        FloatingActionButton fabaddbene = findViewById(R.id.floatingActionButton77);
        fabaddbene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                mobileuploadlayout.animate().translationX(-2000);
                selfieupload.animate().translationX(0);


            }
        });
        FloatingActionButton showprogressbar = findViewById(R.id.floatingActionButton101);
        showprogressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                selfieupload.animate().translationX(-2000);
                //sendemailprogress.animate().translationX(0);
                //sendmailback.animate().translationX(0);
                otp_layout.animate().translationX(0);


            }
        });

        sendmailback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                selfieupload.animate().translationX(0);
                //sendemailprogress.animate().translationX(2000);
                //sendmailback.animate().translationX(2000);
                otp_layout.animate().translationX(2000);


            }
        });
        final FloatingActionButton selfieback = findViewById(R.id.floatingActionButton99);
        selfieback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                mobileuploadlayout.animate().translationX(0);
                selfieupload.animate().translationX(2000);


            }
        });
        sendemailprogress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validationacc() == false){
                    return;
                }

            }
        });
    }
    private void sendotp(){
        SharedPreferences sharedPreferences = KYC.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserid",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("***************openotppage*******************");
                        System.out.println(response);
                        send_otp.setVisibility(View.GONE);
                        resend_otp.setEnabled(false);
                        resend_otp.setVisibility(View.VISIBLE);
                        otpresend_countdown();
                        // Intent intent = new Intent(SignUp.this, Payment.class);
                        // startActivity(intent);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(KYC.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(KYC.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    public void otpresend_countdown(){
        // resend_otp=(Button) findViewById(R.id.textView123);
        new CountDownTimer(90000, 1000) {

            public void onTick(long millisUntilFinished) {
                //resend_otp.setEnabled(false);
                resend_otp.setTextColor(Color.parseColor("#a4a4a4"));
                resend_otp.setText("Resend in : " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                resend_otp.setText("Click To re-send otp");
                resend_otp.setTextColor(Color.parseColor("#ffffff"));
                resend_otp.setEnabled(true);
                resend_otp.setBackgroundResource(R.drawable.roundedbutton);

            }
        }.start();

    }

    private void handlePermission() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    SELECT_PICTURE);
        }
    }
    private void handlePermissioncamera() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            System.out.println("camera permission initiated");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_CODE);
        }else{
            Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
            System.out.println("camera intiated");
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case SELECT_PICTURE:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                        if (showRationale) {
                            //  Show your own message here
                            System.out.println("11111111111111111");
                        } else {
                            System.out.println("22222222222222");
                            showSettingsAlert();
                        }
                    }

                }


        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                System.out.println("camera permission granted");
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else
            {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
                System.out.println("camera permission denied");
            }
        }




    }
    private void showSettingsAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        openAppSettings(KYC.this);
                    }
                });
        alertDialog.show();
    }
    public static void openAppSettings(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (resultCode == RESULT_OK) {
                    if (requestCode == SELECT_PICTURE) {
                        // Get the url from data
                        Uri selectedImage_comp_Uri = data.getData();
                        System.out.println(selectedImage_comp_Uri);
                        if (null != selectedImage_comp_Uri) {
                            // Get the path from the Uri
                            comp_path = getPath(selectedImage_comp_Uri);
                            System.out.println(comp_path);
                            Bitmap yourSelectedImage = BitmapFactory.decodeFile(comp_path);
                            selectedImageUri = getcomp_ImageUri(getApplicationContext(), yourSelectedImage);
                            path = get_galley_comp_Path(selectedImageUri);
                            System.out.print(path);


                            // Set the image in ImageView
                            findViewById(R.id.imageView333).post(new Runnable() {
                                @Override
                                public void run() {
                                    ((ImageView) findViewById(R.id.imageView333)).setImageURI(selectedImageUri);


                                }
                            });


                        }
                    }
                }
            }
        }).start();

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            photo = (Bitmap) data.getExtras().get("data");
            imageview52.setImageBitmap(photo);
            System.out.println(photo);
            tempUri = getImageUri(getApplicationContext(), photo);
            System.out.print(tempUri);
            getcameraPath(tempUri);


        } else if (requestCode == EMAIL_INTENT) {
            Intent intent = new Intent(KYC.this, Moreoptions.class);
            startActivity(intent);
            finish();


        }

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String imageuripath = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "", null);
        return Uri.parse(imageuripath);
    }
    public Uri getcomp_ImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 10, bytes);
        String imageuripath = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "", null);
        return Uri.parse(imageuripath);
    }


    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String selectedpath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        System.out.println(selectedpath);
        cursor.close();

        return selectedpath;
    }
    /* public String getcameraPath(Uri uri) {
         Cursor cursor = getContentResolver().query(uri, null, null, null, null);
         cursor.moveToFirst();
         String document_id = cursor.getString(0);
         document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
         cursor.close();

         cursor = getContentResolver().query(
                 android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                 null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
         cursor.moveToFirst();
         camerap = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
         System.out.println(camerap);
         cursor.close();

         return camerap;
     }
     */
    public String getcameraPath(Uri uri) {

        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                camerap = cursor.getString(idx);
                cursor.close();
            }
        }
        return camerap;
    }

    public String get_galley_comp_Path(Uri uri) {

        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                comp__rap = cursor.getString(idx);
                cursor.close();
            }
        }
        return comp__rap;
    }
    // Get the real path from the URI
  /*  public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            System.out.println(res);
        }
        cursor.close();
        return res;
    }

   */
    private void getprofiledetails(){

        SharedPreferences sharedPreferences = KYC.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);

                            mailname = profileresponse.getString("name");
                            mailemail = profileresponse.getString("email");
                            mailphone = profileresponse.getString("phone");
                            System.out.println("This is mail name");
                            System.out.println(mailname);
                            System.out.println(mailemail);
                            System.out.println(mailphone);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //     message = "cannot connect";
                    new MaterialStyledDialog.Builder(KYC.this)
                            .setTitle("Internet Error")
                            .setDescription("There will be issue displaying your info,Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(KYC.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(KYC.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
    private boolean Validationacc(){
        String otpverification = enterotp.getText().toString().trim();
        String panverification = enterpan.getText().toString().trim();
        if (path.equals("null")||path == "null" ||path.equals(null)|| path.isEmpty()){
            Toast.makeText(KYC.this,"Upload your PAN card image",Toast.LENGTH_LONG).show();
            return false;

        }
        if (camerap.equals("null")||camerap == "null" ||camerap.equals(null)|| camerap.isEmpty()){
            Toast.makeText(KYC.this,"Upload your selfie",Toast.LENGTH_LONG).show();
            return false;

        }

        if (panverification.equals("") || panverification == "" || panverification.equals(null) || panverification == null){
            Toast.makeText(KYC.this, "Enter pan number",Toast.LENGTH_LONG);
            return false;
        }
        if(otpverification.equals("")|| otpverification == "" || otpverification.equals(null) || otpverification == null){
            Toast.makeText(KYC.this, "Enter otp",Toast.LENGTH_LONG);
            return false;
        }else{
            verify_otp();
        }

        return true;
    }

    private void verify_otp(){

        otp_method_verification = enterotp.getText().toString().trim();
        SharedPreferences sharedPreferences = KYC.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/verify_otp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            System.out.println("***********Verify Otp****************");
                            System.out.println(response);
                            JSONObject otpresponse = new JSONObject(response);
                            verifyotp=otpresponse.getString("Details");
                            System.out.println("***********Details****************");
                            System.out.println(verifyotp);
                            if (verifyotp.equals("OTP Matched")){

                                pan_and_otp();
                                Intent send_report = new Intent(Intent.ACTION_SEND_MULTIPLE);
                                send_report.putExtra(Intent.EXTRA_EMAIL, new String[]{ "kyc@nexdha.com"});
                                send_report.putExtra(Intent.EXTRA_SUBJECT, "KYC Details of "+mailname);

                                // send_report.putExtra(Intent.EXTRA_STREAM, tempUri );
                                //send_report.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uris);

                                ArrayList<Uri> uris = new ArrayList<Uri>();
                                uris.add(Uri.fromFile(new File(path)));
                                uris.add(Uri.fromFile(new File(camerap)));
                                System.out.println(camerap);
                                String[] bits = camerap.split("/");
                                String lastOne = bits[bits.length-1];
                                System.out.println(lastOne);
                                StringTokenizer tokens = new StringTokenizer(lastOne, ".");
                                String first = tokens.nextToken();
                                System.out.println(first);

                                send_report.putExtra(Intent.EXTRA_TEXT, "Name : "+mailname+"\r\nEmail : "+mailemail+"\r\nPhone : "+mailphone+"\r\nKYC Verification id : "+first);
                                send_report.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);

                                send_report.setType("text/plain");
                                startActivityForResult(Intent.createChooser(send_report, "Choose an Email client"), 77);


                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                parseVolleyError(error);

                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(KYC.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //This is for Headers If You Needed

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("otp", otp_method_verification);


                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };




        RequestQueue requestQueue = Volley.newRequestQueue(KYC.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
    public void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            String otperror = data.getString("error");

            System.out.println("OTP");
            System.out.println(responseBody);
            System.out.println(data);
            System.out.println(otperror);
            if (otperror.equals("OTP does not match")){
                Toast.makeText(KYC.this,"Invalid OTP. Please Enter valid OTP",Toast.LENGTH_LONG).show();

            }

        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        getprofiledetails();
        enterotp.setText("");
        enterpan.setText("");
    }

    private void pan_and_otp(){

        final String final_otp = enterotp.getText().toString().trim();
        final String final_pan = enterpan.getText().toString().trim();

        SharedPreferences sharedPreferences = KYC.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/otppan",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(KYC.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(KYC.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("otp",final_otp);
                params.put("pan",final_pan);
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(KYC.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

    private void activity_log_KYC(){

        SharedPreferences sharedPreferences = KYC.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(KYC.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(KYC.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","115");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(KYC.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

}


