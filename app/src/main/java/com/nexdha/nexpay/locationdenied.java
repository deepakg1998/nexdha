package com.nexdha.nexpay;

import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class locationdenied extends AppCompatActivity {
    Button permission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_locationdenied);
        permission = findViewById(R.id.button35);
        permission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(locationdenied.this, Dashboard.class);
                startActivity(intent);

            }
        });

    }


    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(locationdenied.this, Dashboard.class);
        startActivity(intent);

       }

}
