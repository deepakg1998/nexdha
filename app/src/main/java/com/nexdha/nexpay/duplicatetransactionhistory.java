package com.nexdha.nexpay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.OnCompleteListener;
import com.google.android.play.core.tasks.Task;
import com.nexdha.nexpay.ApiClient.MyMediatorInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nexdha.nexpay.constant.SERVER;

public class duplicatetransactionhistory extends AppCompatActivity {
    private MyMediatorInterface myMediatorInterface;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public ImageView transactionhistoryclose,nodata;
    private List<transactionhistoryview> listItems;
    ProgressBar historyprogress;

    ReviewManager reviewManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_duplicatetransactionhistory);
        historyprogress = findViewById(R.id.progressBar9);
        transactionhistoryclose = (ImageView) findViewById(R.id.imageView15);

        transactionhistoryclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intenty = new Intent(duplicatetransactionhistory.this, Dashboard.class);
                startActivity(intenty);
                finish();

            }
        });
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        activity_log_transaction_history();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listItems = new ArrayList<>();

        loadRecyclerview();

        SharedPreferences sharedPreferences = duplicatetransactionhistory.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAA");
        System.out.println(token1);




    }

    private void activity_log_transaction_history(){

        SharedPreferences sharedPreferences = duplicatetransactionhistory.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(duplicatetransactionhistory.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(duplicatetransactionhistory.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","106");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(duplicatetransactionhistory.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
    //////////////////////////////////////////////////in app review
    private void init() {
        reviewManager = ReviewManagerFactory.create(this);
        // findViewById(R.id.btn_rate_app).setOnClickListener(view -> showRateApp());
        showRateApp();
    }

    /**
     * Shows rate app bottom sheet using In-App review API
     * The bottom sheet might or might not shown depending on the Quotas and limitations
     * https://developer.android.com/guide/playcore/in-app-review#quotas
     * We show fallback dialog if there is any error
     */
    public void showRateApp() {
        Task<ReviewInfo> request = reviewManager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // We can get the ReviewInfo object

                ReviewInfo reviewInfo = task.getResult();

                Task<Void> flow = reviewManager.launchReviewFlow(this, reviewInfo);
                flow.addOnCompleteListener(task1 -> {
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                });
            } else {
                // There was some problem, continue regardless of the result.
                // show native rate app dialog on error
                // showRateAppFallbackDialog();
            }
        });

    }
    //////////////////////////////////////////////////in app review
    private void loadRecyclerview(){

        SharedPreferences sharedPreferences = duplicatetransactionhistory.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("-------------------------------history---------------------------------------");
        System.out.println(token1);
        final String token200 = token1;
        nodata = (ImageView) findViewById(R.id.imageView45);



        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/save_PG_Tran_Response",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                           // JSONObject transactionobject = new JSONObject(response);
                           // JSONArray transactionarray = transactionobject.getJSONArray(null);
                            JSONArray transactionarray = new JSONArray(response);
                            System.out.println("this is the history response");
                            System.out.println(transactionarray);

                            if (transactionarray==null || transactionarray.length()==0){
                                historyprogress.setVisibility(View.GONE);
                                nodata.setVisibility(View.VISIBLE);
                            }else{
                                for (int i=0; i<transactionarray.length();i++){
                                    JSONObject o = transactionarray.getJSONObject(i);
                                    transactionhistoryview item = new transactionhistoryview(

                                            o.getString("amount"),
                                            o.getString("udf3"),
                                            o.getString("created_at"),
                                            o.getString("transaction_id"),
                                            o.getString("response_message"),
                                            o.getString("address_line_1"),
                                            o.getString("state"),
                                            o.getString("id"),
                                            o.getString("name"),
                                            o.getString("order_id")

                                    );

                                    System.out.println("this is the item response");
                                    System.out.println(item);


                                    listItems.add(item);
                                    historyprogress.setVisibility(View.GONE);
                                }
                                adapter = new MyAdapter(listItems,getApplicationContext(),myMediatorInterface);
                                recyclerView.setAdapter(adapter);
                                historyprogress.setVisibility(View.GONE);

                            }

                          //  init();
                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getApplicationContext(), volleyError.getMessage(), Toast.LENGTH_LONG).show()

                    }

                })


        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }
        };



        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }



    public void onBackPressed() {
        //Execute your code here
        Intent intenty = new Intent(duplicatetransactionhistory.this, Dashboard.class);
        startActivity(intenty);
        finish();

    }
}