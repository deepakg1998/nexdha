package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.nexdha.nexpay.activity.LandlordListActivity;
import com.nexdha.nexpay.activity.SettingsActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyBeneficiaryaAdapter extends RecyclerView.Adapter<MyBeneficiaryaAdapter.ViewHolder>{

    private List<beneficiary> listitems;
    private Context context;
    private Activity activity;
    String previousActivity = SettingsActivity.previousActivity;






    public MyBeneficiaryaAdapter(List<beneficiary> listitems, Context context) {
        this.listitems = listitems;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beneficiarylist,parent,false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final beneficiary listItem = listitems.get(position);

        if (position % 2 == 1) {
            holder.lv1.setBackgroundResource(R.drawable.card_recycler_view_white);

            //holder.lv2.setBackgroundColor(Color.WHITE);
        } else {
            holder.lv1.setBackgroundResource(R.drawable.card_recycler_view_blue);

            // holder.lv2.setBackgroundColor(Color.parseColor("#E5F3F9"));
        }

        holder.benename.setText(listItem.getAmount());
        holder.beneacc.setText(listItem.getName());
        holder.beneifsc.setText(listItem.getCreated_at());
        holder.benenickname.setText(listItem.getNickname());
        holder.deletebeneficiaryimage.setOnClickListener(view -> {

            listItem.getid();
            final String deleteid = listItem.getid();
            System.out.println("this is selected id");
            System.out.println(listItem.getid());

            //-------------Alertbox start--------------//
            final AlertDialog.Builder alertbox = new AlertDialog.Builder(view.getRootView().getContext());
            alertbox.setMessage("Are you sure you want to delete this beneficiary ?");
            alertbox.setTitle("Delete");
            alertbox.setIcon(R.drawable.recyclebin);


            alertbox.setNeutralButton("OK",
                    (arg0, arg1) -> {
                        SharedPreferences sharedPreferences = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                        final String token1 = sharedPreferences.getString("token", "");
                        //-------------api start------------------//
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/delete_casa",
                                response -> {
                                    //Nothing

                                    try {
                                        JSONObject response4 = new JSONObject(response);
                                        System.out.println("**********this is id********");
                                        System.out.println(response4);
                                        notifyItemRemoved(position);
                                        notifyDataSetChanged();
                                        listitems.remove(position);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }, error -> {
                            String message = "";
                            if (error instanceof NetworkError) {
                                message = "Cannot connect to internet......please check your internet connection";
                            } else if (error instanceof ServerError) {
                                message = "";
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to internet......please check your internet connection";
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to internet......please check your internet connection";
                            } else if (error instanceof ParseError) {
                                message = "Cannot connect to internet......please check your internet connection";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection Timedout......please check your internet connection";
                            }
                            if (!message.isEmpty()) {
                                //   Toast.makeText(OTP.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() {
                                String T = "";
                                Map<String, String> params = new HashMap<>();


                                try {
                                    JSONObject response1 = new JSONObject(token1);
                                    T = response1.getString("token");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                String TokenS = "token " + T;

                                params.put("Authorization", TokenS);

                                return params;
                            }

                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                params.put("id", deleteid);


                                return params;

                            }


                        };

                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                        stringRequest.setShouldCache(false);
                        requestQueue.add(stringRequest);
                        //------------api end-------------------------//

                    });
            alertbox.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0,
                                            int arg1) {

                        }

                    });

            alertbox.show();


            //-----------------Alerbox end---------------//


        });


        /// Edit beneficiary
        holder.editbeneficiary.setOnClickListener(view -> {
            //bottom sheet for edit beneficiary
            listItem.getNickname(); //beneficiary nickname

            SharedPreferences sharedPreferences = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            final String token1 = sharedPreferences.getString("token", "");
            System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
            System.out.println(token1);
            final String token200 = token1;

            // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
            StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //Nothing

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String message = "";

                    if (error instanceof NetworkError) {
                        //   message = "cannot profile connect";
                        new MaterialStyledDialog.Builder(context)
                                .setTitle("Internet Error")
                                .setDescription("Check Your Interenet Connection")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.warninginternet)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(false)
                                .setCancelable(false)
                                .withDivider(true)
                                .setNegativeText("Cancel")

                                .show();
                    } else if (error instanceof AuthFailureError) {
                        message = "There is a problem connecting to server. Please try after sometime.";
                    } else if (error instanceof NoConnectionError) {
                        message = "There is a problem connecting to server. Please try after sometime.";
                    } else if (error instanceof ParseError) {
                        message = "Connection Timedout. Please try after sometime";
                    }
                    if (!message.isEmpty()) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }) {

                //-------------------------outerheader-----------------------//
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String T = "";
                    Map<String, String> params = new HashMap<>();


                    // params.put("Content-Type", "application/json; charset=UTF-8");
                    try {
                        JSONObject response1 = new JSONObject(token200);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        // do your work with response object
                        T = response1.getString("token");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("44444444444444444444444444");
                    System.out.println(token200);
                    ///check t!blank

                    String TokenS = "token " + T;
                    System.out.println(T);

                    params.put("Authorization", TokenS);

                    return params;
                }

                protected Map<String, String> getParams() {
                    //  String updateid = "";
                    // String updatetranid="";
                    Map<String, String> params = new HashMap<>();

                    // System.out.println("make my trippppppppppppppp");
                    // System.out.println(Transactionid);

                    // System.out.println("mapppppp");
                    // System.out.println(Updatetranidapi);


                    params.put("AcitvityID", "109");
                    return params;
                }


            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);


            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final Dialog settingsDialog = new Dialog(context);
            settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            settingsDialog.setContentView(inflater.inflate(R.layout.edit_beneficiary_bottom_sheet, null));
            settingsDialog.show();
            final EditText editbeneficiary_nickname = settingsDialog.findViewById(R.id.editText19);
            String removeSpecial = listItem.getNickname();
            String finalResult = removeSpecial.replaceAll("[^a-zA-Z0-9]", "");
            editbeneficiary_nickname.setText(finalResult);
            Button closeimage_popup_button = settingsDialog.findViewById(R.id.button15);
            closeimage_popup_button.setOnClickListener(view12 -> settingsDialog.dismiss());
            Button update_beneficiary = settingsDialog.findViewById(R.id.button23);
            update_beneficiary.setOnClickListener(view1 -> {

                SharedPreferences sharedPreferences1 = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                final String token11 = sharedPreferences1.getString("token", "");
                System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
                System.out.println(token11);
                final String token2001 = token11;


                /////------update start--------////
                final String bene_edited_nick_name = editbeneficiary_nickname.getText().toString().trim();

                String finalResult1 = bene_edited_nick_name.replaceAll("[^a-zA-Z0-9]", "");
                if (bene_edited_nick_name.equals("") || bene_edited_nick_name == "") {
                    Toast.makeText(context, "Please provide Nick Name", Toast.LENGTH_LONG).show();
                    return;
                }
                SharedPreferences sharedPreferences2 = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                final String token2 = sharedPreferences2.getString("token", "");

                StringRequest stringRequest1 = new StringRequest(Request.Method.POST, SERVER + "/api/update_beneficiary",
                        response -> {
                            //Nothing

                            try {
                                JSONObject response4 = new JSONObject(response);
                                System.out.println("**********this is id********");
                                System.out.println(response4.getString("Status"));
                                if (response4.getString("Status") == "Successfully updated" || response4.getString("Status").equals("Successfully updated")) {
                                    settingsDialog.dismiss();
                                    if (previousActivity != null && previousActivity.equals("beneficiaryList")) {
                                        ((Activity) context).finish();
                                        Intent intent = new Intent(context, Beneficiarylist.class);
                                        context.startActivity(intent);
                                    } else if (previousActivity != null && previousActivity.equals("landlordList")) {
                                        ((Activity) context).finish();
                                        Intent intent = new Intent(context, LandlordListActivity.class);
                                        context.startActivity(intent);
                                    }
                                    Toast.makeText(context, "Updated", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }, error -> {
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ServerError) {
                        message = "";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ParseError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection Timedout......please check your internet connection";
                    }
                    if (!message.isEmpty()) {
                        //   Toast.makeText(OTP.this, message, Toast.LENGTH_SHORT).show();
                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() {
                        String T = "";
                        Map<String, String> params = new HashMap<>();


                        // params.put("Content-Type", "application/json; charset=UTF-8");
                        try {
                            JSONObject response1 = new JSONObject(token2);
                            System.out.println("!!!!!!!!!!!!!!!!!!");
                            System.out.println(response1.getString("token"));
                            // do your work with response object
                            T = response1.getString("token");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("44444444444444444444444444");
                        System.out.println(token2001);
                        ///check t!blank

                        String TokenS = "token " + T;
                        System.out.println(T);

                        params.put("Authorization", TokenS);

                        return params;
                    }

                    protected Map<String, String> getParams() {

                        Map<String, String> params = new HashMap<>();
                        params.put("id", listItem.getid());
                        params.put("second_name", finalResult1);


                        return params;

                    }


                };

                RequestQueue requestQueue1 = Volley.newRequestQueue(context);
                stringRequest1.setShouldCache(false);
                requestQueue1.add(stringRequest1);
                //------------update end-------------------------//
            });
    /*     final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View dialogView = inflater.inflate(R.layout.edit_beneficiary_bottom_sheet, null);
        dialogView.setBackgroundColor(Color.TRANSPARENT);
        final EditText editbeneficiary_nickname = dialogView.findViewById(R.id.editText19);
        editbeneficiary_nickname.setText(listItem.getNickname());


            TextView update_beneficiary = dialogView.findViewById(R.id.textView229);
            update_beneficiary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    SharedPreferences sharedPreferences = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                    final String token1 = sharedPreferences.getString("token", "");
                    System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
                    System.out.println(token1);
                    final String token200 = token1;



                    /////------update start--------////
                    final String bene_edited_nick_name = editbeneficiary_nickname.getText().toString().trim();
                    if (bene_edited_nick_name.equals("") || bene_edited_nick_name == ""){
                        Toast.makeText(context,"Please provide Nick Name",Toast.LENGTH_LONG).show();
                        return;
                    }
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/update_beneficiary",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    //Nothing

                                    try{
                                        JSONObject response4 = new JSONObject(response);
                                        System.out.println("**********this is id********");
                                        System.out.println(response4.getString("Status"));
                                        if (response4.getString("Status") == "Successfully updated" || response4.getString("Status").equals("Successfully updated")){
                                            Intent intent = new Intent(context, Beneficiarylist.class);
                                            context.startActivity(intent);
                                            ((Activity)context).finish();
                                            Toast.makeText(context, "Updated", Toast.LENGTH_LONG).show();
                                        }
                                       }catch(JSONException e){
                                        e.printStackTrace();
                                    }



                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String message = "";
                            if (error instanceof NetworkError) {
                                message = "Cannot connect to internet......please check your internet connection";
                            } else if (error instanceof ServerError) {
                                message = "";
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to internet......please check your internet connection";
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to internet......please check your internet connection";
                            } else if (error instanceof ParseError) {
                                message = "Cannot connect to internet......please check your internet connection";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection Timedout......please check your internet connection";
                            }
                            if (!message.isEmpty()) {
                                //   Toast.makeText(OTP.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    {

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError{
                            String T="" ;
                            Map<String,String> params = new HashMap<>();


                            // params.put("Content-Type", "application/json; charset=UTF-8");
                            try {
                                JSONObject response1 = new JSONObject(token200);
                                System.out.println("!!!!!!!!!!!!!!!!!!");
                                System.out.println(response1.getString("token"));
                                // do your work with response object
                                T=response1.getString("token");


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            System.out.println("44444444444444444444444444");
                            System.out.println(token200);
                            ///check t!blank

                            String TokenS = "token "+ T;
                            System.out.println(T);

                            params.put("Authorization",TokenS);

                            return params;
                        }

                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<>();
                            params.put("id", "1");
                            params.put("second_name", bene_edited_nick_name);


                            return params;

                        }


                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    stringRequest.setShouldCache(false);
                    requestQueue.add(stringRequest);
                    //------------update end-------------------------//
                }
            });

        BottomSheetDialog dialog = new BottomSheetDialog(context);
        dialog.setContentView(dialogView);
        dialog.show();

        */


        });


    }




    @Override
    public int getItemCount() {
        return listitems.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView benename;
        public TextView beneacc;
        public TextView beneifsc;
        public TextView benenickname;
        public ImageView deletebeneficiaryimage;
        public ImageView editbeneficiary;
        public CardView lv1;


        public ViewHolder(View itemView) {
            super(itemView);

            benename = (TextView) itemView.findViewById(R.id.textView101);
            beneacc = (TextView) itemView.findViewById(R.id.textView102);
            beneifsc = (TextView) itemView.findViewById(R.id.textView103);
            benenickname = (TextView) itemView.findViewById(R.id.nick_name_text_101);
            deletebeneficiaryimage = (ImageView) itemView.findViewById(R.id.imageView48);
            editbeneficiary = (ImageView) itemView.findViewById(R.id.imageView55);
            lv1=(CardView) itemView.findViewById(R.id.bene_card);





        }
    }



}
