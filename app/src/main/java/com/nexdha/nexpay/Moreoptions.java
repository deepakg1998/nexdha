package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.appupdate.AppUpdateOptions;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.nexdha.nexpay.activity.LandlordListActivity;
import com.nexdha.nexpay.activity.RentTransactionDetails;
import com.nexdha.nexpay.activity.SettingsActivity;
import com.nexdha.nexpay.activity.SmeActivity;
import com.nexdha.nexpay.activity.UpiTransactionDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Moreoptions extends AppCompatActivity {
    TextView pname,pemail,pphone,viewbeneficiary,viewLandlordList,aboutus,privacypolicy,termsandconditions,feesandcharges,
            refundpolicy,support,transactionhistory,share,security,faq,version,appversion,logout;
    private ImageView imageview16;
    KeyguardManager km;
    private LinearLayout securitytext;
    LinearLayout history;
    public static String previousActivity = null;
    public static final String MYPREFERENCES = "MyPrefs";
    public static String publicname = null;
    public static String publicemail = null;
    public static String publicnumber = null;
    String panindia = "";
    private AppUpdateManager appUpdateManager;
    private static final int IMMEDIATE_APP_UPDATE_REQ_CODE = 124;
    public final int MY_REQUEST_CODE = 100;
    SharedPreferences sharedPreferences;
    public static final String WELCOMEPREFERENCE = "welpref";
    SharedPreferences welcomepreference;
    SeekBar t1eligibility;
    double transactioninteger;
    double numberofdaysinteger;
    TextView kycverification;
    LinearLayout sme_layout, settings;
    TextView textView3;
    Button button245;
    TextView smeStart;
    Button button412;
    CheckBox checkBox10;
    EditText editpannumber;
    ProgressBar progressBar13;
    ConstraintLayout back_button_layout;
    ConstraintLayout refer_constraint;
    LinearLayout refer_linear;
    ProgressBar referal_progress;
    public static String versionName;
    Button button65;
    EditText referal_phone_number;
    String get_referal_phone;
    String emailto, emailsubject, name;


    @SuppressLint({"WrongViewCast", "SetTextI18n", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_moreoptions);
        textView3 = findViewById(R.id.textView3);
        alreadyrequested_api();
        activity_log_more_options();
        appversion = (TextView) findViewById(R.id.textView98);
        kycverification = findViewById(R.id.textView228);
        back_button_layout = findViewById(R.id.constraintLayout11);
        progressBar13 = findViewById(R.id.progressBar13);
        smeStart = findViewById(R.id.start_sme);
        refer_constraint = findViewById(R.id.layout_two);
        refer_linear = findViewById(R.id.refer_layout);
        referal_progress = findViewById(R.id.progressBar28);
        button65 = findViewById(R.id.button65);
        referal_phone_number = findViewById(R.id.editTextTextPersonName5);
        settings = findViewById(R.id.settings);
        sessionCheck();
        settings.setOnClickListener(view -> startActivity(new Intent(Moreoptions.this, SettingsActivity.class)));
        button65.setOnClickListener(v -> {
            referal_progress.setVisibility(View.VISIBLE);
            get_referal_phone = referal_phone_number.getText().toString().trim();
            if (get_referal_phone.isEmpty() || get_referal_phone.equals("") || get_referal_phone.equals(null)) {
                Toast.makeText(Moreoptions.this, "Please Enter referral Phone Number ", Toast.LENGTH_SHORT).show();
                referal_progress.setVisibility(View.GONE);
                return;
            }
            referal_details();


        });

        smeStart.setOnClickListener(v -> startActivity(new Intent(this, SmeActivity.class)));


        history = findViewById(R.id.history);
        t1eligibility = findViewById(R.id.seekbarT1);
        t1eligibility.setOnTouchListener((view, motionEvent) -> true);
        totaltrans();
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            appversion.setText("Nexdha v " + versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        sharedPreferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);
        welcomepreference = getSharedPreferences(WELCOMEPREFERENCE, Context.MODE_PRIVATE);
        profileinfo();
        appUpdateManager = AppUpdateManagerFactory.create(this);

        pname = findViewById(R.id.textView54);
        pemail = findViewById(R.id.textView58);
        pphone = findViewById(R.id.textView56);
        sme_layout = findViewById(R.id.sme_layout);
        // imageview16 = (ImageView) findViewById(R.id.imageView16);
        back_button_layout.setOnClickListener(v -> finish());

        version = (TextView) findViewById(R.id.textView84);
        security = findViewById(R.id.textView131);
        km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        if (km.isKeyguardSecure()) {
            security.setText("Protected");
        } else {
            security.setText("Not Protected");
            security.setTextColor(Color.parseColor("#11398a"));
        }
        securitytext = findViewById(R.id.thisissecuritylayout);
        securitytext.setOnClickListener(view -> {
            if (km.isKeyguardSecure()) {
                new MaterialStyledDialog.Builder(Moreoptions.this)
                        .setTitle("Protected")
                        .setDescription("Your device is protected by Android.")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.security)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(true)
                        .setCancelable(false)
                        .withDivider(true)
                        .setPositiveText("Okay")
                        .onPositive((dialog, which) -> dialog.dismiss())
                        .show();
            } else {
                new MaterialStyledDialog.Builder(Moreoptions.this)
                        .setTitle("Not Protected")
                        .setDescription("Your device is not protected, we recommend you to secure your device either by pin/pattern/fingerprint.")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.unprotected)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(true)
                        .setCancelable(false)
                        .withDivider(true)
                        .setPositiveText("That's Ok")
                        .onPositive((dialog, which) -> dialog.dismiss())
                        .setNegativeText("Open Settings")
                        .onNegative((dialog, which) -> {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            dialog.dismiss();
                        })
                        .show();
            }
        });
        viewbeneficiary = (TextView) findViewById(R.id.textView66);
        viewbeneficiary.setOnClickListener(view -> {
            Intent intent = new Intent(Moreoptions.this, Beneficiarylist.class);
            previousActivity = "beneficiaryList";
            startActivity(intent);
        });


        viewLandlordList = (TextView) findViewById(R.id.landlord_list);
        viewLandlordList.setOnClickListener(view -> {
            Intent intent = new Intent(Moreoptions.this, LandlordListActivity.class);
            previousActivity = "landlordList";
            startActivity(intent);
        });

        textView3.findViewById(R.id.textView3);
/*
        textView3.setText("Already request has submitted");
*/


        sme_layout.setOnClickListener(view -> {
            final LayoutInflater inflater = (LayoutInflater) Moreoptions.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final Dialog customView1 = new Dialog(Moreoptions.this);
            customView1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            customView1.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            customView1.setContentView(inflater.inflate(R.layout.popupforsme, null));
            customView1.setCancelable(true);
            customView1.show();
            button245 = customView1.findViewById(R.id.button245);
            button412 = customView1.findViewById(R.id.button412);
            checkBox10 = customView1.findViewById(R.id.checkBox10);
            editpannumber = customView1.findViewById(R.id.editpannumber);


            button245.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customView1.dismiss();
                }
            });
            button412.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    panindia = editpannumber.getText().toString().trim();
                    System.out.println(panindia);
                    if (panindia.isEmpty() || panindia.length() == 0 || panindia.equals("") || panindia == null) {
                        Toast.makeText(Moreoptions.this, "Please enter your company pan", Toast.LENGTH_SHORT).show();

                    } else if (panindia.length() != 10) {
                        Toast.makeText(Moreoptions.this, "Please enter correct pan number", Toast.LENGTH_LONG).show();

                    } else if (!checkBox10.isChecked()) {
                        Toast.makeText(Moreoptions.this, "Please Agree If you are SME", Toast.LENGTH_SHORT).show();


                        System.out.println("notchecked");
                    } else {
                        progressBar13.setVisibility(View.VISIBLE);
                        sme_api();
                        System.out.println("apiiiiiiiiiiiiiiiiiiiiiii");
                        customView1.dismiss();
                    }
/*
                                                    submitBtn.setEnabled(true);
*/
                    /* customView1.dismiss();*/

                }

                                               /* Intent intent = new Intent(Dashboard.this, Dashboard.class);
                                                startActivity(intent);
                                                finish();*/
            });


        });




        aboutus = (TextView) findViewById(R.id.textView60);
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://nexdha.com/#features/"));
                startActivity(viewIntent);
            }
        });
        privacypolicy = (TextView) findViewById(R.id.textView64);
        privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntentprivacy =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://nexdha.com/privacypolicy.html"));
                startActivity(viewIntentprivacy);
            }
        });
        termsandconditions = (TextView) findViewById(R.id.textView61);
        termsandconditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntentterms =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://nexdha.com/termsandconditions.html"));
                startActivity(viewIntentterms);
            }
        });
        feesandcharges = (TextView) findViewById(R.id.textView51);
        feesandcharges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntentfeesandcharges =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://nexdha.com/feesandcharges.html"));
                startActivity(viewIntentfeesandcharges);
            }
        });
        refundpolicy = (TextView) findViewById(R.id.textView63);
        refundpolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntentfeesandcharges =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://nexdha.com/refundpolicy.html"));
                startActivity(viewIntentfeesandcharges);
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheetTDialog();
            }
            private void showBottomSheetTDialog() {
                final View dialogViewDashboard = getLayoutInflater().inflate(R.layout.transaction_history_filter_bottom_sheet, null);
                RelativeLayout filter1 = dialogViewDashboard.findViewById(R.id.filt_1);
                RelativeLayout filter2 = dialogViewDashboard.findViewById(R.id.filt_2);
                RelativeLayout filter3 = dialogViewDashboard.findViewById(R.id.filt_3);

                if (Dashboard.verify!=null&&Dashboard.verify.equals("Yes")){
                    filter2.setVisibility(View.VISIBLE);

                }else if (Dashboard.verify!=null&&Dashboard.verify.equals("No")){
                    filter2.setVisibility(View.GONE);
                }

                filter1.setOnClickListener(view -> {
                    Intent intent = new Intent(Moreoptions.this, new_design_transaction.class);
                    startActivity(intent);
                });
                filter2.setOnClickListener(view -> {
                    Intent intent = new Intent(Moreoptions.this, UpiTransactionDetails.class);
                    startActivity(intent);
                });

                filter3.setOnClickListener(v -> {
                    Intent intent = new Intent(Moreoptions.this, RentTransactionDetails.class);
                    startActivity(intent);
                });
                BottomSheetDialog dialog = new BottomSheetDialog(Moreoptions.this);
                dialog.setContentView(dialogViewDashboard);
                dialog.show();
                dialog.setOnDismissListener(dialog1 -> onResume());
            }

        });
        share = (TextView) findViewById(R.id.textView122);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "https://play.google.com/store/apps/details?id=com.nexdha.nexpay&hl=en";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Credit Card Payments Made Seamless");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
        faq = (TextView) findViewById(R.id.textView62);
        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Intent intent = new Intent(Moreoptions.this, FAQ.class);
                //startActivity(intent);

                Intent faq =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://www.nexdha.com/faq.html"));
                startActivity(faq);
            }
        });
        logout = (TextView) findViewById(R.id.textView71);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(Moreoptions.this)


                        .setTitle("Logout")
                        .setMessage("Are you sure you want to logout?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                SharedPreferences.Editor editorlogout = sharedPreferences.edit();
                                editorlogout.clear();
                                editorlogout.apply();

                                SharedPreferences.Editor editorotp = welcomepreference.edit();
                                editorotp.clear();
                                editorotp.apply();

                                finishAffinity();
                                Intent intent = new Intent(Moreoptions.this, Verification.class);
                                startActivity(intent);
                            }
                        }).create().show();

            }
        });
        support = (TextView) findViewById(R.id.textView70);
        support.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("IntentReset")
            @Override
            public void onClick(View view) {
             /*   Intent viewIntentsupport =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("http://nexdha.com/#contact"));
                                startActivity(viewIntentsupport);

                                */

                new MaterialStyledDialog.Builder(Moreoptions.this)
                        .setTitle("Support")
                        .setDescription("For support please contact support@nexdha.com or call 8667451930 during business hours (09 AM - 06 PM)")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.email)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(true)
                        .setCancelable(false)
                        .withDivider(true)
                        .setPositiveText("Got it")
                        .onPositive((dialog, which) -> dialog.dismiss())
                        /*.setNeutralText("Make call")
                        .onNeutral(((dialog, which) -> {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:"+"8667451930"));//change the number
                            startActivity(callIntent);
                        }))  */
                        .setNegativeText("Send mail")
                        .onNegative(((dialog, which) -> {
                            Intent mailIntent = new Intent(Intent.ACTION_VIEW);
                            Uri data = Uri.parse("mailto:?subject=" + "Issue from " + publicname + " with number " + publicnumber + "&body=" + "My name is " + publicname + ", my mobile number is " + publicnumber + "." + "\r\n \r\nIssue description : " + "&to=" + "support@nexdha.com");
                            mailIntent.setData(data);
                            startActivity(Intent.createChooser(mailIntent, "Send mail via..."));
                        }))
                        .show();
            }
        });
    }


    private void sessionCheck() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/counter_check",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println(response);
                        String getResponse = jsonObject.getString("detail");

                        if (getResponse.equals("1")) {
                            String allowUser = jsonObject.getString("allow_user");
                            if (allowUser.equals("0")) {
                                final LayoutInflater layoutInflaterBlock = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog blockUserDialog = new Dialog(this);
                                blockUserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                blockUserDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                blockUserDialog.setContentView(layoutInflaterBlock.inflate(R.layout.block_user_layout, null));
                                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                blockUserDialog.getWindow().setLayout(width, height);
                                blockUserDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                blockUserDialog.setCancelable(false);
                                blockUserDialog.show();
                            } else {
                                //Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            }
                        } else if (getResponse.equals("Invalid token")) {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            });
                        } else {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("auth", TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }


    public void sme_api() {
        SharedPreferences sharedPreferences = Moreoptions.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("4444444444444444444444444444444444444444444444444444444");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest request = new StringRequest(Request.Method.POST, SERVER + "/api/sme_sms", response -> {
            System.out.println(response);
            alreadyrequested_api();

        }, error -> {
            Log.e("error is ", "" + error);
            System.out.println(error);
        }) {


            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }




            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("pan",panindia);
                System.out.println(panindia);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(Moreoptions.this);
        request.setShouldCache(false);
        requestQueue.add(request);
    }

    public void alreadyrequested_api() {
        SharedPreferences sharedPreferences = Moreoptions.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("4444444444444444444444444444444444444444444444444444444");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest request = new StringRequest(Request.Method.GET,SERVER+"/api/sme_sms", new Response.Listener<String>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    String parse_token = json.getString("status");
                    System.out.println(parse_token);

                    if (parse_token.equals("True")){
                        textView3.setText("Request submitted");
                        textView3.setTextColor(Color.parseColor("#58A837"));
                        sme_layout.setEnabled(false);

                    }
                    progressBar13.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, error -> {
            Log.e("error is ", "" + error);
            System.out.println(error);
        }) {


            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();
                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }



         /*   protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("pan",(panindia));


                System.out.println(panindia);
                return params;
            }*/
        };
        RequestQueue requestQueue = Volley.newRequestQueue(Moreoptions.this);
        request.setShouldCache(false);
        requestQueue.add(request);
    }

    private void referal_details(){

        SharedPreferences sharedPreferences = Moreoptions.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/t43_reference",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            if (profileresponse.getString("status").equals("success")){
                                referal_progress.setVisibility(View.GONE);
                                Toast.makeText(Moreoptions.this, "Referred successfully", Toast.LENGTH_SHORT).show();
                                finish();
                            }else if (profileresponse.getString("status").equals("already exist")){
                                referal_phone_number.setError("Already Referred");
                                referal_progress.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //     message = "cannot connect";
                    new MaterialStyledDialog.Builder(Moreoptions.this)
                            .setTitle("Internet Error")
                            .setDescription("There will be issue displaying your info,Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Moreoptions.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                params.put("phone", get_referal_phone);
                params.put("referred_by", Dashboard.responsephonenumber);

                return params;

            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Moreoptions.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
        private void profileinfo(){

        SharedPreferences sharedPreferences = Moreoptions.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            pname.setText(profileresponse.getString("name"));
                            if (profileresponse.getString("id").equals("36476")) {
                                pname.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkUpdate();

                                        //Toast.makeText(Moreoptions.this, "Test one", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            pemail.setText(profileresponse.getString("email"));
                            pphone.setText(profileresponse.getString("phone"));
                            publicname = profileresponse.getString("name");
                            publicemail = profileresponse.getString("email");
                            publicnumber = profileresponse.getString("phone");
                            if (profileresponse.getString("kyc").equals("V") || profileresponse.getString("kyc") == "V") {
                                kycverification.setText("Verified");
                                kycverification.setTextColor(Color.parseColor("#58A837"));
                                kycverification.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        return;


                                    }
                                });
                            }else if (profileresponse.getString("kyc").equals("P") || profileresponse.getString("kyc") == "P"){
                                kycverification.setText("Pending");
                                kycverification.setTextColor(Color.BLUE);
                                kycverification.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        return;
                                    }
                                });
                            }else if (profileresponse.getString("kyc").equals("Rej") || profileresponse.getString("kyc") == "Rej"){
                                kycverification.setText("Rejected");
                                kycverification.setTextColor(Color.RED);
                                kycverification.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        return;
                                    }
                                });
                            }else if (profileresponse.getString("kyc").equals("Ret") || profileresponse.getString("kyc") == "Ret"){
                                kycverification .setTextColor(Color.parseColor("#11398a"));
                                kycverification.setText("Retry\n(Click here to Upload KYC.)");
                                kycverification.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(Moreoptions.this, grantreadandwritepermission.class);
                                        startActivity(intent);
                                    }
                                });
                            }
                            else{
                                kycverification .setTextColor(Color.parseColor("#11398a"));
                                kycverification.setText("(Click here to verify)");
                                kycverification.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(Moreoptions.this, grantreadandwritepermission.class);
                                        startActivity(intent);
                                    }
                                });
                            }
                            if (profileresponse.getString("reference_status").equals("1")){
                                refer_constraint.setVisibility(View.VISIBLE);
                                refer_linear.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
               //     message = "cannot connect";
                    new MaterialStyledDialog.Builder(Moreoptions.this)
                            .setTitle("Internet Error")
                            .setDescription("There will be issue displaying your info,Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Moreoptions.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };

            RequestQueue requestQueue = Volley.newRequestQueue(Moreoptions.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);

        }

    private void checkUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                try {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, this, AppUpdateOptions.newBuilder(AppUpdateType.FLEXIBLE).setAllowAssetPackDeletion(true).build(), MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(this, "no update available", Toast.LENGTH_SHORT).show();
                System.out.println("No update available");
            }

        });

        appUpdateManager.registerListener(installStateUpdatedListener);


    }

    private final InstallStateUpdatedListener installStateUpdatedListener = installState -> {
        if (installState.installStatus() == InstallStatus.DOWNLOADED) {
            popupSnackBarForCompleteUpdate();
        }


    };

    @Override
    protected void onStop() {

        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(installStateUpdatedListener);
        }

        super.onStop();
    }

    @Override
    protected void onResume() {

        sessionCheck();

        profileinfo();
        super.onResume();
        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(result -> {
            if (result.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                try {
                    appUpdateManager.startUpdateFlowForResult(result, AppUpdateType.FLEXIBLE, Moreoptions.this
                            , MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void popupSnackBarForCompleteUpdate() {

        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();

        Snackbar snackbar = Snackbar.make(findViewById(R.id.context), "New app is ready to install!", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Install", v -> appUpdateManager.completeUpdate());
        snackbar.show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                Toast.makeText(this, "Update failed", Toast.LENGTH_SHORT).show();
                System.out.println("Update failed " + resultCode);
                checkUpdate();


            }
        }
    }

    private void totaltrans() {

        SharedPreferences sharedPreferences = Moreoptions.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/total_transactions",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject transactionresponse = new JSONObject(response);
                            String totaltransaction = transactionresponse.getString("Result");
                            System.out.println(totaltransaction);
                            String numberofdays = transactionresponse.getString("No of days");
                            if (totaltransaction == null || totaltransaction == "null" || totaltransaction.equals(null)) {
                                t1eligibility.setProgress(0);
                            }
                            //set1//
                            else {
                                transactioninteger = Double.parseDouble(totaltransaction);
                                numberofdaysinteger = Double.parseDouble(numberofdays);


                            //set2 //
                             if (transactioninteger <= 200) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(2);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(3);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(4);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(5);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(6);
                                }

                            }
                            //set3//
                            else if (transactioninteger <= 500) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(3);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(4);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(5);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(6);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(7);
                                }

                            }
                            //set4//
                            else if (transactioninteger <= 1500) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(4);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(5);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(6);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(7);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(8);
                                }

                            }

                            //set5//
                            else if (transactioninteger <= 2500) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(5);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(6);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(7);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(8);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(9);
                                }

                            }
                            //set6//
                            else if (transactioninteger <= 4500) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(6);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(7);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(8);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(9);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(10);
                                }

                            }
                            //set7//
                            else if (transactioninteger <= 6000) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(10);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(11);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(12);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(13);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(14);
                                }

                            }
                            //set8//
                            else if (transactioninteger <= 8000) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(15);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(17);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(18);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(19);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(20);
                                }

                            }
                            //set9//
                            else if (transactioninteger <= 10000) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(20);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(21);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(22);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(23);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(24);
                                }

                            }
                            //set10//
                            else if (transactioninteger <= 13000) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(25);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(26);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(27);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(28);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(30);
                                }

                            }
                            //set11//
                            else if (transactioninteger <= 16000) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(31);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(33);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(37);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(48);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(50);
                                }

                            }
                            //set12//
                            else if (transactioninteger <= 20000) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(31);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(33);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(37);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(68);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(80);
                                }

                            }
                            //set13//
                            else if (transactioninteger <= 25000) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(31);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(43);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(47);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(88);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(95);
                                }

                            }
                            //set14 - final//
                            else if (transactioninteger > 25000) {
                                if (numberofdaysinteger < 5) {
                                    t1eligibility.setProgress(31);
                                } else if (numberofdaysinteger < 10) {
                                    t1eligibility.setProgress(43);
                                } else if (numberofdaysinteger < 15) {
                                    t1eligibility.setProgress(47);
                                } else if (numberofdaysinteger < 25) {
                                    t1eligibility.setProgress(88);
                                } else if (numberofdaysinteger <= 30) {
                                    t1eligibility.setProgress(99);
                                } else if (numberofdaysinteger > 30) {
                                    t1eligibility.setProgress(100);
                                }

                            }
                        }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //     message = "cannot connect";
                    new MaterialStyledDialog.Builder(Moreoptions.this)
                            .setTitle("Internet Error")
                            .setDescription("There will be issue displaying your info,Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Moreoptions.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Moreoptions.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
    private void activity_log_more_options(){

        SharedPreferences sharedPreferences = Moreoptions.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Moreoptions.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Moreoptions.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","117");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Moreoptions.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

}
