package com.nexdha.nexpay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {
    private static final int SPLASH_TIME_OUT = 1000;
    ImageView splash_spin;
    public static String serverName;

    static {
        System.loadLibrary("keys");
    }

    public static native String getNativeKey1();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_main);
        splash_spin = findViewById(R.id.imageView60);


        getServer();

        splash_spin.startAnimation(AnimationUtils.loadAnimation(this, R.anim.splash_rotate));


    }

    private void getServer() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://nexdha.com/HztEnnj_My{6.y'.json",
                response -> {
                    try {
                        JSONObject response1 = new JSONObject(response);
                        System.out.println(response1.getString("nexpro"));
                        serverName = response1.getString("nexpro");
                        System.out.println(getNativeKey1());


                        new Handler().postDelayed(() -> {
                            if (isEmulator()) {
                                Toast.makeText(MainActivity.this, "App run on emulator", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Intent homeIntent = new Intent(MainActivity.this, Verification.class);//verification
                                startActivity(homeIntent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }
                        }, SPLASH_TIME_OUT);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {
            Toast.makeText(this, "Server is down. Please try again later", Toast.LENGTH_SHORT).show();
            System.out.println(error instanceof ClientError);


            String message = null;
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            assert message != null;
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    @SuppressLint("HardwareIds")
    boolean isEmulator() {
        return (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.HARDWARE.contains("goldfish")
                || Build.HARDWARE.contains("ranchu")
                || Build.HARDWARE.equals("vbox86")
                || Build.HARDWARE.toLowerCase().contains("nox")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MODEL.toLowerCase().contains("droid4x")
                || Build.MANUFACTURER.contains("Genymotion")
                || Build.PRODUCT.contains("sdk_google")
                || Build.PRODUCT.contains("google_sdk")
                || Build.PRODUCT.contains("sdk")
                || Build.PRODUCT.contains("sdk_x86")
                || Build.PRODUCT.contains("vbox86p")
                || Build.PRODUCT.contains("emulator")
                || Build.PRODUCT.contains("simulator")
                || Build.PRODUCT.toLowerCase().contains("nox")
                || Build.BOARD.toLowerCase().contains("nox")
                || Build.BOOTLOADER.toLowerCase().contains("nox")
                || Build.SERIAL.toLowerCase().contains("nox");
    }


}
