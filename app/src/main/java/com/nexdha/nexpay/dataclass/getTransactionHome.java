package com.nexdha.nexpay.dataclass;

import com.google.gson.annotations.SerializedName;

public class getTransactionHome {


    @SerializedName("udf3")
    String udf3;
    @SerializedName("udf5")
    String udf5;
    @SerializedName("amount")
    String amount;

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public String getUdf5() {
        return udf5;
    }

    public void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


}
