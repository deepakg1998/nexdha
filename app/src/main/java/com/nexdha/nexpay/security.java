package com.nexdha.nexpay;

import android.app.KeyguardManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

public class security extends AppCompatActivity {
    String securityback = "okay";
    KeyguardManager km;
    ProgressBar progressbar7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_security);
        progressbar7 = findViewById(R.id.progressBar7);
        progressbar7.setVisibility(View.GONE);


        km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        if (km.isKeyguardSecure()) {
            Intent intent = km.createConfirmDeviceCredentialIntent(null, null);
            intent.putExtra("FROM_ACTIVITY", "security");
            startActivityForResult(intent, 1234);
            //progressbar7.setVisibility(View.VISIBLE);


        } else {
            progressbar7.setVisibility(View.VISIBLE);
            Intent intent = new Intent(this, updateverification.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            progressbar7.setVisibility(View.GONE);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234) {
            if (resultCode == RESULT_OK) {
                Intent homeIntent = new Intent(security.this, updateverification.class);
                startActivity(homeIntent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                securityback = "done";
                progressbar7.setVisibility(View.GONE);
                //   finish();

            } else {
                finish();
                progressbar7.setVisibility(View.GONE);
            }

        }
    }


    @Override
    public void onBackPressed() {
      super.finish();

           }







}
