package com.nexdha.nexpay;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nexdha.nexpay.constant.SERVER;

public class Typeoftransaction extends AppCompatActivity {

    Button proceed;
    CheckBox checkbox2,checkbox3,checkbox5,checkbox9;
    ImageView imageview31,imageview14;
    TextView textview88;
    TextView textview229;
    TextView textview89;
    TextView textview243;
    public static String transactiontype = null;
    public static String transactionamount = null;
    String signupdays;
    double signupinteger;
    double inum ;
    double personamount ;
    String percentage_t1 = null;
    String percentage_t2 = null;
    String percentage_sds = null;
    String type_of_sds = "";
    ProgressBar progressbar13;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_typeoftransaction);
        gettransactiontotal();
        percentage_api();
        activity_log_type_tran();
        //sdsactivation();
        holiday_finder();
        progressbar13 = findViewById(R.id.progressBar13);
        imageview14 = findViewById(R.id.imageView14);
        imageview14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        textview88 = findViewById(R.id.textView88);
        textview229 = findViewById(R.id.textView229);
        textview89 = findViewById(R.id.textView89);
        textview243 = findViewById(R.id.textView243);
        checkbox2 = findViewById(R.id.checkBox2);
        checkbox3 = findViewById(R.id.checkBox3);
        checkbox5 = findViewById(R.id.checkBox5);
        checkbox9 = findViewById(R.id.checkBox9);





        checkbox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (checkbox2.isChecked()){
                    checkbox3.setChecked(false);
                    checkbox5.setChecked(false);
                    checkbox9.setChecked(false);
                    transactiontype = "train";
                }
            }
        });
        checkbox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               if (checkbox3.isChecked()){
                   checkbox2.setChecked(false);
                   checkbox5.setChecked(false);
                   checkbox9.setChecked(false);
                   transactiontype = "ship";
                }
            }
        });

        checkbox5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (checkbox5.isChecked()){
                    checkbox2.setChecked(false);
                    checkbox3.setChecked(false);
                    checkbox9.setChecked(false);
                    transactiontype = type_of_sds;
                }
            }
        });

        checkbox9.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (checkbox9.isChecked()) {
                    checkbox2.setChecked(false);
                    checkbox3.setChecked(false);
                    checkbox5.setChecked(false);
                    transactiontype = "hds";
                }
            }
        });

        proceed = (Button)findViewById(R.id.button26);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation() == false){
                    return;
                }
                Intent intent = new Intent(Typeoftransaction.this, Payment.class);
                startActivity(intent);

            }
        });
        imageview31 = (ImageView)findViewById(R.id.imageView31);
        imageview31.setOnClickListener(view -> new MaterialStyledDialog.Builder(Typeoftransaction.this)
                .setTitle("Info")
                .setDescription("1. Same Day Payments (T+0) carry a transaction fee of "+ percentage_sds+"%. Eligible users will be allowed to do the transaction only during bank working day from 12:00 AM to 04:00 PM only.The amount will be paid to the benefeciary on the same bank working day.\n" +
                        "2. Next Day Payments (T+1) carry a transaction fee of "+percentage_t1+"%. The amount will be paid to the benefeciary within 1 bank working day.\n" +
                        "3. Two day Payments (T+2) carry a transaction fee of "+percentage_t2+"%. The amount will reach the beneficiary in two banking days")
                .setHeaderColor(R.color.md_divider_white)
                .setIcon(R.drawable.iicon)
                .withDarkerOverlay(true)
                .withDialogAnimation(true)
                .setCancelable(false)
                .withDivider(true)
                .setNegativeText("Close")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })

                .show());



    }



    private boolean validation(){

       /* if ((checkbox2.isChecked() == true && signupinteger < 30) || (checkbox2.isChecked() == true && inum < personamount)){
            Toast.makeText(Typeoftransaction.this,"You are not eligible for T+1 Transaction ", Toast.LENGTH_SHORT).show();
            return false;
        }

        */


        if (checkbox2.isChecked() != true && checkbox3.isChecked() != true && checkbox5.isChecked() != true && checkbox9.isChecked() != true){
            Toast.makeText(Typeoftransaction.this,"Please select type of transaction ", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;


    }


    @Override
    public void onResume() {
        super.onResume();
        gettransactiontotal();
        activity_log_type_tran();
    }

        //-----------------total-transaction-details---------------------//
    private void gettransactiontotal(){

        SharedPreferences sharedPreferences = Typeoftransaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/total_transactions",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject transactionresponse = new JSONObject(response);
                            System.out.println(transactionresponse);
                            if ((transactionresponse.getString("Result").equals(null)) || (transactionresponse.getString("Result") == null) || (transactionresponse.getString("Result") == "null") || (transactionresponse.getString("Result").equals("null"))){
                                inum = 0;
                                transactionamount = "null";
                            }else{
                                transactionamount = transactionresponse.getString("Result");
                                inum = Double.parseDouble(transactionamount);
                            }

                            signupdays = transactionresponse.getString("No of days");
                            System.out.println(signupdays);
                            signupinteger = Double.parseDouble(signupdays);

                          //  personamount = Double.parseDouble(Dashboard.amountlimit);
                          /*  if (transactionamount.equals(null) || transactionamount.equals("null")){
                                checkbox2.setVisibility(View.GONE);
                                textview88.setVisibility(View.GONE);
                            }else{
                                final double inum = Double.parseDouble(transactionamount);
                                final double personamount = Double.parseDouble(Dashboard.amountlimit);
                                if ((inum  >= personamount) && (signupinteger >= 30) ){
                                    checkbox2.setVisibility(View.VISIBLE);
                                    textview88.setVisibility(View.VISIBLE);
                                }else{
                                    checkbox2.setVisibility(View.GONE);
                                    textview88.setVisibility(View.GONE);
                                }
                            }

                           */

                            /*userid = profileresponse.getString("id");
                            responsephonenumber = profileresponse.getString("phone");
                            fraudverification();*/
                            //   developerverification();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, error -> {
                    String message = "";

                    if (error instanceof NetworkError) {
                        //   message = "cannot profile connect";
                        new MaterialStyledDialog.Builder(Typeoftransaction.this)
                                .setTitle("Internet Error")
                                .setDescription("Check Your Interenet Connection")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.warninginternet)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(false)
                                .setCancelable(false)
                                .withDivider(true)
                                .setNegativeText("Cancel")
                                .onNegative((dialog, which) -> {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                })
                                .setPositiveText("Open Settings")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                        // dialog.dismiss();
                                    }
                                })
                                .show();
                    } else if (error instanceof AuthFailureError) {
                        message = "There is a problem connecting to server. Please try after sometime.";
                    } else if (error instanceof NoConnectionError) {
                        message = "There is a problem connecting to server. Please try after sometime.";
                    } else if (error instanceof ParseError) {
                        message = "Connection Timedout. Please try after sometime";
                    }
                    if (!message.isEmpty()) {
                        Toast.makeText(Typeoftransaction.this, message, Toast.LENGTH_SHORT).show();
                    }
                })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() {
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Typeoftransaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
    private void holiday_finder(){

        SharedPreferences sharedPreferences = Typeoftransaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/holidayfinder",
                response -> {
                    try {
                        JSONObject profileresponse = new JSONObject(response);
                        if(profileresponse.getString("status").equals("Success")){
                            if(profileresponse.getString("tomorrow_holiday").equals("True")){
                                checkbox9.setVisibility(View.VISIBLE);
                                textview243.setVisibility(View.VISIBLE);
                            }

                            if(profileresponse.getString("today_holiday").equals("True")){
                                checkbox5.setVisibility(View.VISIBLE);
                                textview229.setVisibility(View.VISIBLE);
                                type_of_sds = "hds_jet";
                                progressbar13.setVisibility(View.GONE);
                            }else{
                                sdsactivation();
                            }

                        }else{
                            sdsactivation();
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Typeoftransaction.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Typeoftransaction.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }




        };

        RequestQueue requestQueue = Volley.newRequestQueue(Typeoftransaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
    private void sdsactivation(){

        SharedPreferences sharedPreferences = Typeoftransaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

       StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/sds_activation",
               response -> {
                   try {
                       JSONObject transactionresponse = new JSONObject(response);
                       System.out.println(transactionresponse);
                       if ((transactionresponse.getString("status").equals("true"))){

                           checkbox5.setVisibility(View.VISIBLE);
                           textview229.setVisibility(View.VISIBLE);
                           type_of_sds = "jet";
                           System.out.println("true");
                       }
                      // holiday_finder();
                       progressbar13.setVisibility(View.GONE);


                   } catch (JSONException e) {
                       e.printStackTrace();
                   }

               }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Typeoftransaction.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Typeoftransaction.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }




        };

        RequestQueue requestQueue = Volley.newRequestQueue(Typeoftransaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

    private void activity_log_type_tran(){

        SharedPreferences sharedPreferences = Typeoftransaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Typeoftransaction.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Typeoftransaction.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","116");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Typeoftransaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


    private void percentage_api(){

        SharedPreferences sharedPreferences = Typeoftransaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");

       System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/percentage",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            percentage_t1=profileresponse.getString("t11");
                            percentage_t2=profileresponse.getString("t22");
                            percentage_sds=profileresponse.getString("sds");
                            String holiday_hds = profileresponse.getString("holidayt1");
                            textview88.setText("T+1 (Transaction + one bank working day),"+ percentage_t1 +"%per transaction.");
                            textview89.setText("T+2 (Transaction + two bank working days), "+ percentage_t2 +"% per transaction.");
                            textview229.setText("Same Day Settlement between 12:00 AM - 09:00 PM, "+percentage_sds+"% per transaction.");
                            textview243.setText("Holiday Settlement (T+1),"+ holiday_hds +"% per transaction.");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Typeoftransaction.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Typeoftransaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }


}
