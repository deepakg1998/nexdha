package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.nexdha.nexpay.activity.UpiTransactionDetails;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.test.pg.secure.pgsdkv4.PaymentGatewayPaymentInitializer;
import com.test.pg.secure.pgsdkv4.PaymentParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class Scan extends AppCompatActivity {
    public TextView textView247, textView265,textView258;
    public EditText editTextTextPersonName2;
    public Button button57;
    public Spinner spinner4;
    public ImageView imageView65;
    public TextView textView264, textView268;
    public Button button59;
    String Amountgiven;
    String mail;
    String name;
    String phone;
    String orderidgiven;
    String currencygiven;
    String descriptatives;
    String cityloc;
    String stateloc;
    String Address_Line1;
    String Address_Line2;
    String ZipCodegiven;
    String Countrys;
    String cardmaskeds;
    String modeofpayment;
    String setUdf1only;
    String setUdf2only;
    String setUdf3only;
    String setUdf4only;
    String setUdf5ony;
    String error_descs;
    String hashs;
    String transaction_ids;
    String response_messages;
    String onselect;
    String getpgtran;
    public ProgressBar progressBar23;
    public String helloss;
    public String helloss1;
    public CheckBox checkBox11;
    public String upiidonly;
    public String onlynamecome;
    public String forUPIchecking1;
    public String forsaveupi;
    public String sharedpurposename;
    public String name1;
    public String emaili;
    public String nameonlyies;
    public String phonee;
    public String fullid;
    public String abc;

    float percentage_UPI1;
    float strdecimalformat;
    String Finalamount;
    String cardpercentage121 = "";
    ArrayList<String> isplist, ispids, ispvendorid, paylist, payid;
    String myDate ;
    TextView textview257;
    ProgressBar progressbar26;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_scanactivity);
        textview257 = findViewById(R.id.textView257);
        progressbar26 = findViewById(R.id.progressBar26);
        checkupiidpresent();
        forgetuserdetails();
        amountlimitation();
        gettranid();
        percentageUPI();
                               /*  percentage_UPI1 =profileresponse.getString("upi");
                            System.out.println(percentage_UPI1);*/

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());

        myDate = format.format(new Date());
        System.out.println(myDate);
        spinner4 = findViewById(R.id.spinner4);
        paylist = new ArrayList<>();
        payid = new ArrayList<>();
        purposeofmoney();
        button59 =findViewById(R.id.button59);
        editTextTextPersonName2 = findViewById(R.id.editTextTextPersonName2);
        editTextTextPersonName2.requestFocus();
        checkBox11 = findViewById(R.id.checkBox11);
        textView265 = findViewById(R.id.textView265);
        textView247 = findViewById(R.id.textView247);
        textView258=findViewById(R.id.textView258);
        imageView65 = findViewById(R.id.imageView65);
/*
        textView264 =findViewById(R.id.textView264);
*/

        textView268 = findViewById(R.id.textView268);
        textView268.setText("Final Amount - ₹ 0");

       /* String value = getIntent().getExtras().getString("vpaonly[1]");
        System.out.println(value);*/
        upiidonly = getIntent().getExtras().getString("VPA");
        System.out.println(upiidonly);
        nameonlyies = getIntent().getExtras().getString("nameonlyies");
        System.out.println(nameonlyies);

/*
        String upiidonly = getIntent().getExtras().getString("upiidonly");
*/

/*
        String fromscanname = getIntent().getExtras().getString("nameonlyies");
*/
        /* textView265.setText(fromscanname);
         */
        textView265.setText(nameonlyies);
        System.out.println(nameonlyies);
/*
        textView265.setText(forspinnercall);
*/
        System.out.println(upiidonly);
        textView247.setText(upiidonly);

        button57 = findViewById(R.id.button57);


        editTextTextPersonName2.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here

                // yourEditText...
            }


            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                final String print = editTextTextPersonName2.getText().toString();
                System.out.println(print);
                textview257.setVisibility(View.GONE);

/*
                final Integer result2 = Integer.parseInt(print);
*/
                System.out.println(print.length());
                textView268.setText("");

                if (print.length() >= 2) {
                    if (Double.parseDouble(print) <= Double.parseDouble(helloss) && Double.parseDouble(print) >= Double.parseDouble(helloss1)) {
                        final double hello = Double.parseDouble(print);
                        final double hello1 = Double.parseDouble(abc);
                        percentage_UPI1 = (float) (hello +((hello / 100) * hello1) );
                    //   String strdecimalformat = new DecimalFormat("##.00").format(Double.parseDouble(String.valueOf(percentage_UPI1)));//old code
                        String strdecimalformat = new DecimalFormat("##,##,##,##,##,##,##,##,##,###.00").format(Double.parseDouble(String.valueOf(percentage_UPI1)));


                        Finalamount = strdecimalformat;
                        textView268.setText("Final Amount - ₹" + " " + Finalamount);
                        System.out.println(Finalamount);
                    }
                    else{
                        textview257.setText("Enter Amount Between ₹" + helloss1 + " and ₹" + helloss);
                        textview257.setVisibility(View.VISIBLE);
                    }

                }


            }
        });
        button57.setOnClickListener(v -> {
            progressbar26.setVisibility(View.VISIBLE);
            button57.setEnabled(false);
            button57.setBackgroundResource(R.drawable.pageoneconstraintthree);
            String print = editTextTextPersonName2.getText().toString();
            System.out.println(print);


            if (validate1() == false) {
                button57.setEnabled(true);
                button57.setBackgroundResource(R.drawable.roundedbutton);
                progressbar26.setVisibility(View.GONE);
                return;
            }
            Double i = Double.parseDouble(print);
            System.out.println(i);

/*
                    Integer i = Integer.valueOf(print);
*/
            System.out.println(i);
            if (i <= Double.parseDouble(helloss) && i >= Double.parseDouble(helloss1)) {
                check_upi_availability();
            } else {
                Toast.makeText(Scan.this, "Enter Amount between ₹ " + helloss1 + " to ₹ " + helloss, Toast.LENGTH_LONG).show();
                button57.setEnabled(true);
                button57.setBackgroundResource(R.drawable.roundedbutton);
                progressbar26.setVisibility(View.GONE);

            }
        });
        imageView65.setOnClickListener(v -> {
            Intent intenty = new Intent(Scan.this, Upitransaction.class);
            startActivity(intenty);
        });

    }
private void check_upi_availability(){

    SharedPreferences sharedPreferences = Scan.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
    final String token1 = sharedPreferences.getString("token", "");
    System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
    System.out.println(token1);
    final String token200 = token1;
    System.out.println(token200);
    // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
    StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/upi_activation",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Nothing
                    try{
                        JSONObject responpopup = new JSONObject(response);
                        String sds_status = responpopup.getString("status");
                        if (sds_status == "true" || sds_status.equals("true")){
                            itemClicked();
                        }else{
                            progressbar26.setVisibility(View.GONE);
                            new MaterialStyledDialog.Builder(Scan.this)

                                    .setTitle("Closed")
                                    .setDescription("UPI transaction is closed for now. Please try after sometime.")
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.transactionfailed)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(true)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .setPositiveText("Got it")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            Intent intent = new Intent(Scan.this, Dashboard.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    })
                                    .show();
                        }

                    }catch(JSONException e){
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            String message = "";

            if (error instanceof NetworkError) {
                //   message = "cannot profile connect";
                new MaterialStyledDialog.Builder(Scan.this)
                        .setTitle("Internet Error")
                        .setDescription("Check Your Internet Connection")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.warninginternet)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .setNegativeText("Cancel")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                            }
                        })
                        .setPositiveText("Open Settings")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                // dialog.dismiss();
                            }
                        })
                        .show();
            } else if (error instanceof AuthFailureError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof NoConnectionError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout. Please try after sometime";
            }
            if (!message.isEmpty()) {
                Toast.makeText(Scan.this, message, Toast.LENGTH_SHORT).show();
            }
        }
    })

    {

        //-------------------------outerheader-----------------------//
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError{
            String T="" ;
            Map<String,String> params = new HashMap<>();


            // params.put("Content-Type", "application/json; charset=UTF-8");
            try {
                JSONObject response1 = new JSONObject(token200);
                System.out.println("!!!!!!!!!!!!!!!!!!");
                System.out.println(response1.getString("token"));
                // do your work with response object
                T=response1.getString("token");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println("44444444444444444444444444");
            System.out.println(token200);
            ///check t!blank

            String TokenS = "token "+ T;
            System.out.println(T);

            params.put("Authorization",TokenS);

            return params;
        }

        protected Map<String,String> getParams(){
            //  String updateid = "";
            // String updatetranid="";
            Map<String, String> params = new HashMap<>();

            // System.out.println("make my trippppppppppppppp");
            // System.out.println(Transactionid);

            // System.out.println("mapppppp");
            // System.out.println(Updatetranidapi);




            params.put("AcitvityID","113");
            return params;
        }


    };

    RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
    stringRequest.setShouldCache(false);
    requestQueue.add(stringRequest);



}
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PGConstants.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String paymentResponse = data.getStringExtra(PGConstants.PAYMENT_RESPONSE);
                    System.out.println("paymentResponse: " + paymentResponse);

                    if (paymentResponse.equals("null")) {
                        System.out.println("Transaction Error!");
                    } else {
                        JSONObject profileresponse = new JSONObject(paymentResponse);
                        Amountgiven = profileresponse.getString("amount");//
                        mail = profileresponse.getString("email");//
                        name = profileresponse.getString("name");//
                        phone = profileresponse.getString("phone");//
                        orderidgiven = profileresponse.getString("order_id");//
                        cardmaskeds = profileresponse.getString("cardmasked");//
                        hashs = profileresponse.getString("hash");
                        currencygiven = profileresponse.getString("currency");//
                        descriptatives = profileresponse.getString("description");//
                        cityloc = profileresponse.getString("city");//
                        stateloc = profileresponse.getString("state");//
                        myDate = profileresponse.getString("address_line_1");//
                        System.out.println(Address_Line1);
                        Address_Line2 = profileresponse.getString("address_line_2");//
                        System.out.println( Address_Line2);
                        ZipCodegiven = profileresponse.getString("zip_code");//
                        Countrys = profileresponse.getString("country");//
/*
                        Amountgiven = profileresponse.getString("PG_RETURN_URL");//
*/
/*
                    ReturnUrl= profileresponse.getString("PG_MODE");//
*/
                        modeofpayment = profileresponse.getString("payment_mode");//
                        setUdf1only = profileresponse.getString("udf1");//
                        setUdf2only = profileresponse.getString("udf2");//
                        nameonlyies = profileresponse.getString("udf3");//
                        System.out.println(nameonlyies);
                        setUdf4only = profileresponse.getString("udf4");//
                        setUdf5ony = profileresponse.getString("udf5");//
                        error_descs = profileresponse.getString("error_desc");
                        response_messages= profileresponse.getString("response_message");
                        transaction_ids = profileresponse.getString("transaction_id");
                        if(response_messages.equals("Transaction successful")){
                            upi_sms();
                            new MaterialStyledDialog.Builder(this)
                                    .setTitle("Transaction Successful")
                                    .setDescription("Your amount will be credited within 15 minutes" + ".\nPlease Rate us on playstore.")
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.tickicon)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(true)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .setPositiveText("Got it")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            Intent intent = new Intent(Scan.this, UpiTransactionDetails.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    })
                                    .show();
                        }else {
                            new MaterialStyledDialog.Builder(this)
                                    .setTitle("Failed")
                                    .setDescription(response_messages)
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.transactionfailed)
                                    .withIconAnimation(false)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(true)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .setPositiveText("Got it")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            Intent intent = new Intent(Scan.this, UpiTransactionDetails.class);
                                            intent.putExtra("STRING_I_NEED", "forcancelled");
                                            startActivity(intent);
                                            finish();
                                        }
                                    })
                                    .show();
                        }
                        Aggrepay_upi();                  }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void upi_sms(){

        SharedPreferences sharedPreferences = Scan.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/upi_sms",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("*******************sms_response************************");
                        System.out.println(response);
                        // Toast.makeText(Payment.this,"SMS Sent",Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Scan.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        })

        {
            protected Map<String,String> getParams(){;
                Map<String, String> params = new HashMap<>();

                params.put("amt",Amountgiven);
                params.put("transid",transaction_ids);
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);




    }

    public void Aggrepay_upi() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/save_PG_Tran_Response",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("*******************************************");
                        System.out.println(response);
                        try {
                            JSONObject helloresponse = new JSONObject(response);
                            System.out.println(helloresponse);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Scan.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {


            //This is for Headers If You Needed

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("state", stateloc);//
                params.put("udf1", setUdf1only);//
                params.put("udf2", setUdf2only);//
                params.put("udf3", nameonlyies);//
                params.put("udf4", upiidonly);//
                params.put("udf5", setUdf5ony);//
/*
                params.put("udf6", "setUdf6only");
*/
                params.put("address_line_1", myDate);//
                params.put("address_line_2", Address_Line2);//
                params.put("city", cityloc);//
                params.put("amount", Amountgiven);//
                params.put("country", sharedpurposename);//
                params.put("currency", currencygiven);//
                params.put("description", descriptatives);//
                params.put("email", mail);//
                params.put("name", name);//
                params.put("order_id", orderidgiven);//
                params.put("phone", phone);//
                params.put("payment_channel", "Upi Aggrepay");//
                params.put("response_message", response_messages);//
                params.put("zip_code", ZipCodegiven);//
                params.put("payment_mode", modeofpayment);//
                params.put("cardmasked", cardmaskeds);//
                params.put("hash", hashs);//
                params.put("transaction_id", transaction_ids);//
                params.put("error_desc", error_descs);//
                return params;

            }


        };


        RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }
    /*  public void successtrans() {
          final LayoutInflater inflater = (LayoutInflater) Scan.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          final Dialog successonly = new Dialog(Scan.this);
          successonly.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
          successonly.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
          successonly.setContentView(inflater.inflate(R.layout.popupfortransactionsuccess, null));
          successonly.setCancelable(true);
          successonly.show();
      }*/
    public void onBackPressed() {
        //Execute your code here

        Intent intenty = new Intent(Scan.this, Upitransaction.class);
        startActivity(intenty);

    }

  /*  public void failuretrans() {
        final LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Dialog failureonly = new Dialog(this);
        failureonly.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        failureonly.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        failureonly.setContentView(inflater.inflate(R.layout.popupfortransactionfailure, null));
        failureonly.setCancelable(true);
        Button close = failureonly.findViewById(R.id.button60);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        failureonly.show();
    }*/


    public void amountlimitation() {
        SharedPreferences sharedPreferences = Scan.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/upi_limit",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("*******************************************");
                        System.out.println(response);
                        try {
                            JSONObject upiamountlimit = new JSONObject(response);
                            System.out.println(upiamountlimit);
                            helloss = (upiamountlimit.getString("max_amt"));
                            System.out.println(helloss);
                            helloss1 = (upiamountlimit.getString("min_amt"));
                            System.out.println(helloss1);
                          /*  String print = editTextTextPersonName2.getText().toString();
                            System.out.println(print);*/


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Scan.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };


        RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    public void itemClicked() {
        //code to check if this checkbox is checked!
        //CheckBox checkBox = (CheckBox)v;

        SharedPreferences sharedPreferences = Scan.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;



        String get_entered_amt = editTextTextPersonName2.getText().toString();
        String convert_entered_amt = new DecimalFormat("##.00").format(Double.parseDouble(get_entered_amt));

        if (checkBox11.isChecked()) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/save_upi ",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            System.out.println("*******************************************");
                            System.out.println(response);
                            try {
                                JSONObject forsaveupis = new JSONObject(response);
                                System.out.println(forsaveupis);
                                forsaveupi = forsaveupis.getString("statuss");
                                System.out.println(forsaveupi);
                                System.out.println(convert_entered_amt);
                               // Integer result2 = Integer.parseInt(print);
                                if (forsaveupi.equals("Success")) {
                                    PaymentParams pgPaymentParams = new PaymentParams();
                                    pgPaymentParams.setAPiKey("fe4023d4-db2d-49c4-902b-1e24dd602a48");
                                    pgPaymentParams.setAmount(Finalamount);
                                    pgPaymentParams.setEmail(mail);
                                    pgPaymentParams.setName(name);
                                    pgPaymentParams.setPhone(phone);
                                    pgPaymentParams.setOrderId(orderidgiven);
                                    pgPaymentParams.setCurrency("INR");
                                    pgPaymentParams.setDescription("UPI");
                                    pgPaymentParams.setCity("UPI");
                                    pgPaymentParams.setState("upi");
                                    pgPaymentParams.setAddressLine1(myDate);
                                    pgPaymentParams.setAddressLine2(convert_entered_amt);
                                    pgPaymentParams.setZipCode("123456");
                                    pgPaymentParams.setCountry(sharedpurposename);
                                    pgPaymentParams.setReturnUrl(SERVER+"/api/save_PG_Tran_Response");
                                    pgPaymentParams.setMode("C");
                                    pgPaymentParams.setUdf1(setUdf1only);
                                    pgPaymentParams.setUdf2("UPI-AT");
                                    pgPaymentParams.setUdf3(nameonlyies);
                                    pgPaymentParams.setUdf4(upiidonly);
                                    pgPaymentParams.setUdf5("Udf5");
                                    PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, Scan.this);
                                    pgPaymentInitialzer.initiatePaymentProcess();
                                } else {
                                    PaymentParams pgPaymentParams = new PaymentParams();
                                    pgPaymentParams.setAPiKey("fe4023d4-db2d-49c4-902b-1e24dd602a48");
                                    pgPaymentParams.setAmount(Finalamount);
                                    pgPaymentParams.setEmail(mail);
                                    pgPaymentParams.setName(name);
                                    pgPaymentParams.setPhone(phone);
                                    pgPaymentParams.setOrderId(orderidgiven);
                                    pgPaymentParams.setCurrency("INR");
                                    pgPaymentParams.setDescription("UPI");
                                    pgPaymentParams.setCity("UPI");
                                    pgPaymentParams.setState("upi");
                                    pgPaymentParams.setAddressLine1(myDate);
                                    pgPaymentParams.setAddressLine2(convert_entered_amt);
                                    pgPaymentParams.setZipCode("123456");
                                    pgPaymentParams.setCountry(sharedpurposename);
                                    pgPaymentParams.setReturnUrl(SERVER+"/api/save_PG_Tran_Response");
                                    pgPaymentParams.setMode("C");
                                    pgPaymentParams.setUdf1(setUdf1only);
                                    pgPaymentParams.setUdf2("UPI-AT");
                                    pgPaymentParams.setUdf3(nameonlyies);
                                    pgPaymentParams.setUdf4(upiidonly);
                                    pgPaymentParams.setUdf5("Udf5");
                                    PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, Scan.this);
                                    pgPaymentInitialzer.initiatePaymentProcess();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ServerError) {
                        message = "";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ParseError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection Timedout......please check your internet connection";
                    }
                    if (!message.isEmpty()) {
                        Toast.makeText(Scan.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }) {


                //This is for Headers If You Needed
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String T = "";
                    Map<String, String> params = new HashMap<>();


                    // params.put("Content-Type", "application/json; charset=UTF-8");
                    try {
                        JSONObject response1 = new JSONObject(token200);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        // do your work with response object
                        T = response1.getString("token");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("44444444444444444444444444");
                    System.out.println(token200);
                    ///check t!blank

                    String TokenS = "token " + T;
                    System.out.println(T);

                    params.put("Authorization", TokenS);

                    return params;
                }

                protected Map<String, String> getParams() {
                    String upiidonly = getIntent().getExtras().getString("VPA");
                    String onlynamecome = getIntent().getExtras().getString("nameonlyies");
                    Map<String, String> params = new HashMap<>();
                    params.put("sBeni_Upi_Id", upiidonly);
                    params.put("sName", onlynamecome);

                    return params;

                }


            };


            RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);


        } else {

            //Integer result2 = Integer.parseInt(print);
            PaymentParams pgPaymentParams = new PaymentParams();
            pgPaymentParams.setAPiKey("fe4023d4-db2d-49c4-902b-1e24dd602a48");
            pgPaymentParams.setAmount(Finalamount);
            pgPaymentParams.setEmail(mail);
            pgPaymentParams.setName(name);
            pgPaymentParams.setPhone(phone);
            pgPaymentParams.setOrderId(orderidgiven);
            pgPaymentParams.setCurrency("INR");
            pgPaymentParams.setDescription("UPI");
            pgPaymentParams.setCity("UPI");
            pgPaymentParams.setState("upi");
            pgPaymentParams.setAddressLine1(myDate);
            pgPaymentParams.setAddressLine2(convert_entered_amt);
            pgPaymentParams.setZipCode("123456");
            pgPaymentParams.setCountry(sharedpurposename);
            pgPaymentParams.setReturnUrl(SERVER+"/api/save_PG_Tran_Response");
            pgPaymentParams.setMode("C");
            pgPaymentParams.setUdf1(setUdf1only);
            pgPaymentParams.setUdf2("UPI-AT");
            pgPaymentParams.setUdf3(nameonlyies);
            pgPaymentParams.setUdf4(upiidonly);
            pgPaymentParams.setUdf5("Udf5");
            PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, Scan.this);
            pgPaymentInitialzer.initiatePaymentProcess();
            progressbar26.setVisibility(View.GONE);
        }
    }
    public void checkupiidpresent() {
        SharedPreferences sharedPreferences = Scan.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/check_upi_exist ",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("*******************************************");
                        System.out.println(response);
                        try {
                            JSONObject checkingupis = new JSONObject(response);
                            System.out.println(checkingupis);
                            forUPIchecking1 = (checkingupis.getString("upi_id_already_exist"));
                            System.out.println(forUPIchecking1);
                            if (forUPIchecking1.equals("No")) {
                                checkBox11.setVisibility(View.GONE);
                            } else {
                                checkBox11.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Scan.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {


            //This is for Headers If You Needed
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }

            protected Map<String, String> getParams() {
                String upiidonly = getIntent().getExtras().getString("VPA");
                Map<String, String> params = new HashMap<>();
                params.put("sBeni_Upi_Id", upiidonly);
                return params;

            }


        };


        RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
    @Override
    public void onResume() {
        super.onResume();
        gettranid();
        sharedpurposename="";
        editTextTextPersonName2.setText("");
        button57.setEnabled(true);
        button57.setBackgroundResource(R.drawable.roundedbutton);

    }

    public void purposeofmoney() {
        try {
            paylist.clear();
            payid.clear();
            StringRequest stringRequest1 = new StringRequest(Request.Method.GET, SERVER + "/api/Purpose",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //beatslist.add("Select Beats");
                            try {

                                JSONArray jsonArray = new JSONArray(response);
                                if (jsonArray != null) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        if (i == 0) {
                                            paylist.add("----Select Purpose Of Payment----");
                                            payid.add("0");
                                        }
                                        JSONObject dataObject = jsonArray.optJSONObject(i);
                                        String purp = dataObject.optString("purpose");
                                        String purpid = dataObject.optString("id");
                                        //   SharedPreferences.Editor ediclear
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //
                                        //tor = spinnerpreference.edit();

                                        // editor.putString("casa_id",ids);
                                        // editor.apply();
                                        // System.out.println("This");
                                        // System.out.println(ids);
                                        paylist.add(purp);
                                        payid.add(purpid);
                                    }
                                    // Creating adapter for spinner
                                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Scan.this, R.layout.spinner_item, paylist);
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    spinner4.setAdapter(dataAdapter);


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String message = "";

                            if (error instanceof NetworkError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ServerError) {
                                Log.e("stackcheck1111", "3" + error.toString());
                                NetworkResponse response = error.networkResponse;
                                if (response != null) {
                                    Log.d("qqqqqqqqqqq", String.valueOf(response.statusCode));
                                    int statusCode = response.statusCode;
                                    if (statusCode == 404) {
                                        message = "No ISP Available in this Company";
                                        Log.d("qqqqqq", " " + response.data);

                                        //ShowAlert("Server is down. Please try after some time!");
                                        //json = new String(response.data);
                                        //json = trimMessage(json, "status");
                                        // if (json != null)
                                        //    message = json;


                                    } else {
                                        message = "The server could not be found. Please try again after some time!!";

                                    }
                                } else {
                                    message = "The server could not be found. Please try again after some time!!";

                                }
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                            }
                            Log.e("eeeee", "onErrorResponse: " + error.toString());
                            if (!message.isEmpty()) {
                                Toast.makeText(Scan.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            RequestQueue requestQueue1 = Volley.newRequestQueue(Scan.this);
            stringRequest1.setShouldCache(false);
            requestQueue1.add(stringRequest1);
        } catch (Exception e) {
        }
        spinner4 = findViewById(R.id.spinner4);


/*            spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                    System.out.println(spinner4.getSelectedItem().toString());
                    onselect = spinner4.getSelectedItem().toString();

*//*
                                                    monthh[0] =spinner.getSelectedItem().toString();
*//*
                    System.out.println(onselect);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });*/
        sharedpurposename = "";
        spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    sharedpurposename="";


                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

                        //save this value in shared pref
                        String paynamelist = paylist.get(position);
                        sharedpurposename = paynamelist;
                      /*  SharedPreferences.Editor editor = purposepreference.edit();
                        System.out.println("Hi purpose id is here");
                        System.out.println(paynamelist);
                        editor.putString("purpose",paynamelist);
                        editor.apply();
                        */

                    }
                } catch (Exception e) {
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void forgetuserdetails() {
        SharedPreferences sharedPreferences = Scan.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            name = profileresponse.getString("name");
                            System.out.println(name);
                            mail = profileresponse.getString("email");
                            System.out.println(mail);
                            phone = profileresponse.getString("phone");
                            System.out.println(phone);
                            setUdf1only = profileresponse.getString("id");
                            System.out.println(setUdf1only);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Scan.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    public void gettranid() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/order_id",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respontran = new JSONObject(response);
                            orderidgiven = respontran.getString("transactionid");
                            System.out.println("this is the trrrrrrrrrrrrrrrrrrrrrransaaaaaaaaaaaaaction id");
                            System.out.println(orderidgiven);
                            /* fullid = "NEXEB"+getpgtran;*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Scan.this, "", Toast.LENGTH_SHORT).show();

                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    public void percentageUPI() {

        SharedPreferences sharedPreferences = Scan.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/percentage",
                response -> {
                    try {
                        JSONObject profileresponse = new JSONObject(response);
                        abc = profileresponse.getString("upi");
                        System.out.println(abc);

                      /*  cardpercentage121 =abc;
                        System.out.println(cardpercentage121);
*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Scan.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Scan.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    public boolean validate1() {
        String print = editTextTextPersonName2.getText().toString();
        System.out.println(print);
/*
        Integer i = Integer.valueOf(print);
*/

        if (print==null||print.length() == 0 ||print.equals("") || print.isEmpty()){
            Toast.makeText(Scan.this, "Please fill the amount ", Toast.LENGTH_LONG).show();
            return false;
        }
        if (sharedpurposename.isEmpty() || sharedpurposename.length() == 0 || sharedpurposename == null|| sharedpurposename==("----Select Purpose Of Payment----")||sharedpurposename.equals("")||sharedpurposename.equals("----Select Purpose Of Payment----"))
        {
            Toast.makeText(Scan.this, "Please Select Purpose of Payment ", Toast.LENGTH_LONG).show();
            return false;

        }

      /*  if (print <= Integer.parseInt(helloss) && print >= Integer.parseInt(helloss1)) {
            itemClicked();
        }*/
        return true;
                      /*  String print = editTextTextPersonName2.getText().toString();
                        System.out.println(print);*/

                      /*  Integer i = Integer.valueOf(print);
                        System.out.println(i);

                       */
    }
}

