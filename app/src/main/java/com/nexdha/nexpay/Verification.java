package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Verification extends AppCompatActivity {
    public static final String MyPREFERENCES = "MyPrefs";
    SharedPreferences sharedpreferences;

    public static final String WELCOMEPREFERENCE = "welpref";
    String timeRestriction;
    String token;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SharedPreferences welcomepreference = Verification.this.getSharedPreferences("welpref", Context.MODE_PRIVATE);
        final String welcome200 = welcomepreference.getString("verify", "");


        sharedpreferences = Verification.this.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        welcomepreference = Verification.this.getSharedPreferences(WELCOMEPREFERENCE, 0);

        token = sharedpreferences.getString("token", null);
        if (token != null) {
            System.out.println("token: " + token);
            SharedPreferences sharedPreferences = Verification.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
            final String token1 = sharedPreferences.getString("token", "");
            @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/counter_check",
                    response -> {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println(response);
                            String getResponse = jsonObject.getString("detail");

                            if (getResponse.equals("1")) {
                                String allowUser = jsonObject.getString("allow_user");
                                if (allowUser.equals("0")) {
                                    final LayoutInflater layoutInflaterBlock = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    final Dialog blockUserDialog = new Dialog(this);
                                    blockUserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                    blockUserDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                    blockUserDialog.setContentView(layoutInflaterBlock.inflate(R.layout.block_user_layout, null));
                                    int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                                    int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                    blockUserDialog.getWindow().setLayout(width, height);
                                    blockUserDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                    blockUserDialog.setCancelable(false);
                                    blockUserDialog.show();
                                } else {
                                    startActivity(new Intent(this, security.class));
                                    finish();
                                }
                            } else if (getResponse.equals("Invalid token")) {
                                final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog sessionDialog = new Dialog(this);
                                sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                                int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                sessionDialog.getWindow().setLayout(width, height);
                                sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                sessionDialog.setCancelable(false);
                                sessionDialog.show();
                                AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                                logoutSession.setOnClickListener(view -> {
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.clear();
                                    editor.apply();
                                    startActivity(new Intent(this, MainActivity.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                });
                            } else {
                                final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog sessionDialog = new Dialog(this);
                                sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                                int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                sessionDialog.getWindow().setLayout(width, height);
                                sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                sessionDialog.setCancelable(false);
                                sessionDialog.show();
                                AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                                logoutSession.setOnClickListener(view -> {
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.clear();
                                    editor.apply();
                                    startActivity(new Intent(this, MainActivity.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }, error -> {
                System.out.println(error instanceof ClientError);


                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    String T = "";
                    Map<String, String> params = new HashMap<>();
                    try {
                        JSONObject response1 = new JSONObject(token1);
                        T = response1.getString("token");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String TokenS = T;

                    params.put("auth", TokenS);

                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);


        } else {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }
        setContentView(R.layout.activity_verification);
    }
    public void resendotp(){
        SharedPreferences sharedPreferences = Verification.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserid",
                response -> {
                    System.out.println("***************openotppage*******************");
                    System.out.println(response);
                    try {
                        JSONObject otpresponse = new JSONObject(response);
                        timeRestriction = otpresponse.getString("status");

                        if (timeRestriction.equals("otp have time restriction")) {
                            Toast.makeText(Verification.this, "Next OTP will sent after 60 seconds", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(Verification.this, "OTP sent", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {
            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(Verification.this, message, Toast.LENGTH_SHORT).show();
            }
        })

        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Verification.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }


}
