package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.easebuzz.payment.kit.PWECouponsActivity;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.nexdha.nexpay.activity.PayUGatewayActivity;
import com.nexdha.nexpay.activity.RentTransactionDetails;
import com.nexdha.nexpay.activity.UpiTransactionDetails;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import datamodels.PWEStaticDataModel;

public class Dashboard extends AppCompatActivity implements LocationListener {

    RelativeLayout filter1, filter2, filter3;
    ConstraintLayout mobileView, dthView, electricityView, waterView, bookView, fastTagView, internetView, educationView;
    CardView mobileView1, dthView1, electricityView1, waterView1, bookView1, fastTagView1, internetView1, educationView1;
    TextView getTotalTransaction;
    public static String verify = null;
    public static String notificationclass_id = null;
    public static String notificationclass_text = null;
    private Button button27, button54, payRentButton;
    Button button3;
    public Button button38;
    public ImageView upilayoutii;
    private ImageView imageview2;
    ImageView imageview63;
    private long pressedTime;
    private ImageView imageview5, imagelogo;
    PopupWindow popupWindow;
    LinearLayout linearedit, linear_layout_t_five, linearLayout17;
    TextView textView110;
    Button cancelBtn, submitBtn;
    TextView dashboardusername, eta, etaoneday, todaytransapi, noticeapitext, eta_t_five_text;
    public static String etaOne;
    private LinearLayout accountclick;
    private ConstraintLayout historyclick, addBeneficiary;
    private CardView importantpopup;
    private CardView noticelayout;
    Space noticespace, impmsgspace;
    CheckBox donotshowagain;
    public static String onedayonly = null;
    HorizontalScrollView horizontalScrollView;
    public static final String MYCHECKPREFERENCES = "MyCheckPrefs";
    public static String kycverification = null;
    public static String t1;
    public static String amexPercant;
    public static String amountlimit = null;
    public static String app_image_name = null;
    public static String locationverification = null;
    public static String pay_details_email = null;
    public static String pay_details_name = null;
    public static String responsephonenumber = null;
    public static String userid = null;
    TextView textView112;
    TextView textview296;
    ImageView image_view_tick;
    int number_of_entries = 0;
    TextView textView7;
    ProgressBar progresbar27;
   public ReviewManager reviewManager;
   public ReviewInfo reviewInfo = null;
   public String  noticeapistring1;

    /*
    ImageView editimage;
*/


    String final_settlement_amount = "";
String hello ="";
String zolo ="";
String update_or_submitbilldate ="";
    SharedPreferences sharedcheckPreferences;
 public  String verifyfraudtruefalse,checkboxresult,dashlat,dashlong,dashfinalloc,dashboardlocationupdate,popupstatus,responsemessagefromholypop,responsedatefromholypop,noticeapistring;
/*  int mDay;
    int year;
   int month;*/
ArrayList<String> arrlist= new ArrayList<String>();

    LocationManager locationManagerdashboard;
    double locationlatitudedashboard,locationlongitudedashboard;
    public static String nextpageupiid = null;
    ImageView imageview49;
    Space heightadjustspace;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;

    ImageView onlineimage;

    Space onlineimagespace;
    ConstraintLayout imagelayout;
    ConstraintLayout main_bg;
    ConstraintLayout escrow;
    String Transaction_tot_amount = null;
    Spinner spinner;
    LinearLayout alreadybill;
    ConstraintLayout billcycleconstraint;

    LayoutInflater inflater ;
    Dialog loadingDialog ;
    ConstraintLayout t_constraint;

    Spinner amount_list_spinner;
    ArrayList<String> amount_list;

    Button button64;

    String get_t_amount = "";
    String eb_t_order_id = "";
    String t_four_three_eta="";
    String eb_hash_new = "";
    String hello123 ="";
    String response_message_from_gateway_dash = "";
    String tx_id_from_gateway_dash = "";

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        inflater = (LayoutInflater) Dashboard.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(Dashboard.this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.inflate_layout, null));
        loadingDialog.setCancelable(false);
        loadingDialog.show();


        getTotalTransactionAmount();
        sme_api1();
        initiate_api1();
        noticeapi();
        activity_log_dashboard();

        sessionCheck();
        gettransactiontotal();

       /*
        init();
*/


        //-------- location -------//
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //Toast.makeText(Dashboard.this,"Please 222 Location",Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);

        }

        setContentView(R.layout.activity_pageone);
        escrow = findViewById(R.id.constraintLayout7);
        progresbar27 = findViewById(R.id.progressBar27);
        progresbar27.setVisibility(View.GONE);
        amount_list = new ArrayList<>(Arrays.asList("Select Amount","75k","1 Lakh","1.5 Lakhs","2 Lakhs","2.5 Lakhs", "3 Lakhs"));
       /* amount_list.add("1 Lakh");
        amount_list.add("1.5 Lakhs");
        amount_list.add("2 Lakhs");
        amount_list.add("2.5 Lakhs");
        amount_list.add("3 Lakhs");*/

        amount_list_spinner = findViewById(R.id.spinner8);

        amount_list_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {


                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

//save this value in shared pref
                        String paynamelist = amount_list.get(position);
                        if (paynamelist.equals("Select Amount")){
                            get_t_amount = "0";
                        }else if (paynamelist.equals("75k")) {
                            get_t_amount = "75000";
                        } else if (paynamelist.equals("1 Lakh")) {
                            get_t_amount = "100000";
                        } else if (paynamelist.equals("1.5 Lakhs")) {
                            get_t_amount = "150000";
                        } else if (paynamelist.equals("2 Lakhs")) {
                            get_t_amount = "200000";
                        } else if (paynamelist.equals("2.5 Lakhs")) {
                            get_t_amount = "250000";
                        } else if (paynamelist.equals("3 Lakhs")) {
                            get_t_amount = "300000";
                        }


                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mobileView = (ConstraintLayout) findViewById(R.id.mobile_view);
        dthView = (ConstraintLayout) findViewById(R.id.dth_view);
        electricityView = (ConstraintLayout) findViewById(R.id.electricity_view);
        waterView = (ConstraintLayout) findViewById(R.id.water_view);
        bookView = (ConstraintLayout) findViewById(R.id.book_view);
        fastTagView = findViewById(R.id.fast_tag_view);
        internetView = findViewById(R.id.internet_view);
        educationView = findViewById(R.id.education_view);

        mobileView1 = findViewById(R.id.mobile_view_1);
        dthView1 = findViewById(R.id.dth_view_1);
        electricityView1 = findViewById(R.id.electricity_view_1);
        waterView1 = findViewById(R.id.water_view_1);
        bookView1 = findViewById(R.id.book_view_1);
        fastTagView1 = findViewById(R.id.fast_tag_view_1);
        internetView1 = findViewById(R.id.internet_view_1);
        educationView1 = findViewById(R.id.education_view_1);

        horizontalScrollView = findViewById(R.id.horizontalScrollView);


        mobileView1.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        dthView1.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        electricityView1.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        waterView1.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        bookView1.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        fastTagView1.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        internetView1.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        educationView1.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());

        mobileView.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        dthView.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        electricityView.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        waterView.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        bookView.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        fastTagView.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        internetView.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());
        educationView.setOnClickListener(view -> Snackbar.make(main_bg, "Coming soon...", Snackbar.LENGTH_LONG).setDuration(5000).show());

        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("token:  " + token1);


        textView7 = findViewById(R.id.textView7);

        textView7.setOnClickListener(v -> {
            final LayoutInflater inflater = (LayoutInflater) Dashboard.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final Dialog settingsDialog = new Dialog(Dashboard.this);
            settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            settingsDialog.setContentView(inflater.inflate(R.layout.escrow_layout, null));
            settingsDialog.show();
            Button button_close = settingsDialog.findViewById(R.id.button66);
            button_close.setOnClickListener(v1 -> settingsDialog.dismiss());
        });
        t_constraint = findViewById(R.id.t_linear);
        t_constraint.setOnClickListener(v -> {

            final LayoutInflater inflater = (LayoutInflater)Dashboard.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final Dialog settingsDialog = new Dialog(Dashboard.this);
            settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            settingsDialog.setContentView(inflater.inflate(R.layout.escrow_layout, null));
            settingsDialog.show();
            Button button_close = settingsDialog.findViewById(R.id.button66);
            button_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    settingsDialog.dismiss();
                }
            });
        });
        addBeneficiary = findViewById(R.id.add_beneficiary_button);
        addBeneficiary.setOnClickListener(v -> {
            Intent intent = new Intent(Dashboard.this, AddBeneficiary.class);
            startActivity(intent);
        });

        amount_list_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {


                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

//save this value in shared pref
                        String final_tran_amout = amount_list.get(position);


                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        upilayoutii =findViewById(R.id.imageView70);
        linear_layout_t_five = findViewById(R.id.llt);

        button64 = findViewById(R.id.button64);
        button64.setOnClickListener(v -> {
            progresbar27.setVisibility(View.VISIBLE);
            if (!fourty_three()){
                progresbar27.setVisibility(View.GONE);
                return;
            }
            easebuzz_hash_generation();


        });

        textview296 = findViewById(R.id.textView296);
        textview296.setVisibility(View.GONE);
        textview296.setOnClickListener(view -> {
            if (verifyfraudtruefalse.equals("true")) {
                new MaterialStyledDialog.Builder(Dashboard.this)
                        .setTitle("Account Blocked")
                        .setDescription("Your account is being blocked and you are not allowed to do any transactions or proceed to any page in the app. For furthur details contact support@nexdha.com / 8667451930.")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.cancel)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)

                        .show();
            } else {
                getLocationdashboard();
                locationupdateprofile();
                Intent intent = new Intent(Dashboard.this, grantreadandwritepermission.class);
                startActivity(intent);
            }
        });


//

        arrlist.clear();
        final String[] monthh = new String[1];
                                         /*   String[] monthName = { "January", "February", "March", "April", "May", "June", "July",
                                                    "August", "September", "October", "November", "December" };*/
        SimpleDateFormat fmt = new SimpleDateFormat("dd");
        Calendar cal = Calendar.getInstance();
        System.out.println(cal);
        System.out.println(cal.get(YEAR));
        System.out.println(cal.get(MONTH));
        //cal.clear();

                                      /*      Calendar c = Calendar.getInstance();
                                            int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
                                            System.out.println(monthMaxDays);*/
/*
                                            cal.set(year, month , 1);
*/
        int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        System.out.println(daysInMonth);

        //yourtextview.setText(Html.fromHtml(text));
        arrlist.add("Select");
        for (int i = 1; i < daysInMonth + 1; i++) {
                                                /*array11 = daysInMonth[i];
                                                array11.add(daysInMonth);*/
            arrlist.add(String.valueOf(i));
                                                /*System.out.println(arrlist);
                                                System.out.println(fmt.format(cal.getTime()));
                                                cal.add(Calendar.DAY_OF_MONTH, 1);*/
        }

        System.out.println(cal);
        System.out.println(arrlist);


        //


        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        main_bg = findViewById(R.id.main_layout);

        // diwalibg();
        /*button38 =findViewById(R.id.button38);*/
        heightadjustspace = findViewById(R.id.heightadjustspace);
        imageview49 = (ImageView) findViewById(R.id.imageView49);
        linearedit =findViewById(R.id.linearedit);
        textView112 = findViewById(R.id.textView112);
        alreadybill = findViewById(R.id.alreadybill);
        onlineimage = findViewById(R.id.onlineimage);
        textView110 = findViewById(R.id.textView110);
        textView110.setVisibility(View.GONE);
        button54 = findViewById(R.id.button54);
        button54.setVisibility(View.GONE);
       /* billcycleconstraint = findViewById(R.id.billcycleconstraint);
        billcycleconstraint.setVisibility(View.GONE);*/
        String imageUrl = "https://www.nexdha.com/appimage/" + noticeapistring1;
        //Loading image using Picasso
        // Picasso.get().load(imageUrl).into(onlineimage);
        Glide.with(this)

                .load("https://www.nexdha.com/appimage/appdisplay.png")
                .into(onlineimage);


      /*  LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        textView112.setLayoutParams(params);*/

        // imageblink();
        onlineimagespace = findViewById(R.id.imagelayoutspace);
        imagelayout = findViewById(R.id.imagelayout);

        sharedcheckPreferences = getSharedPreferences(MYCHECKPREFERENCES, Context.MODE_PRIVATE);
        profileinfo();
        getETAdashboard();
        getonedayETAdashboard();
        //  todaytransactionapi();
        importantpopup = findViewById(R.id.importantmessage);
        impmsgspace = findViewById(R.id.impmsgspace);
        getnoticemessage();
        noticelayout = findViewById(R.id.notice);
        noticespace = findViewById(R.id.noticespace);
        todaytransapi = findViewById(R.id.textView137);
        noticeapitext = findViewById(R.id.textView206);
        historyclick = (ConstraintLayout) findViewById(R.id.historyclick);
        historyclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (verifyfraudtruefalse.equals("true")) {
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Account Blocked")
                            .setDescription("Your account is being blocked and you are not allowed to do any transactions or proceed to any page in the app. For furthur details contact support@nexdha.com / 8667451930.")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.cancel)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .show();
                    return;
                } else {
                    showBottomSheetTDialog();
                    getLocationdashboard();
                    locationupdateprofile();
                    Intent intent = new Intent(Dashboard.this, new_design_transaction.class);
                    // startActivity(intent);
                }

            }

            private void showBottomSheetTDialog() {
                final View dialogViewDashboard = getLayoutInflater().inflate(R.layout.transaction_history_filter_bottom_sheet, null);
                filter1 = dialogViewDashboard.findViewById(R.id.filt_1);
                filter2 = dialogViewDashboard.findViewById(R.id.filt_2);
                filter3 = dialogViewDashboard.findViewById(R.id.filt_3);

                if (verify != null && verify.equals("Yes")) {
                    filter2.setVisibility(View.VISIBLE);
                } else if (verify != null && verify.equals("No")) {
                    filter2.setVisibility(View.GONE);
                }


                filter1.setOnClickListener(view -> {
                    Intent intent = new Intent(Dashboard.this, new_design_transaction.class);
                    startActivity(intent);
                });
                filter2.setOnClickListener(view -> {
                    Intent intent = new Intent(Dashboard.this, UpiTransactionDetails.class);
                    startActivity(intent);
                });

                filter3.setOnClickListener(v -> {
                    Intent intent = new Intent(Dashboard.this, RentTransactionDetails.class);
                    startActivity(intent);
                });
                BottomSheetDialog dialog = new BottomSheetDialog(Dashboard.this);
                dialog.setContentView(dialogViewDashboard);
                dialog.show();
                dialog.setOnDismissListener(dialog1 -> onResume());
            }
        });
        upilayoutii = findViewById(R.id.imageView70);
        upilayoutii.setOnClickListener(view -> {
       /* Intent intent = new Intent(Dashboard.this, Upitransaction.class);
        startActivity(intent);*/
            check_upi_availability();
        });
        button54 = findViewById(R.id.button54);
        button54.setOnClickListener(view -> {
            update_or_submitbilldate = "save";
            edit_imagee();
        });
        linearedit.setOnClickListener(view -> {
            update_or_submitbilldate ="update";
            edit_imagee();
        });




     /*   accountclick = (LinearLayout)findViewById(R.id.imageView2);
        accountclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (verifyfraudtruefalse.equals("true")){
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Account Blocked")
                            .setDescription("Your account is being blocked and you are not allowed to do any transactions or proceed to any page in the app. For furthur details contact support@nexdha.com / 8667451930.")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.cancel)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)

                            .show();
                    return;
                }else{
                    getLocationdashboard();
                    locationupdateprofile();
                    Intent intent = new Intent(Dashboard.this, Moreoptions.class);
                    startActivity(intent);
                }

            }
        });*/
        dashboardusername = (TextView)findViewById(R.id.textView5);
        eta = (TextView)findViewById(R.id.textView133);
        etaoneday = (TextView)findViewById(R.id.textView94);
        eta_t_five_text = findViewById(R.id.textView263);


        imageview63 = findViewById(R.id.imageView63);
        imageview63.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (verifyfraudtruefalse.equals("true")){
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Account Blocked")
                            .setDescription("Your account is being blocked and you are not allowed to do any transactions or proceed to any page in the app. For furthur details contact support@nexdha.com / 8667451930.")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.cancel)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .show();
                    return;
                }else{
                    getLocationdashboard();
                    locationupdateprofile();
                    openPagefive();
                }

            }
        });

        button3 = findViewById(R.id.button3);

        button3.setOnClickListener(v -> {
            getLocationdashboard();
            locationupdateprofile();
            if (verifyfraudtruefalse.equals("true")) {
                new MaterialStyledDialog.Builder(Dashboard.this)
                        .setTitle("Account Blocked")
                        .setDescription("Your account is being blocked and you are not allowed to do any transactions or proceed to any page in the app. For furthur details contact support@nexdha.com / 8667451930.")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.cancel)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .show();
                return;
            } else {
                if (Transaction_tot_amount != null && !kycverification.equals("V")) {
                    //Toast.makeText(Dashboard.this,"KYC Error",Toast.LENGTH_SHORT).show();
                    String kyc_msg = "";
                    switch (kycverification) {
                        case "Ret":
                            kyc_msg = "We request you to Retry your KYC. There were some issues verifying your KYC so please Retry your KYC.";
                            break;
                        case "Rej":
                            kyc_msg = "Your KYC is Rejected. You are not allowed to proceed with the transaction any further";
                            break;
                        case "N.V":
                            kyc_msg = "You have not submitted your KYC. Do provide your KYC in YOUR ACCOUNT page before proceeding with the Transactions";
                            break;
                        case "P":
                            kyc_msg = "We have not yet verified your KYC. Please wait till we verify your KYC.";
                            break;
                    }
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("KYC")
                            .setDescription(kyc_msg)
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warning1)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(true)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Got it")
                            .onNegative((dialog, which) -> {

                            })

                            .show();
                } else {
                    final SharedPreferences sharedcheckPreferences = Dashboard.this.getSharedPreferences("MyCheckPrefs", Context.MODE_PRIVATE);
                    final String statuscheck = sharedcheckPreferences.getString("status", "");
                    System.out.println("MMMMMMMMMMMMMMstatusMMMMMMMMMMMMMMMM");
                    System.out.println(statuscheck);
                    final String checkstatus200 = statuscheck;
          /*  Intent intent = new Intent ( Dashboard.this, Payment.class);
            startActivity(intent);
            */
                    if (checkstatus200.equals("checked")) {
                        Intent intent = new Intent(Dashboard.this, new_payment.class);//typeoftransaction
                        startActivity(intent);
                    } else if (checkstatus200.equals("NOT checked") || checkstatus200.equals("") || checkstatus200.equals(null)) {
                        checkboxmethod();
                    }


                }
            }


        });


        payRentButton = findViewById(R.id.pay_rent_button);
        payRentButton.setOnClickListener(v -> {
            if (verifyfraudtruefalse.equals("true")) {
                new MaterialStyledDialog.Builder(Dashboard.this)
                        .setTitle("Account Blocked")
                        .setDescription("Your account is being blocked and you are not allowed to do any transactions or proceed to any page in the app. For furthur details contact support@nexdha.com / 8667451930.")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.cancel)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .show();
                return;
            } else {
                if (kycverification.equals("V")) {
                    startActivity(new Intent(Dashboard.this, PayUGatewayActivity.class));
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("Rental")
                            .setMessage("Please submit KYC to activate Rental Payment for your Account.")
                            .setPositiveButton("Close", null).create().show();
                }

            }

        });

        button27 = findViewById(R.id.button27);
        button27.setOnClickListener(view -> {

            if (verifyfraudtruefalse.equals("true")) {
                new MaterialStyledDialog.Builder(Dashboard.this)
                        .setTitle("Account Blocked")
                        .setDescription("Your account is being blocked and you are not allowed to do any transactions or proceed to any page in the app. For furthur details contact support@nexdha.com / 8667451930.")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.cancel)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)

                        .show();
                return;
            } else {
                final View dialogViewdashboard = getLayoutInflater().inflate(R.layout.bottom_sheet_dashboard, null);
                Button enterupiid = dialogViewdashboard.findViewById(R.id.button28);
                Button scanqrcode = dialogViewdashboard.findViewById(R.id.button29);
                enterupiid.setOnClickListener(view1 -> {
                    Intent intent = new Intent(Dashboard.this, Enterupi_id.class);
                    startActivity(intent);
                });
                scanqrcode.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Intent intent = new Intent ( Dashboard.this, commonscreenupi.class);
                        // startActivity(intent);

                        IntentIntegrator integrator = new IntentIntegrator(Dashboard.this);
                        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);  // optional
                        integrator.setOrientationLocked(false);                         // allow barcode scanner in potrait mode
                        integrator.initiateScan();


                    }
                });
                BottomSheetDialog dialog = new BottomSheetDialog(Dashboard.this);
                dialog.setContentView(dialogViewdashboard);
                dialog.show();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        onResume();
                    }
                });
            }
        });

        // FloatingActionButton faq = findViewById(R.id.floatingActionButton2);
        // faq.setOnClickListener(new View.OnClickListener() {
        //     @Override
        //    public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
        //         Intent intent = new Intent(getApplicationContext(), FAQ.class);
        //        startActivity(intent);


        //   }
        //   });


    }


    private void sessionCheck() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/counter_check",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println(response);
                        String getResponse = jsonObject.getString("detail");

                        if (getResponse.equals("1")) {
                            String allowUser = jsonObject.getString("allow_user");
                            if (allowUser.equals("0")) {
                                final LayoutInflater layoutInflaterBlock = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog blockUserDialog = new Dialog(this);
                                blockUserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                blockUserDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                blockUserDialog.setContentView(layoutInflaterBlock.inflate(R.layout.block_user_layout, null));
                                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                blockUserDialog.getWindow().setLayout(width, height);
                                blockUserDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                blockUserDialog.setCancelable(false);
                                blockUserDialog.show();
                            } else {
                                // Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            }
                        } else if (getResponse.equals("Invalid token")) {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            });
                        } else {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("auth", TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    //////////////////////////////////////////////////////in app review
  /*  private void getReviewInfo() {
        reviewManager = ReviewManagerFactory.create(getApplicationContext());
        Task<ReviewInfo> manager = reviewManager.requestReviewFlow();
        manager.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                reviewInfo = task.getResult();
            } else {
                Toast.makeText(getApplicationContext(), "In App ReviewFlow failed to start", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void startReviewFlow() {
        if (reviewInfo != null) {
            Task<Void> flow = reviewManager.launchReviewFlow(this, reviewInfo);
            flow.addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(Task<Void> task) {
                    Toast.makeText(getApplicationContext(), "In App Rating complete", Toast.LENGTH_LONG).show();
                }
            });
        }
        else {
            Toast.makeText(getApplicationContext(), "In App Rating failed", Toast.LENGTH_LONG).show();
        }

}*/
///////////////////////////in app review
    public void imageblink(){
        new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {
                heightadjustspace.setVisibility(View.GONE);
                imageview49.setVisibility(View.VISIBLE);
               // otpcountdisplay.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                heightadjustspace.setVisibility(View.VISIBLE);
                imageview49.setVisibility(View.GONE);
                imageblink();

            }
        }.start();

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(Dashboard.this,"Please 123 Location",Toast.LENGTH_SHORT).show();
                    finish();
                    Intent intent = new Intent(Dashboard.this, locationdenied.class);
                    startActivity(intent);


                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
    public void getLocationdashboard(){
        try{
            locationManagerdashboard = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            locationManagerdashboard.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,5000, 5,this);




        }catch(SecurityException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        locationlatitudedashboard=location.getLatitude();
        locationlongitudedashboard=location.getLongitude();
        dashlat= Double.toString(locationlatitudedashboard);
        dashlong= Double.toString(locationlongitudedashboard);
        dashfinalloc=dashlat+","+dashlong;
        locationupdateprofile();


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

     //   Toast.makeText(Dashboard.this,"Please enable Location",Toast.LENGTH_SHORT).show();

    }





    @Override
    public void onBackPressed() {

        if (pressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            finish();
        } else {
            Toast.makeText(getBaseContext(), "Press back again to exit", Toast.LENGTH_SHORT).show();
        }
        pressedTime = System.currentTimeMillis();
    }

    private void get_order_id() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/order_id",
                response -> {
                    try {
                        JSONObject respontran = new JSONObject(response);
                        String temp_order_id = respontran.getString("transactionid");
                        eb_t_order_id = "NEXEB" + temp_order_id;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, "", Toast.LENGTH_SHORT).show();

                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void get_t_eta(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/t43_eta",
                response -> {

                    try {
                        JSONObject respontran = new JSONObject(response);
                        t_four_three_eta = respontran.getString("T43_ETA");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                message = "cannot connect";
            } else if (error instanceof AuthFailureError) {
                message = "";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout";
            }
            if (!message.isEmpty()) {
                Toast.makeText(Dashboard.this, "", Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private boolean fourty_three(){

        if (get_t_amount == "0" || get_t_amount.equals("0")){
            Toast.makeText(Dashboard.this,"Please Select Amount ",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void gettransactiontotal(){

        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/total_transactions",
                response -> {
                    try {
                        JSONObject transactionresponse = new JSONObject(response);
                        System.out.println(transactionresponse);
                        transactionresponse.getString("Result");
                        if (transactionresponse.getString("Result") == "null" || transactionresponse.getString("Result").equals("null")) {
                            Transaction_tot_amount = null;
                        } else {
                            Transaction_tot_amount = transactionresponse.getString("Result");
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                //   message = "cannot profile connect";
                new MaterialStyledDialog.Builder(Dashboard.this)
                        .setTitle("Internet Error")
                        .setDescription("Check Your Interenet Connection")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.warninginternet)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .setNegativeText("Cancel")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                dialog.dismiss();
                            }
                        })
                        .setPositiveText("Open Settings")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                // dialog.dismiss();
                            }
                        })
                        .show();
            } else if (error instanceof AuthFailureError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof NoConnectionError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout. Please try after sometime";
            }
            if (!message.isEmpty()) {
                Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


    private void checkboxmethod(){
        ///---Test---//


        final LayoutInflater inflater = (LayoutInflater)Dashboard.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Dialog settingsDialog = new Dialog(Dashboard.this);
        settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(inflater.inflate(R.layout.checkbox, null));
        settingsDialog.show();
        donotshowagain = (CheckBox) settingsDialog.findViewById(R.id.checkBox);
        Button button41 = settingsDialog.findViewById(R.id.button41);
        button41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkboxresult = "NOT checked";

                if (donotshowagain.isChecked()) {
                    checkboxresult = "checked";
                }


                SharedPreferences.Editor check1editor = sharedcheckPreferences.edit();

                check1editor.putString("status",checkboxresult);
                check1editor.apply();
                Intent intent = new Intent ( Dashboard.this, new_payment.class);//type of transaction
                startActivity(intent);
                settingsDialog.dismiss();

                // Do what you want to do on "OK" action

                return;
            }
        });

        Button button24 = settingsDialog.findViewById(R.id.button24);
        button24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkboxresult = "NOT checked";

                if (donotshowagain.isChecked()) {
                    checkboxresult = "checked";
                }



                SharedPreferences.Editor check1editor = sharedcheckPreferences.edit();

                check1editor.putString("status",checkboxresult);
                check1editor.apply();
                //Dashboard.super.onResume();
                settingsDialog.dismiss();
                // Do what you want to do on "OK" action

                return;
            }
        });




        //----Test End---//

        /*

        AlertDialog.Builder adb = new AlertDialog.Builder(Dashboard.this);
        LayoutInflater adbInflater = LayoutInflater.from(Dashboard.this);
        View checkview = adbInflater.inflate(R.layout.checkbox,null);
        //adbInflater.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        donotshowagain = (CheckBox) checkview.findViewById(R.id.checkBox);

        adb.setView(checkview);
        //  adb.setTitle("Attention");
        //  adb.setMessage(Html.fromHtml("This is is"));
        adb.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                checkboxresult = "NOT checked";

                if (donotshowagain.isChecked()) {
                    checkboxresult = "checked";
                }


                SharedPreferences.Editor check1editor = sharedcheckPreferences.edit();

                check1editor.putString("status",checkboxresult);
                check1editor.apply();
                Intent intent = new Intent ( Dashboard.this, Typeoftransaction.class);
                startActivity(intent);

                // Do what you want to do on "OK" action

                return;
            }

        });
        adb.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                checkboxresult = "NOT checked";

                if (donotshowagain.isChecked()) {
                    checkboxresult = "checked";
                }


                SharedPreferences.Editor check1editor = sharedcheckPreferences.edit();

                check1editor.putString("status",checkboxresult);
                check1editor.apply();
                Dashboard.super.onResume();

                // Do what you want to do on "OK" action

                return;
            }

        });


        adb.show();

        */


    }




    public void openPagefive() {
        Intent intent = new Intent(this, Moreoptions.class);
        startActivity(intent);
    }


    //-----------------Location Update---------------------//
    public void locationupdateprofile(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/update_loc",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     //----Nothing----//
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";

                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                if (dashfinalloc == null){
                    dashboardlocationupdate = "Not Provided";
                    System.out.println("this is setvaluelatlong 1");
                    System.out.println(dashboardlocationupdate);
                }else{
                    dashboardlocationupdate = dashfinalloc;
                    System.out.println("this is setvaluelatlong 2 (actual location)");
                    System.out.println(dashboardlocationupdate);
                }



                params.put("id", userid);
                params.put("location", dashboardlocationupdate);


                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    //------------------location update end------------------------//


    //-----------------profiledetails---------------------//
    private void profileinfo(){

        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                response -> {
                    try {
                        JSONObject profileresponse = new JSONObject(response);
                        dashboardusername.setText(profileresponse.getString("name"));
                        userid = profileresponse.getString("id");
                        pay_details_name = profileresponse.getString("name");
                        pay_details_email = profileresponse.getString("email");
                        responsephonenumber = profileresponse.getString("phone");
                        amountlimit = profileresponse.getString("amountlimit");
                        app_image_name = profileresponse.getString("image_name");
                        kycverification = profileresponse.getString("kyc");
                        t1 = profileresponse.getString("t1");
                        amexPercant = profileresponse.getString("amex");


                        System.out.println(app_image_name);

                        if (profileresponse.getString("reference_status").equals("1")) {
                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Dashboard.this, R.layout.spinner_item, amount_list);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            amount_list_spinner.setAdapter(dataAdapter);
                            get_order_id();
                            get_t_eta();
                            escrow.setVisibility(View.VISIBLE);
                        }

                        if (kycverification.equals("V")) {
                            //textview296.setVisibility(View.VISIBLE);
                            textview296.setEnabled(false);
                            textview296.setTextColor(Color.parseColor("#58A837"));
                            textview296.setText("Verified");
                        } else if (kycverification.equals("Ret")) {
                            //textview296.setVisibility(View.VISIBLE);
                            textview296.setEnabled(true);
                            textview296.setTextColor(Color.parseColor("#8E9F23"));
                            textview296.setText("(Click to Retry KYC)");
                        } else if (kycverification.equals("Rej")) {
                            // textview296.setVisibility(View.VISIBLE);
                            textview296.setEnabled(false);
                            textview296.setTextColor(Color.parseColor("#C03232"));
                            textview296.setText("Rejected");
                        } else if (kycverification.equals("N.V")) {
                            //textview296.setVisibility(View.VISIBLE);
                            textview296.setEnabled(true);
                            textview296.setTextColor(Color.parseColor("#122c3d"));
                            textview296.setText("(Click to verify KYC)");
                        } else if (kycverification.equals("P")) {
                            // textview296.setVisibility(View.VISIBLE);
                            textview296.setEnabled(false);
                            textview296.setTextColor(Color.parseColor("#c9c9c9"));
                            textview296.setText("Pending");
                        }


                        if (number_of_entries == 0) {
                            if (profileresponse.getString("sds_advertisement").equals("2") && profileresponse.getString("verifylocation").equals("N.V")) {
                                //online popup image

                                final Dialog settingsDialog = new Dialog(Dashboard.this);
                                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout, null));
                                settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                ImageView adimage_layout = settingsDialog.findViewById(R.id.imageView61);
                                adimage_layout.setVisibility(View.VISIBLE);
                                //  Button closeimage_popup_button = settingsDialog.findViewById(R.id.image_popup_cancel);

                                ImageView close_image = settingsDialog.findViewById(R.id.close_image);

                                Button button63 = settingsDialog.findViewById(R.id.button63);
                                String imageUrl_ad = "https://www.nexdha.com/appimage/" + app_image_name;

                                //Loading image using Picasso
                                //   Picasso.get().load(imageUrl_ad)
                                //      .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                //      .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(adimage_layout);
                                //closeimage_popup_button.setVisibility(View.VISIBLE);
                                // close_image.setVisibility(View.VISIBLE);
                                button63.setVisibility(View.VISIBLE);
                                button63.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent viewIntentprivacy =
                                                new Intent("android.intent.action.VIEW",
                                                        Uri.parse("http://nexdha.com/knowmore.html"));
                                        startActivity(viewIntentprivacy);
                                    }
                                });

                                close_image.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        settingsDialog.dismiss();
                                    }
                                });
                                settingsDialog.show();

                            } else if (profileresponse.getString("sds_advertisement").equals("1") && profileresponse.getString("verifylocation").equals("N.V")) {
                                //online popup image
                                final Dialog settingsDialog = new Dialog(Dashboard.this);
                                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout, null));
                                settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                ImageView adimage_layout = settingsDialog.findViewById(R.id.imageView57);
                                adimage_layout.setVisibility(View.VISIBLE);
                                //Button closeimage_popup_button = settingsDialog.findViewById(R.id.image_popup_cancel);
                                ImageView close_image = settingsDialog.findViewById(R.id.close_image);
                                LinearLayout linearLayout17 = settingsDialog.findViewById(R.id.linearLayout17);

                                String imageUrl_ad = "https://www.nexdha.com/appimage/" + app_image_name;

                                //Loading image using Picasso
                                /*  Picasso.get().load(imageUrl_ad)
                                 *//*
                                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
*//*
                                        .priority(HIGH)

                                        *//* .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)*//*
                                        .into(adimage_layout);*/

                                Glide.with(getApplicationContext())
                                        .load(imageUrl_ad).into(adimage_layout);
                                //closeimage_popup_button.setVisibility(View.VISIBLE);
                                //close_image.setVisibility(View.VISIBLE);

                                linearLayout17.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        settingsDialog.dismiss();
                                    }
                                });
                                settingsDialog.show();
                            }
                            number_of_entries = number_of_entries + 1;
                        }


                        if (profileresponse.getString("verifylocation").equals("N.V")) {
                            verify = "Yes";
                            upilayoutii.setVisibility(View.VISIBLE);
                            linear_layout_t_five.setVisibility(View.VISIBLE);
                            get_t_five_eta();
                        } else {
                            verify = "No";
                            upilayoutii.setVisibility(View.GONE);
                        }
                        fraudverification();
                        //   developerverification();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                 //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                   // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                     dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                 // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }



    private void appdisplay_method(){

        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);

                            locationverification = profileresponse.getString("verifylocation");

                            if (locationverification.equals("N.V")){

                                //online popup image
                                final Dialog settingsDialog = new Dialog(Dashboard.this);
                                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout, null));
                                settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                ImageView adimage_layout = settingsDialog.findViewById(R.id.imageView57);
                               LinearLayout linearLayout17 =settingsDialog.findViewById(R.id.linearLayout17);
                                adimage_layout.setVisibility(View.VISIBLE);
                               // Button closeimage_popup_button = settingsDialog.findViewById(R.id.image_popup_cancel);

                                ImageView close_image = settingsDialog.findViewById(R.id.close_image);

                                String imageUrl_ad = "https://www.nexdha.com/appimage/" + noticeapistring1;

                                //Loading image using Picasso
                                Picasso.get().load(imageUrl_ad)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                        .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                                        .into(adimage_layout);
                              //  closeimage_popup_button.setVisibility(View.VISIBLE);
                              //  close_image.setVisibility(View.VISIBLE);
                                linearLayout17.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        settingsDialog.dismiss();
                                    }
                                });
                                settingsDialog.show();

                                //online popup image close
                            }



                            fraudverification();
                            //   developerverification();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Internet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


    //-----------------fraud Details---------------------//
    private void fraudverification(){

        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;



        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/fraudtable",
                response -> {
                    try {
                        JSONObject fraudresponse = new JSONObject(response);
                        verifyfraudtruefalse = fraudresponse.getString("status");
                        System.out.println(verifyfraudtruefalse);
                        if (verifyfraudtruefalse.equals("true")) {
                            new MaterialStyledDialog.Builder(Dashboard.this)
                                    .setTitle("Account Blocked")
                                    .setDescription("Your account is being blocked and you are not allowed to do any transactions or proceed to any page in the app. For further details contact support@nexdha.com .")
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.cancel)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(false)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .show();
                        }
                        loadingDialog.dismiss();
                        System.out.println("This is the fraud response ******" + fraudresponse.getString("status"));

                        //  responsephonenumber = profileresponse.getString("phone");
                        //   developerverification();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                //progressbar14.setVisibility(View.GONE);
               // Toast.makeText(Dashboard.this, "Please Restart App", Toast.LENGTH_SHORT).show();

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();




                params.put("phone", responsephonenumber);


                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

    //----------endfraudverification-------------//

    //-------------Today Transaction----------------//

    private void todaytransactionapi(){


        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/today_trans_user",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            todaytransapi.setText(profileresponse.getString("Transaction_count"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                /*
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
                */

            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

   /* private void init() {
        reviewManager = ReviewManagerFactory.create(this);
        // findViewById(R.id.btn_rate_app).setOnClickListener(view -> showRateApp());
        showRateApp();
    }

    *//**
     * Shows rate app bottom sheet using In-App review API
     * The bottom sheet might or might not shown depending on the Quotas and limitations
     * https://developer.android.com/guide/playcore/in-app-review#quotas
     * We show fallback dialog if there is any error
     *//*
    public void showRateApp() {
        Task<ReviewInfo> request = reviewManager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // We can get the ReviewInfo object

                ReviewInfo reviewInfo = task.getResult();

                Task<Void> flow = reviewManager.launchReviewFlow(this, reviewInfo);
                flow.addOnCompleteListener(task1 -> {
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                });
            } else {
                // There was some problem, continue regardless of the result.
                // show native rate app dialog on error
                // showRateAppFallbackDialog();
            }
        });
    }*/
    public void edit_imagee() {
                                           /*mDay = cal.get(Calendar. DAY_OF_MONTH);
                                            System.out.println(mDay);*/
/*
                                            String month1 = monthName[cal.get(Calendar.DAY_OF_MONTH)];
 */
        String array_final[] = new String[arrlist.size()];
        for (int j = 0; j < arrlist.size(); j++) {
            array_final[j] = arrlist.get(j);
        }
        //String mnth[]={String.valueOf(arrlist)/*String.valueOf(arrlist.add(i+1))*/};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Dashboard.this, R.layout.spinner_item, array_final);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final LayoutInflater inflater = (LayoutInflater) Dashboard.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Dialog customView = new Dialog(Dashboard.this);
        customView.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        customView.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        customView.setContentView(inflater.inflate(R.layout.popups, null));
        customView.setCancelable(true);
        customView.show();


        cancelBtn = customView.findViewById(R.id.button245);

        submitBtn = customView.findViewById(R.id.button412);
        if(update_or_submitbilldate .equals("save")){
            submitBtn.setText("SUBMIT");
        }
        else if (update_or_submitbilldate.equals("update")){
            submitBtn.setText("UPDATE");
        }



        spinner = customView.findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
/*
                                            spinner.getSelectedItem().toString();
*/
        System.out.println(spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                System.out.println(spinner.getSelectedItem().toString());
                hello = spinner.getSelectedItem().toString();

/*
                                                    monthh[0] =spinner.getSelectedItem().toString();
*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //instantiate popup window
/*
                popupWindow = new PopupWindow(customView, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
*/

        //display the popup window
      /*  editimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customView.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hello.equals("Select")) {
                    Toast.makeText(Dashboard.this, "Please select the Bill date", Toast.LENGTH_LONG).show();
                    /*  submitBtn.setEnabled(false);*/
                } else {
                    if(update_or_submitbilldate .equals("save")){
                        initiate_api();
                    }
                    else if (update_or_submitbilldate.equals("update")){
                    bill_date_update();
                    }
/*
                                                        submitBtn.setEnabled(true);
*/
                    customView.dismiss();

                }

                                                   /* Intent intent = new Intent(Dashboard.this, Dashboard.class);
                                                    startActivity(intent);
                                                    finish();*/
            }
        });


    }
    public void sme_api1() {
        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("4444444444444444444444444444444444444444444444444444444");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest request = new StringRequest(Request.Method.GET,SERVER+"/api/sme_sms", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    String parse_token = json.getString("status");
                    System.out.println(parse_token);

                    if (parse_token.equals("False")){
                        main_bg = findViewById(R.id.main_layout);



                       /* Snackbar.make(main_bg, "Avail special offer for SME. If you are SME click here.", Snackbar.LENGTH_LONG).setDuration(15000)
                                .setAction("Request", new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View view) {

                                        Intent intent = new Intent(Dashboard.this, Moreoptions.class);
                                        startActivity(intent);
                                    }

                                }).setActionTextColor(Color.parseColor("#f94d00")).show();  */


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }, error -> {
            Log.e("error is ", "" + error);
            System.out.println(error);
        }) {


            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();
                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                      String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }



         /*   protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("pan",(panindia));


                System.out.println(panindia);
                return params;
            }*/
        };
        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        request.setShouldCache(false);
        requestQueue.add(request);
    }

    public void initiate_api1() {
        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("4444444444444444444444444444444444444444444444444444444");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest request = new StringRequest(Request.Method.GET,SERVER+"/api/ccdate_save", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    String parse_token = json.getString("status");
                    System.out.println(parse_token);
                    textView110 =findViewById(R.id.textView110);
                    button54 =findViewById(R.id.button54);
                    image_view_tick = findViewById(R.id.tick_imageview);
                /*    editimage =findViewById(R.id.editimage);
                    editimage.setClickable(false);*/

                    if (parse_token.equals("Not Provided")){

                        textView110.setVisibility(View.VISIBLE);

                        button54.setVisibility(View.VISIBLE);
                        textView112.setVisibility(View.GONE);
                        image_view_tick.setVisibility(View.GONE);
/*
                        editimage.setVisibility(View.GONE);
*/
/*
                        billcycleconstraint.setVisibility(View.VISIBLE);
*/
                    } else {
                        String string = parse_token;
                        String lines[] = string.split("-");
                        String [] z = lines[2].split("T");
                        System.out.println(z[0]);
                        System.out.println("sdfsd");
                        String text = "<font color=#f94d00>Bill Date </font> <font color=#00000><b>" +"-" +" "+json.getString("date") + "</font>";
                        //yourtextview.setText(Html.fromHtml(text));
                        textView112.setText(Html.fromHtml(text));
                        //textView112.setText(" Bill date set to " + "' "+ z[0] + "' " + "of every month");
                        textView110.setVisibility(View.GONE);

                        button54.setVisibility(View.GONE);
                        textView112.setVisibility(View.VISIBLE);
                        //image_view_tick.setVisibility(View.VISIBLE);
                        image_view_tick.setVisibility(View.GONE);
/*
                        editimage.setVisibility(View.VISIBLE);
*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error is ", "" + error);
                        System.out.println(error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("onlydate", hello);

                System.out.println(hello);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);

    }

    private void getTotalTransactionAmount() {
        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");

        StringRequest request = new StringRequest(Request.Method.GET, SERVER + "/api/pending_trans", response -> {
            try {
                JSONObject json = new JSONObject(response);
                String parseAmount = json.getString("transaction_amount");
                NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
                String parseAmount2 = numberFormat.format(new BigDecimal(parseAmount));

                getTotalTransaction = findViewById(R.id.total_transaction_amount);

                getTotalTransaction.setText(parseAmount2);
                getTotalTransaction.setTextColor(Color.parseColor("#EEEEEE"));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        },
                error -> {
                    Log.e("error is ", "" + error);
                    System.out.println(error);
                }) {
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token1);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = "token " + T;

                params.put("Authorization", TokenS);

                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);


    }

    public void initiate_api() {
        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("4444444444444444444444444444444444444444444444444444444");
        System.out.println(token1);
        final String token200 = token1;
       /* if (arrlist.equals("Select")) {
            Toast.makeText(Dashboard.this, "Please select the Bill date", Toast.LENGTH_LONG).show();
        } else {*/
            StringRequest request = new StringRequest(Request.Method.POST,SERVER+"/api/ccdate_save", new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    System.out.println(response);
                    initiate_api1();

                }


            }, error -> {
                Log.e("error is ", "" + error);
                System.out.println(error);
            }) {


            /*    //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Authorization","token  18a753a70886b74099aef9b51f3e4d769ee75e5b"*//* + token1*//*);
                    return headers;
                } */@Override
                public Map<String, String> getHeaders() {
                    String T="" ;
                    Map<String,String> params = new HashMap<>();
                    // params.put("Content-Type", "application/json; charset=UTF-8");
                    try {
                        JSONObject response1 = new JSONObject(token200);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        // do your work with response object
                        T=response1.getString("token");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("44444444444444444444444444");
                    System.out.println(token200);
                    ///check t!blank

                    String TokenS = "token "+ T;
                    System.out.println(T);

                    params.put("Authorization",TokenS);

                    return params;
                }




        protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("onlydate", hello);
           /* alreadybill.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));*/
/*
            alreadybill.addView(textView112);
*/
                    System.out.println(hello);
                    return params;
                }
            };
            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            queue.add(request);

    }
    public void bill_date_update() {
        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("4444444444444444444444444444444444444444444444444444444");
        System.out.println(token1);
        final String token200 = token1;
       /* if (arrlist.equals("Select")) {
            Toast.makeText(Dashboard.this, "Please select the Bill date", Toast.LENGTH_LONG).show();
        } else {*/
        StringRequest request = new StringRequest(Request.Method.POST,SERVER+"/api/update_ccdate", response -> {

            System.out.println(response);
            initiate_api1();

        }, error -> {
            Log.e("error is ", "" + error);
            System.out.println(error);
        }) {


            /*    //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Authorization","token  18a753a70886b74099aef9b51f3e4d769ee75e5b"*//* + token1*//*);
                    return headers;
                } */@Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();
                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }




            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("onlydate", hello);
           /* alreadybill.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));*/
/*
            alreadybill.addView(textView112);
*/
                System.out.println(hello);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);

    }

    private void easebuzz_hash_generation() {




        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/easebuzzhash",
                response -> {
                    //Nothing
                    try {
                        JSONObject parse_response_hash = new JSONObject(response);

                        final_settlement_amount = get_t_amount;
                        System.out.println("This is final amount *****************");
                        System.out.println(get_t_amount);
                        System.out.println(final_settlement_amount);

                        eb_hash_new = parse_response_hash.getString("hash");

                        eb_before_new();

                    } catch (JSONException error) {
                        System.out.println(error);
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                progresbar27.setVisibility(View.GONE);

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<>();


                params.put("txnid",eb_t_order_id);
                params.put("amount",String.valueOf(Float.parseFloat(get_t_amount)));
                params.put("productinfo","Me");
                params.put("firstname",pay_details_name);
                params.put("email_id",pay_details_email);
                params.put("udf1",userid);
                params.put("udf2","udf2");
                params.put("udf3","Nexdha");
                params.put("udf4","86674519300");
                params.put("udf5",SampleAppConstants.PG_UDF5);
                System.out.println(SampleAppConstants.PG_UDF5);
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
private void eb_after_tran(){




    SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
    final String token1 = sharedPreferences.getString("token", "");
    System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
    System.out.println(token1);
    final String token200 = token1;

    // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
    StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/after_marked_pgtran",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Nothing



                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            String message = "";
            progresbar27.setVisibility(View.GONE);

            if (error instanceof NetworkError) {
                //   message = "cannot profile connect";
                new MaterialStyledDialog.Builder(Dashboard.this)
                        .setTitle("Internet Error")
                        .setDescription("Check Your Interenet Connection")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.warninginternet)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .setNegativeText("Cancel")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                dialog.dismiss();
                            }
                        })
                        .setPositiveText("Open Settings")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                // dialog.dismiss();
                            }
                        })
                        .show();
            } else if (error instanceof AuthFailureError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof NoConnectionError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout. Please try after sometime";
            }
            if (!message.isEmpty()) {
                Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
            }
        }
    })

    {

        //-------------------------outerheader-----------------------//
        @Override
        public Map<String, String> getHeaders() {
            String T = "";
            Map<String, String> params = new HashMap<>();


            // params.put("Content-Type", "application/json; charset=UTF-8");
            try {
                JSONObject response1 = new JSONObject(token200);
                System.out.println("!!!!!!!!!!!!!!!!!!");
                System.out.println(response1.getString("token"));
                // do your work with response object
                T = response1.getString("token");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println("44444444444444444444444444");
            System.out.println(token200);
            ///check t!blank

            String TokenS = "token "+ T;
            System.out.println(T);

            params.put("Authorization",TokenS);

            return params;
        }

        protected Map<String,String> getParams(){
            Map<String, String> params = new HashMap<>();
            params.put("response_message",response_message_from_gateway_dash);
            params.put("transaction_id",tx_id_from_gateway_dash);
            return params;
        }


    };

    RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
    stringRequest.setShouldCache(false);
    requestQueue.add(stringRequest);



}
    private void eb_before_new(){
        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/marked_pgtran",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing
                        try{
                            JSONObject parse_response_hash = new JSONObject(response);



                            Intent intentProceed = new Intent(Dashboard.this, PWECouponsActivity.class);
                            intentProceed.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); // This is mandatory flag
                            intentProceed.putExtra("key","W33DPORCU5");
                            intentProceed.putExtra("address1",t_four_three_eta);//
                            intentProceed.putExtra("address2",final_settlement_amount);//
                            intentProceed.putExtra("city","Chennai");//
                            intentProceed.putExtra("state","TamilNadu");//
                            intentProceed.putExtra("txnid",eb_t_order_id);//
                            intentProceed.putExtra("amount",Float.valueOf(get_t_amount));//
                            intentProceed.putExtra("productinfo","Me");//
                            intentProceed.putExtra("surl",SERVER+"/api/after_marked_pgtran");
                            intentProceed.putExtra("furl",SERVER+"/api/after_marked_pgtran");
                            intentProceed.putExtra("country","Referral");//
                            intentProceed.putExtra("firstname",pay_details_name);//
                            intentProceed.putExtra("email",pay_details_email);//
                            intentProceed.putExtra("phone",responsephonenumber);//
                            intentProceed.putExtra("pay_mode","production");
                            intentProceed.putExtra("udf1",userid);//
                            intentProceed.putExtra("udf2","udf2");//
                            intentProceed.putExtra("udf3","Nexdha");//
                            intentProceed.putExtra("udf4","86674519300");//
                            intentProceed.putExtra("udf5",SampleAppConstants.PG_UDF5);//
                            intentProceed.putExtra("hash",eb_hash_new);

                            startActivityForResult(intentProceed, PWEStaticDataModel.PWE_REQUEST_CODE);
                            progresbar27.setVisibility(View.GONE);

                            //eb_before();



                        }catch (JSONException error){
                            System.out.println(error);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                progresbar27.setVisibility(View.GONE);

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<>();


                params.put("eta",t_four_three_eta);
                params.put("receiving_amount",String.valueOf(Float.parseFloat(get_t_amount)));
                params.put("tran_amount",String.valueOf(Float.parseFloat(get_t_amount)));
                params.put("email",Dashboard.pay_details_email);
                params.put("name",Dashboard.pay_details_name);
                params.put("order_id",eb_t_order_id);
                params.put("phone",Dashboard.responsephonenumber);
                params.put("tran_type","T+43");
                params.put("transaction_id",eb_t_order_id);
                params.put("user_id",Dashboard.userid);
                params.put("vendor_code","86674519300");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

    @Override
    public void onResume() {
        super.onResume();

        sessionCheck();


        //
        amount_list_spinner.setSelection(0);
        get_t_amount = "0";
        profileinfo();
        arrlist.clear();
        final String[] monthh = new String[1];
                                         /*   String[] monthName = { "January", "February", "March", "April", "May", "June", "July",
                                                    "August", "September", "October", "November", "December" };*/
        SimpleDateFormat fmt = new SimpleDateFormat("dd");
        Calendar cal = Calendar.getInstance();
        System.out.println(cal);
        System.out.println(cal.get(YEAR));
        System.out.println(cal.get(MONTH));
        //cal.clear();

                                      /*      Calendar c = Calendar.getInstance();
                                            int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
                                            System.out.println(monthMaxDays);*/
/*
                                            cal.set(year, month , 1);
*/
        int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        System.out.println(daysInMonth);

        //yourtextview.setText(Html.fromHtml(text));
        arrlist.add("Select");
        for (int i = 1; i < daysInMonth + 1; i++) {
                                                /*array11 = daysInMonth[i];
                                                array11.add(daysInMonth);*/
            arrlist.add(String.valueOf(i));
                                                /*System.out.println(arrlist);
                                                System.out.println(fmt.format(cal.getTime()));
                                                cal.add(Calendar.DAY_OF_MONTH, 1);*/
        }

        System.out.println(cal);
        System.out.println(arrlist);







        amount_list_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {


                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

//save this value in shared pref
                        String paynamelist = amount_list.get(position);
                        if (paynamelist.equals("Select Amount")){
                            get_t_amount = "0";
                        }else if (paynamelist.equals("75k")){
                            get_t_amount = "75000";
                        }
                        else if (paynamelist.equals("1 Lakh")){
                            get_t_amount = "100000";
                        }else if (paynamelist.equals("1.5 Lakhs")){
                            get_t_amount = "150000";
                        }else if (paynamelist.equals("2 Lakhs")){
                            get_t_amount = "200000";
                        }else if (paynamelist.equals("2.5 Lakhs")){
                            get_t_amount = "250000";
                        }else if (paynamelist.equals("3 Lakhs")){
                            get_t_amount = "300000";
                        }


                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //
    }
    //-------------------Get ETA-----------------//
    public void getETAdashboard(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/eta",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        try{
                            JSONObject respontran = new JSONObject(response);
                            eta.setText(respontran.getString("transaction_delivery_date"));

                        }catch(JSONException e){
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }
    //---------------END-----------//

    //-------------------Get ETA for t+1-----------------//
    public void getonedayETAdashboard(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/t1_eta",
                response -> {


                    try {
                        JSONObject respontran = new JSONObject(response);
                        etaOne = respontran.getString("transaction_delivery_date");
                        etaoneday.setText(respontran.getString("transaction_delivery_date"));
                        onedayonly = respontran.getString("transaction_delivery_date");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                message = "cannot connect";
            } else if (error instanceof AuthFailureError) {
                message = "";
            } else if (error instanceof NoConnectionError) {
                message = "cannot connect";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout";
            }
            if (!message.isEmpty()) {
                Toast.makeText(Dashboard.this, "", Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }
    //-------END-------//

    public void get_t_five_eta(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/t5_eta",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        try{
                            JSONObject respontran = new JSONObject(response);
                            eta_t_five_text.setText(respontran.getString("transaction_delivery_date"));
                        }catch(JSONException e){
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    //-------------Notice Message in Dashboard-----------------//

    public void getnoticemessage(){



        final TextView impmessagepopup = findViewById(R.id.textView205);

        @SuppressLint("SetTextI18n") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER +"/api/holy_pop",
                response -> {

                    try {
                        JSONObject responpopup = new JSONObject(response);
                        popupstatus = responpopup.getString("status");
                        if (popupstatus == "1" || popupstatus.equals("1")) {
                            importantpopup.setVisibility(View.VISIBLE);
                            impmsgspace.setVisibility(View.VISIBLE);
                            responsemessagefromholypop = responpopup.getString("reson");
                            responsedatefromholypop = responpopup.getString("date");
                            if (responsemessagefromholypop == "sunday" || responsemessagefromholypop.equals("sunday")) {
                                impmessagepopup.setText("T+1 and T+5 settlements will not be processed(" + responsedatefromholypop + " " + responsemessagefromholypop + ").");
                            } else {
                                impmessagepopup.setText("T+1 and T+5 settlements will not be processed(" + responsedatefromholypop + ") due to " + responsemessagefromholypop + ".");
                            }

                        }
                        System.out.println("Response Status");
                        System.out.println(responpopup.getString("status"));
                        //  eta.setText(responpopup.getString("transaction_delivery_date"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }
    //--------END---------------//
private void diwalibg(){



    StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/diwali_bg",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject profileresponse = new JSONObject(response);
                        String diwali_string = profileresponse.getString("output");
                            if (diwali_string == "1" || diwali_string.equals("1")){
                                main_bg.setBackgroundResource(R.drawable.diwalibg);}

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

                /*
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
                */

        }
    });

    RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
    stringRequest.setShouldCache(false);
    requestQueue.add(stringRequest);

}
    //-------------Notice Api----------------//

    private void noticeapi(){



        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/up_reson",
                response -> {
                    try {
                        JSONArray noticearray = new JSONArray(response);
                        for (int i = 0; i < noticearray.length(); i++) {
                            JSONObject noticeobject = noticearray.getJSONObject(i);
                            noticeapistring = noticeobject.getString("status");
                            noticeapistring1 = noticeobject.getString("reson");
                            if (noticeapistring == "1" || noticeapistring.equals("1")) {
                                noticespace.setVisibility(View.VISIBLE);
                                noticelayout.setVisibility(View.VISIBLE);
                                noticeapitext.setText(noticeobject.getString("reson"));
                            } else if (noticeapistring == "2" || noticeapistring.equals("2")) {
                                onlineimagespace.setVisibility(View.VISIBLE);
                                imagelayout.setVisibility(View.VISIBLE);
                            } else if (noticeapistring == "3" || noticeapistring.equals("3")) {
                                notificationclass_id = noticeapistring;
                                notificationclass_text = noticeobject.getString("reson");
                                Intent intent = new Intent(Dashboard.this, Notification.class);
                                startActivity(intent);
                            } else if (noticeapistring == "4" || noticeapistring.equals("4")) {
                                notificationclass_id = noticeapistring;
                                notificationclass_text = noticeobject.getString("reson");
                                Intent intent = new Intent(Dashboard.this, Notification.class);
                                startActivity(intent);
                            } else if (noticeapistring == "5" || noticeapistring.equals("5")) {
                                //appdisplay_method();

                                //online popup image
                                final Dialog settingsDialog = new Dialog(Dashboard.this);
                                settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.image_layout, null));
                                settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                ImageView adimage_layout = settingsDialog.findViewById(R.id.imageView57);
                                adimage_layout.setVisibility(View.VISIBLE);
                                //Button closeimage_popup_button = settingsDialog.findViewById(R.id.image_popup_cancel);
                                ImageView close_image = settingsDialog.findViewById(R.id.close_image);
                                LinearLayout linearLayout17 = settingsDialog.findViewById(R.id.linearLayout17);

                                String imageUrl_ad = "https://www.nexdha.com/appimage/" + noticeapistring1;

                                //Loading image using Picasso
                                /*   Picasso.get().load(imageUrl_ad)
                                 *//* .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                                        .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)*//*
                                        .into(adimage_layout);*/
                                Glide.with(getApplicationContext())
                                        .load(imageUrl_ad)
                                        .into(adimage_layout);
                                //closeimage_popup_button.setVisibility(View.VISIBLE);
                                //   close_image.setVisibility(View.VISIBLE);

                                linearLayout17.setOnClickListener(view -> settingsDialog.dismiss());
                                settingsDialog.show();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                /*
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
                */

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    //--------------Notice End-------------//



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);





        if (requestCode == PWEStaticDataModel.PWE_REQUEST_CODE) {
            // Request recieved by EB
            if (data == null) {
                Toast.makeText(Dashboard.this, "Please Retry Transaction", Toast.LENGTH_SHORT).show();
                finish();
            }else{

                System.out.println("Easebuzz");
                System.out.println(data.getStringExtra("result"));

                if (data.getStringExtra("result") == null) {
                    new MaterialStyledDialog.Builder(this)

                            .setTitle("Error")
                            .setDescription("There was an error in your transaction, if the amount has been debited from your account it will be credited to your beneficiary bank account within T+2 bank working days.")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.transactionfailed)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(true)
                            .setCancelable(false)
                            .withDivider(true)
                            .setPositiveText("Got it")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } else {

                    if ((data.getStringExtra("result") == "user_cancelled") || (data.getStringExtra("result").equals("user_cancelled"))) {
                        new MaterialStyledDialog.Builder(this)
                                .setTitle("Transaction Cancelled")
                                .setDescription("You have cancelled the Transaction")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.transactionfailed)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(true)
                                .setCancelable(false)
                                .withDivider(true)
                                .setPositiveText("Got it")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                         dialog.dismiss();
                                    }
                                })
                                .show();
                    } else {
                        String parse_response = data.getStringExtra("payment_response");
                        try {
                            JSONObject profileresponse = new JSONObject(parse_response);




                            response_message_from_gateway_dash = profileresponse.getString("status");//
                            tx_id_from_gateway_dash = profileresponse.getString("txnid");//


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if ((data.getStringExtra("result") == "payment_successfull") || (data.getStringExtra("result").equals("payment_successfull"))) {
                            System.out.println(data.getStringExtra("payment_response"));

                            new MaterialStyledDialog.Builder(this)
                                    .setTitle("Transaction Successful")
                                    .setDescription("Your amount will be credited by " + t_four_three_eta + ".\nRate us on playstore.")
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.tickicon)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(true)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .setPositiveText("Got it")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();

                        } else {

                            new MaterialStyledDialog.Builder(this)
                                    .setTitle("Transaction Failed")
                                    .setDescription(response_message_from_gateway_dash)
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.transactionfailed)
                                    .withIconAnimation(false)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(true)
                                    .setCancelable(false)
                                    .withDivider(true)

                                    .setNegativeText("Close")
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        }
                        eb_after_tran();
                    }


                }

            }
        }




        else if (result != null) {
            String value = result.getContents();
            System.out.println(value);


          /*  // SPLIT //
            String str = result.getContents();
            System.out.println("This is str");
            System.out.println(str);
            String questionmark = "\\?";
            String and = "&";
            String array1 = str.split(questionmark)[1];
            System.out.println(array1);
            String array2[] = array1.split(and);
            System.out.println("This is array2");
            System.out.println(Arrays.toString(array2));

           */

            // String code="upi://pay?cu=INR&pa=12345@upi&pn=Jega%20guru";






            if (value != null) {
                //Timber.d("qrcode=%s", value);
                // do something
              /*  System.out.println("This is qr output");
                System.out.println(value);
                String array6 = array5.replaceAll("%20"," ");
                qrdisplay.setText("Payee name = "+array6);
                textviewupi.setText("Payee address = "+array2);
               */
                String code = result.getContents();
                //Log.e("Aakash","code: "+code);
                if (code.contains("upi://pay")) {
                    String[] spiltted = code.split("\\?");

                    //Log.e("Aakash","spillted:" +spiltted[0]+" "+spiltted[1]);
                    String[] tiny = spiltted[1].split("&");
                    //Log.e("Aakash","tiny: "+tiny[0]+" "+tiny[1]);
                    //String [] small=tiny[1].split("@");
                    //Log.e("Aakash","small: "+small[0]+" "+small[1]);
                    for (int i = 0; i < 3; i++) {
                        if (tiny[i].contains("pa")) {
                            //Log.e("Aakash","result:is in "+i+" = "+tiny[i]);
                            //System.out.println(tiny[i]);
                           String tinyfinaladdress = tiny[i].split("pa=")[1];
                            nextpageupiid = tinyfinaladdress;
                            Intent intent = new Intent(Dashboard.this, commonscreenupi.class);
                            intent.putExtra("FROM_UPI_ACTIVITY", "qrdashboard");
                            startActivity(intent);
                            System.out.println(tinyfinaladdress);
//                            qrdisplay.setText("Payee upi address = " + tinyfinaladdress);
                            //  beneficiaryupi.setVisibility(View.VISIBLE);
                            //  verify.setVisibility(View.VISIBLE);
                            //  enterbeneupi.setVisibility(View.GONE);
                            //  beneficiaryupi.setText(tinyfinaladdress);
                        }
                    }

                    for (int i = 0; i < 3; i++) {
                        if (tiny[i].contains("pn")) {
                            //Log.e("Aakash","result:is in "+i+" = "+tiny[i]);
                            //System.out.println(tiny[i]);
                            String tinyfinalname = tiny[i].split("pn=")[1];
                            System.out.println(tinyfinalname);
                          //  trimname = tinyfinalname.replaceAll("%20", " ");
                            //textviewupi.setText("Payee name = " + trimname);
                        }
                    }
                }else{
                    Toast.makeText(Dashboard.this,"Invalid Qr code",Toast.LENGTH_SHORT).show();
                }


            } else {
                // Timber.d("QR code capture canceled or failed");
              //  beneficiaryupi.setVisibility(View.GONE);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }







    //--------------Developer verification-------------------//
    private void developerverification(){

        if (SERVER.equals("http://13.233.83.214") && userid.equals("1")){
            Intent intent = new Intent(Dashboard.this, Notification.class);
            startActivity(intent);
            finish();
        }else if (SERVER.equals("http://3.18.225.128") && userid.equals("3")){
            Intent intent = new Intent(Dashboard.this, Notification.class);
            startActivity(intent);
            finish();
        }
        else{
            onResume();
        }


    }

    private void check_upi_availability(){

        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;
        System.out.println(token200);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/upi_activation",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing
                        try{
                            JSONObject responpopup = new JSONObject(response);
                            String sds_status = responpopup.getString("status");
                            if (sds_status == "true" || sds_status.equals("true")){
                                Intent intent = new Intent(Dashboard.this, Upitransaction.class);
                                startActivity(intent);
                            }else{
                                new MaterialStyledDialog.Builder(Dashboard.this)

                                        .setTitle("Closed")
                                        .setDescription("UPI transaction is closed for now. Please try after sometime.")
                                        .setHeaderColor(R.color.md_divider_white)
                                        .setIcon(R.drawable.transactionfailed)
                                        .withDarkerOverlay(true)
                                        .withDialogAnimation(true)
                                        .setCancelable(false)
                                        .withDivider(true)
                                        .setPositiveText("Got it")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                               dialog.dismiss();
                                            }
                                        })
                                        .show();
                            }

                        }catch(JSONException e){
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Internet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }




        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }




    //-----developer End-----------//
    private void activity_log_dashboard(){

        SharedPreferences sharedPreferences = Dashboard.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;
System.out.println(token200);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Dashboard.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Internet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative((dialog, which) -> {
                                // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","113");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Dashboard.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


}