package com.nexdha.nexpay;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nexdha.nexpay.constant.SERVER;

public class Enterupi_id extends AppCompatActivity {
    ImageView imageviewgreentick,imageredcross,imageviewback;
    TextView verify,verified,cancel,enterupiidtext;
    EditText enterupi;
    ProgressBar verifyloading;
    Button proceedbutton;
    public static String nextpageupiid = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
      //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_enterupi_id);
        enterupiidtext = findViewById(R.id.textView209);
        cancel = findViewById(R.id.textView211);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        verified = findViewById(R.id.textView212);
        verified.setVisibility(View.GONE);
        imageviewback = findViewById(R.id.imageView34);
        imageviewback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        proceedbutton = findViewById(R.id.button31);
        proceedbutton.setVisibility(View.GONE);
        imageviewgreentick = findViewById(R.id.imageView32);
        imageredcross = findViewById(R.id.imageView33);
        enterupi = findViewById(R.id.editText15);
        verifyloading = findViewById(R.id.progressBar8);
        verify = findViewById(R.id.textView210);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // final String transfername = enterupi.getText().toString().trim();
                imageredcross.setVisibility(View.GONE);
                imageviewgreentick.setVisibility(View.GONE);
                verifyloading.setVisibility(View.VISIBLE);
                if (Validationacc() == false){
                    verifyloading.setVisibility(View.GONE);
                    return;
                }
                final String finalbeneficiaryupi = enterupi.getText().toString().trim();


                StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/verify_upi",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println("***************UPI response*******************");
                                // System.out.println(response);

                                try {
                                    JSONObject respontran = new JSONObject(response);
                                    System.out.println(respontran.getString("vendor"));
                                    String checkvpa = respontran.getString("vendor");
                                    if (checkvpa.contains("data")){
                                        System.out.println("Valid VPA");
                                        JSONObject respontran2 = respontran.getJSONObject("vendor");
                                        System.out.println(respontran2.getString("data"));
                                        JSONObject respontran3 = respontran2.getJSONObject("data");
                                        System.out.println(respontran3.getString("status"));
                                        enterupiidtext.setTextColor(Color.parseColor("#3AA41E"));
                                        enterupiidtext.setText("Valid UPI");
                                        verifyloading.setVisibility(View.GONE);
                                        imageviewgreentick.setVisibility(View.VISIBLE);
                                        imageredcross.setVisibility(View.GONE);
                                        verify.setVisibility(View.GONE);
                                        proceedbutton.setVisibility(View.VISIBLE);
                                        enterupi.setEnabled(false);
                                        enterupi.setVisibility(View.GONE);
                                        verified.setText("UPI ID : "+finalbeneficiaryupi);
                                        verified.setVisibility(View.VISIBLE);
                                        nextpageupiid = finalbeneficiaryupi;

                                    }else if (checkvpa.contains("error")){
                                        JSONObject respontran2 = respontran.getJSONObject("vendor");
                                        System.out.println(respontran2.getString("error"));
                                        JSONObject respontran3 = respontran2.getJSONObject("error");
                                        System.out.println(respontran3.getString("message"));
                                        enterupiidtext.setTextColor(Color.RED);
                                        enterupiidtext.setText(respontran3.getString("message"));
                                        verifyloading.setVisibility(View.GONE);
                                        imageviewgreentick.setVisibility(View.GONE);
                                        imageredcross.setVisibility(View.VISIBLE);
                                    }
                                    // JSONObject respontran2 = respontran.getJSONObject("vendor");
                                    // System.out.println(respontran2.getString("data"));
                                    // JSONObject respontran3 = respontran2.getJSONObject("data");
                                    // System.out.println(respontran3.getString("status"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                               // progressbar1.setVisibility(View.GONE);
                               // imageview2.setVisibility(View.VISIBLE);
                               // proceed.setVisibility(View.VISIBLE);
                               // upiid = transfername;
                                // Intent intent = new Intent(SignUp.this, Payment.class);
                                // startActivity(intent);

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message = "";
                        if (error instanceof NetworkError) {
                            message = "Cannot connect to internet......please check your internet connection";
                        } else if (error instanceof ServerError) {
                            message = "";
                        } else if (error instanceof AuthFailureError) {
                            message = "Cannot connect to internet......please check your internet connection";
                        } else if (error instanceof NoConnectionError) {
                            message = "Cannot connect to internet......please check your internet connection";
                        } else if (error instanceof ParseError) {
                            message = "Cannot connect to internet......please check your internet connection";
                        } else if (error instanceof TimeoutError) {
                            message = "Connection Timedout......please check your internet connection";
                        }
                        if (!message.isEmpty()) {
                            Toast.makeText(Enterupi_id.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                })

                {


                    protected Map<String, String> getParams(){
                        Map<String, String> params = new HashMap<>();

                        params.put("virtual_payee_address",finalbeneficiaryupi);
                        return params;

                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(Enterupi_id.this);
                stringRequest.setShouldCache(false);
                requestQueue.add(stringRequest);
            }
        });
        proceedbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Enterupi_id.this, commonscreenupi.class);
                intent.putExtra("FROM_UPI_ACTIVITY", "enter_upi_id");
                startActivity(intent);
            }
        });


    }

    private boolean Validationacc(){
        enterupi = findViewById(R.id.editText15);
        final String validateupi = enterupi.getText().toString().trim();


        if (validateupi.isEmpty() || validateupi.length() == 0 || validateupi.equals("") || validateupi == null){
            Toast.makeText(Enterupi_id.this,"Please Enter UPI id ",Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}
