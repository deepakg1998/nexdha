package com.nexdha.nexpay;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiConfig {

    @Multipart
    @POST("api/android_FileUploadView")
    Call uploadFile(@Part MultipartBody.Part file, @Part("file") RequestBody name);
    @Multipart
    @POST("/api/android_FileUploadView2")
    Call < ServerResponse > uploadMulFile(@Part("userid") int description,@Part("cc_no") RequestBody cardNumber, @Part MultipartBody.Part file1, @Part MultipartBody.Part file2, @Part MultipartBody.Part file3, @Part MultipartBody.Part file4);

}
