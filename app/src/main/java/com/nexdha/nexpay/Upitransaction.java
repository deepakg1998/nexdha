package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Upitransaction extends AppCompatActivity {

    public Button button40, button55, button56, button58;
    public EditText editTextTextPersonName, editTextTextPersonName4;
    public TextView textView264;
    public ImageView imageView68, imageView66;
    public ConstraintLayout hello;
    public ConstraintLayout scanner, constraintLayout30, constraintLayout26, upiiss;
    public ConstraintLayout cl1, cl2;
    public String VPA, VPA1;
    public String spaceremove;
    public Spinner spinner3;
    public String upiidonly;
    public String onylies;
    public ProgressBar progressBar22;
    public ImageView imageView67, imageView69;
    public String only2, status;
    public String nameonlyies;
    public TextView textView265, textView266, textView262;
    public ProgressBar progressBar23;
    ArrayList<String> arraylistforUPIid = new ArrayList<String>();
    public String forgettheupids;
    public String forUPIID;
    public String Forrcheckingthis = "john";
    private boolean isFirstOnResume;

    /*  private boolean isFirstOnResume1;
      private boolean isFirstOnResume2;
      public boolean isFirstOnResume3;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_upitransaction);
        this.isFirstOnResume = (savedInstanceState == null);
/*this.isFirstOnResume1 =(savedInstanceState == null);
        this.isFirstOnResume2 =(savedInstanceState == null);
        this.isFirstOnResume3 =(savedInstanceState == null);
*/
        constraintLayout30 = findViewById(R.id.constraintLayout30);
//        Again=true;
        get_allUpiid();
        /*  UPItoenter();*/
/*
        constraintLayout30.animate().translationX(-2000);
*/
        editTextTextPersonName4 = findViewById(R.id.editTextTextPersonName4);
        constraintLayout26=findViewById(R.id.constraintLayout26);
        textView264=findViewById(R.id.textView264);
        constraintLayout26.setVisibility(View.GONE);
        textView262=findViewById(R.id.textView262);
        upiiss=findViewById(R.id.upiiss);
        imageView69 = findViewById(R.id.imageView69);
        imageView69.setVisibility(View.GONE);
        imageView67 = findViewById(R.id.imageView67);
        imageView67.setVisibility(View.GONE);
        progressBar22 = findViewById(R.id.progressBar22);
        progressBar22.setVisibility(View.GONE);
        progressBar23=findViewById(R.id.progressBar23);
        progressBar23.setVisibility(View.GONE);
        button55 = findViewById(R.id.button55);
        spinner3 = findViewById(R.id.spinner3);
        button40 = findViewById(R.id.button40);
        button56 = findViewById(R.id.button56);
        button58 = findViewById(R.id.button58);
        scanner = findViewById(R.id.scanner);
        scanner.animate().translationX(0);
        cl1 = findViewById(R.id.cl1);
        cl2 = findViewById(R.id.cl2);
        cl2.setVisibility(View.GONE);
        imageView68 = findViewById(R.id.imageView68);
        editTextTextPersonName = findViewById(R.id.editTextTextPersonName);
        hello = findViewById(R.id.hello);
        hello.setTranslationX(2000);
        imageView66 = findViewById(R.id.imageView66);
        button40.setOnClickListener(v -> {
/*
                progressBar23.setVisibility(View.VISIBLE);
*/
            onResume();
            Intent intent = new Intent(Upitransaction.this, scanactivity.class);
            startActivity(intent);
        });
        button55.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  scanner.setVisibility(View.GONE);

                hello.setVisibility(View.VISIBLE);*/
                /*  get_allUpiid();*/
                /*  UPItoenter();*/
                cl1.setVisibility(View.GONE);
                cl2.setVisibility(View.VISIBLE);
                scanner.animate().translationX(-2000);
                hello.animate().translationX(0);


            }
        });
  /*      editTextTextPersonName4.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
              //  validate();
                //button56.setEnabled(false);
            }
        });*/

        imageView68.setOnClickListener(v -> {
/*
            progressBar23.setVisibility(View.VISIBLE);
*/
            onResume();

            Intent intent = new Intent(Upitransaction.this, scanactivity.class);
            startActivity(intent);
        });
        imageView66.setOnClickListener(v -> finish());
        button58.setOnClickListener(v -> {
/*
            scanner.(View.VISIBLE);
*/
            cl1.setVisibility(View.VISIBLE);
            cl2.setVisibility(View.GONE);
            scanner.animate().translationX(0);
            hello.animate().translationX(2000);


/*
            scanner.setVisibility(View.VISIBLE);
*/

        });
        button56.setOnClickListener(v -> {
            button56.setEnabled(false);
            button56.setBackgroundResource(R.drawable.pageoneconstraintthree);


            validate();
            VPA1 = editTextTextPersonName4.getText().toString();

        });


    }
    public void get_allUpiid(){
        SharedPreferences sharedPreferences = Upitransaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;
        arraylistforUPIid.clear();
        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/save_upi",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            System.out.println(jsonArray);

/*
                            System.out.println(validvpa.getString("valid"));
*/

                    /*        for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String value3 = jsonObject1.optString("upi_id");
                                System.out.println(value3);
                                System.out.println(jsonArray.length());
                            }
*/
                            if (jsonArray != null) {
                                constraintLayout26.setVisibility(View.GONE);
                                textView262.setVisibility(View.GONE);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    if (i == 0) {
                                        arraylistforUPIid.add("Name and UPI");
/*
                                        arraylistforUPIid.add("0");
*/
                                    }
                                    JSONObject dataObject = jsonArray.optJSONObject(i);
                                    String nameonlyies = dataObject.optString("name");
                                    String VPA = dataObject.optString("upi_id");
                                    //   SharedPreferences.Editor editor = spinnerpreference.edit();

                                    // editor.putString("casa_id",ids);
                                    // editor.apply();
                                    // System.out.println("This");
                                    // System.out.println(ids);
                                 /*   arraylistforUPIid.add(nameonly);
                                    arraylistforUPIid.add(upiid);*/
                                    arraylistforUPIid.add(nameonlyies +" "+"-"+ " "+ VPA);

                                }
                                // Creating adapter for spinner
                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Upitransaction.this, R.layout.spinnerss_items, arraylistforUPIid);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner3.setAdapter(dataAdapter);

                              /*  ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Makefinal);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner3.setAdapter(adapter);*/

                            }
                            else {
                                constraintLayout26.setVisibility(View.GONE);
                                textView262.setVisibility(View.GONE);

                                    arraylistforUPIid.add("No Beneficiary Available");
                                    // Creating adapter for spinner
                                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(Upitransaction.this, R.layout.spinner_item, arraylistforUPIid);
                                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    spinner3.setAdapter(dataAdapter2);


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Upitransaction.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Upitransaction.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            //-------------------------outerheader-----------------------//
            @Override
         /*   public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();*/

            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }
          /*  protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("sUpiId", VPA);
                return params;
            }*/


        };


        RequestQueue requestQueue = Volley.newRequestQueue(Upitransaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);





        spinner3 = findViewById(R.id.spinner3);
 /*   String Makefinal[] = new String[arraylistforUPIid.size()];
    for (int j = 0; j < arraylistforUPIid.size(); j++) {
        Makefinal[j] = arraylistforUPIid.get(j);
    }
    arraylistforUPIid.add("Select UPI id");

    ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Makefinal);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner3.setAdapter(adapter);*/
        upiidonly ="";
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


          /*  System.out.println(spinner3.getSelectedItem().toString());
            upiidonly = spinner3.getSelectedItem().toString();*/

/*
                                                    monthh[0] =spinner.getSelectedItem().toString();
*/
                try {


                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

//save this value in shared pref
                        String paynamelist = arraylistforUPIid.get(position);
                        upiidonly = paynamelist;
                        upiidonly = spinner3.getSelectedItem().toString();


                    }
                } catch (Exception e) {
                }

                System.out.println(upiidonly);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    /* public void onBackPressed() {
         //Execute your code here
         progressBar23.setVisibility(View.GONE);

         finish();

     }*/
 /*   public void onRestart() {
        super.onRestart();
        //When BACK BUTTON is pressed, the activity on the stack is restarted
        //Do what you want on the refresh procedure here
        progressBar23.setVisibility(View.GONE);

    }*/

    @Override
    public void onResume() {
        super.onResume();

        button56.setEnabled(true);
        button56.setBackgroundResource(R.drawable.roundedbutton);
        imageView69.setVisibility(View.GONE);
        imageView67.setVisibility(View.GONE);
        progressBar23.setVisibility(View.GONE);
        if (isFirstOnResume) {
            this.isFirstOnResume = false;
            progressBar23.setVisibility(View.GONE);
            button40.setEnabled(true);
            button40.setBackgroundResource(R.drawable.roundedbutton);
        }
        else{
            progressBar23.setVisibility(View.VISIBLE);
            button40.setEnabled(false);
            button40.setBackgroundResource(R.drawable.pageoneconstraintthree);

        }
        /*} else if (isFirstOnResume1) {
            this.isFirstOnResume1= false;
            // special case
            progressBar23.setVisibility(View.VISIBLE);
        }else if(isFirstOnResume2){
            this.isFirstOnResume2= false;
            progressBar23.setVisibility(View.GONE);
        }else if(isFirstOnResume3){
            this.isFirstOnResume3= false;
            progressBar23.setVisibility(View.VISIBLE);
        }else{
            progressBar23.setVisibility(View.GONE);
        }*/
      /*  if(Forrcheckingthis=="john"){
            progressBar23.setVisibility(View.VISIBLE);

        }
        else{
            progressBar23.setVisibility(View.GONE);

        }
*/
  /*      if (Intent == new Intent(Upitransaction.this, scanactivity.class)) {
        progressBar23.setVisibility(View.VISIBLE);
    }else{
            progressBar23.setVisibility(View.VISIBLE);
        }
        }
*/
        // Start camera on resume
/*if(Again==true){
    progressBar23.setVisibility(View.VISIBLE);
}else{
    progressBar23.setVisibility(View.GONE);

}*/


    }
/*
    public void onBackPressed() {
        super.onBackPressed();
progressBar23.setVisibility(View.GONE);
    }
*/


    private void checkingValidVPA() {
        progressBar22.setVisibility(View.VISIBLE);

        SharedPreferences sharedPreferences = Upitransaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/verify_upi",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject validvpa = new JSONObject(response);
                            System.out.println(validvpa);

                            JSONObject onlyVPA = new JSONObject(validvpa.getString("Output"));
                            System.out.println(onlyVPA);

                            status = onlyVPA.getString("status");

                            if (status.equals("OK")) {
                                only2 = onlyVPA.getString("valid");
                                nameonlyies = onlyVPA.getString("name");
                                if (only2.equals("true")) {
                                    progressBar22.setVisibility(View.GONE);
                                    imageView67.setVisibility(View.VISIBLE);
                                    imageView69.setVisibility(View.GONE);
                                    textView264.setVisibility(View.GONE);
                                    Intent intents = new Intent(Upitransaction.this, Scan.class);
                                    intents.putExtra("nameonlyies", nameonlyies);
                                    intents.putExtra("VPA", VPA1);
                                    intents.putExtra("upiidonly", upiidonly);
                                    startActivity(intents);
                                } else {
                                    progressBar22.setVisibility(View.GONE);
                                    textView264.setText("Please enter valid UPI");
                                    imageView69.setVisibility(View.VISIBLE);
                                    button56.setEnabled(true);
                                    button56.setBackgroundResource(R.drawable.roundedbutton);

                                }
                            } else {
                                progressBar22.setVisibility(View.GONE);
                                textView264.setText("Please Check the UPI");
                                imageView69.setVisibility(View.VISIBLE);
                                button56.setEnabled(true);
                                button56.setBackgroundResource(R.drawable.roundedbutton);
                            }

                            System.out.println(only2);

                            System.out.println(nameonlyies);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Upitransaction.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Upitransaction.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            //-------------------------outerheader-----------------------//
            @Override
         /*   public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();*/

            public Map<String, String> getHeaders() {
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("sUpiId", VPA1);
                return params;
            }


        };


        RequestQueue requestQueue = Volley.newRequestQueue(Upitransaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }
    public void UPItoenter(){
        SharedPreferences sharedPreferences = Upitransaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/save_upi ",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("*******************************************");
                        System.out.println(response);
                        try {
                            JSONObject checkingupis = new JSONObject(response);
                            System.out.println(checkingupis);
                            forUPIID = (checkingupis.getString("upi_id_already_exist"));
                            System.out.println(forUPIID);
                            if (forUPIID.equals("No")) {
                                constraintLayout26.setVisibility(View.GONE);

                            } else {
                                constraintLayout26.setVisibility(View.VISIBLE);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Upitransaction.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {


            //This is for Headers If You Needed
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }

            protected Map<String, String> getParams() {
                String upiidonly = getIntent().getExtras().getString("VPA");
                Map<String, String> params = new HashMap<>();
                params.put("sBeni_Upi_Id", upiidonly);
                return params;

            }


        };


        RequestQueue requestQueue = Volley.newRequestQueue(Upitransaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    public boolean validate(){
        if (editTextTextPersonName4.length() == 0 || editTextTextPersonName4.equals("") || editTextTextPersonName4 == null){
            progressBar22.setVisibility(View.GONE);
            Toast.makeText(Upitransaction.this, "Please provide UPI id ", Toast.LENGTH_LONG).show();

            button56.setEnabled(true);
            button56.setBackgroundResource(R.drawable.roundedbutton);
            return false;
        }

     /*   if (upiidonly.length() == 0 || upiidonly.equals("Name and UPI") || upiidonly == null || upiidonly.equals("")) {
            Toast.makeText(Upitransaction.this,"Please Select UPI id ",Toast.LENGTH_LONG).show();
            return false;

        }*/

        progressBar22.setVisibility(View.VISIBLE);

        checkingValidVPA();

        return true;

    }
}
