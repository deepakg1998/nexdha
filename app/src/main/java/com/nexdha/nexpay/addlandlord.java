package com.nexdha.nexpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.nexdha.nexpay.activity.RentTransactionCancelledActivity;
import com.nexdha.nexpay.activity.RentTransactionFailedActivity;
import com.nexdha.nexpay.activity.RentTransactionSuccessActivity;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.test.pg.secure.pgsdkv4.PaymentGatewayPaymentInitializer;
import com.test.pg.secure.pgsdkv4.PaymentParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class addlandlord extends AppCompatActivity {
    public static String account_number, ifsc, beneficiary_name = "null", beneficiary_id, vendor_code, beneficiary_phone, purpose_of_payment = "null", transaction_type = "null", card_type, beneficiary_amount, calc_per, eta_tran, gateway_init, amex_percent, transaction_display;
    public ImageView imageView84;
    public Spinner spinner9;
    public Button button69;
    Spinner spinner;
    public ConstraintLayout constraintLayout38, CSVLAY;
    public LinearLayout linlay;
    public String sendgateway;
    public EditText editTextTextPersonName6;
    public ConstraintLayout constraintLayout39;
    String n_order_id, eb_order_id;
    public ConstraintLayout snackbar;
    String t_type = "T+1";
    SwitchCompat amexSwitch;
    boolean amexType;
    LayoutInflater inflater;
    Dialog loadingDialog;
    String cardType;
    String convenienceFee;
    Boolean isSelectLandlord = true, isEnterAmount = true;
    String amount_from_gateway, card_from_gateway, description_from_gateway, email_from_gateway, error_from_gateway, hash_from_gateway, card_name_from_gateway, order_number_from_gateway, channe_from_gateway, mode_of_pay_from_gateway, phone_from_gateway;
    String response_code_from_gateway, response_message_from_gateway, bank_ref_eb, tx_id_from_gateway, udf1_gateway, udf2_gateway, udf3_gateway, udf4_gateway, udf5_gateway;
    String basis_AddressLine1, basis_AddressLine2, basis_Amountdone, basis_Cardmask, basis_Citydone, basis_Countrydone, basis_Currencydone, basis_Descriptiongiven;
    String basis_Emailgiven, basis_Errordescription, basis_Hashvalue, basis_cardName, basis_Orderno, basis_channelpayment, basis_modeofpay, basis_cell, basis_responsecode;
    String basis_responsemessage, basis_Statedone, basis_Transactionid, basis_udf01, basis_udf02, basis_udf03, basis_udf04, basis_udf05, basis_pincode;
    String cf_amount, cf_error_desc, cf_signature, cf_order_id, cf_payment_mode, cf_response_message, cf_transaction_id, cf_dt;
    String amount_calculation, final_calculated_amount;
    String cashfree_token_var, retrying_tran;
    ArrayList<String> id, first_name, second_name, phone, email, address_id, Account_number, IFSC, location, notes, is_active, dle, account_type;

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        inflater = (LayoutInflater) addlandlord.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(addlandlord.this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.inflate_layout, null));
        loadingDialog.setCancelable(false);
        loadingDialog.show();

        setContentView(R.layout.activity_addlandlord);
        spinner9 = findViewById(R.id.spinner9);
        CSVLAY = findViewById(R.id.CSVLAY);
        linlay = findViewById(R.id.linlay);
        button69 = findViewById(R.id.button69);
        constraintLayout38 = findViewById(R.id.constraintLayout38);
        constraintLayout38.setVisibility(View.GONE);
        CSVLAY.setVisibility(View.GONE);
        editTextTextPersonName6 = findViewById(R.id.editTextTextPersonName6);
        imageView84 = findViewById(R.id.imageView84);

        TextView showConvenienceFee = findViewById(R.id.convenience_fee);
        showConvenienceFee.setText(Dashboard.t1 + "%");
        TextView showEta = findViewById(R.id.eta1);
        showEta.setText(Dashboard.etaOne);
        id = new ArrayList<>();
        first_name = new ArrayList<>();
        second_name = new ArrayList<>();
        phone = new ArrayList<>();
        email = new ArrayList<>();
        address_id = new ArrayList<>();
        Account_number = new ArrayList<>();
        IFSC = new ArrayList<>();
        location = new ArrayList<>();
        notes = new ArrayList<>();
        is_active = new ArrayList<>();
        dle = new ArrayList<>();
        account_type = new ArrayList<>();
        save_casa_rental();
        get_order_id();

        amexSwitch = findViewById(R.id.switch_rental);
        amexSwitch.setText("AMEX / Corporate Cards (" + Dashboard.amexPercant + "%)");
        amexSwitch.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b) {
                amexType = true;
            } else {
                amexType = false;
            }

        });

        button69.setOnClickListener(view -> validation());
        imageView84.setOnClickListener(view -> finish());
        linlay.setOnClickListener(view -> {
            Intent intent = new Intent(addlandlord.this,
                    landlord.class);
            startActivity(intent);
        });
        spinner9.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                try {
                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();
                    } else {

//save this value in shared pref
                        // String ispid = id.get(position);
                        System.out.println("This ------------------ is---------------ispid------------");
                        //System.out.println(ispid);
                        String isplists = id.get(position);
                        System.out.println(isplists);
                        String ispvendorlist = first_name.get(position);
                        System.out.println(ispvendorlist);

                        String final_acc_number = Account_number.get(position);
                        System.out.println(final_acc_number);

                        String final_ifsc = IFSC.get(position);
                        System.out.println(final_ifsc);

                        String final_phone = phone.get(position);
                        System.out.println(final_phone);

                        //  beneficiary_id = id;
                        beneficiary_name = isplists;
                        vendor_code = ispvendorlist;
                        account_number = final_acc_number;
                        ifsc = final_ifsc;
                        beneficiary_phone = final_phone;
                    /*    SharedPreferences.Editor editor = spinnerpreference.edit();
                        System.out.println("Hi I am here");
                        System.out.println(ispid);
                        editor.putString("id",ispid);
                        editor.apply();

                        */

                        //-------casa name save-------//
                     /*   SharedPreferences.Editor editorcasaname = spinnernamepreference.edit();
                        System.out.println("Hi I am here");
                        System.out.println(isplists);
                        editorcasaname.putString("first_name",isplists);
                        editorcasaname.apply();

                        */

                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @SuppressLint("DefaultLocale")
    public void validation() {
        spinner = findViewById(R.id.spinner9);
        beneficiary_name = spinner.getSelectedItem().toString();
        sendgateway = editTextTextPersonName6.getText().toString();
        snackbar = findViewById(R.id.landlord_snackbar);
        if (beneficiary_name.equals("Select landlord")) {
           // Snackbar.make(snackbar, "Please select Landlord", Snackbar.LENGTH_LONG).setDuration(4000).show();
            Toast.makeText(addlandlord.this, "Please select landlord", Toast.LENGTH_SHORT).show();
            isSelectLandlord = false;
        } else {
            isSelectLandlord = true;
        }
        if (sendgateway.isEmpty() || editTextTextPersonName6.length() == 0 || editTextTextPersonName6 == null) {
            assert editTextTextPersonName6 != null;
            editTextTextPersonName6.setError("Please Enter amount");
            isEnterAmount = false;
        } else if (Float.parseFloat(editTextTextPersonName6.getText().toString()) < 100) {
            editTextTextPersonName6.setError("Minimum amount should be ₹100");
            isEnterAmount = false;
        } else if (Float.parseFloat(editTextTextPersonName6.getText().toString()) > 600000) {
            editTextTextPersonName6.setError("Maximum amount should be ₹6,00,000");
            isEnterAmount = false;
        } else {
            isEnterAmount = true;
        }
        if (isSelectLandlord && isEnterAmount) {
            if (amexType) {
                cardType = "AMEXDINER";
                convenienceFee = Dashboard.amexPercant;
            } else {
                cardType = "Other";
                convenienceFee = Dashboard.t1;
            }
            sendgateway = editTextTextPersonName6.getText().toString();
            amount_calculation = String.valueOf(((Double.parseDouble(sendgateway) / 100) * Double.parseDouble(convenienceFee)) + Double.parseDouble(sendgateway));
            final_calculated_amount = String.valueOf(Double.parseDouble(String.format("%.2f", Double.parseDouble(amount_calculation))));
            System.out.println(final_calculated_amount);
            System.out.println(cardType);
            aggrepay_transaction();
        }

    }

    private void get_order_id() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, constant.SERVER + "/api/order_id",
                response -> {
                    try {
                        JSONObject respontran = new JSONObject(response);
                        n_order_id = respontran.getString("transactionid");
                        System.out.println("this is the trrrrrrrrrrrrrrrrrrrrrransaaaaaaaaaaaaaction id");
                        System.out.println(n_order_id);
                        eb_order_id = "NEXEB" + n_order_id;
                        /*if (retrying_tran.equals("yes") || retrying_tran == "yes"){
                            cashfree_token();
                        }*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                message = "cannot connect";
            } else if (error instanceof AuthFailureError) {
                message = "";
            } else if (error instanceof NoConnectionError) {
                message = "cannot connect";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout";
            }
            if (!message.isEmpty()) {
                Toast.makeText(addlandlord.this, "", Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(addlandlord.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void aggrepay_transaction() {

        final PaymentParams pgPaymentParams = new PaymentParams();
        pgPaymentParams.setAPiKey("fe4023d4-db2d-49c4-902b-1e24dd602a48");//Account2
        pgPaymentParams.setAmount(final_calculated_amount);
        pgPaymentParams.setEmail(Dashboard.pay_details_email);
        pgPaymentParams.setName(Dashboard.pay_details_name);
        pgPaymentParams.setPhone(Dashboard.responsephonenumber);
        pgPaymentParams.setOrderId(n_order_id);
        pgPaymentParams.setCurrency(SampleAppConstants.PG_CURRENCY);
        pgPaymentParams.setDescription("Me");
        pgPaymentParams.setCity(cardType);
        pgPaymentParams.setState("rental");
        pgPaymentParams.setAddressLine1(Dashboard.onedayonly);
        pgPaymentParams.setAddressLine2(sendgateway);
        pgPaymentParams.setZipCode(SampleAppConstants.PG_ZIPCODE);
        pgPaymentParams.setCountry("Rent");
        pgPaymentParams.setReturnUrl(SampleAppConstants.PG_RETURN_URL);
        pgPaymentParams.setMode(SampleAppConstants.PG_MODE);
        pgPaymentParams.setUdf1(Dashboard.userid);
        pgPaymentParams.setUdf2("udf2");
        pgPaymentParams.setUdf3(beneficiary_name);
        pgPaymentParams.setUdf4(vendor_code);
        pgPaymentParams.setUdf5(SampleAppConstants.PG_UDF5);
        PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, addlandlord.this);
        pgPaymentInitialzer.initiatePaymentProcess();
    }


    //Setting payment listener (paste this line after init() method)

    private void sendsuccessfulonedaysms() {

        SharedPreferences sharedPreferences = addlandlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/t1_TSMS",
                response -> {
                    // Toast.makeText(Payment.this,"SMS Sent",Toast.LENGTH_LONG).show();
                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                message = "cannot connect";
            } else if (error instanceof AuthFailureError) {
                message = "";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout";
            }
            if (!message.isEmpty()) {
                Toast.makeText(addlandlord.this, "", Toast.LENGTH_SHORT).show();
            }
        }) {
            protected Map<String, String> getParams() {
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);


                params.put("amt", amount_from_gateway);
                params.put("transid", tx_id_from_gateway);
                // params.put("amt","100.00");
                // params.put("transid","RPMA23456");
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(addlandlord.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void sendsuccessful_sds_sms() {

        SharedPreferences sharedPreferences = addlandlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/sds_sms",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(Payment.this,"SMS Sent",Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(addlandlord.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            protected Map<String, String> getParams() {
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);


                params.put("amt", amount_from_gateway);
                params.put("transid", tx_id_from_gateway);
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(addlandlord.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PGConstants.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String paymentResponse = data.getStringExtra(PGConstants.PAYMENT_RESPONSE);
                    System.out.println("paymentResponse: " + paymentResponse);


                    if (paymentResponse.equals("null")) {
                        System.out.println("Transaction Error!");

                        new MaterialStyledDialog.Builder(this)
                                .setTitle("Error")
                                .setDescription("There was an error in your transaction, if the amount has been debited from your account it will be credited to your beneficiary bank account within T+2 bank working days.")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.transactionfailed)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(true)
                                .setCancelable(false)
                                .withDivider(true)
                                .setPositiveText("Got it")
                                .onPositive((dialog, which) -> finish())
                                .show();


                    } else {

                        JSONObject response = new JSONObject(paymentResponse);

                        basis_AddressLine1 = response.getString("address_line_1");
                        basis_AddressLine2 = response.getString("address_line_2");
                        amount_from_gateway = response.getString("amount");
                        basis_Cardmask = response.getString("cardmasked");
                        basis_Citydone = response.getString("city");
                        basis_Countrydone = response.getString("country");
                        basis_Currencydone = response.getString("currency");
                        basis_Descriptiongiven = response.getString("description");
                        basis_Emailgiven = response.getString("email");
                        basis_Errordescription = response.getString("error_desc");
                        basis_Hashvalue = response.getString("hash");
                        basis_cardName = response.getString("name");
                        basis_Orderno = response.getString("order_id");
                        basis_channelpayment = response.getString("payment_channel");
                        basis_modeofpay = response.getString("payment_mode");
                        basis_cell = response.getString("phone");
                        basis_responsecode = response.getString("response_code");
                        basis_responsemessage = response.getString("response_message");

                        basis_Statedone = response.getString("state");
                        tx_id_from_gateway = response.getString("transaction_id");

                       /* SharedPreferences.Editor editor = sharedpreferences3.edit();
                        editor.putString("transaction_id",paymentResponse);
                        editor.apply();

                        */


                        basis_udf01 = response.getString("udf1");
                        basis_udf02 = response.getString("udf2");
                        basis_udf03 = response.getString("udf3");
                        basis_udf04 = response.getString("udf4");
                        basis_udf05 = response.getString("udf5");
                        basis_pincode = response.getString("zip_code");
                    }
                    if (basis_responsemessage.equals("Transaction successful")) {
                        if ((basis_Statedone.equals("t1")) || (basis_Statedone.equals("t1_azypay")) || basis_Statedone.equals("hds")) {
                            sendsuccessfulonedaysms();
                        } else if (basis_Statedone.equals("t2")) {
                            sendsuccessfulsms();
                        } else if (basis_Statedone.equals("t0sds") || basis_Statedone.equals("t0hds")) {
                            sendsuccessful_sds_sms();
                        } else if (basis_Statedone.equals("t5")) {
                            sendsuccessful_t_five_sms();
                        }
                        Intent intent = new Intent(addlandlord.this, RentTransactionSuccessActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        if (new_payment.transaction_type.equals("t5")) {
                            Intent intent = new Intent(addlandlord.this, RentTransactionCancelledActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            Intent intent = new Intent(addlandlord.this, RentTransactionCancelledActivity.class);
                            startActivity(intent);
                            finish();
                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Intent intent = new Intent(addlandlord.this, RentTransactionFailedActivity.class);
                startActivity(intent);
                finish();
                //Write your code if there's no result
            }



      /*  SharedPreferences sharedPreferencescasaname = Payment.this.getSharedPreferences("spinnernameprefs", Context.MODE_PRIVATE);
        final String casaname=sharedPreferencescasaname.getString("first_name","");
        System.out.println("this is the nnnnnnnnnnnnnnaaaaaaaaaaaaaammmmmmmmmmmmmmeeeeeeeeeeee limit");
        System.out.println(casaname);
        final String casa2name=casaname;
        */


            StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/save_PG_Tran_Response",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            System.out.println("*******************************************");
                            System.out.println(response);


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ServerError) {
                        message = "";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ParseError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection Timedout......please check your internet connection";
                    }
                    if (!message.isEmpty()) {
                        Toast.makeText(addlandlord.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }) {


                //This is for Headers If You Needed

                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("address_line_1", basis_AddressLine1);
                    params.put("address_line_2", basis_AddressLine2);
                    params.put("amount", amount_from_gateway);
                    params.put("cardmasked", basis_Cardmask);
                    params.put("city", basis_Citydone);
                    params.put("country", basis_Countrydone);
                    params.put("currency", basis_Currencydone);
                    params.put("description", basis_Descriptiongiven);
                    params.put("email", basis_Emailgiven);
                    params.put("error_desc", "Waste");
                    params.put("hash", basis_Hashvalue);
                    params.put("name", basis_cardName);
                    params.put("order_id", basis_Orderno);
                    params.put("payment_channel", basis_channelpayment);
                    params.put("payment_mode", basis_modeofpay);
                    params.put("phone", basis_cell);
                    params.put("response_code", basis_responsecode);
                    params.put("response_message", basis_responsemessage);
                    params.put("state", basis_Statedone);
                    params.put("transaction_id", tx_id_from_gateway);
                    params.put("udf1", basis_udf01);
                    params.put("udf2", basis_udf02);
                    params.put("udf3", basis_udf03);
                    params.put("udf4", basis_udf04);
                    params.put("udf5", basis_udf05);
                    params.put("zip_code", "123456");
                    return params;

                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(addlandlord.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        }
    }


    private void sendsuccessfulsms() {

        SharedPreferences sharedPreferences = addlandlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/TSMS",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(Payment.this,"SMS Sent",Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(addlandlord.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            protected Map<String, String> getParams() {
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);


                params.put("amt", amount_from_gateway);
                params.put("transid", tx_id_from_gateway);
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(addlandlord.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void sendsuccessful_t_five_sms() {

        SharedPreferences sharedPreferences = addlandlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/t5_TSMS",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(Payment.this,"SMS Sent",Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(addlandlord.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            protected Map<String, String> getParams() {
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);


                params.put("amt", amount_from_gateway);
                params.put("transid", tx_id_from_gateway);
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(addlandlord.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }


    public void save_casa_rental() {
        try {

            id.clear();
            first_name.clear();
            phone.clear();
            StringRequest stringRequest1 = new StringRequest(Request.Method.GET, constant.SERVER + "/api/save_casa_rental",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //beatslist.add("Select Beats");
                            try {

                                JSONArray jsonArray = new JSONArray(response);
                                if (jsonArray.length() > 0) {
                                    CSVLAY.setVisibility(View.VISIBLE);
                                    loadingDialog.dismiss();
                                    constraintLayout38.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        if (i == 0) {
                                            id.add("Select landlord");
                                            first_name.add("0");
                                            phone.add("0");
                                            email.add("0");
                                            Account_number.add("0");
                                            IFSC.add("0");
                                            account_type.add("0");
                                        }
                                        JSONObject dataObject = jsonArray.optJSONObject(i);
                                        String beats = dataObject.optString("first_name");
                                        String nick = dataObject.optString("second_name");
                                        String ids = dataObject.optString("id");
                                        String vid = dataObject.optString("notes");
                                        String vaccount_number = dataObject.optString("Account_number");
                                        String vifsc = dataObject.optString("IFSC");
                                        String vphone = dataObject.optString("phone");
                                        //String vaccount_type = dataObject.optString("account_type");

                                        System.out.println("this is vid");
                                        System.out.println(vid);

                                        id.add(beats + "(" + nick + ")");
                                        first_name.add(vid);
                                        phone.add(vphone);
                                        //  email.add(vaccount_number);
                                        Account_number.add(vaccount_number);
                                        IFSC.add(vifsc);
                                        //account_type.add(vaccount_type);

                                    }
                                    Log.e("Aakash", id + "");
                                    // Creating adapter for spinner
                                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(addlandlord.this, R.layout.spinner_item, id);
                                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    spinner9.setAdapter(dataAdapter2);


                                } else {
                                    loadingDialog.dismiss();
                                    constraintLayout38.setVisibility(View.VISIBLE);
                                    CSVLAY.setVisibility(View.GONE);

                                    id.add("No landlord details");
                                    first_name.add("0");
                                    phone.add("0");
                                    email.add("0");
                                    Account_number.add("0");
                                    IFSC.add("0");
                                    account_type.add("0");
                                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(addlandlord.this, R.layout.spinner_item, id);
                                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    spinner9.setAdapter(dataAdapter2);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String message = "";

                            if (error instanceof NetworkError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ServerError) {
                                Log.e("stackcheck1111", "3" + error.toString());
                                NetworkResponse response = error.networkResponse;
                                if (response != null) {
                                    Log.d("qqqqqqqqqqq", String.valueOf(response.statusCode));

                                    int statusCode = response.statusCode;
                                    if (statusCode == 404) {
                                        message = "No ISP Available in this Company";
                                        Log.d("qqqqqq", " " + Arrays.toString(response.data));


                                    } else {
                                        message = "The server could not be found. Please try again after some time!!";

                                    }
                                } else {
                                    message = "The server could not be found. Please try again after some time!!";

                                }
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                            }
                            Log.e("eeeee", "onErrorResponse: " + error.toString());
                            if (!message.isEmpty()) {
                                Toast.makeText(addlandlord.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }) {

                final SharedPreferences sharedPreferences = addlandlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                final String token1 = sharedPreferences.getString("token", "");
                final String token200 = token1;
                //-------------spinnerheader-----------------//

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String T = "";
                    Map<String, String> params = new HashMap<>();


                    // params.put("Content-Type", "application/json; charset=UTF-8");
                    try {
                        JSONObject response1 = new JSONObject(token200);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        // do your work with response object
                        T = response1.getString("token");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("44444444444444444444444444");
                    System.out.println(token200);
                    ///check t!blank

                    String TokenS = "token " + T;
                    System.out.println(T);

                    params.put("Authorization", TokenS);

                    return params;
                }

            };


            RequestQueue requestQueue1 = Volley.newRequestQueue(addlandlord.this);
            stringRequest1.setShouldCache(false);
            requestQueue1.add(stringRequest1);
        } catch (Exception e) {

        }
    }


}