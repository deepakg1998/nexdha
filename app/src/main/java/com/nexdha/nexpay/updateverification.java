package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.appupdate.AppUpdateOptions;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class updateverification extends AppCompatActivity {
    public static String dbversion, compareversionName;
    private AppUpdateManager appUpdateManager;
    private static final int IMMEDIATE_APP_UPDATE_REQ_CODE = 124;
    public final int MY_REQUEST_CODE = 100;
    Dialog loadingDialog;
    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appupdateverification);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_security);
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.loading_layout, null));
        loadingDialog.setCancelable(false);
        loadingDialog.show();
        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
        //checkUpdate();/////////////in app update

        try {
            compareversionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }



        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/up_app",
                response -> {
                    try {
                        JSONArray jsonarray = new JSONArray(response);
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject jsonobject = jsonarray.getJSONObject(i);
                            dbversion = jsonobject.getString("version");
                        }
                        System.out.println("This is version update");
                        System.out.println(dbversion);
                        System.out.println(compareversionName);
                        if (dbversion == "6.1.8" || dbversion.equals("6.1.8")) {
                            Intent intent = new Intent(updateverification.this, Dashboard.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        } else {
                            Intent intent = new Intent(updateverification.this, UpdateApp.class);
                            startActivity(intent);
                            finish();
                        }




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
                    String message = "";

                    if (error instanceof NetworkError) {
                        //     message = "cannot connect";
                        new MaterialStyledDialog.Builder(updateverification.this)
                                .setTitle("Internet Error")
                                .setDescription("There will be issue displaying your info,Check Your Interenet Connection")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.warninginternet)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(false)
                                .setCancelable(false)
                                .withDivider(true)
                                .setNegativeText("Cancel")
                                .onNegative((dialog, which) -> {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                })
                                .setPositiveText("Open Settings")
                                .onPositive((dialog, which) -> {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                })
                                .show();
                    } else if (error instanceof AuthFailureError) {
                        message = "";
                    } else if (error instanceof NoConnectionError) {
                        message = "cannot connect";
                    } else if (error instanceof ParseError) {
                        message = "Connection Timedout";
                    }
                    if (!message.isEmpty()) {
                        Toast.makeText(updateverification.this, message, Toast.LENGTH_SHORT).show();
                    }
                });



        RequestQueue requestQueue = Volley.newRequestQueue(updateverification.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
    private void checkUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                try {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, this, AppUpdateOptions.newBuilder(AppUpdateType.FLEXIBLE).setAllowAssetPackDeletion(true).build(), MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(this, "no update available", Toast.LENGTH_SHORT).show();
                System.out.println("No update available");
            }

        });

        appUpdateManager.registerListener(installStateUpdatedListener);


    }

    private final InstallStateUpdatedListener installStateUpdatedListener = installState -> {
        if (installState.installStatus() == InstallStatus.DOWNLOADED) {
           // popupSnackBarForCompleteUpdate();
        }


    };

    @Override
    protected void onStop() {

        if (appUpdateManager != null) {
            appUpdateManager.unregisterListener(installStateUpdatedListener);
        }

        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(result -> {
            if (result.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                try {
                    appUpdateManager.startUpdateFlowForResult(result, AppUpdateType.IMMEDIATE, updateverification.this
                            , MY_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void popupSnackBarForCompleteUpdate() {


    }

    private void startUpdateFlow(AppUpdateInfo appUpdateInfo) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                Toast.makeText(this, "Update failed", Toast.LENGTH_SHORT).show();
                System.out.println("Update failed " + resultCode);


            }
        }
    }
    @Override
    public void onBackPressed() {
       finishAffinity();

    }
}
