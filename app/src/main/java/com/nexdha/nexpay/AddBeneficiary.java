package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SECRET_KEY;
import static com.nexdha.nexpay.constant.SERVER;
import static com.nexdha.nexpay.constant.SITE_KEY;
import static com.nexdha.nexpay.constant.url;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class AddBeneficiary extends AppCompatActivity {
    private ImageView imageview10;
    EditText firstname, accountnumber, reenteraccountnumber, ifsccode, beneficiaryemail, beneficiarynumber, nickname;
    TextView textView, ifsclocation;
    Button save;
    ProgressBar progressifsc;
    LottieAnimationView progressbene;
    String bankifsc, ifscgetstring, ifscbranchstore, ifscbankstore, ifscstate;
    String beneemail = "";
    private ImageView warning;
    String previousActivity, permissionsave;
    String api_id = null;
    String reason = null;
    String nickname_parameter;
    String userfirstname;
    String resultbenenumber;
    String firstvendorcode;
    String secondvendorcode;
    String bankaccount;
    RequestQueue queue;
    String TAG = MainActivity.class.getSimpleName();
    LayoutInflater inflater;
    Dialog loadingDialog;
    StringBuilder sb;
    String getKey = MainActivity.getNativeKey1();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pagethree);
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.save_success, null));
        loadingDialog.setCancelable(false);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        activity_log_addbeneficiary();
        sessionCheck();

        Intent intent = getIntent();
        previousActivity = intent.getStringExtra("FROM_ACTIVITY");
        progressbene = findViewById(R.id.progressBar3);
        progressbene.setVisibility(View.GONE);


        firstname = findViewById(R.id.editText2);
        nickname = findViewById(R.id.editText17);
        accountnumber = findViewById(R.id.editText7);
        reenteraccountnumber = findViewById(R.id.editText);
        ifsccode = findViewById(R.id.editText13);
        beneficiaryemail = findViewById(R.id.editText10);
        beneficiarynumber = findViewById(R.id.editText14);
        textView = findViewById(R.id.textView8);
        queue = Volley.newRequestQueue(getApplicationContext());
        progressifsc = findViewById(R.id.progressBar5);
        progressifsc.setVisibility(View.GONE);


        save = (Button) findViewById(R.id.button5);
        // save.setEnabled(false);
        // save.setBackgroundResource(R.drawable.pageoneconstraintthree);
        save.setEnabled(true);
        save.setBackgroundResource(R.drawable.roundedbutton);
        save.setTextColor(Color.WHITE);
        save.setOnClickListener(v -> {


            if (!Validationacc()) {
                return;
            } else if (api_id.equals("1")) {
                new MaterialStyledDialog.Builder(AddBeneficiary.this)
                        .setTitle("Info")
                        .setDescription(reason)
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.warning1)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .setNegativeText("Cancel")
                        .onNegative((dialog, which) -> {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);

                            dialog.dismiss();
                        })
                        .setPositiveText("Proceed")
                        .onPositive((dialog, which) -> {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            // dialog.dismiss();

                            save.setEnabled(false);
                            save.setBackgroundResource(R.drawable.pageoneconstraintthree);

                            activity_log_save_beneficiary();
                            addbene();
                            //checkRecaptcha();
                        })
                        .show();
            } else {
                activity_log_save_beneficiary();
                //checkRecaptcha();
                addbene();
            }


        });
        warning = (ImageView) findViewById(R.id.imageView42);
        warning.setOnClickListener(v -> new MaterialStyledDialog.Builder(AddBeneficiary.this)
                .setTitle("Note")
                .setDescription("Using credit card to send money to your own bank account is prohibited")
                .setHeaderColor(R.color.md_divider_white)
                .setIcon(R.drawable.warning1)
                .withDarkerOverlay(true)
                .withDialogAnimation(true)
                .setCancelable(false)
                .withDivider(true)
                .setPositiveText("Got it")
                .onPositive((dialog, which) -> dialog.dismiss())
                .show());
        imageview10 = (ImageView) findViewById(R.id.imageView10);
        imageview10.setOnClickListener(v -> finish());


        ifsccode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {


                progressifsc.setVisibility(View.VISIBLE);


                if (s.length() >= 11) {
                    System.out.println("This is method initialisation");
                    bankifsc = ifsccode.getText().toString().trim();
                    ifscmethod();
                } else if (s.length() == 0) {
                    progressifsc.setVisibility(View.GONE);
                }


                // TODO Auto-generated method stub
            }
        });


    }

    private void checkRecaptcha() {
        SafetyNet.getClient(AddBeneficiary.this).verifyWithRecaptcha(SITE_KEY.trim())
                .addOnSuccessListener(this, response -> {
                    if (!Objects.requireNonNull(response.getTokenResult()).isEmpty()) {
                        handleSiteVerify(response.getTokenResult());
                    }
                })
                .addOnFailureListener(this, e -> {
                    if (e instanceof ApiException) {
                        ApiException apiException = (ApiException) e;
                        int status = apiException.getStatusCode();
                        System.out.println("Error code: " + status);
                        Log.d(TAG, "Error message: " +
                                CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                    } else {
                        Log.d(TAG, "Unknown type of error: " + e.getMessage());
                    }
                });
    }

    private void handleSiteVerify(String responseToken) {
        StringRequest request = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("success")) {
                            addbene();
                            //Toast.makeText(AddBeneficiary.this, "You are not a robot", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AddBeneficiary.this, "Human verification failed. Please try again later ", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "JSON exception: " + ex.getMessage());

                    }

                },
                error -> {

                    Toast.makeText(AddBeneficiary.this, "Server error, Please try again later", Toast.LENGTH_SHORT).show();
                    finish();


                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("secret", SECRET_KEY.trim());
                params.put("response", responseToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);


    }

    private void sessionCheck() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/counter_check",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println(response);
                        String getResponse = jsonObject.getString("detail");

                        if (getResponse.equals("1")) {
                            String allowUser = jsonObject.getString("allow_user");
                            if (allowUser.equals("0")) {
                                final LayoutInflater layoutInflaterBlock = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog blockUserDialog = new Dialog(this);
                                blockUserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                blockUserDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                blockUserDialog.setContentView(layoutInflaterBlock.inflate(R.layout.block_user_layout, null));
                                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                blockUserDialog.getWindow().setLayout(width, height);
                                blockUserDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                blockUserDialog.setCancelable(false);
                                blockUserDialog.show();
                            } else {
                                //Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            }
                        } else if (getResponse.equals("Invalid token")) {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            });
                        } else {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("auth", TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public void addbene() {
        progressbene.setVisibility(View.VISIBLE);
        userfirstname = firstname.getText().toString().trim();
        final String nick_name = nickname.getText().toString().trim();
        bankaccount = accountnumber.getText().toString().trim();
        beneemail = beneficiaryemail.getText().toString().trim();
        if (beneemail.isEmpty()) {
            beneemail = "venkat@nexdha.com";
        }
        final String benenumber = beneficiarynumber.getText().toString().trim();
        resultbenenumber = benenumber.substring(Math.max(benenumber.length() - 10, 0)).replaceAll("-", "");
        final String bankreaccount = reenteraccountnumber.getText().toString().trim();
        bankifsc = ifsccode.getText().toString().trim();
        boolean temp = true;


        SharedPreferences sharedPreferences = AddBeneficiary.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/save_casa_2022",
                response -> {
                    progressbene.setVisibility(View.GONE);
                    System.out.println("*******************************************");
                    System.out.println(response);
                    try {
                        JSONObject response4 = new JSONObject(response);
                        System.out.println("**********this is id********");
                        String status = response4.getString("status");
                        if (status.equals("Token not generated")) {
                            Toast.makeText(this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                        } else {
                            loadingDialog.show();
                        }

                        new Handler().postDelayed(this::finish, 2000);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {
            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(AddBeneficiary.this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            //This is for Headers If You Needed

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                if (nick_name.equals(null) || nick_name == "" || nick_name.equals("")) {
                    nickname_parameter = userfirstname;
                } else {
                    nickname_parameter = nick_name;
                }

                try {
                    String getHashKey = resultbenenumber + userfirstname + nickname_parameter + beneemail + bankaccount + bankifsc + getKey;
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    byte[] digest = md.digest(getHashKey.getBytes());
                    sb = new StringBuilder();
                    for (int i = 0; i < digest.length; i++) {
                        sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                    }
                    System.out.println(sb);

                } catch (NoSuchAlgorithmException e) {
                    throw new RuntimeException(e);
                }

                params.put("name", userfirstname);
                params.put("nick_name", nickname_parameter);
                params.put("phone", resultbenenumber);
                params.put("email", beneemail);
                params.put("bankAccount", bankaccount);
                params.put("ifsc", bankifsc);
                //params.put("bank_branch", ifscbranchstore);
                //params.put("bank_name", ifscbankstore);
                //params.put("location", "removed(01-07-2019)");
                //params.put("notes", "none");
                params.put("user_type", sb.toString());

                return params;

            }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            String T = "";
            Map<String, String> params = new HashMap<>();


            // params.put("Content-Type", "application/json; charset=UTF-8");
            try {
                JSONObject response1 = new JSONObject(token200);
                System.out.println("!!!!!!!!!!!!!!!!!!");
                System.out.println(response1.getString("token"));
                // do your work with response object
                T = response1.getString("token");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println("44444444444444444444444444");
            System.out.println(token200);
            ///check t!blank

            String TokenS = "token " + T;
            System.out.println(T);

            params.put("Authorization", TokenS);

            return params;
        }
    };


    //    if (useremail.matches(emailPattern)) {
    //
    //   } else {
    //      Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
    //  }


    RequestQueue requestQueue = Volley.newRequestQueue(AddBeneficiary.this);
    stringRequest.setShouldCache(false);
    requestQueue.add(stringRequest);

}

//--------second add vendor api----------//

//-----end-----//
//--------third add vendor api----------//


//--end----//


    @Override
    public void onStop() {
        super.onStop();
        progressbene.setVisibility(View.GONE);
    }

    private boolean Validationacc() {
        final String bankaccount = accountnumber.getText().toString().trim();
        final String bankreaccount = reenteraccountnumber.getText().toString().trim();
        final String userfirstname = firstname.getText().toString().trim();
        final String benenumber = beneficiarynumber.getText().toString().trim();
        bankifsc = ifsccode.getText().toString().trim().toUpperCase(Locale.ROOT);


        if (userfirstname.isEmpty()) {
            firstname.setError("Please Enter Beneficiary name", null);
            firstname.requestFocus();
            return false;
        } else {
            firstname.clearFocus();
        }
        if (userfirstname.length() >= 50) {
            firstname.setError("Please Enter a Valid Name", null);
            firstname.requestFocus();
            return false;
        } else {
            firstname.clearFocus();
        }

        if (bankaccount.isEmpty()) {
            accountnumber.setError("Please Enter Account Number", null);
            accountnumber.requestFocus();
            return false;
        } else {
            accountnumber.clearFocus();
        }
        if (bankreaccount.isEmpty()) {
            reenteraccountnumber.setError("Please Re-enter Account Number", null);
            reenteraccountnumber.requestFocus();
            return false;
        } else {
            reenteraccountnumber.clearFocus();
        }
        if (!bankaccount.equals(bankreaccount)) {
            reenteraccountnumber.setError("Account Numbers Does not match", null);
            reenteraccountnumber.requestFocus();
            return false;
        } else {
            reenteraccountnumber.clearFocus();
        }


        if (bankifsc.isEmpty()) {
            ifsccode.setError("Please Enter IFSC", null);
            ifsccode.requestFocus();
            return false;
        } else {
            ifsccode.clearFocus();
        }
        if (bankifsc.length() != 11) {
            ifsccode.setError("Please Enter Valid IFSC", null);
            ifsccode.requestFocus();
            //Toast.makeText(AddBeneficiary.this,"Please Enter Valid IFSC ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            ifsccode.clearFocus();
        }

        if (bankifsc.equals("PYTM0123456")) {
            ifsccode.setError("Paytm bank not allowed", null);
            ifsccode.requestFocus();
            return false;
        } else {
            ifsccode.clearFocus();
        }

        if (benenumber.isEmpty()) {
            beneficiarynumber.setError("Please Enter Beneficiary number", null);
            beneficiarynumber.requestFocus();
            return false;
        } else {
            beneficiarynumber.clearFocus();
        }

        if (benenumber.length() != 10) {
            beneficiarynumber.setError("Invalid Beneficiary number", null);
            beneficiarynumber.requestFocus();
            return false;
        } else {
            beneficiarynumber.clearFocus();
        }

        if (ifscgetstring.isEmpty()) {
            ifsccode.setError("Click save once you see the Bank branch below entered IFSC code", null);
            ifsccode.requestFocus();
            return false;
        } else {
            ifsccode.clearFocus();
        }


        if (ifscgetstring.equals("Not Found")) {
            ifsccode.setError("Please Enter a Valid IFSC", null);
            ifsccode.requestFocus();
            return false;
        } else {
            ifsccode.clearFocus();
        }

        return true;
    }


    //-----------------profiledetails---------------------//
    private void activity_log_addbeneficiary(){

        SharedPreferences sharedPreferences = AddBeneficiary.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                response -> {
                    //Nothing

                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                //   message = "cannot profile connect";
                new MaterialStyledDialog.Builder(AddBeneficiary.this)
                        .setTitle("Internet Error")
                        .setDescription("Check Your Interenet Connection")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.warninginternet)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .setNegativeText("Cancel")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                dialog.dismiss();
                            }
                        })
                        .setPositiveText("Open Settings")
                        .onPositive((dialog, which) -> {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            // dialog.dismiss();
                        })
                        .show();
            } else if (error instanceof AuthFailureError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout. Please try after sometime";
            }
            if (!message.isEmpty()) {
                Toast.makeText(AddBeneficiary.this, message, Toast.LENGTH_SHORT).show();
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","110");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(AddBeneficiary.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

    //-----------------profiledetails---------------------//
    private void activity_log_save_beneficiary(){

        SharedPreferences sharedPreferences = AddBeneficiary.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(AddBeneficiary.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(AddBeneficiary.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","120");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(AddBeneficiary.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

    @Override
    protected void onResume() {
        super.onResume();
        sessionCheck();
    }

    private void ifscmethod(){

        ifsclocation=(TextView)findViewById(R.id.textView117);
        SharedPreferences sharedPreferences = AddBeneficiary.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/ifsc",
                response -> {
                    try {
                        JSONObject ifscresponse = new JSONObject(response);

                            System.out.println("This is ifsc response 1");
                            System.out.println(ifscresponse);
                            ifscgetstring = ifscresponse.getString("status");
                            api_id = ifscresponse.getString("id");
                            reason = ifscresponse.getString("reason");
                            System.out.println("This is ifsc array 1");
                            System.out.println(ifscgetstring);
                            if (ifscgetstring.equals("Not Found")){
                                progressifsc.setVisibility(View.GONE);
                                ifsclocation.setTextColor(Color.RED);
                                ifsclocation.setText("Not a Valid IFSC Code");
                            }else{
                                JSONObject ifscarray = ifscresponse.getJSONObject("status");
                                ifscstate = ifscarray.getString("STATE");
                                if (ifscstate.contains("TAMIL NADU") || ifscstate.contains("KERALA") || ifscstate.contains("KARNATAKA") || ifscstate.contains("ANDHRA PRADESH") || ifscstate.contains("TELANGANA") || ifscstate.contains("MAHARASHTRA")) {
                                    permissionsave = "allowed";
                                } else {
                                    permissionsave = "notallowed";
                                }

                                ifsclocation.setTextColor(Color.parseColor("#9B9B9B"));
                                ifsclocation.setText(ifscarray.getString("BRANCH"));
                                ifscbranchstore = ifscarray.getString("BRANCH");
                                ifscbankstore = ifscarray.getString("BANK");
                                progressifsc.setVisibility(View.GONE);
                            }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                message = "cannot connect";
            } else if (error instanceof AuthFailureError) {
                message = "";
            } else if (error instanceof NoConnectionError) {
                message = "cannot connect";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout";
            }
            if (!message.isEmpty()) {
                Toast.makeText(AddBeneficiary.this, "", Toast.LENGTH_SHORT).show();
            }
        })

        {
            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("ifsc",bankifsc);
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(AddBeneficiary.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
}
