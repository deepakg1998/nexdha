package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class onboarding2 extends AppCompatActivity {
    TextView nexttwo,backone;
    float x1,x2,y1,y2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
      //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_onboarding2);
        activity_log_onboarding_2();
       /* nexttwo = (TextView) findViewById(R.id.textView80);
        nexttwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(onboarding2.this, onboarding3.class);
                startActivity(intent);
            }
        });
        backone = (TextView) findViewById(R.id.textView86);
        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              finish();
            }
        });

        */
        FloatingActionButton nextonboard2 = findViewById(R.id.floatingActionButton3);
        nextonboard2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                Intent intent = new Intent(getApplicationContext(), onboardin1.class);
                startActivity(intent);
                finish();


            }
        });
        FloatingActionButton nextonboard3 = findViewById(R.id.floatingActionButton2);
        nextonboard3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                Intent intent = new Intent(getApplicationContext(), onboarding3.class);
                startActivity(intent);
                finish();


            }
        });
    }
    public boolean onTouchEvent(MotionEvent touchEvent){
        switch (touchEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();
                if(x1 < x2){
                 finish();
                }else if(x1 > x2){
                    Intent i = new Intent(onboarding2.this, onboarding3.class);
                    startActivity(i);
                }
                if (y1 < y2){
                    finish();
                }else if (y1 > y2){
                    Intent i = new Intent(onboarding2.this, onboarding3.class);
                    startActivity(i);

                }
                break;
        }
        return false;
    }
    //-----developer End-----------//
    private void activity_log_onboarding_2(){

        SharedPreferences sharedPreferences = onboarding2.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                response -> {
                    //Nothing

                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                //   message = "cannot profile connect";
                new MaterialStyledDialog.Builder(onboarding2.this)
                        .setTitle("Internet Error")
                        .setDescription("Check Your Interenet Connection")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.warninginternet)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(false)
                        .setCancelable(false)
                        .withDivider(true)
                        .setNegativeText("Cancel")
                        .onNegative((dialog, which) -> {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            dialog.dismiss();
                        })
                        .setPositiveText("Open Settings")
                        .onPositive((dialog, which) -> {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            // dialog.dismiss();
                        })
                        .show();
            } else if (error instanceof AuthFailureError) {
                message = "There is a problem connecting to server. Please try after sometime.";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout. Please try after sometime";
            }
            if (!message.isEmpty()) {
                Toast.makeText(onboarding2.this, message, Toast.LENGTH_SHORT).show();
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","101");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(onboarding2.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
}
