package com.nexdha.nexpay;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageDecoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.nexdha.nexpay.constant.SERVER;

public class new2kyc extends AppCompatActivity {
    Button uploadkyc;
    Button aadarselect;
    Button aadarselect1;
    Button upload1;
    LinearLayout linearLayout15;
    LinearLayout backarrows;
    ProgressBar panprogress;
   ImageView tickforpan;
    ProgressBar panprogress1;
    ProgressBar progressBar16;
    ImageView tickforpan2;
    ImageView tickaadar1;
   ImageView front1;
   Button button377;
   ImageView imageView58;
    Button takeselfie;
    Button uploaadar;
    int i =0;
    int j=0;
    String kyc_uid1 ="";
    String userid="";
/*
    String pan = "pan \"world\"!";
*/
    String filevarr = "pan";
    String filevarr2 ="selfie";
    String filevarr3 ="aadhaarf";
    String filevarr4 ="aadhaarb";

    String Result1 = "";
    String Result2 = "";
    String Result3 = "";
    String Result4 = "";
    ProgressBar progressBar18;
    ProgressBar progressBar20;
    ProgressBar progressBar21;
    ProgressBar progressBar19;





    ConstraintLayout constraintLayout21;
    ImageView selectionforpanimage;
    ImageView A1 ;
    ImageView A2;
    Button button3666;

    private static final int SELECT_PICTURE = 100;
    public static Uri selectedImageUri =null;
    String comp_path = null;
    Uri path = null;
    private static final int CAMERA_REQUEST = 1888;
    private static final int EMAIL_INTENT = 77;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    String camerap = null;
    String comp__rap = null;
    private static final int SELECT_PICTURE1 = 101;
    public static Uri selectedImageUri1;
    String comp_path1 = "null";
    String path1 = "null";
    private static final int SELECT_PICTURE2 = 102;
    public static Uri selectedImageUri2;
    String comp_path2 = "null";
    String path2 = "null";

    ConstraintLayout mobileuploadlayout;
    ConstraintLayout AAadar;
    ConstraintLayout AAadar2;
    ConstraintLayout selfieupload;
    ImageView sefieimage;
    Bitmap photo;
    Button sendemailprogress;
    Uri tempUri;
    //FloatingActionButton sendmailback;
    ImageView sendmailback;
    Button cancel;
    String mailname,mailphone,mailemail;
    ConstraintLayout otp_layout;
    EditText enterotp;
    EditText enterpan;
    Button send_otp;
    Button resend_otp;
    String verifyotp;
    String otp_method_verification;
    Button button36;
    Button uploaadar2;
    ProgressBar progressBar17;
    ImageView tickaadar2;
    Uri selectedImage_comp_Uri;
    Uri selectedImage_comp_Uri1;
    Uri selectedImage_comp_Uri2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_new2kyc);
        System.out.println("this is 5656565656");
        System.out.println(selectedImageUri);
        selectedImageUri =null;
        selectedImageUri1=null;
        selectedImageUri2=null;
        tempUri=null;

        System.out.println("this is 5656565656");
        System.out.println(selectedImageUri);

        activity_log_KYC();
        getprofiledetails();
        constraintLayout21 =findViewById(R.id.constraintLayout21);

        uploaadar = findViewById(R.id.uploaadar);
        uploaadar2 =findViewById(R.id.uploaadar2);
        button36 =findViewById(R.id.button36);
        mobileuploadlayout = findViewById(R.id.mobileuploadlayouts121);
        mobileuploadlayout.animate().translationX(0);
        selfieupload = findViewById(R.id.selfieuploady);
        selfieupload.setTranslationX(2000);
        AAadar =findViewById(R.id.aadar);
        AAadar.setTranslationX(2000);
        AAadar2 =findViewById(R.id.aadar2);
        AAadar2.setTranslationX(2000);
        backarrows = findViewById(R.id.backarrows);
        backarrows.setVisibility(View.GONE);
        linearLayout15 =findViewById(R.id.linearLayout15);
        linearLayout15.setVisibility(View.VISIBLE);
        progressBar18 =findViewById(R.id.progressBar18);
        progressBar18.setVisibility(View.GONE);
        progressBar20=findViewById(R.id.progressBar20);
        progressBar20.setVisibility(View.GONE);
        progressBar21=findViewById(R.id.progressBar21);
        progressBar21.setVisibility(View.GONE);
        progressBar19=findViewById(R.id.progressBar19);
        progressBar19.setVisibility(View.GONE);
        upload1 =findViewById(R.id.button5555);
        tickforpan =findViewById(R.id.imageView622);
        tickforpan.setVisibility(View.GONE);
        panprogress =findViewById(R.id.progressBar14);
        panprogress.setVisibility(View.GONE);
        panprogress1 =findViewById(R.id.progressBar15);
        panprogress1.setVisibility(View.GONE);
        tickforpan2 =findViewById(R.id.imageView52);
        tickforpan2.setVisibility(View.GONE);
        progressBar16 = findViewById(R.id.progressBar16);
        progressBar16.setVisibility(View.GONE);
        tickaadar1=findViewById(R.id.tickaadar1);
        tickaadar1.setVisibility(View.GONE);
        progressBar17=findViewById(R.id.progressBar17);
        progressBar17.setVisibility(View.GONE);
        tickaadar2 =findViewById(R.id.tickaadar2);
        tickaadar2.setVisibility(View.GONE);
        selectionforpanimage =findViewById(R.id.panimage);
        sendemailprogress = findViewById(R.id.button388);
        // sendemailprogress.setTranslationX(2000);
        sefieimage = findViewById(R.id.sefieimage);
        //sendmailback = findViewById(R.id.floatingActionButton11);
        sendmailback = findViewById(R.id.imageView588);
        // sendmailback.setTranslationX(2000);
/*
        otp_layout = findViewById(R.id.otplayouts);
*/
        button3666 =findViewById(R.id.button3666);
        send_otp = findViewById(R.id.button43);
        send_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send_otp.setEnabled(false);
                send_otp.setBackground(getDrawable(R.drawable.pageoneconstraintthree));
                sendotp();
            }
        });
        imageView58 = findViewById(R.id.imageView58);
        imageView58.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent send = new Intent(new2kyc.this, Moreoptions.class);
                startActivity(send);*/
                finish();
            }
        });
        resend_otp = findViewById(R.id.button44);
        resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resend_otp.setEnabled(false);
                resend_otp.setBackground(getDrawable(R.drawable.pageoneconstrainttwo));
                sendotp();
            }
        });
        resend_otp = findViewById(R.id.button44);
        enterotp = findViewById(R.id.editText188);
        enterpan = findViewById(R.id.editText200);
        cancel=findViewById(R.id.button401);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        handlePermission();
        // handlePermissioncamera();
        takeselfie = findViewById(R.id.button377);
        takeselfie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            /*    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);

             */
                handlePermissioncamera();

                // dispatchTakePictureIntent();

            }
        });
        sefieimage = findViewById(R.id.sefieimage);
        sefieimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            /*    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);

             */

                handlePermissioncamera();
                // dispatchTakePictureIntent();


            }
        });
        button3666.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                progressBar18.setVisibility(View.VISIBLE);

            }
        });

        selectionforpanimage = findViewById(R.id.panimage);
        selectionforpanimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                progressBar18.setVisibility(View.VISIBLE);

                /*Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);*/


            }
        });
        linearLayout15 = findViewById(R.id.linearLayout15);
        linearLayout15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (i==0) {
                    if (Validationacc() == false) {
                        return;
                    } else {
                        if (Result1 == "success" | Result1.equals("success")) {
                            System.out.println("1ndddddddddddd");
                            mobileuploadlayout.animate().translationX(-2000);
                            selfieupload.animate().translationX(0);
                            backarrows.setVisibility(View.VISIBLE);
                            i = i + 1;
                        }
                        else{
                            Toast.makeText(getApplicationContext(), "Please upload PAN image", Toast.LENGTH_LONG).show();

                        }

                    }
                }
                else if(i==1) {
                    if (validationforselfie() == false) {
                        return;
                    } else {
                        if (Result2 == "success" | Result2.equals("success")) {

                            backarrows.setVisibility(View.VISIBLE);
                            System.out.println("2ndddddddddddd");
                            selfieupload.animate().translationX(-2000);
                            AAadar.animate().translationX(0);
                            linearLayout15.setVisibility(View.VISIBLE);
                            i = i + 1;
                        } else {
                            Toast.makeText(getApplicationContext(), "Please upload SELFIE image", Toast.LENGTH_LONG).show();

                        }

                    }
                }
                else if(i==2){
                      if (validationforaf() == false) {
                        return;
                    }else {
                          if (Result3 == "success" | Result3.equals("success")) {

                              backarrows.setVisibility(View.VISIBLE);
                              AAadar.animate().translationX(-2000);
                              AAadar2.animate().translationX(0);
                              linearLayout15.setVisibility(View.GONE);
                              i = i + 1;
                              System.out.println(i);
                          } else {
                              Toast.makeText(getApplicationContext(), "Please upload AADHAAR front image", Toast.LENGTH_LONG).show();

                          }
                      }


                }
            }
        });
        A1 = findViewById(R.id.A1);
        A1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE1);
                progressBar20.setVisibility(View.VISIBLE);

            }
        });
        A2 = findViewById(R.id.A2);
        A2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE2);
                progressBar21.setVisibility(View.VISIBLE);

            }
        });
        aadarselect = findViewById(R.id.selectaadar);
        aadarselect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE1);
                progressBar20.setVisibility(View.VISIBLE);

            }
        });
        aadarselect1 = findViewById(R.id.selectaadar2);
        aadarselect1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE2);
                progressBar21.setVisibility(View.VISIBLE);

            }
        });
  /*      FloatingActionButton aadari1 = findViewById(R.id.floatingActionButton9);
        aadari1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              *//*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        *//*
             *//*   AAadar.animate().translationX(2000);
                selfieupload.animate().translationX(0);*//*

            }
        });*/
        /*FloatingActionButton aadarback = findViewById(R.id.floatingActionButton10);
        aadarback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              *//*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        *//*
              *//*  AAadar2.animate().translationX(2000);
                AAadar.animate().translationX(0);*//*


            }
        });
*/
       /* FloatingActionButton aadari = findViewById(R.id.floatingActionButtonaadar277);
        aadari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              *//*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        *//*
              *//*  AAadar.animate().translationX(-2000);
                AAadar2.animate().translationX(0);*//*


            }
        });*/
        FloatingActionButton showprogressbar = findViewById(R.id.floatingActionButton101);
        showprogressbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                selfieupload.animate().translationX(-2000);
                //sendemailprogress.animate().translationX(0);
                //sendmailback.animate().translationX(0);
                AAadar.animate().translationX(0);


            }
        });

        sendmailback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                selfieupload.animate().translationX(0);
                //sendemailprogress.animate().translationX(2000);
                //sendmailback.animate().translationX(2000);
                AAadar.animate().translationX(2000);


            }
        });
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");
        System.out.println("12434");

        backarrows = findViewById(R.id.backarrows);
        backarrows.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
                           if(i==3){
                                   i=i-1;
                                   AAadar2.animate().translationX(2000);
                                   AAadar.animate().translationX(0);
                                   backarrows.setVisibility(View.VISIBLE);
                                   linearLayout15.setVisibility(View.VISIBLE);

                         }
                          else if(i==2) {
                                   i = i - 1;
                                   AAadar.animate().translationX(2000);
                                   selfieupload.animate().translationX(0);
                                   linearLayout15.setVisibility(View.VISIBLE);


                           }
                        else if(i==1) {
                                   i = i - 1;
                                   selfieupload.animate().translationX(2000);
                                   mobileuploadlayout.animate().translationX(0);
                                   System.out.println("12434");
                                   linearLayout15.setVisibility(View.VISIBLE);
                                   backarrows.setVisibility(View.GONE);
                           }
                           }




        });
        sendemailprogress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             /*   if (Validationacc() == false){
                    return;
                }*/

            }
        });
        upload1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("selectedddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
                if (Validationacc() == false){
                    return;
                }else{
                    panprogress.setVisibility(View.VISIBLE);
                    upload11forpan();
                }

              //  Validationacc();


            }
        });
        uploaadar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("selectedddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
                if (validationforab() == false){
                    return;
                }else{
                    progressBar17.setVisibility(View.VISIBLE);
                    uploadonlyforaadarback();
                }

            }
        });
        uploaadar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("selectedddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
                  if (validationforaf() == false){
                    return;
                }else{
                      progressBar16.setVisibility(View.VISIBLE);

                      uploadonlyaadarfront();
                  }


            }
        });
        button36.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("selectedddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
                  if (validationforselfie() == false){
                    return;
                }else{
                      panprogress1.setVisibility(View.VISIBLE);

                      upload12forselfie();
                  }

             //   Validationacc();

            }
        });
    }
    private void sendotp(){
        SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserid",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("***************openotppage*******************");
                        System.out.println(response);
                        send_otp.setVisibility(View.GONE);
                        resend_otp.setEnabled(false);
                        resend_otp.setVisibility(View.VISIBLE);
                        otpresend_countdown();
                        // Intent intent = new Intent(SignUp.this, Payment.class);
                        // startActivity(intent);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new2kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(new2kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

     /*   StringRequest stringRequest = new StringRequest(Request.Method.GET,"http://192.168.68.107/api/timer_delay",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("***************waitttttttttttttttttttttttttttttttttttttttttttttttttttttttttt*******************");
                        System.out.println(response);

                            panprogress.setVisibility(View.GONE);
                            tickforpan.setVisibility(View.VISIBLE);

                            *//*panprogress1.setVisibility(View.GONE);
                            tickforpan2.setVisibility(View.VISIBLE);*//*



                        // Intent intent = new Intent(SignUp.this, Payment.class);
                        // startActivity(intent);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new2kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });

       *//* {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };*//*

        RequestQueue requestQueue = Volley.newRequestQueue(new2kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
*/

    private void uploadonlyaadarfront(){
       /* SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;*/

        File r3 = new File(FileUtil1.getPath(selectedImageUri1,this));
        RequestBody foraf = RequestBody.create(MediaType.parse("image/jpeg"),r3);
        RequestBody foraadarfront = RequestBody.create(MediaType.parse("text/plain"),filevarr3);
        MultipartBody.Part filename = MultipartBody.Part.createFormData("file3", mailphone +"_af.jpeg", foraf);
        apiconfig1 getResponse = AppConfig.getRetrofit().create(apiconfig1.class);
        Call< ServerResponse > call = getResponse.uploadMulFile(Integer.parseInt(userid),foraadarfront,filename);

        call.enqueue(new Callback< ServerResponse >() {
            @Override
            public void onResponse(Call<ServerResponse> call,retrofit2.Response<ServerResponse> response) {
                System.out.println("This one");
                ServerResponse serverResponse = response.body();
                System.out.println("This two "+serverResponse);
                if (serverResponse != null) {
                    System.out.println("this");
                    System.out.println(response);
                    Toast.makeText(getApplicationContext(), "Click NEXT To continue", Toast.LENGTH_LONG).show();
                    progressBar16.setVisibility(View.GONE);
                    tickaadar1.setVisibility(View.VISIBLE);
/*
                    linearLayout15.setVisibility(View.VISIBLE);
*/
                     Result3="success";


                } else {
                   /* System.out.println("This six");*/
                  /*  assert serverResponse != null;*/
                 /*  Log.v("Response", serverResponse.toString());
                    System.out.println("This seven "+ serverResponse.toString()); */
                }
                progressBar16.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call< ServerResponse > call, Throwable t) {

            }
        });

    }
    private void profileinfofornw2kyc(){

        SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/kyc_create_userid ",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      System.out.println(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(new2kyc.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new2kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userid", userid);
                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(new2kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
    private void uploadonlyforaadarback(){
       /* SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;*/

        File r3 = new File(FileUtil1.getPath(selectedImageUri2,this));
        RequestBody forab= RequestBody.create(MediaType.parse("image/jpeg"),r3);
        RequestBody foraadarback = RequestBody.create(MediaType.parse("text/plain"),filevarr4);
        MultipartBody.Part filename = MultipartBody.Part.createFormData("file4", mailphone +"_ab.jpeg", forab);
        apiconfig1 getResponse = AppConfig.getRetrofit().create(apiconfig1.class);
        Call< ServerResponse > call = getResponse.uploadMulFile(Integer.parseInt(userid),foraadarback,filename);

        call.enqueue(new Callback< ServerResponse >() {
            @Override
            public void onResponse(Call<ServerResponse> call,retrofit2.Response<ServerResponse> response) {
                System.out.println("This one");
                ServerResponse serverResponse = response.body();
                System.out.println("This two "+serverResponse);
                if (serverResponse != null) {
                    System.out.println("this");
                    System.out.println(response);
                    Toast.makeText(getApplicationContext(), "Upload Success", Toast.LENGTH_LONG).show();
                    progressBar17.setVisibility(View.GONE);
                    tickaadar2.setVisibility(View.VISIBLE);
                  /*  Intent intent = new Intent(new2kyc.this,Moreoptions.class);
                    startActivity(intent);*/
                    finish();



                    if (serverResponse.getSuccess()) {
                        System.out.println("This four");
                    } else {
                        System.out.println("This five");
                    }
                } else {
                   /* System.out.println("This six");
                    assert serverResponse != null;
                    System.out.println("This seven "+ serverResponse.toString());*/
                }
                progressBar17.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call< ServerResponse > call, Throwable t) {

            }
        });

    }
    private void upload12forselfie(){
       /* SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;*/

        File r2 = new File(FileUtil1.getPath(tempUri,this));
        RequestBody forselfie = RequestBody.create(MediaType.parse("image/jpeg"),r2);
        RequestBody forselfie1 = RequestBody.create(MediaType.parse("text/plain"),filevarr2);
        MultipartBody.Part filename = MultipartBody.Part.createFormData("file1", mailphone +"_s.jpeg", forselfie);
        apiconfig1 getResponse = AppConfig.getRetrofit().create(apiconfig1.class);
        Call< ServerResponse > call = getResponse.uploadMulFile(Integer.parseInt(userid),forselfie1,filename);

        call.enqueue(new Callback< ServerResponse >() {
            @Override
            public void onResponse(Call<ServerResponse> call,retrofit2.Response<ServerResponse> response) {
                System.out.println("This one");
                ServerResponse serverResponse = response.body();
                System.out.println("This two "+serverResponse);
                if (serverResponse != null) {
                    System.out.println("this");
                    System.out.println(response);
                    Toast.makeText(getApplicationContext(), "Click NEXT To continue", Toast.LENGTH_LONG).show();
                    Result2="success";
                    panprogress1.setVisibility(View.GONE);
                    tickforpan2.setVisibility(View.VISIBLE);
                    linearLayout15.setVisibility(View.VISIBLE);

                    if (serverResponse.getSuccess()) {
                        System.out.println("This four");
                    } else {
                        System.out.println("This five");
                    }
                } else {
                    System.out.println("This six");
                    assert serverResponse != null;
                    System.out.println("This seven "+ serverResponse.toString());
                }
                panprogress1.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call< ServerResponse > call, Throwable t) {

            }
        });

    }

    public void otpresend_countdown(){
        // resend_otp=(Button) findViewById(R.id.textView123);
        new CountDownTimer(90000, 1000) {

            public void onTick(long millisUntilFinished) {
                //resend_otp.setEnabled(false);
                resend_otp.setTextColor(Color.parseColor("#a4a4a4"));
                resend_otp.setText("Resend in : " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                resend_otp.setText("Click To re-send otp");
                resend_otp.setTextColor(Color.parseColor("#ffffff"));
                resend_otp.setEnabled(true);
                resend_otp.setBackgroundResource(R.drawable.roundedbutton);

            }
        }.start();

    }

    private void handlePermission() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    SELECT_PICTURE);
        }
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    SELECT_PICTURE1);
        }
        else if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    SELECT_PICTURE2);
        }
    }
    private void handlePermissioncamera() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            System.out.println("camera permission initiated");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_CODE);
            progressBar19.setVisibility(View.GONE);

        }else{
/*
            Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
*/
            System.out.println("camera intiated");
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
            progressBar19.setVisibility(View.GONE);

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case SELECT_PICTURE:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                        if (showRationale) {
                            //  Show your own message here
                            System.out.println("11111111111111111");
                        } else {
                            System.out.println("22222222222222");
                            showSettingsAlert();
                        }
                    }

                }
            case SELECT_PICTURE1:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                        if (showRationale) {
                            //  Show your own message here
                            System.out.println("11111111111111111");
                        } else {
                            System.out.println("22222222222222");
                            showSettingsAlert();
                        }
                    }

                }
            case SELECT_PICTURE2:
                for (int i = 0; i < permissions.length; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                        if (showRationale) {
                            //  Show your own message here
                            System.out.println("11111111111111111");
                        } else {
                            System.out.println("22222222222222");
                            showSettingsAlert();
                        }
                    }

                }


        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                System.out.println("camera permission granted");
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else
            {
                System.out.println("camera permission denied");
            }
        }




    }
    private void showSettingsAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        openAppSettings(new2kyc.this);
                    }
                });
        alertDialog.show();
    }
    public static void openAppSettings(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (resultCode == RESULT_OK) {
                    if (requestCode == SELECT_PICTURE) {
                        // Get the uri from data
                        selectedImage_comp_Uri = data.getData();
                        System.out.println(selectedImage_comp_Uri);
                        if (null != selectedImage_comp_Uri) {
                            // Get the path from the Uri
                            comp_path = getPath(selectedImage_comp_Uri);
                            System.out.println(comp_path);
                            final Bitmap yourSelectedImage = BitmapFactory.decodeFile(comp_path);
                            selectedImageUri = getcomp_ImageUri(getApplicationContext(), yourSelectedImage);
                           // selectedImageUri = getcomp_ImageUri(getApplicationContext(), pan_test);
                            path = Uri.parse(get_galley_comp_Path(selectedImageUri));
                            System.out.println(path);


                       /*     Uri selectedImg = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImg);
                    selectedImageUri = getImageUri(getApplicationContext(),bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                            // Set the image in ImageView
                            findViewById(R.id.panimage).post(new Runnable() {
                                @Override
                                public void run() {
                                   // ((ImageView) findViewById(R.id.panimage)).setImageURI(selectedImageUri);
                                    ((ImageView) findViewById(R.id.panimage)).setImageBitmap(Bitmap.createScaledBitmap(yourSelectedImage, 300, 200, false));
                                    progressBar18.setVisibility(View.GONE);
                                }


                            });
                        }


                    }

                  else if (requestCode == SELECT_PICTURE1) {
                        // Get the url from data
                        Uri selectedImage_comp_Uri1 = data.getData();
                        System.out.println(selectedImage_comp_Uri1);
                        if (null != selectedImage_comp_Uri1){
                            // Get the path from the Uri
                            comp_path1 = getPath1(selectedImage_comp_Uri1);
                            System.out.println(comp_path1);
                            final Bitmap yourSelectedImage1 = BitmapFactory.decodeFile(comp_path1);
                            selectedImageUri1 = getcomp_ImageUri1(getApplicationContext(), yourSelectedImage1);
                            path1 = get_galley_comp_Path1(selectedImageUri1);
                            System.out.print(path1);


                            // Set the image in ImageView
                            findViewById(R.id.A1).post(new Runnable() {
                                @Override
                                public void run() {
/*
                                    ((ImageView) findViewById(R.id.A1)).setImageURI(selectedImageUri1);
*/
                                    ((ImageView) findViewById(R.id.A1)).setImageBitmap(Bitmap.createScaledBitmap(yourSelectedImage1, 300, 200, false));

                                    progressBar20.setVisibility(View.GONE);


                                }
                            });

                        }
                    }
                    else if (requestCode == SELECT_PICTURE2) {
                        // Get the url from data
                        Uri selectedImage_comp_Uri2 = data.getData();
                        System.out.println(selectedImage_comp_Uri2);
                        if (null != selectedImage_comp_Uri2){
                            // Get the path from the Uri
                            comp_path2 = getPath2(selectedImage_comp_Uri2);
                            System.out.println(comp_path2);
                            final Bitmap yourSelectedImage2 = BitmapFactory.decodeFile(comp_path2);
                            selectedImageUri2 = getcomp_ImageUri2(getApplicationContext(), yourSelectedImage2);
                            path2 = get_galley_comp_Path2(selectedImageUri2);
                            System.out.print(path2);


                            // Set the image in ImageView
                            findViewById(R.id.A2).post(new Runnable() {
                                @Override
                                public void run() {
/*
                                    ((ImageView) findViewById(R.id.A2)).setImageURI(selectedImageUri2);
*/
                                    ((ImageView) findViewById(R.id.A2)).setImageBitmap(Bitmap.createScaledBitmap(yourSelectedImage2, 300, 200, false));

                                    progressBar21.setVisibility(View.GONE);


                                }
                            });

                        }
                    }
                }
            }
        }).start();

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            photo = (Bitmap) data.getExtras().get("data");
            sefieimage.setImageBitmap(photo);
            System.out.println(photo);
            tempUri = getImageUri(getApplicationContext(), photo);

            System.out.print(tempUri);
            getcameraPath(tempUri);

        } else if (requestCode == EMAIL_INTENT) {
            Intent intent = new Intent(new2kyc.this, Moreoptions.class);
            startActivity(intent);
            finish();


        }

    }



    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String imageuripath = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "", null);
        return Uri.parse(imageuripath);
    }
    public Uri getcomp_ImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String imageuripath = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "", null);
        return Uri.parse(imageuripath);
    }


    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String selectedpath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        System.out.println(selectedpath);
        cursor.close();

        return selectedpath;
    }
    /* public String getcameraPath(Uri uri) {
         Cursor cursor = getContentResolver().query(uri, null, null, null, null);
         cursor.moveToFirst();
         String document_id = cursor.getString(0);
         document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
         cursor.close();

         cursor = getContentResolver().query(
                 android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                 null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
         cursor.moveToFirst();
         camerap = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
         System.out.println(camerap);
         cursor.close();

         return camerap;
     }
     */
    public String getcameraPath(Uri uri) {

        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                camerap = cursor.getString(idx);
                cursor.close();
            }
        }
        return camerap;
    }

    public String get_galley_comp_Path(Uri uri) {

        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                comp__rap = cursor.getString(idx);
                cursor.close();
            }
        }
        return comp__rap;
    }
    ////////////////////////////////////foraadar1
    public String get_galley_comp_Path1(Uri uri) {

        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                comp__rap = cursor.getString(idx);
                cursor.close();
            }
        }
        return comp__rap;
    }
    public Uri getcomp_ImageUri1(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String imageuripath = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "", null);
        return Uri.parse(imageuripath);
    }


    public String getPath1(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String selectedpath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        System.out.println(selectedpath);
        cursor.close();

        return selectedpath;
    }
    /////////////////////////////////////////////////////////////////////////////////////foraadarback
    public String get_galley_comp_Path2(Uri uri) {

        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                comp__rap = cursor.getString(idx);
                cursor.close();
            }
        }
        return comp__rap;
    }
    public Uri getcomp_ImageUri2(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String imageuripath = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "", null);
        return Uri.parse(imageuripath);
    }


    public String getPath2(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String selectedpath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        System.out.println(selectedpath);
        cursor.close();

        return selectedpath;
    }
    // Get the real path from the URI
  /*  public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            System.out.println(res);
        }
        cursor.close();
        return res;
    }

   */
    private void getprofiledetails(){

        SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            userid = profileresponse.getString("id");
                            mailname = profileresponse.getString("name");
                            mailemail = profileresponse.getString("email");
                            mailphone = profileresponse.getString("phone");
                            profileinfofornw2kyc();
                            System.out.println("This is mail name");
                            System.out.println(mailname);
                            System.out.println(mailemail);
                            System.out.println(mailphone);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //     message = "cannot connect";
                    new MaterialStyledDialog.Builder(new2kyc.this)
                            .setTitle("Internet Error")
                            .setDescription("There will be issue displaying your info,Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new2kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(new2kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
/*    private boolean Validationacc(){
       *//* String otpverification = enterotp.getText().toString().trim();
        String panverification = enterpan.getText().toString().trim();*//*
        if (selectedImageUri.equals(null)||selectedImageUri == null ||selectedImageUri.equals(null)){
            Toast.makeText(new2kyc.this,"Upload your PAN card image",Toast.LENGTH_LONG).show();
            return false;

        }
        if (tempUri.equals(null) ||tempUri.equals(null)){
            Toast.makeText(new2kyc.this,"Upload your selfie",Toast.LENGTH_LONG).show();
            return false;

        }

        if (selectedImageUri1.equals("") || selectedImageUri1 == "" || path1.equals(null) || path1 == null){
            Toast.makeText(new2kyc.this, "Upload your Aadar front image",Toast.LENGTH_LONG);
            return false;
        }
        if(path2.equals("")|| path2 == "" || path2.equals(null) || path2 == null){
            Toast.makeText(new2kyc.this, "Upload your Aadar Back image",Toast.LENGTH_LONG);
            return false;
        }else{
            verify_otp();
        }

        return true;
    }*/
public boolean Validationacc(){

        if (selectedImageUri == null){
            Toast.makeText(new2kyc.this,"Select your PAN card image",Toast.LENGTH_LONG).show();

            return false;
        }
        return true;
    }
   public boolean validationforselfie(){
       if (tempUri == null){
           Toast.makeText(new2kyc.this,"Take a SELFIE",Toast.LENGTH_SHORT).show();
           return false;
       }
       return true;
   }
    public boolean validationforaf(){
        if (selectedImageUri1 == null){
            Toast.makeText(new2kyc.this,"Select your AADHAAR front image",Toast.LENGTH_SHORT).show();
            return false;

        }
        return true;

    }
    public boolean validationforab(){
        if (selectedImageUri2 == null){
            Toast.makeText(new2kyc.this,"Select your AADHAAR back image",Toast.LENGTH_SHORT).show();
            return false;

        }
        return true;

    }
    private void verify_otp(){

        otp_method_verification = enterotp.getText().toString().trim();
        SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/verify_otp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            System.out.println("***********Verify Otp****************");
                            System.out.println(response);
                            JSONObject otpresponse = new JSONObject(response);
                            verifyotp=otpresponse.getString("Details");
                            System.out.println("***********Details****************");
                            System.out.println(verifyotp);
                            if (verifyotp.equals("OTP Matched")){

                                pan_and_otp();
                                Intent send_report = new Intent(Intent.ACTION_SEND_MULTIPLE);
                                send_report.putExtra(Intent.EXTRA_EMAIL, new String[]{ "kyc@nexdha.com"});
                                send_report.putExtra(Intent.EXTRA_SUBJECT, "KYC Details of "+mailname);

                                // send_report.putExtra(Intent.EXTRA_STREAM, tempUri );
                                //send_report.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uris);

                                ArrayList<Uri> uris = new ArrayList<Uri>();
                                uris.add(Uri.fromFile(new File(String.valueOf(path))));
                                uris.add(Uri.fromFile(new File(camerap)));
                                System.out.println(camerap);
                                String[] bits = camerap.split("/");
                                String lastOne = bits[bits.length-1];
                                System.out.println(lastOne);
                                StringTokenizer tokens = new StringTokenizer(lastOne, ".");
                                String first = tokens.nextToken();
                                System.out.println(first);

                                send_report.putExtra(Intent.EXTRA_TEXT, "Name : "+mailname+"\r\nEmail : "+mailemail+"\r\nPhone : "+mailphone+"\r\nKYC Verification id : "+first);
                                send_report.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);

                                send_report.setType("text/plain");
                                startActivityForResult(Intent.createChooser(send_report, "Choose an Email client"), 77);


                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                parseVolleyError(error);

                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new2kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //This is for Headers If You Needed

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("otp", otp_method_verification);


                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };




        RequestQueue requestQueue = Volley.newRequestQueue(new2kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
    public void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            String otperror = data.getString("error");

            System.out.println("OTP");
            System.out.println(responseBody);
            System.out.println(data);
            System.out.println(otperror);
            if (otperror.equals("OTP does not match")){
                Toast.makeText(new2kyc.this,"Invalid OTP. Please Enter valid OTP",Toast.LENGTH_LONG).show();

            }

        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        System.out.println(selectedImage_comp_Uri);
        if (selectedImage_comp_Uri == null){
            progressBar18.setVisibility(View.GONE);
        }
        if (tempUri== null){
            progressBar19.setVisibility(View.GONE);
        }
        if (selectedImage_comp_Uri1 == null){
            progressBar20.setVisibility(View.GONE);
        }
        if (selectedImage_comp_Uri2 == null){
            progressBar21.setVisibility(View.GONE);
        }
        getprofiledetails();
        enterotp.setText("");
        enterpan.setText("");
    }

    private void pan_and_otp(){

        final String final_otp = enterotp.getText().toString().trim();
        final String final_pan = enterpan.getText().toString().trim();

        SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/otppan",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(new2kyc.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Internet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new2kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("otp",final_otp);
                params.put("pan",final_pan);
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(new2kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

    private void activity_log_KYC(){

        SharedPreferences sharedPreferences = new2kyc.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(new2kyc.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new2kyc.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","115");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(new2kyc.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
    private void upload11forpan(){
        File r1 = new File(FileUtil1.getPath(selectedImageUri,this));
       // File r1 = new File(getPath(path));
        RequestBody requestBody1 = RequestBody.create(MediaType.parse("image/jpeg"),r1);
        RequestBody requestBody2 = RequestBody.create(MediaType.parse("text/plain"),filevarr);
        MultipartBody.Part fileToUpload1 = MultipartBody.Part.createFormData("file2", mailphone +"_p.jpeg", requestBody1);
        apiconfig1 getResponse = AppConfig.getRetrofit().create(apiconfig1.class);
        Call< ServerResponse > call = getResponse.uploadMulFile(Integer.parseInt(userid),requestBody2,fileToUpload1);

        call.enqueue(new Callback< ServerResponse >() {
            @Override
            public void onResponse(Call<ServerResponse> call,retrofit2.Response<ServerResponse> response) {
                System.out.println("This one");
                ServerResponse serverResponse = response.body();
                System.out.println("This two "+response);
                if (serverResponse != null) {
                    System.out.println("this");
                    System.out.println(response);
                    Toast.makeText(getApplicationContext(), "Click NEXT To continue", Toast.LENGTH_LONG).show();
                    panprogress.setVisibility(View.GONE);
                    tickforpan.setVisibility(View.VISIBLE);
/*
                    linearLayout15.setVisibility(View.VISIBLE);
*/
                    Result1 = "success";

                    if (serverResponse.getSuccess()) {
                        System.out.println("This four");
                    } else {
                        System.out.println("This five");

                    }
                } else {
                /*    System.out.println("This six");
                    assert serverResponse != null;
                    System.out.println("This seven "+ serverResponse.toString());*/
                }
                panprogress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call< ServerResponse > call, Throwable t) {

            }
        });
    }
    }
