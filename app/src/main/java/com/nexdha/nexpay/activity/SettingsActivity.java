package com.nexdha.nexpay.activity;

import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nexdha.nexpay.Beneficiarylist;
import com.nexdha.nexpay.MainActivity;
import com.nexdha.nexpay.Moreoptions;
import com.nexdha.nexpay.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity {

    ImageView backButton, nameEdit, emailEdit;
    EditText emailEditText, nameEditText;
    TextView settingName;
    TextView settingEmail;
    Button updateName, cancelNamePopup, updateEmail, cancelEmailPopup;
    RelativeLayout beneficiaryList, landlordList, shareButton, logoutButton, checkUpdate;
    SharedPreferences sharedPreferences, welcomePreference;
    public static final String MYPREFERENCES = "MyPrefs";
    public static final String WELCOMEPREFERENCE = "welpref";
    public static String previousActivity = null;
    public String userName = "", deleteTokenResponse;
    public String userEmail = "";
    Dialog customView1;
    String versionCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_settings);

        sharedPreferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);
        welcomePreference = getSharedPreferences(WELCOMEPREFERENCE, Context.MODE_PRIVATE);
        try {
            versionCheck = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        backButton = findViewById(R.id.setting_back_btn);
        settingName = findViewById(R.id.setting_name);
        settingEmail = findViewById(R.id.setting_email);
        beneficiaryList = findViewById(R.id.setting_beneficiary_list);
        landlordList = findViewById(R.id.setting_landlord_list);
        shareButton = findViewById(R.id.setting_share_btn);
        logoutButton = findViewById(R.id.setting_logout_btn);
        nameEdit = findViewById(R.id.setting_name_edit_popup);
        emailEdit = findViewById(R.id.setting_email_edit_popup);
        checkUpdate = findViewById(R.id.setting_update_btn);

        shareAppLink();
        logoutApp();
        onBackPress();
        openBeneficiaryList();
        openLandlordList();
        nameEditPopup();
        emailEditPopup();
        checkForUpdate();
        sessionCheck();

        settingEmail.setText(Moreoptions.publicemail);
        settingName.setText(Moreoptions.publicname);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sessionCheck();
    }

    private void checkForUpdate() {
        checkUpdate.setOnClickListener(view -> {

            if (Moreoptions.versionName.equals(versionCheck)) {
                Toast.makeText(SettingsActivity.this, "The app is already the latest version", Toast.LENGTH_SHORT).show();

            } else {
                Intent viewUpdate =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://play.google.com/store/search?q=nexdha"));
                startActivity(viewUpdate);
            }

        });

    }

    @SuppressLint("InflateParams")
    private void emailEditPopup() {

        emailEdit.setOnClickListener(view -> {
            final LayoutInflater inflater = (LayoutInflater) SettingsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            customView1 = new Dialog(SettingsActivity.this);
            customView1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            customView1.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            customView1.setContentView(inflater.inflate(R.layout.pop_up_edit_email, null));
            customView1.setCancelable(false);
            customView1.show();
            updateEmail = customView1.findViewById(R.id.setting_email_submit);
            cancelEmailPopup = customView1.findViewById(R.id.setting_email_cancel);
            cancelEmailPopup.setOnClickListener(view1 -> customView1.dismiss());
            emailEditText = customView1.findViewById(R.id.setting_email_edit);
            emailEditText.setText(Moreoptions.publicemail);
            updateEmail.setOnClickListener(view1 -> emailValidation());

        });

    }

    private void emailValidation() {
        userEmail = emailEditText.getText().toString();
        if (userEmail.length() == 0) {
            Toast.makeText(SettingsActivity.this, "Please enter the email", Toast.LENGTH_SHORT).show();
        } else {
            callApiServiceEmail();
        }
    }

    private void sessionCheck() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/counter_check",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println(response);
                        String getResponse = jsonObject.getString("detail");

                        if (getResponse.equals("1")) {
                            String allowUser = jsonObject.getString("allow_user");
                            if (allowUser.equals("0")) {
                                final LayoutInflater layoutInflaterBlock = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog blockUserDialog = new Dialog(this);
                                blockUserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                blockUserDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                blockUserDialog.setContentView(layoutInflaterBlock.inflate(R.layout.block_user_layout, null));
                                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                blockUserDialog.getWindow().setLayout(width, height);
                                blockUserDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                blockUserDialog.setCancelable(false);
                                blockUserDialog.show();
                            } else {
                                //Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            }
                        } else if (getResponse.equals("Invalid token")) {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            });
                        } else {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("auth", TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void callApiServiceEmail() {
        SharedPreferences sharedPreferences = SettingsActivity.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");

        StringRequest request = new StringRequest(Request.Method.POST, SERVER + "/api/update_email", response -> {
            try {
                JSONObject json = new JSONObject(response);
                String parseAmount = json.getString("status");

                if (parseAmount.equals("success")) {
                    customView1.dismiss();
                    finish();
                    Toast.makeText(SettingsActivity.this, "update successfully", Toast.LENGTH_SHORT).show();

                } else {
                    customView1.dismiss();
                    Toast.makeText(SettingsActivity.this, "Oops something went wrong. Try again later", Toast.LENGTH_SHORT).show();

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        },
                error -> Log.e("error is ", "" + error)) {
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = "token " + T;

                params.put("Authorization", TokenS);

                return params;
            }

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("new_email", userEmail);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);


    }


    @SuppressLint("InflateParams")
    private void nameEditPopup() {

        nameEdit.setOnClickListener(view -> {
            final LayoutInflater inflater = (LayoutInflater) SettingsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            customView1 = new Dialog(SettingsActivity.this);
            customView1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            customView1.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            customView1.setContentView(inflater.inflate(R.layout.pop_up_edit_name, null));
            customView1.setCancelable(false);
            customView1.show();
            updateName = customView1.findViewById(R.id.setting_name_submit);
            cancelNamePopup = customView1.findViewById(R.id.setting_name_cancel);
            cancelNamePopup.setOnClickListener(view1 -> customView1.dismiss());
            nameEditText = customView1.findViewById(R.id.setting_name_edit);
            nameEditText.setText(Moreoptions.publicname);
            updateName.setOnClickListener(view1 -> nameValidation());

        });


    }

    private void nameValidation() {
        userName = nameEditText.getText().toString();

        if (userName.length() == 0) {
            Toast.makeText(SettingsActivity.this, "Please enter the name", Toast.LENGTH_SHORT).show();

        } else {
            callApiServiceName();

        }


    }

    private void callApiServiceName() {
        SharedPreferences sharedPreferences = SettingsActivity.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");

        StringRequest request = new StringRequest(Request.Method.POST, SERVER + "/api/update_user_name", response -> {
            try {
                JSONObject json = new JSONObject(response);
                String parseAmount = json.getString("status");

                if (parseAmount.equals("success")) {
                    customView1.dismiss();
                    finish();
                    Toast.makeText(SettingsActivity.this, "update successfully", Toast.LENGTH_SHORT).show();

                } else {
                    customView1.dismiss();
                    Toast.makeText(SettingsActivity.this, "Oops something went wrong. Try again later", Toast.LENGTH_SHORT).show();

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        },
                error -> Log.e("error is ", "" + error)) {
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = "token " + T;

                params.put("Authorization", TokenS);

                return params;
            }

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("new_name", userName);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(request);


    }

    private void openLandlordList() {

        landlordList.setOnClickListener(view -> {
            Intent intent = new Intent(SettingsActivity.this, LandlordListActivity.class);
            previousActivity = "landlordList";
            startActivity(intent);
        });
    }

    private void openBeneficiaryList() {

        beneficiaryList.setOnClickListener(view -> {
            Intent intent = new Intent(SettingsActivity.this, Beneficiarylist.class);
            previousActivity = "beneficiaryList";
            startActivity(intent);
        });


    }

    private void onBackPress() {

        backButton.setOnClickListener(view -> finish());

    }

    private void logoutApp() {

        logoutButton.setOnClickListener(view -> new AlertDialog.Builder(SettingsActivity.this)
                .setTitle("Logout")
                .setMessage("Are you sure you want to logout?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                    clearToken();
                }).create().show());


    }

    private void clearToken() {
        SharedPreferences sharedPreferences = SettingsActivity.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/veliye",
                response -> {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        deleteTokenResponse = jsonObject.getString("status");
                        if (deleteTokenResponse.equals("changed")) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear();
                            editor.apply();
                            startActivity(new Intent(this, MainActivity.class));
                            finishAffinity();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                        } else {
                            Toast.makeText(this, "Oops. Unable to logout.", Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = "token " + T;

                params.put("Authorization", TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void shareAppLink() {

        shareButton.setOnClickListener(view -> {

            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "https://play.google.com/store/apps/details?id=com.nexdha.nexpay&hl=en";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Credit Card Payments Made Seamless");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        });


    }
}