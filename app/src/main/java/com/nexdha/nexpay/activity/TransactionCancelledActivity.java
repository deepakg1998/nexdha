package com.nexdha.nexpay.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.nexdha.nexpay.R;
import com.nexdha.nexpay.new_design_transaction;

public class TransactionCancelledActivity extends AppCompatActivity {
    private static final long SPLASH_TIME_OUT = 2500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_transaction_cancelled);

        new Handler().postDelayed(() -> {
            Intent intent = new Intent(this, new_design_transaction.class);
            intent.putExtra("STRING_I_NEED", "forcancelled");
            startActivity(intent);
            finish();
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }
}