package com.nexdha.nexpay.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nexdha.nexpay.MainActivity;
import com.nexdha.nexpay.R;

import java.io.PrintWriter;
import java.io.StringWriter;

public class PayUGatewayActivity extends AppCompatActivity {
    StringBuilder hashString;
    WebView webView;
    ProgressBar progressBar;
    String saltKey = MainActivity.getNativeKey1(), getHashKey;
    Intent intent;
    String getUrl;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pay_gatewat_test);
        webView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progress_bar_webview);
        intent = getIntent();
        getUrl = intent.getStringExtra("URL");

        try {
            WebView.setWebContentsDebuggingEnabled(false);
            this.webView.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    PayUGatewayActivity.this.progressBar.setVisibility(View.GONE);
                }

                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    PayUGatewayActivity.this.progressBar.setVisibility(View.GONE);
                    view.loadUrl(url);
                    return true;
                }

                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    PayUGatewayActivity.this.progressBar.setVisibility(View.GONE);
                    return super.shouldOverrideUrlLoading(view, request);
                }

                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    PayUGatewayActivity.this.progressBar.setVisibility(View.GONE);
                }

                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    PayUGatewayActivity.this.progressBar.setVisibility(View.GONE);
                }
            });
            this.webView.setWebChromeClient(new WebChromeClient() {
                public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                    Log.d("WebView", consoleMessage.message());
                    System.out.println(consoleMessage.message());
                    String hash = consoleMessage.message();


                    return true;
                }
            });
            WebSettings webSettings = this.webView.getSettings();
            this.webView.addJavascriptInterface(new MyJavaScriptInterface(this), "Android");
            webSettings.setJavaScriptEnabled(true);
            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
            webSettings.setDomStorageEnabled(true);
            this.webView.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                    return super.onJsAlert(view, url, message, result);
                }
            });
        } catch (Exception var6) {
            StringWriter stringWriter = new StringWriter();
            var6.printStackTrace(new PrintWriter(stringWriter));
            String str = stringWriter.toString();
            Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
        }
        this.webView.loadUrl(getUrl);

    }

    public class MyJavaScriptInterface {
        Context mContext;

        MyJavaScriptInterface(Context c) {
            this.mContext = c;
        }

        @JavascriptInterface
        public void showToast(String message) {
            Intent data = new Intent();
            data.putExtra("payuResponse", message);
            PayUGatewayActivity.this.setResult(101, data);
            PayUGatewayActivity.this.finish();
        }
    }


    @Override
    public void onBackPressed() {
        return;
    }
}