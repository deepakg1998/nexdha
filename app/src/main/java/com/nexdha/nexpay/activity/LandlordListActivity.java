package com.nexdha.nexpay.activity;


import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nexdha.nexpay.MyBeneficiaryaAdapter;
import com.nexdha.nexpay.R;
import com.nexdha.nexpay.beneficiary;
import com.nexdha.nexpay.landlord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LandlordListActivity extends AppCompatActivity {

    private RecyclerView beneficiaryrecyclerView;
    private RecyclerView.Adapter beneficiaryadapter;
    public ImageView beneficiarylistclose, nobeneficiary;
    private List<beneficiary> listItems;
    beneficiary item;
    ProgressBar beneficiarybar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_landlord_list);
        beneficiarybar = findViewById(R.id.progressBar6);
        beneficiarylistclose = (ImageView) findViewById(R.id.back_button_landlord_list);
        beneficiarylistclose.setOnClickListener(v -> finish());

        beneficiaryrecyclerView = (RecyclerView) findViewById(R.id.landlord_list_view);
        beneficiaryrecyclerView.setHasFixedSize(true);
        beneficiaryrecyclerView.setLayoutManager(new LinearLayoutManager(this));

        listItems = new ArrayList<>();
        activity_log_beneficiary_list();
        loadBeneficiaryRecyclerview();


        SharedPreferences sharedPreferences = LandlordListActivity.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAA");
        System.out.println(token1);


        FloatingActionButton fabaddbene = findViewById(R.id.add_landlord_from_list);
        fabaddbene.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), landlord.class);
            startActivity(intent);
            finish();


        });


    }

/*
    @Override
    public void onResume() {
        String action = getIntent().getAction();
        // Prevent endless loop by adding a unique action, don't restart if action is present
        if(action == null || !action.equals("Already created")) {

            Intent intent = new Intent(this, Beneficiarylist.class);
            startActivity(intent);
            finish();
        }
        // Remove the unique action so the next time onResume is called it will restart
        else
            getIntent().setAction(null);
        super.onResume();
      // this.recreate();
      //  listItems.remove(listItems.size() - 1);
    //    beneficiaryadapter.notifyDataSetChanged();




    }
    */


    private void loadBeneficiaryRecyclerview() {
        nobeneficiary = (ImageView) findViewById(R.id.imageView46);

        SharedPreferences sharedPreferences = LandlordListActivity.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("-------------------------------history---------------------------------------");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/save_casa_rental",
                new Response.Listener<String>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(String response) {
                        try {
                            // JSONObject transactionobject = new JSONObject(response);
                            // JSONArray transactionarray = transactionobject.getJSONArray(null);
                            JSONArray beneficiaryarray = new JSONArray(response);
                            System.out.println("this is the history response");
                            System.out.println(beneficiaryarray);

                            if (beneficiaryarray == null || beneficiaryarray.length() == 0) {
                                beneficiarybar.setVisibility(View.GONE);
                                nobeneficiary.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < beneficiaryarray.length(); i++) {
                                JSONObject o = beneficiaryarray.getJSONObject(i);
                                item = new beneficiary(

                                        o.getString("first_name"),
                                        o.getString("Account_number"),
                                        o.getString("IFSC"),
                                        o.getString("id"),
                                        o.getString("second_name")

                                );
                                System.out.println("this is the item name response");
                                System.out.println(o.getString("first_name"));
                                System.out.println("this is the item response");
                                System.out.println(item);


                                listItems.add(item);
                                beneficiarybar.setVisibility(View.GONE);
                            }
                            // beneficiaryadapter = new MyBeneficiaryaAdapter(listItems,getApplicationContext());
                            beneficiaryadapter = new MyBeneficiaryaAdapter(listItems, LandlordListActivity.this);
                            beneficiaryrecyclerView.setAdapter(beneficiaryadapter);
                            beneficiaryadapter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getApplicationContext(), volleyError.getMessage(), Toast.LENGTH_LONG).show()

                    }

                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void activity_log_beneficiary_list() {

        SharedPreferences sharedPreferences = LandlordListActivity.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, error -> {
                    String message = "";

                    if (error instanceof NetworkError) {
                        //   message = "cannot profile connect";
                        new MaterialStyledDialog.Builder(LandlordListActivity.this)
                                .setTitle("Internet Error")
                                .setDescription("Check Your Interenet Connection")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.warninginternet)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(false)
                                .setCancelable(false)
                                .withDivider(true)
                                .setNegativeText("Cancel")
                                .onNegative((dialog, which) -> {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                })
                                .setPositiveText("Open Settings")
                                .onPositive((dialog, which) -> {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                })
                                .show();
                    } else if (error instanceof AuthFailureError) {
                        message = "There is a problem connecting to server. Please try after sometime.";
                    } else if (error instanceof ParseError) {
                        message = "Connection Timedout. Please try after sometime";
                    }
                    if (!message.isEmpty()) {
                        Toast.makeText(LandlordListActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }) {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                params.put("AcitvityID", "111");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(LandlordListActivity.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

}