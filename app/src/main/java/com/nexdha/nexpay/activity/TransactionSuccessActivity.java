package com.nexdha.nexpay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.nexdha.nexpay.R;
import com.nexdha.nexpay.new_design_transaction;

public class TransactionSuccessActivity extends AppCompatActivity {

    private static final long SPLASH_TIME_OUT = 2500;
    LottieAnimationView lottieAnimationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_transaction_success);


        lottieAnimationView = findViewById(R.id.lottie_2);

        new Handler().postDelayed(() -> {
            startActivity(new Intent(this, new_design_transaction.class));
            finish();
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        return;
    }
}