package com.nexdha.nexpay.activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nexdha.nexpay.R;

public class SmeActivity extends AppCompatActivity {

    ImageView backButton, successTick;
    EditText monthlyAmount, enterPurpose;
    Button submitSme, attachDocument;
    ProgressBar uploadDocumentProgress;
    String enteredAmount, enteredPurpose;
    Uri documentPath = null;
    TextView showDocumentName, showDocumentSize;
    Bitmap document;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sme);

        backButton = findViewById(R.id.back_button_sme);
        successTick = findViewById(R.id.attach_success);
        monthlyAmount = findViewById(R.id.monthly_expected_amount);
        enterPurpose = findViewById(R.id.purpose_sme);
        submitSme = findViewById(R.id.submit_sme);
        attachDocument = findViewById(R.id.attach_bank_document);
        uploadDocumentProgress = findViewById(R.id.upload_document);
        showDocumentName = findViewById(R.id.show_document_name);
        showDocumentSize = findViewById(R.id.show_document_size);

        submitSme.setOnClickListener(v -> {

            if (validation()) {
                uploadSme();
            }
        });
        backButton.setOnClickListener(v -> finish());
        attachDocument.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setType("application/pdf");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select PDF"), 1);
        });


    }


    @SuppressLint("SetJavaScriptEnabled")
    private void uploadSme() {
        WebView w = (WebView) findViewById(R.id.web_view);

        // loading http://www.google.com url in the the WebView.
        w.loadUrl("http://www.google.com");

        // this will enable the javascript.
        w.getSettings().setJavaScriptEnabled(true);

        // WebViewClient allows you to handle
        // onPageFinished and override Url loading.
        w.setWebViewClient(new WebViewClient());
    }

    private boolean validation() {
        enteredAmount = monthlyAmount.getText().toString();
        enteredPurpose = enterPurpose.getText().toString();


        if (enteredAmount.isEmpty()) {
            monthlyAmount.setError("Please enter amount");
            return false;
        }


        if (Integer.parseInt(enteredAmount) < 25000) {
            monthlyAmount.setError("Minimum amount should be 25,0000");
            return false;
        }

        if (Integer.parseInt(enteredAmount) > 5000000) {
            monthlyAmount.setError("Maximum amount should be 50,00,000");
            return false;
        }

        if (enteredPurpose.isEmpty()) {
            enterPurpose.setError("Please enter purpose");
            return false;
        }

        if (documentPath == null) {
            Toast.makeText(this, "Please upload document", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;


    }

    public static final int REQUEST_CODE_DOC = 1;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            Toast.makeText(this, "No document selected", Toast.LENGTH_SHORT).show();

        } else {
            if (requestCode == REQUEST_CODE_DOC && resultCode == RESULT_OK) {
                documentPath = data.getData();
                @SuppressLint("Recycle")
                Cursor returnCursor =
                        getContentResolver().query(documentPath, null, null, null, null);
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                returnCursor.moveToFirst();
                showDocumentName.setText(returnCursor.getString(nameIndex));

            }
        }


    }
}