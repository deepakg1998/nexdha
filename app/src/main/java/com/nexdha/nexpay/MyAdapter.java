package com.nexdha.nexpay;


import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nexdha.nexpay.ApiClient.ApiInterfaceGetOrderDetails;
import com.nexdha.nexpay.ApiClient.MyMediatorInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    ProgressBar progressBar;
    private final List<transactionhistoryview> listitems;
    private final Context context;
    Date d, etadate, currentdate;
    String comparisonfirstdate, comparisonseconddate;
    int result;
    public String orderId;
    MyMediatorInterface myMediatorInterface;


    public MyAdapter(List<transactionhistoryview> listitems, Context context, MyMediatorInterface myMediatorInterface) {
        this.listitems = listitems;
        this.context = context;
        this.myMediatorInterface = myMediatorInterface;


    }

    @SuppressLint("SetTextI18n")
    public void showPopup(View view, String orderId, int position) {
        @SuppressLint("InflateParams")


        final LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Dialog customView1 = new Dialog(view.getContext());
        customView1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        customView1.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        customView1.setContentView(inflater.inflate(R.layout.pop_up_screen, null));
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        customView1.getWindow().setLayout(width, height);
        customView1.setCancelable(true);
        customView1.show();
        ImageView btnDismiss = (ImageView) customView1.findViewById(R.id.imageView1);
        btnDismiss.setOnClickListener(v -> customView1.dismiss());

        /*
        View popupView = LayoutInflater.from(view.getContext()).inflate(R.layout.pop_up_screen, null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, true);
        popupWindow.setAnimationStyle(R.style.popup_window_animation);
        ImageView btnDismiss = (ImageView) popupView.findViewById(R.id.imageView1);
        progressBar = popupView.findViewById(R.id.progressBar31);
        btnDismiss.setOnClickListener(v -> popupWindow.dismiss());
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        popupWindow.setElevation(100);
        popupWindow.showAsDropDown(popupView, 0, 0);  */
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ApiInterfaceGetOrderDetails service = retrofit.create(ApiInterfaceGetOrderDetails.class);
        Map<String, Object> jsonObject1 = new ArrayMap<>();
        jsonObject1.put("order_id", orderId);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json"), (new JSONObject(jsonObject1)).toString());

        Call<ResponseBody> call = service.getData(body);

        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    progressBar = customView1.findViewById(R.id.progressBar31);
                    progressBar.setVisibility(View.GONE);

                    assert response.body() != null;
                    try {
                        String item = response.body().string();
                        JSONArray jsonArray = new JSONArray(item);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject parse_response_hash = jsonArray.getJSONObject(i);
                            String order = parse_response_hash.getString("order_id");
                            String name = parse_response_hash.getString("name");
                            String email = parse_response_hash.getString("email");
                            String phoneNumber = parse_response_hash.getString("phone");
                            String amount = parse_response_hash.getString("address_line_2");
                            String responseMessage = parse_response_hash.getString("response_message");
                            String state = parse_response_hash.getString("state");
                            String responseEta = parse_response_hash.getString("address_line_1");
                            String senderName = parse_response_hash.getString("udf3");
                            String totalAmount = parse_response_hash.getString("amount");

                            String strdecimalformat = new DecimalFormat("##,##,##,##,##,##,##,##,##,###.00").format(Double.parseDouble(String.valueOf(totalAmount)));

                            String strdecimalformat2 = new DecimalFormat("##,##,##,##,##,##,##,##,##,###.00").format(Double.parseDouble(String.valueOf(amount)));


                            ((TextView) customView1.findViewById(R.id.show_type)).setText(state);
                            ((TextView) customView1.findViewById(R.id.show_order)).setText(order);
                            ((TextView) customView1.findViewById(R.id.show_name)).setText(name);
                            ((TextView) customView1.findViewById(R.id.show_amount)).setText("₹ " + strdecimalformat);
                            ((TextView) customView1.findViewById(R.id.show_response_message)).setText(responseMessage);
                            ((TextView) customView1.findViewById(R.id.show_response_message)).setTextColor(Color.parseColor("#4BB543"));
                            //((TextView) popupWindow.getContentView().findViewById(R.id.show_type)).setText(state);
                            ((TextView) customView1.findViewById(R.id.show_eta)).setText(responseEta);
                            ((TextView) customView1.findViewById(R.id.show_eta)).setTextColor(Color.BLUE);
                            ((TextView) customView1.findViewById(R.id.total_amount)).setText("₹ " + strdecimalformat2);
                            ((TextView) customView1.findViewById(R.id.show_beneficiary_name)).setText(senderName);


                            notifyDataSetChanged();
                        }

                    } catch (JSONException | IOException e) {
                        Toast.makeText(view.getContext(), "Json failure", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(view.getContext(), "Server failure", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(view.getContext(), "Response failure", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new ViewHolder(v);

    }

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        final transactionhistoryview listItem = listitems.get(position);

        if (position % 2 == 1) {
            holder.lv2.setBackgroundResource(R.drawable.card_recycler_view_white);

            //holder.lv2.setBackgroundColor(Color.WHITE);
        } else {
            holder.lv2.setBackgroundResource(R.drawable.card_recycler_view_blue);

            // holder.lv2.setBackgroundColor(Color.parseColor("#E5F3F9"));
        }
        currentdate = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat createddateformat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            d = createddateformat.parse(listItem.getCreateddate());
            System.out.println("0909090");
            System.out.println(d);
            etadate = createddateformat.parse(listItem.getaddress_line_1());
            System.out.println("0808080");
            System.out.println(d);
            createddateformat.applyPattern("EEEE dd MMMM yy");
            System.out.println("0707070");
            System.out.println(d);
            comparisonfirstdate = createddateformat.format(currentdate);
            comparisonseconddate = createddateformat.format(etadate);
            Date final1 = createddateformat.parse(comparisonfirstdate);
            Date final2 = createddateformat.parse(comparisonseconddate);
            assert final1 != null;
            System.out.println(final1.compareTo(final2));
            assert final2 != null;
            System.out.println(final2.compareTo(final1));

            long diff = final2.getTime() - final1.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            String daydifference = Long.toString(days);
            System.out.println(daydifference);
            result = Integer.parseInt(daydifference);


            if (final1.compareTo(final2) <= 0) {
                System.out.println("earlier");
            }


        } catch (ParseException ex) {

        }

/*
        if (listItem.getState().equals("t1") || listItem.getState().equals("t1_azypay")) {
            holder.paymenttype.setText("T+1");
        } else if (listItem.getState().equals("t2")) {
            holder.paymenttype.setText("T+2");
        } else if (listItem.getState().equals("t0sds") || listItem.getState().equals("t0hds")) {
            holder.paymenttype.setText("Same Day");
        } else if (listItem.getState().equals("upi")) {
            holder.paymenttype.setText("UPI");
        } else if (listItem.getState().equals("t5")) {
            holder.paymenttype.setText("T+5");
        } else {
            holder.paymenttype.setText("T+2");
        }  */
        String strdecimalformat = new DecimalFormat("##,##,##,##,##,##,##,##,##,###.00").format(Double.parseDouble(String.valueOf(listItem.getAmount())));

        holder.tranamount.setText("₹ " + strdecimalformat);
        System.out.println("This is id");
        System.out.println(listItem);
        holder.tranname.setText("Beneficiary Name: " + listItem.getSendername());
        holder.trancreateddate.setText(listItem.getCreateddate());
        holder.tranpg_tran_id.setText("Transaction id: " + listItem.getpg_tran_id());

        holder.historyDetails.setOnClickListener(view -> {
            orderId = listItem.getOrder_id();
            myMediatorInterface.setClick(orderId, position);
            showPopup(view, orderId, position);

        });
        //holder.tranresponse_message.setText(listItem.getresponse_message());
        //if (listItem.getSendername().equals("Aakash") || listItem.getSendername().equals("Guru")) {
        //    holder.invoice.setVisibility(View.GONE);
        // } else {
        //    holder.invoice.setVisibility(View.GONE);
        // }
       /* holder.invoice.setOnClickListener(view -> {
            Intent intent = new Intent(context, Webview.class);
            intent.putExtra("URL", "http://www.nexdha.com/invoice/" + listItem.getid());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });  */
        holder.setIsRecyclable(false);
        // holder.transactionseekbar.setOnTouchListener((view, motionEvent) -> true);


        if (listItem.getresponse_message().equals("Transaction successful") || listItem.getresponse_message().equals("Transaction successful")) {
            if (listItem.getState().equals("t0sds")) {
                // holder.tranaddress_line_1.setText(listItem.getaddress_line_1());
            } else if (listItem.getState().equals("upi")) {
                // holder.tranaddress_line_1.setText(listItem.getaddress_line_1());
            } else {
                // holder.tranaddress_line_1.setText(listItem.getaddress_line_1() + "\n(01 PM - 03 PM) or (03 PM - 06 PM)");
            }

        } else {
            // holder.tranaddress_line_1.setTextColor(Color.parseColor("#E42815"));
            // holder.tranaddress_line_1.setText("NIL");
            // holder.progresstext.setVisibility(View.GONE);
            //holder.seekbarlayout.setVisibility(View.GONE);
        }


    }


    @Override
    public int getItemCount() {
        return listitems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // public CardView cv1;
        public TextView tranamount;
        public TextView tranname;
        public TextView trancreateddate;
        public TextView tranpg_tran_id;
        //public TextView tranresponse_message;
        //public TextView tranaddress_line_1;
        // public TextView progresstext;
        //public TextView paymenttype;
        //public TextView invoice;
        public CardView lv2;
        ImageView historyDetails;
        //public SeekBar transactionseekbar;

        public ViewHolder(View itemView) {
            super(itemView);
            lv2 = (CardView) itemView.findViewById(R.id.lv2);
            //  cv1 = (CardView) itemView.findViewById(R.id.cv1);
            tranamount = (TextView) itemView.findViewById(R.id.transactionhistoryamount);
            tranname = (TextView) itemView.findViewById(R.id.transactionhistoryname);
            trancreateddate = (TextView) itemView.findViewById(R.id.transactionhistorydate);
            tranpg_tran_id = (TextView) itemView.findViewById(R.id.textView41);
            historyDetails = itemView.findViewById(R.id.history_details_btn);

            //tranresponse_message = (TextView) itemView.findViewById(R.id.textView116);
            //ranaddress_line_1 = (TextView) itemView.findViewById(R.id.textView203);
            //seekbarlayout = (LinearLayout) itemView.findViewById(R.id.seekbarlayout);
            //progresstext = (TextView) itemView.findViewById(R.id.textView85);
            //invoice = (TextView) itemView.findViewById(R.id.textView213);
            // paymenttype = (TextView) itemView.findViewById(R.id.textView118);
            // transactionseekbar = (SeekBar) itemView.findViewById(R.id.transactionseekBar);
        }
    }
}
