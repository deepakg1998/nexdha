package com.nexdha.nexpay;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.test.pg.secure.pgsdkv4.PaymentParams;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.test.pg.secure.pgsdkv4.PaymentGatewayPaymentInitializer;

import com.easebuzz.payment.kit.PWECouponsActivity;
import datamodels.PWEStaticDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.nexdha.nexpay.constant.SERVER;

//Easebuzz



public class Payment extends AppCompatActivity implements LocationListener, AdapterView.OnItemSelectedListener{

    private static final String TAG ="Main5ActivityLog" ;
    TextView textView245;
    EditText amountfinance;
    Button paybutton,button10;
    Spinner spinnercasa,purposepaycasa;
    ArrayList<String> isplist, ispids,ispvendorid,paylist,payid;
    ArrayAdapter<String>dataAdapter;
    ArrayAdapter<String>dataAdapter2;
    private ImageView imageView9;

    TextView amextext;
    String AddressLine1;
    String AddressLine2;
    String Amountdone;
    String Cardmask;
    String Citydone;
    String Countrydone;
    String Currencydone;
    String Descriptiongiven;
    String Emailgiven;
    String Errordescription;
    String Hashvalue;
    String cardName;
    String Orderno;
    String channelpayment;
    String modeofpay;
    String cell;
    String responsecode;
    String responsemessage;
    String Statedone;
    String Transactionid;
    String udf01;
    String udf02;
    String udf03;
    String udf04;
    String udf05;
    String pincode;
    String entered;
    String cardtypetext="";
    String getetaapi;
    String useridudf1;
    String paymentdashboardlocationupdate;

    String easebuzz_bank_Ref;

    String sdsacti;


    String gateway_easebuz_hash;


    //Azypay method
    float azypaylimit;
    String azypaylimit_str;
    String allow_easebuzz;

    //  String finance;
    Switch toggle,switch2;
    float financepercentage,fixedamountcalculation;
    String strAmout,fixedamount,formattedValue;
    boolean isReciever, isswitchreceiver;

    String getpgtran;
    String cardpercentage=null;
    String percentage_dis = null;
    String apikeyt12 = null;
    String transactiontypestring = null;

    String percentage_t1 = null;
    String percentage_t1_display = null;
    String percentage_t2 = null;
    String percentage_t2_display = null;
    String percentage_sds = null;
    String percentage_sds_display = null;
    String percentage_amex_hds = null;
    String percentage_amex_normal = null;

    String eb_or_not = null;






    String profileparaname,profileparaphone,profileparaemail,profileparaid;

    String Updatetranidapi;
    String updateid;
    Switch meorreceiver;

    LocationManager locationManager;
    double locationlatitude;
    double locationlongitude;
    String latlonglocation;
    String setvaluelatlong;
    String slatitude;
    String slongitude;
    String conveniencefee;
    String fcmtoken;

    double latitudelocation;
    double longitudelocation;


    // Intent intentProceed = new Intent(Payment.this, PWECouponsActivity.class);



    // public static final String SPINNERSAVE = "spinnerprefs" ;
    // public static final String SPINNERNAMESAVE = "spinnernameprefs" ;
    //public static final String PURPOSESAVE = "purposeprefs" ;
    public static final String IDPREFERENCE = "idpref" ;
    //public static final String TRANPREFERENCE = "tranpref" ;

    String sharedprefispid = "";
    String sharedprefisplist = "";
    String sharedprefvendorispid = "";
    String sharedpurposename = "";

    // SharedPreferences spinnerpreference;
    //SharedPreferences spinnernamepreference;
    // SharedPreferences purposepreference;
    SharedPreferences sharedPreferences2;
    //SharedPreferences sharedpreferences3;

    String payoutupdate;

    String easebuzz_tran_id = "";
    String easebuzz_gateway_result = "";
    ProgressBar bottom_progress;
    String holiday_percentage_t1 = "";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);


        setContentView(R.layout.activity_pagetwo);
        textView245=findViewById(R.id.textView245);

        reminderdate();
        percentage_api();

       /* if (Typeoftransaction.transactiontype == "train" || Typeoftransaction.transactiontype.equals("train")){
            System.out.println("T+1 transaction");
            cardpercentage = "2.85";
            apikeyt12 = "165ddff4-7fe4-461a-b9ee-2749e1fa99e2";
            transactiontypestring = "t1";
            getoneETA();
        }else if (Typeoftransaction.transactiontype == "ship" || Typeoftransaction.transactiontype.equals("ship")){
            System.out.println("T+2 transaction");
            cardpercentage = "2.5";
            apikeyt12 = "bcbd8fc4-261d-4484-9e44-fcf62d6219c1";
            transactiontypestring = "t2";
            getETA();
        }else if (Typeoftransaction.transactiontype == "jet" || Typeoftransaction.transactiontype.equals("jet")){
            System.out.println("T+0 transaction");
            cardpercentage = "3.9";
            apikeyt12 = "58c1a0ec-d7c1-4495-b2b2-24af75e1da38";
            transactiontypestring = "t0sds";
            sdsactivation();
        }else{
            System.out.println(Typeoftransaction.transactiontype);
        }

        */

        activity_log_make_payment();

        spinnercasa = findViewById(R.id.spinnercasa);
        isplist = new ArrayList<>();
        ispids = new ArrayList<>();
        ispvendorid = new ArrayList<>();
        loadCASA();
        profileinfo();
        getLocation();
        gettranid();
        getuseridforudf1();
        get_azypay_details();



        purposepaycasa = findViewById(R.id.spinner2);
        paylist = new ArrayList<>();
        payid = new ArrayList<>();
        loadpaypurpose();

        amountfinance=findViewById(R.id.editText11);
        //  formattedValue = String.format("%.2f", financepercentage);
        //  System.out.println(formattedValue);

        isReciever = true;





        // purpose=findViewById(R.id.editText12);
        // spinnerpreference = getSharedPreferences(SPINNERSAVE, Context.MODE_PRIVATE);
        // spinnernamepreference = getSharedPreferences(SPINNERNAMESAVE, Context.MODE_PRIVATE);
        // purposepreference = getSharedPreferences(PURPOSESAVE, Context.MODE_PRIVATE);
        sharedPreferences2 = getSharedPreferences(IDPREFERENCE, Context.MODE_PRIVATE);
        //sharedpreferences3 = getSharedPreferences(TRANPREFERENCE, Context.MODE_PRIVATE);
        //toggle switch
        toggle=(Switch)findViewById(R.id.toggleButton2);
        switch2=(Switch)findViewById(R.id.switch2);
        // Context context = null;
        //switch2.setTypeface(ResourcesCompat.getFont(context, R.font.montserrat));
        final TextView paybyme=findViewById(R.id.textView6);

        paybutton = (Button) findViewById(R.id.button12);

        amextext = (TextView) findViewById(R.id.textView185);

        // Disable button
        //   paybutton.setEnabled(true);
        //   paybutton.setBackgroundColor(Color.parseColor("#dfdfdf"));

        spinnercasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();
                    } else {

//save this value in shared pref
                        String ispid = ispids.get(position);
                        System.out.println("This ------------------ is---------------ispid------------");
                        System.out.println(ispid);
                        String isplists = isplist.get(position);
                        String ispvendorlist = ispvendorid.get(position);
                        sharedprefispid = ispid;
                        sharedprefisplist = isplists;
                        sharedprefvendorispid = ispvendorlist;
                    /*    SharedPreferences.Editor editor = spinnerpreference.edit();
                        System.out.println("Hi I am here");
                        System.out.println(ispid);
                        editor.putString("id",ispid);
                        editor.apply();

                        */

                        //-------casa name save-------//
                     /*   SharedPreferences.Editor editorcasaname = spinnernamepreference.edit();
                        System.out.println("Hi I am here");
                        System.out.println(isplists);
                        editorcasaname.putString("first_name",isplists);
                        editorcasaname.apply();

                        */

                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception in loadisp():\n" + e.getMessage());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        purposepaycasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {



                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

                        //save this value in shared pref
                        String paynamelist = paylist.get(position);
                        sharedpurposename = paynamelist;
                      /*  SharedPreferences.Editor editor = purposepreference.edit();
                        System.out.println("Hi purpose id is here");
                        System.out.println(paynamelist);
                        editor.putString("purpose",paynamelist);
                        editor.apply();
                        */

                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception in loadisp():\n" + e.getMessage());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });

        paybutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

             /*   SharedPreferences sharedPreferences1 = Payment.this.getSharedPreferences("spinnerprefs", Context.MODE_PRIVATE);
                final String casaid=sharedPreferences1.getString("id","");
                System.out.println("this is the limit");
                System.out.println(casaid);
                final String casa2id=casaid;


                SharedPreferences sharedPreferencescasaname = Payment.this.getSharedPreferences("spinnernameprefs", Context.MODE_PRIVATE);
                final String casaname=sharedPreferencescasaname.getString("first_name","");
                System.out.println("this is the nnnnnnnnnnnnnnaaaaaaaaaaaaaammmmmmmmmmmmmmeeeeeeeeeeee limit");
                System.out.println(casaname);
                final String casa2name=casaname;
                System.out.println("this is the 222222222nnnnnnnnnnnnnnaaaaaaaaaaaaaammmmmmmmmmmmmmeeeeeeeeeeee limit");
                System.out.println(casa2name);



                SharedPreferences sharedPreferencespurpose = Payment.this.getSharedPreferences("purposeprefs", Context.MODE_PRIVATE);
                final String purposepayid=sharedPreferencespurpose.getString("purpose","");
                System.out.println("this is the limit");
                System.out.println(purposepayid);
                final String purposepay2id=purposepayid;

                 */


                if (validation() == false) {
                    return;
                }


                System.out.println("This is order id after button click");
                System.out.println(getpgtran);


                //-------------old-----------------//
                final Spinner casaidspinner = spinnercasa;
                //--------------new--------------------//
                //   Random rnd = new Random();
                //  int n = 100000 + rnd.nextInt(900000);
                //  SampleAppConstants.PG_ORDER_ID = Integer.toString(n);

                if ((isswitchreceiver == true) && ((Typeoftransaction.transactiontype.equals("hds")) || (Typeoftransaction.transactiontype.equals("hds_jet")))) {
                    cardpercentage = percentage_amex_hds;
                    percentage_dis = percentage_amex_hds;
                    cardtypetext = "AMEXDINER";
                }else if ((isswitchreceiver == true) && ((!Typeoftransaction.transactiontype.equals("hds")) || (!Typeoftransaction.transactiontype.equals("hds_jet")))){
                    cardpercentage = percentage_amex_normal;
                    percentage_dis = percentage_amex_normal;
                    cardtypetext = "AMEXDINER";
                }
                else {
                    //cardpercentage = "2.5";
                    cardpercentage.equals(cardpercentage);
                    percentage_dis.equals(percentage_dis);
                    cardtypetext = "Other";
                }


                if (isReciever == true) {
                    // strAmout = amountfinance.getText().toString().trim();
                    String fakestramount = amountfinance.getText().toString().trim();
                    //  System.out.println(formattedValue);
                    //------fixed Amount Calculation-----//
                    final double stramtfloaformat = Double.parseDouble(fakestramount);
                    strAmout = fakestramount;
                    final double fixedamountcalculationinteger = Double.parseDouble(strAmout);
                    final double cardpercentageconvertion = Double.parseDouble(cardpercentage);
                    fixedamountcalculation = (float) (fixedamountcalculationinteger - ((fixedamountcalculationinteger / 100) * cardpercentageconvertion));
                    String strdecimalformat = new DecimalFormat("##.##").format(Double.parseDouble(String.valueOf(fixedamountcalculation)));
                    System.out.println("This is the strdecimal format : " + strdecimalformat);
                    fixedamount = Float.toString(Float.parseFloat(strdecimalformat));
                    System.out.println("This is fixed Amount withouuuuut toggle click");
                    System.out.println(fixedamount);
                    conveniencefee = "Receiver";

                } else {
                    String percentageconvert = amountfinance.getText().toString();

                    final double financeper = Double.parseDouble(percentageconvert);
                    final double cardpercentageconvertion = Double.parseDouble(cardpercentage);
                    financepercentage = (float) (((financeper / 100) * cardpercentageconvertion) + financeper);

                    String financeconvert = Float.toString(financepercentage);
                    final double stramtfloaformat = Double.parseDouble(financeconvert);
                    String strdecimalformat = new DecimalFormat("##.##").format(stramtfloaformat);
                    System.out.println("This is decimal fromatted : " + strdecimalformat);


                    //strAmout = Float.toString(financepercentage) ;
                    strAmout = strdecimalformat;
                    fixedamount = percentageconvert;
                    System.out.println("This is fixed Amount with toggle click");
                    System.out.println(fixedamount);
                    conveniencefee = "Me";
                }
                //  DecimalFormat decimalFormat = new DecimalFormat("#.00");
                //String Decinumber = decimalFormat.format(str);
                System.out.println("StrAmount = " + strAmout);


                final PaymentParams pgPaymentParams = new PaymentParams();
                //pgPaymentParams.setAPiKey(SampleAppConstants.PG_API_KEY);
                String apikey_modify_amount = amountfinance.getText().toString();
                final double api_key_converted = Double.parseDouble(apikey_modify_amount);

                pgPaymentParams.setAPiKey(apikeyt12);

                pgPaymentParams.setAmount(strAmout);
                pgPaymentParams.setEmail(profileparaemail);
                pgPaymentParams.setName(profileparaname);
                pgPaymentParams.setPhone(profileparaphone);
                pgPaymentParams.setOrderId(getpgtran);
                System.out.println("this is the ttttttttttttttttttttttransaaaaaaaaaaaaaction id");
                System.out.println(getpgtran);

                pgPaymentParams.setCurrency(SampleAppConstants.PG_CURRENCY);
                pgPaymentParams.setDescription(conveniencefee);
                pgPaymentParams.setCity(cardtypetext);
                pgPaymentParams.setState(transactiontypestring);
                pgPaymentParams.setAddressLine1(getetaapi);
                pgPaymentParams.setAddressLine2(fixedamount);
                pgPaymentParams.setZipCode(SampleAppConstants.PG_ZIPCODE);
                pgPaymentParams.setCountry(sharedpurposename);
                pgPaymentParams.setReturnUrl(SampleAppConstants.PG_RETURN_URL);
                pgPaymentParams.setMode(SampleAppConstants.PG_MODE);
                pgPaymentParams.setUdf1(useridudf1);
                pgPaymentParams.setUdf2(sharedprefispid);
                pgPaymentParams.setUdf3(sharedprefisplist);
                pgPaymentParams.setUdf4(sharedprefvendorispid);
                pgPaymentParams.setUdf5(SampleAppConstants.PG_UDF5);



                final View dialogView = getLayoutInflater().inflate(R.layout.bottom_sheet, null);
                bottom_progress = dialogView.findViewById(R.id.progressBar12);
                TextView enteredamount = dialogView.findViewById(R.id.textView189);
                TextView cardType = dialogView.findViewById(R.id.textView191);
                final CheckBox checkbox4 = dialogView.findViewById(R.id.checkBox4);
                cardType.setText(cardtypetext);
                TextView conveniencefeetext = dialogView.findViewById(R.id.textView193);
                conveniencefeetext.setText(percentage_dis+" %");
                TextView paidby = dialogView.findViewById(R.id.textView195);
                paidby.setText(conveniencefee);
                TextView benereceiveamount = dialogView.findViewById(R.id.textView197);
                benereceiveamount.setText("₹ "+fixedamount);
                TextView finalamount = dialogView.findViewById(R.id.textView200);
                //finalamount.setText("₹ "+strAmout);
                finalamount.setText("₹ "+strAmout);
                TextView eta = dialogView.findViewById(R.id.textView202);
                eta.setText(getetaapi);
                TextView beneficiaryname = dialogView.findViewById(R.id.textView37);
                // beneficiaryname.setText("Amount "+sharedprefisplist+" will receive");
                beneficiaryname.setText(sharedprefisplist);
                entered = amountfinance.getText().toString().trim();
                enteredamount.setText("₹ "+entered);
                activity_log_bottom_sheet();

                Button gatewaycancel = dialogView.findViewById(R.id.button22);
                gatewaycancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                        Intent intent = new Intent(Payment.this, Payment.class);
                        startActivity(intent);


                    }
                });



                Button gatewaybutton = dialogView.findViewById(R.id.button21);
                gatewaybutton.setText("PAY ₹ "+strAmout);
                gatewaybutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bottom_progress.setVisibility(View.VISIBLE);
                        if (checkbox4.isChecked()){
                            if (transactiontypestring.equals("t0sds") || transactiontypestring.equals("t0hds")){
                                SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                                final String token1 = sharedPreferences.getString("token", "");
                                System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
                                System.out.println(token1);
                                final String token200 = token1;

                                StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/sds_activation",
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    JSONObject transactionresponse = new JSONObject(response);
                                                    System.out.println(transactionresponse);
                                                    sdsacti = transactionresponse.getString("status");
                                                    if (allow_easebuzz.equals("5") && sdsacti.equals("true") && (Typeoftransaction.transactiontype.equals("jet") || Typeoftransaction.transactiontype.equals("hds_jet"))){
                                                        eb_or_not = "yes";
                                                        easebuzz_hash();
                                                    }else if(!allow_easebuzz.equals("5") && sdsacti.equals("true") && (Typeoftransaction.transactiontype.equals("jet") || Typeoftransaction.transactiontype.equals("hds_jet"))){

                                                        bottom_progress.setVisibility(View.GONE);
                                                        eb_or_not = "no";
                                                        PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, Payment.this);
                                                        pgPaymentInitialzer.initiatePaymentProcess();

                                                    }else if (sdsacti.equals("false")){
                                                        bottom_progress.setVisibility(View.GONE);
                                                        new MaterialStyledDialog.Builder(Payment.this)
                                                                .setTitle("SDS Closed")
                                                                .setDescription("SDS transaction has been closed for today, please try T+1 / T+2 transaction.")
                                                                .setHeaderColor(R.color.md_divider_white)
                                                                .setIcon(R.drawable.warninginternet)
                                                                .withDarkerOverlay(true)
                                                                .withDialogAnimation(false)
                                                                .setCancelable(false)
                                                                .withDivider(true)
                                                                .setNegativeText("Cancel")
                                                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                                    @Override
                                                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                        // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                                                        dialog.dismiss();
                                                                    }
                                                                })

                                                                .show();
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        String message = "";

                                        if (error instanceof NetworkError) {
                                            //   message = "cannot profile connect";
                                            new MaterialStyledDialog.Builder(Payment.this)
                                                    .setTitle("Internet Error")
                                                    .setDescription("Check Your Interenet Connection")
                                                    .setHeaderColor(R.color.md_divider_white)
                                                    .setIcon(R.drawable.warninginternet)
                                                    .withDarkerOverlay(true)
                                                    .withDialogAnimation(false)
                                                    .setCancelable(false)
                                                    .withDivider(true)
                                                    .setNegativeText("Cancel")
                                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                                            dialog.dismiss();
                                                        }
                                                    })
                                                    .setPositiveText("Open Settings")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                                            // dialog.dismiss();
                                                        }
                                                    })
                                                    .show();
                                        } else if (error instanceof AuthFailureError) {
                                            message = "There is a problem connecting to server. Please try after sometime.";
                                        } else if (error instanceof NoConnectionError) {
                                            message = "There is a problem connecting to server. Please try after sometime.";
                                        } else if (error instanceof ParseError) {
                                            message = "Connection Timedout. Please try after sometime";
                                        }
                                        if (!message.isEmpty()) {
                                            Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })

                                {

                                    //-------------------------outerheader-----------------------//
                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError{
                                        String T="" ;
                                        Map<String,String> params = new HashMap<>();


                                        // params.put("Content-Type", "application/json; charset=UTF-8");
                                        try {
                                            JSONObject response1 = new JSONObject(token200);
                                            System.out.println("!!!!!!!!!!!!!!!!!!");
                                            System.out.println(response1.getString("token"));
                                            // do your work with response object
                                            T=response1.getString("token");


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        System.out.println("44444444444444444444444444");
                                        System.out.println(token200);
                                        ///check t!blank

                                        String TokenS = "token "+ T;
                                        System.out.println(T);

                                        params.put("Authorization",TokenS);

                                        return params;
                                    }




                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
                                stringRequest.setShouldCache(false);
                                requestQueue.add(stringRequest);
                            }else{
                                if ((allow_easebuzz == "5" || allow_easebuzz.equals("5")) &&  (Typeoftransaction.transactiontype == "train" || Typeoftransaction.transactiontype.equals("train") || Typeoftransaction.transactiontype.equals("hds"))){
                                    eb_or_not = "yes";
                                    easebuzz_hash();
                                    //easebuzz_before_save();
                                }else{
                                    bottom_progress.setVisibility(View.GONE);
                                    eb_or_not = "no";
                                    PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, Payment.this);
                                    pgPaymentInitialzer.initiatePaymentProcess();
                                }

                            }



                        }else{
                            bottom_progress.setVisibility(View.GONE);
                            Toast.makeText(Payment.this, "You have not agreed to the terms and conditions", Toast.LENGTH_SHORT).show();
                            return;
                        }


                    }
                });
                BottomSheetDialog dialog = new BottomSheetDialog(Payment.this);
                dialog.setContentView(dialogView);
                dialog.show();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        onResume();
                    }
                });
                //  PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, Payment.this);
                // pgPaymentInitialzer.initiatePaymentProcess();


                SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                final String token1 = sharedPreferences.getString("token", "");
                System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
                System.out.println(token1);
                final String token200 = token1;



                spinnercasa.setOnItemSelectedListener(Payment.this);
                purposepaycasa.setOnItemSelectedListener(Payment.this);


                StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/casa_transaction",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                System.out.println("********************thisssss***********************");
                                System.out.println(response);

                                try{
                                    JSONObject response4 = new JSONObject(response);
                                    System.out.println("**********this is id********");
                                    System.out.println(response4.getString("id"));
                                }catch(JSONException e){
                                    e.printStackTrace();
                                }

                                SharedPreferences.Editor editor = sharedPreferences2.edit();
                                editor.putString("id",response);
                                editor.apply();

                              /*  try{
                                    JSONObject response2 = new JSONObject(response);
                                    System.out.println("*******************************************thisssssss");
                                    System.out.println(response2.getString("id"));
                                    Updatetranidapi=response2.getString("id");
                                }catch (JSONException e){
                                    e.printStackTrace();
                                }

                                SharedPreferences.Editor editor = sharedPreferences2.edit();
                                editor.putString("id",response);
                                editor.apply();
                                */

                                // Intent intent = new Intent(Main5Activity.this, Main6Activity.class);
                                // startActivity(intent);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message = "";

                        if (error instanceof NetworkError) {
                            message = "cannot connect";
                        } else if (error instanceof AuthFailureError) {
                            message = "";
                        } else if (error instanceof NoConnectionError) {
                            message = "cannot connect";
                        } else if (error instanceof ParseError) {
                            message = "Connection Timedout";
                        }
                        if (!message.isEmpty()) {
                            Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                        }
                    }
                })


                {

                    //-------------------------outerheader-----------------------//
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError{
                        String T="" ;
                        Map<String,String> params = new HashMap<>();


                        // params.put("Content-Type", "application/json; charset=UTF-8");
                        try {
                            JSONObject response1 = new JSONObject(token200);
                            System.out.println("!!!!!!!!!!!!!!!!!!");
                            System.out.println(response1.getString("token"));
                            // do your work with response object
                            T=response1.getString("token");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("44444444444444444444444444");
                        System.out.println(token200);
                        ///check t!blank

                        String TokenS = "token "+ T;
                        System.out.println(T);

                        params.put("Authorization",TokenS);

                        return params;
                    }


                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();


                        if (latlonglocation == null){
                            setvaluelatlong = "Not Provided";
                            System.out.println("this is setvaluelatlong 1");
                            System.out.println(setvaluelatlong);
                        }else{
                            setvaluelatlong = latlonglocation;
                            System.out.println("this is setvaluelatlong 2 (actual location)");
                            System.out.println(setvaluelatlong);
                        }



                        params.put("attempt", "3");
                        params.put("casa_name", sharedprefisplist);
                        params.put("result_id", "5");
                        params.put("amount",strAmout);
                        params.put("casa_id",sharedprefispid);
                        params.put("currency_id", "2");
                        params.put("payout_id", "0");
                        params.put("pg_tran_id", "0");
                        params.put("notes", sharedpurposename);
                        params.put("location",setvaluelatlong);
                        params.put("c_fees",conveniencefee);
                        System.out.println("999999999999999999999999999999999999");
                        System.out.println(latlonglocation);
                        System.out.println("this is setvaluelatlong 2");
                        System.out.println(setvaluelatlong);

                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
                stringRequest.setShouldCache(false);
                requestQueue.add(stringRequest);



            }
        });



/*
        Spinner spinner = findViewById(R.id.button13);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.selectbeneficiary, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        */


       /* Spinner spinner1 = findViewById(R.id.button14);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.purposeofpayment, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter1);
        spinner1.setOnItemSelectedListener(this);

        */



        button10 = (Button) findViewById(R.id.button10) ;
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPagethree();

            }
        });



        imageView9 = (ImageView) findViewById(R.id.imageView9);
        imageView9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPageone();
            }
        });
        //Toggle AMEX switch
        switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean switchb) {

                if (switchb == true){
                    isswitchreceiver = true;
                    switch2.setTextColor(Color.BLACK);
                    amextext.setTextColor(Color.parseColor("#659A08"));

                }else{
                    isswitchreceiver = false;
                    amextext.setTextColor(Color.BLACK);
                    switch2.setTextColor(Color.parseColor("#659A08"));

                }
            }
        });
        //Toggle switch
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            String percentageconvert = amountfinance.getText().toString();
            final int financeper = Integer.parseInt(percentageconvert);
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true){
                    isReciever = false;
                    toggle.setTextColor(Color.BLACK);
                    paybyme.setTextColor(Color.parseColor("#659A08"));
               /* if(paybyme.getCurrentTextColor()==Color.parseColor("#659A08")){
                        paybyme.setTextColor(Color.BLACK);
                    }else {
                        paybyme.setTextColor(Color.parseColor("#659A08"));
                    } if(toggle.getCurrentTextColor()==Color.BLACK){
                        toggle.setTextColor(Color.parseColor("#659A08"));
                    }else {
                        toggle.setTextColor(Color.BLACK);
                    }

                */

                    //   System.out.println("000000000000000000000000this is toggle on 0000000000000000000000000000");



                    //       financepercentage=(float)(((financeper/100)*2.5)+financeper);
                    //     System.out.println(financepercentage);
                }else{
                    isReciever = true;
                    toggle.setTextColor(Color.parseColor("#659A08"));
                    paybyme.setTextColor(Color.BLACK);
                    System.out.println("000000000000000000000000this is toggle off 0000000000000000000000000000");

                    financepercentage = (float)((financeper*1));

                }
            }
        });
    }






    private boolean validation(){
        getLocation();
        locationupdateprofile();

        if (((Typeoftransaction.transactionamount.equals("null")) || Typeoftransaction.transactionamount == "null") && Float.parseFloat(amountfinance.getText().toString())> 100){
            new MaterialStyledDialog.Builder(Payment.this)
                    .setTitle("100 Rs. for first transaction")
                    .setDescription("New users are allowed to do only 100 Rs for their first transaction.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            dialog.dismiss();
                        }
                    })

                    .show();

            return false;
        }
        if (((Typeoftransaction.transactionamount.equals("null")) || Typeoftransaction.transactionamount == "null")){
            Typeoftransaction.transactionamount = "0";
        }



        if (Double.parseDouble(Typeoftransaction.transactionamount) < Double.parseDouble("110") && (Dashboard.kycverification.equals("Ret"))){
            new MaterialStyledDialog.Builder(Payment.this)
                    .setTitle("Resend KYC")
                    .setDescription("Please Re-send your KYC details by clicking UPLOAD KYC button below.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            Intent homeIntent = new Intent(Payment.this, Dashboard.class);
                            startActivity(homeIntent);
                            dialog.dismiss();
                        }
                    })
                    .setPositiveText("Upload KYC")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            Intent homeIntent = new Intent(Payment.this, grantreadandwritepermission.class);
                            startActivity(homeIntent);
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .show();
            return false;
        }
        if (Double.parseDouble(Typeoftransaction.transactionamount) < Double.parseDouble("110") && (Dashboard.kycverification.equals("P"))){
            new MaterialStyledDialog.Builder(Payment.this)
                    .setTitle("KYC Pending")
                    .setDescription("Your KYC has been received and is under review. Please wait till we verify your KYC.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            Intent homeIntent = new Intent(Payment.this, Dashboard.class);
                            startActivity(homeIntent);
                            dialog.dismiss();
                        }
                    })
                    .show();
            return false;
        }
        if (Double.parseDouble(Typeoftransaction.transactionamount) < Double.parseDouble("110") && (Dashboard.kycverification.equals("Rej"))){
            new MaterialStyledDialog.Builder(Payment.this)
                    .setTitle("KYC Rejected")
                    .setDescription("Your KYC has been REJECTED. Please contact support for further assistance.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            Intent homeIntent = new Intent(Payment.this, Dashboard.class);
                            startActivity(homeIntent);
                            dialog.dismiss();
                        }
                    })

                    .show();
            return false;
        }
       /* if ((Typeoftransaction.transactionamount.equals("100.0") || Typeoftransaction.transactionamount.equals("100") ||Typeoftransaction.transactionamount.equals("102.5") || Typeoftransaction.transactionamount.equals("102.85") || Typeoftransaction.transactionamount.equals("97.5") || Typeoftransaction.transactionamount.equals("97.15")) && Dashboard.kycverification.equals("N.V") ){
            new MaterialStyledDialog.Builder(Payment.this)
                    .setTitle("KYC not verified")
                    .setDescription("Please send your KYC details (your selfie and pan card image) by clicking UPLOAD KYC button below.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            dialog.dismiss();
                        }
                    })
                    .setPositiveText("Upload KYC")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            Intent homeIntent = new Intent(Payment.this, grantreadandwritepermission.class);
                            startActivity(homeIntent);
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .show();
            return false;
        }*/

        if ((Float.parseFloat(amountfinance.getText().toString())> 100 ) && ((Dashboard.kycverification == "N.V") || (Dashboard.kycverification.equals("N.V")))){
            new MaterialStyledDialog.Builder(Payment.this)
                    .setTitle("KYC not verified")
                    .setDescription("Please send your KYC details by clicking UPLOAD KYC button below.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            Intent homeIntent = new Intent(Payment.this, Dashboard.class);
                            startActivity(homeIntent);
                            dialog.dismiss();
                        }
                    })
                    .setPositiveText("Upload KYC")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            Intent homeIntent = new Intent(Payment.this, grantreadandwritepermission.class);
                            startActivity(homeIntent);
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .show();
            return false;
        }


        if (sharedprefispid.isEmpty() || sharedprefispid.length() == 0 || sharedprefispid.equals("") || sharedprefispid == null){
            Toast.makeText(Payment.this,"Please Select Benefciary ",Toast.LENGTH_LONG).show();
            return false;
        }
        if (sharedpurposename.isEmpty() || sharedpurposename.length() == 0 || sharedpurposename.equals("") || sharedpurposename == null){
            Toast.makeText(Payment.this,"Please Select Purpose of Payment ",Toast.LENGTH_LONG).show();
            return false;
        }
        if (Float.parseFloat(amountfinance.getText().toString())< 100 ){
            Toast.makeText(Payment.this,"Please note , the minimum amount is 100 ",Toast.LENGTH_LONG).show();
            return false;
        }

        return true;


    }

    //-------------------spinner-----------------------//
    private void loadCASA() {
        try {
            Log.e(TAG,"________________________________________________________________");
            isplist.clear();
            ispids.clear();
            ispvendorid.clear();
            StringRequest stringRequest1 = new StringRequest(Request.Method.GET, SERVER + "/api/save_casa",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e(TAG, response);
                            //beatslist.add("Select Beats");
                            try {

                                JSONArray jsonArray = new JSONArray(response);
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        if (i == 0) {
                                            isplist.add("Select Beneficiary");
                                            ispids.add("0");
                                            ispvendorid.add("0");
                                        }
                                        JSONObject dataObject = jsonArray.optJSONObject(i);
                                        String beats = dataObject.optString("first_name");
                                        String nick = dataObject.optString("second_name");
                                        String ids = dataObject.optString("id");
                                        String vid = dataObject.optString("notes");
                                        System.out.println("this is vid");
                                        System.out.println(vid);
                                        //   SharedPreferences.Editor editor = spinnerpreference.edit();

                                        // editor.putString("casa_id",ids);
                                        // editor.apply();
                                        // System.out.println("This");
                                        // System.out.println(ids);
                                        isplist.add(beats + "(" +nick+ ")");
                                        ispids.add(ids);
                                        ispvendorid.add(vid);
                                    }
                                    Log.e("Aakash",isplist+"");
                                    // Creating adapter for spinner

                                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(Payment.this, R.layout.spinner_item, isplist);
                                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    spinnercasa.setAdapter(dataAdapter2);


                                }
                                else {
                                    isplist.add("No Beneficiary Available");
                                    ispids.add("0");
                                    ispvendorid.add("0");
                                    // Creating adapter for spinner
                                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(Payment.this, R.layout.spinner_item, isplist);
                                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    spinnercasa.setAdapter(dataAdapter2);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String message = "";

                            if (error instanceof NetworkError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ServerError) {
                                Log.e("stackcheck1111", "3" + error.toString());
                                NetworkResponse response = error.networkResponse;
                                if (response != null) {
                                    Log.d("qqqqqqqqqqq", String.valueOf(response.statusCode));

                                    int statusCode = response.statusCode;
                                    if (statusCode == 404) {
                                        message = "No ISP Available in this Company";
                                        Log.d("qqqqqq", " " + response.data);

                                        //ShowAlert("Server is down. Please try after some time!");
                                        //json = new String(response.data);
                                        //json = trimMessage(json, "status");
                                        // if (json != null)
                                        //    message = json;


                                    } else {
                                        message = "The server could not be found. Please try again after some time!!";

                                    }
                                } else {
                                    message = "The server could not be found. Please try again after some time!!";

                                }
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                            }
                            Log.e("eeeee", "onErrorResponse: " + error.toString());
                            if (!message.isEmpty()) {
                                Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    })


            {

                SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                final String token1 = sharedPreferences.getString("token", "");
                final String token200 = token1;

                //-------------spinnerheader-----------------//

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError{
                    String T="" ;
                    Map<String,String> params = new HashMap<>();


                    // params.put("Content-Type", "application/json; charset=UTF-8");
                    try {
                        JSONObject response1 = new JSONObject(token200);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        // do your work with response object
                        T=response1.getString("token");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("44444444444444444444444444");
                    System.out.println(token200);
                    ///check t!blank

                    String TokenS = "token "+ T;
                    System.out.println(T);

                    params.put("Authorization",TokenS);

                    return params;
                }

            };


            RequestQueue requestQueue1 = Volley.newRequestQueue(Payment.this);
            stringRequest1.setShouldCache(false);
            requestQueue1.add(stringRequest1);
        } catch (Exception e) {
            Log.d(TAG, "Exception in loadISPList():\n" + e.getMessage());
        }
    }

    public void openPageone(){
        finish();
    }
    public void openPagethree(){
        Intent intent = new Intent(this, AddBeneficiary.class);
        intent.putExtra("FROM_ACTIVITY", "Payment");
        startActivity(intent);
        finish();
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(data);
        System.out.println("This is new\n\n");
        // System.out.println(data.getStringExtra("payment_response"));
        // System.out.println(data.getStringExtra("result"));


        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;

        if (requestCode == PWEStaticDataModel.PWE_REQUEST_CODE) {
            // Request recieved by EB
            if (data == null) {
                easebuzz_gateway_result = "crash";
            } else {
                easebuzz_gateway_result = "no_crash";
                System.out.println("Easebuzz");
                System.out.println(data.getStringExtra("result"));

                if (data.getStringExtra("result") == null) {
                    new MaterialStyledDialog.Builder(this)

                            .setTitle("Error")
                            .setDescription("There was an error in your transaction, if the amount has been debited from your account it will be credited to your beneficiary bank account within T+2 bank working days.")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.transactionfailed)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(true)
                            .setCancelable(false)
                            .withDivider(true)
                            .setPositiveText("Got it")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    finish();
                                }
                            })
                            .show();
                } else {

                    if ((data.getStringExtra("result") == "user_cancelled") || (data.getStringExtra("result").equals("user_cancelled"))) {
                        new MaterialStyledDialog.Builder(this)
                                .setTitle("Transaction Cancelled")
                                .setDescription("You have cancelled the Transaction")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.transactionfailed)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(true)
                                .setCancelable(false)
                                .withDivider(true)
                                .setPositiveText("Got it")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        Intent intent = new Intent(Payment.this, new_design_transaction.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                                .show();
                    } else {
                        String parse_response = data.getStringExtra("payment_response");
                        try {
                            JSONObject profileresponse = new JSONObject(parse_response);


                            AddressLine1 = getetaapi;//
                            AddressLine2 = fixedamount;//
                            Amountdone = profileresponse.getString("amount");//
                            Cardmask = profileresponse.getString("cardnum");//
                            Citydone = cardtypetext;//
                            Countrydone = sharedpurposename;//
                            Descriptiongiven = profileresponse.getString("productinfo");//
                            Emailgiven = profileresponse.getString("email");//
                            Errordescription = profileresponse.getString("error");//
                            Hashvalue = profileresponse.getString("hash");//
                            cardName = profileresponse.getString("firstname");//
                            Orderno = profileresponse.getString("easepayid");//
                            channelpayment = profileresponse.getString("payment_source");//
                            modeofpay = profileresponse.getString("card_type");//
                            cell = profileresponse.getString("phone");//
                            responsecode = profileresponse.getString("error_Message");//
                            responsemessage = profileresponse.getString("status");//
                            easebuzz_bank_Ref = profileresponse.getString("bank_ref_num");//
                            Statedone = transactiontypestring;//
                            Transactionid = profileresponse.getString("txnid");//

                            udf01 = profileresponse.getString("udf1");//
                            udf02 = profileresponse.getString("udf2");//
                            udf03 = profileresponse.getString("udf3");//
                            udf04 = profileresponse.getString("udf4");//
                            udf05 = profileresponse.getString("udf5");//


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if ((data.getStringExtra("result") == "payment_successfull") || (data.getStringExtra("result").equals("payment_successfull"))) {
                            System.out.println(data.getStringExtra("payment_response"));
                            payoutupdate = "Pending";
                            if ((Statedone.equals("t1")) || (Statedone.equals("t1_azypay")) || (Statedone.equals("hds"))) {
                                sendsuccessfulonedaysms();
                            } else if (Statedone.equals("t2")) {
                                sendsuccessfulsms();
                            } else if (Statedone.equals("t0sds") || Statedone.equals("t0hds")) {
                                sendsuccessful_sds_sms();
                            }
                            new MaterialStyledDialog.Builder(this)
                                    .setTitle("Transaction Successful")
                                    .setDescription("Your amount will be credited by " + AddressLine1 + ".\nRate us on playstore.")
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.tickicon)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(true)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .setPositiveText("Got it")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            Intent intent = new Intent(Payment.this, new_design_transaction.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    })
                                    .show();

                        } else {
                            payoutupdate = "Failed";
                            new MaterialStyledDialog.Builder(this)
                                    .setTitle("Transaction Failed")
                                    .setDescription(responsemessage)
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.transactionfailed)
                                    .withIconAnimation(false)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(true)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .setPositiveText("Got it")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            Intent intent = new Intent(Payment.this, new_design_transaction.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    })
                                    .show();
                        }
                        easebuzz_transave();
                    }


                }

            }
        } else if (requestCode == PGConstants.REQUEST_CODE) {
            easebuzz_gateway_result = "no_crash";
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String paymentResponse = data.getStringExtra(PGConstants.PAYMENT_RESPONSE);
                    System.out.println("paymentResponse: " + paymentResponse);


                    if (paymentResponse.equals("null")) {
                        System.out.println("Transaction Error!");


                        new MaterialStyledDialog.Builder(this)

                                .setTitle("Error")
                                .setDescription("There was an error in your transaction, if the amount has been debited from your account it will be creadited to your beneficiary bank account within T+2 bank working days.")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.transactionfailed)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(true)
                                .setCancelable(false)
                                .withDivider(true)
                                .setPositiveText("Got it")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        finish();
                                    }
                                })
                                .show();


                    } else {

                        JSONObject response = new JSONObject(paymentResponse);

                        AddressLine1 = response.getString("address_line_1");
                        AddressLine2 = response.getString("address_line_2");
                        Amountdone = response.getString("amount");
                        Cardmask = response.getString("cardmasked");
                        Citydone = response.getString("city");
                        Countrydone = response.getString("country");
                        Currencydone = response.getString("currency");
                        Descriptiongiven = response.getString("description");
                        Emailgiven = response.getString("email");
                        Errordescription = response.getString("error_desc");
                        Hashvalue = response.getString("hash");
                        cardName = response.getString("name");
                        Orderno = response.getString("order_id");
                        channelpayment = response.getString("payment_channel");
                        modeofpay = response.getString("payment_mode");
                        cell = response.getString("phone");
                        responsecode = response.getString("response_code");
                        responsemessage = response.getString("response_message");

                        Statedone = response.getString("state");
                        Transactionid = response.getString("transaction_id");

                       /* SharedPreferences.Editor editor = sharedpreferences3.edit();
                        editor.putString("transaction_id",paymentResponse);
                        editor.apply();

                        */


                        udf01 = response.getString("udf1");
                        udf02 = response.getString("udf2");
                        udf03 = response.getString("udf3");
                        udf04 = response.getString("udf4");
                        udf05 = response.getString("udf5");
                        pincode = response.getString("zip_code");
                    }
                    if (responsemessage.equals("Transaction successful")) {
                        if ((Statedone.equals("t1")) || (Statedone.equals("t1_azypay")) || Statedone.equals("hds")) {
                            sendsuccessfulonedaysms();
                        } else if (Statedone.equals("t2")) {
                            sendsuccessfulsms();
                        } else if (Statedone.equals("t0sds") || Statedone.equals("t0hds")) {
                            sendsuccessful_sds_sms();
                        }
                        payoutupdate = "Pending";
                        new MaterialStyledDialog.Builder(this)
                                .setTitle("Transaction Successful")
                                .setDescription("Your amount will be credited by " + AddressLine1 + ".\nPlease Rate us on playstore.")
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.tickicon)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(true)
                                .setCancelable(false)
                                .withDivider(true)
                                .setPositiveText("Got it")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        Intent intent = new Intent(Payment.this, new_design_transaction.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                                .show();
                    } else {
                        payoutupdate = "Failed";
                        new MaterialStyledDialog.Builder(this)
                                .setTitle("Transaction Failed")
                                .setDescription(responsemessage)
                                .setHeaderColor(R.color.md_divider_white)
                                .setIcon(R.drawable.transactionfailed)
                                .withIconAnimation(false)
                                .withDarkerOverlay(true)
                                .withDialogAnimation(true)
                                .setCancelable(false)
                                .withDivider(true)
                                .setPositiveText("Got it")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        Intent intent = new Intent(Payment.this, new_design_transaction.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }



      /*  SharedPreferences sharedPreferencescasaname = Payment.this.getSharedPreferences("spinnernameprefs", Context.MODE_PRIVATE);
        final String casaname=sharedPreferencescasaname.getString("first_name","");
        System.out.println("this is the nnnnnnnnnnnnnnaaaaaaaaaaaaaammmmmmmmmmmmmmeeeeeeeeeeee limit");
        System.out.println(casaname);
        final String casa2name=casaname;
        */


            StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/save_PG_Tran_Response",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            System.out.println("*******************************************");
                            System.out.println(response);


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String message = "";
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ServerError) {
                        message = "";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof ParseError) {
                        message = "Cannot connect to internet......please check your internet connection";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection Timedout......please check your internet connection";
                    }
                    if (!message.isEmpty()) {
                        Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }) {


                //This is for Headers If You Needed

                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("address_line_1", AddressLine1);
                    params.put("address_line_2", AddressLine2);
                    params.put("amount", Amountdone);
                    params.put("cardmasked", Cardmask);
                    params.put("city", Citydone);
                    params.put("country", Countrydone);
                    params.put("currency", Currencydone);
                    params.put("description", Descriptiongiven);
                    params.put("email", Emailgiven);
                    params.put("error_desc", "Waste");
                    params.put("hash", Hashvalue);
                    params.put("name", cardName);
                    params.put("order_id", Orderno);
                    params.put("payment_channel", channelpayment);
                    params.put("payment_mode", modeofpay);
                    params.put("phone", cell);
                    params.put("response_code", responsecode);
                    params.put("response_message", responsemessage);
                    params.put("state", Statedone);
                    params.put("transaction_id", Transactionid);
                    params.put("udf1", udf01);
                    params.put("udf2", udf02);
                    params.put("udf3", udf03);
                    params.put("udf4", udf04);
                    params.put("udf5", udf05);
                    params.put("zip_code", pincode);

                    return params;

                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String T = "";
                    Map<String, String> params = new HashMap<>();


                    // params.put("Content-Type", "application/json; charset=UTF-8");
                    try {
                        JSONObject response1 = new JSONObject(token200);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        // do your work with response object
                        T = response1.getString("token");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("44444444444444444444444444");
                    System.out.println(token200);
                    ///check t!blank

                    String TokenS = "token " + T;
                    System.out.println(T);

                    params.put("Authorization", TokenS);

                    return params;
                }
            };

            updatebtransaction();

            RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        }
    }

    private void easebuzz_before_save(){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/easebuzzsavepgtran",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("*******************************************");
                        System.out.println(response);

                        Intent intentProceed = new Intent(Payment.this, PWECouponsActivity.class);
                        intentProceed.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); // This is mandatory flag
                        intentProceed.putExtra("key","W33DPORCU5");
                        intentProceed.putExtra("address1",getetaapi);//
                        intentProceed.putExtra("address2",fixedamount);//
                        intentProceed.putExtra("city",cardtypetext);//
                        intentProceed.putExtra("state",transactiontypestring);//
                        intentProceed.putExtra("txnid",easebuzz_tran_id);//
                        intentProceed.putExtra("amount",Float.valueOf(strAmout));//
                        intentProceed.putExtra("productinfo",conveniencefee);//
                        intentProceed.putExtra("surl",SERVER+"/api/easebuzzsaveserverresponse");
                        intentProceed.putExtra("furl",SERVER+"/api/easebuzzsaveserverresponse");
                        intentProceed.putExtra("country",sharedpurposename);//
                        intentProceed.putExtra("firstname",profileparaname);//
                        intentProceed.putExtra("email",profileparaemail);//
                        intentProceed.putExtra("phone",profileparaphone);//
                        intentProceed.putExtra("pay_mode","production");
                        intentProceed.putExtra("udf1",useridudf1);//
                        intentProceed.putExtra("udf2",sharedprefispid);//
                        String[] separated = sharedprefisplist.split("\\(");
                        intentProceed.putExtra("udf3",String.valueOf(separated[0]));//
                        intentProceed.putExtra("udf4",sharedprefvendorispid);//
                        intentProceed.putExtra("udf5",SampleAppConstants.PG_UDF5);//
                        intentProceed.putExtra("hash",gateway_easebuz_hash);
                        bottom_progress.setVisibility(View.GONE);

                        startActivityForResult(intentProceed, PWEStaticDataModel.PWE_REQUEST_CODE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {


            //This is for Headers If You Needed

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("address_line_1", getetaapi);//
                params.put("address_line_2", fixedamount);//
                params.put("amount", strAmout);//
                params.put("city", cardtypetext);//
                params.put("name", profileparaname);//
                params.put("phone", profileparaphone);//
                params.put("country", sharedpurposename);//
                params.put("email", profileparaemail);//
                params.put("order_id", getpgtran);//
                params.put("state", transactiontypestring);//
                params.put("txnid", easebuzz_tran_id);//
                if (Typeoftransaction.transactiontype.equals("hds_jet") || Typeoftransaction.transactiontype.equals("hds")){
                    params.put("holiday_settle","yes");
                }else{
                    params.put("holiday_settle","no");
                }

                return params;

            }


        };


        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    private void crash_message(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/easebuzzcrashsms",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("*******************************************");
                        System.out.println(response);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {


            //This is for Headers If You Needed
            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    private void easebuzz_transave() {



        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/easebuzzsaveserverresponse",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("*******************************************");
                        System.out.println(response);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {


            //This is for Headers If You Needed

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("amount", Amountdone);
                params.put("cardnum", Cardmask);
                params.put("productinfo", Descriptiongiven);
                params.put("email", Emailgiven);
                params.put("error", Errordescription);
                params.put("hash", Hashvalue);
                params.put("firstname", cardName);
                params.put("easepayid", getpgtran);
                params.put("payment_source", channelpayment);
                params.put("card_type", modeofpay);
                params.put("phone", cell);
                params.put("status", responsemessage);
                params.put("txnid", Transactionid);
                params.put("udf1", udf01);
                params.put("udf2", udf02);
                params.put("udf3", udf03);
                params.put("udf4", udf04);
                params.put("udf5", udf05);
                params.put("bank_ref_num", easebuzz_bank_Ref);

                return params;

            }


        };

        updatebtransaction();

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }
    public void reminderdate() {

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/ccdate_reminder",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);
                            System.out.println("*******************************************");
                            System.out.println(response);
                            String name = json.optString("status");
                            System.out.println(name);
                            if(!name.equals("False")){
                                textView245.setText(name);
                                textView245.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }





                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


            //This is for Headers If You Needed

        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }




    private void easebuzz_hash(){



        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/easebuzzhash",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing
                        try{
                            JSONObject parse_response_hash = new JSONObject(response);
                            gateway_easebuz_hash =  parse_response_hash.getString("hash");

                            easebuzz_before_save();

                        }catch (JSONException error){
                            System.out.println(error);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Payment.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<>();


                params.put("txnid",easebuzz_tran_id);
                System.out.println("this is hash");
                System.out.println(easebuzz_tran_id);
                params.put("amount",String.valueOf(Float.parseFloat(strAmout)));
                System.out.println(String.valueOf(Float.parseFloat(strAmout)));
                params.put("productinfo",conveniencefee);
                System.out.println(conveniencefee);
                params.put("firstname",profileparaname);
                System.out.println(profileparaname);
                params.put("email_id",profileparaemail);
                System.out.println(profileparaemail);
                params.put("udf1",useridudf1);
                System.out.println(useridudf1);
                params.put("udf2",sharedprefispid);
                System.out.println(sharedprefispid);
                String[] separated = sharedprefisplist.split("\\(");
                params.put("udf3",String.valueOf(separated[0]));
                System.out.println(String.valueOf(separated[0]));
                params.put("udf4",sharedprefvendorispid);
                System.out.println(sharedprefvendorispid);
                params.put("udf5",SampleAppConstants.PG_UDF5);
                System.out.println(SampleAppConstants.PG_UDF5);
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


    private void activity_log_make_payment(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Payment.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","107");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }
//-----------------Holiday Finder-----------------//
private void check_holiday_settlement(){

    SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
    final String token1 = sharedPreferences.getString("token", "");
    System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
    System.out.println(token1);
    final String token200 = token1;

    StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/holidayfinder",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject profileresponse = new JSONObject(response);
                        if(profileresponse.getString("status").equals("Success")){
                            if(profileresponse.getString("tomorrow_holiday").equals("True")){
                                getetaapi = profileresponse.getString("eta_t1_date");
                            }else{
                                new MaterialStyledDialog.Builder(Payment.this)
                                        .setTitle("HDS Closed")
                                        .setDescription("HDS transaction has been closed for today, please try Other transaction.")
                                        .setHeaderColor(R.color.md_divider_white)
                                        .setIcon(R.drawable.warninginternet)
                                        .withDarkerOverlay(true)
                                        .withDialogAnimation(false)
                                        .setCancelable(false)
                                        .withDivider(true)
                                        .setNegativeText("Cancel")
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                                finish();
                                                dialog.dismiss();
                                            }
                                        })

                                        .show();
                            }

                        }else{
                            new MaterialStyledDialog.Builder(Payment.this)
                                    .setTitle("HDS Closed")
                                    .setDescription("HDS transaction has been closed for today, please try Other transaction.")
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.warninginternet)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(false)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .setNegativeText("Cancel")
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                            finish();
                                            dialog.dismiss();
                                        }
                                    })

                                    .show();
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            String message = "";

            if (error instanceof NetworkError) {
                message = "cannot connect";
            } else if (error instanceof AuthFailureError) {
                message = "";
            } else if (error instanceof NoConnectionError) {
                message = "cannot connect";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout";
            }
            if (!message.isEmpty()) {
                Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
            }
        }
    })

    {

        //-------------------------outerheader-----------------------//
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError{
            String T="" ;
            Map<String,String> params = new HashMap<>();


            // params.put("Content-Type", "application/json; charset=UTF-8");
            try {
                JSONObject response1 = new JSONObject(token200);
                System.out.println("!!!!!!!!!!!!!!!!!!");
                System.out.println(response1.getString("token"));
                // do your work with response object
                T=response1.getString("token");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println("44444444444444444444444444");
            System.out.println(token200);
            ///check t!blank

            String TokenS = "token "+ T;
            System.out.println(T);

            params.put("Authorization",TokenS);

            return params;
        }


    };

    RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
    stringRequest.setShouldCache(false);
    requestQueue.add(stringRequest);


}
    //----------------END---------------------//
//---------------percentage api-------------------//

    private void percentage_api(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/percentage",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            percentage_t1=profileresponse.getString("t11");
                            percentage_t1_display = profileresponse.getString("t11");
                            percentage_t2=profileresponse.getString("t22");
                            percentage_t2_display = profileresponse.getString("t22");
                            percentage_sds=profileresponse.getString("sds");
                            percentage_sds_display = profileresponse.getString("sds");
                            holiday_percentage_t1 = profileresponse.getString("holidayt1");
                            percentage_amex_hds = profileresponse.getString("holidayamex");
                            percentage_amex_normal = profileresponse.getString("amex");

                            if (Typeoftransaction.transactiontype == "train" || Typeoftransaction.transactiontype.equals("train")){
                                System.out.println("T+1 transaction");
                                System.out.println("This is percentage T1");
                                cardpercentage = percentage_t1;
                                System.out.println(percentage_t1);
                                percentage_dis = percentage_t1_display;
                                apikeyt12 = "165ddff4-7fe4-461a-b9ee-2749e1fa99e2";
                                transactiontypestring = "t1";
                                getoneETA();
                            }else if (Typeoftransaction.transactiontype == "ship" || Typeoftransaction.transactiontype.equals("ship")){
                                System.out.println("T+2 transaction");
                                cardpercentage = percentage_t2;
                                percentage_dis = percentage_t2_display;
                                //apikeyt12 = "bcbd8fc4-261d-4484-9e44-fcf62d6219c1";
                                apikeyt12 ="6f56ca93-a2cc-48a2-b380-33e4258dc355";
                                transactiontypestring = "t2";
                                getETA();
                            }else if (Typeoftransaction.transactiontype == "jet" || Typeoftransaction.transactiontype.equals("jet") || Typeoftransaction.transactiontype.equals("hds_jet")){
                                System.out.println("T+0 transaction");
                                cardpercentage = percentage_sds;
                                percentage_dis = percentage_sds_display;
                                apikeyt12 = "58c1a0ec-d7c1-4495-b2b2-24af75e1da38";
                                if (Typeoftransaction.transactiontype.equals("jet")){
                                    transactiontypestring = "t0sds";
                                }else{
                                    transactiontypestring = "t0hds";
                                }

                                sdsactivation();
                            }else if (Typeoftransaction.transactiontype == "hds" || Typeoftransaction.transactiontype.equals("hds")){
                                System.out.println("HDS transaction");
                                //holiday finder api
                                cardpercentage = holiday_percentage_t1;
                                percentage_dis = holiday_percentage_t1;
                                apikeyt12 = "58c1a0ec-d7c1-4495-b2b2-24af75e1da38";
                                transactiontypestring = "hds";
                                check_holiday_settlement();
                            }else{
                                System.out.println(Typeoftransaction.transactiontype);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }



    //---------------percentage api end------------//









    //-------------------updating payment response---------------------//
    private void updatebtransaction(){

        SharedPreferences sharedPreferences2 = Payment.this.getSharedPreferences("idpref", Context.MODE_PRIVATE);
        final String Responsebtranid = sharedPreferences2.getString("id","");
        System.out.println("response id");
        System.out.println(Responsebtranid);
        final String boid=Responsebtranid;

        /* SharedPreferences sharedPreferences3 = Main5Activity.this.getSharedPreferences("tranpref", Context.MODE_PRIVATE);
         final String Responsetranid = sharedPreferences3.getString("transaction_id","");
           System.out.println("response transaction id");
         System.out.println(sharedPreferences3);
         final  String btranid=Responsetranid;
         */



        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/casa_transaction_update",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //----------nothing--------//
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {
            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);

                try{
                    JSONObject response4 = new JSONObject(boid);
                    // JSONObject response3 = new JSONObject(btranid);




                    // updatetranid=response3.getString("transaction_id");

                    System.out.println("this is the seperated id");
                    System.out.println(response4.getString("id"));
                    updateid=response4.getString("id");

                    System.out.println("this is the seperated Transaction id");

                    //  System.out.println("this is the seperated transaction id");
                    System.out.println(Transactionid);

                }catch (JSONException e){
                    e.printStackTrace();
                }


                params.put("id",updateid);
                System.out.println("upppddddddateid");
                System.out.println(updateid);
                params.put("pg_tran_id",Transactionid);
                System.out.println("upppddddddateid");
                System.out.println(Transactionid);
                params.put("payout_id",payoutupdate);
                System.out.println("pppppppayoutiiiiidddd");
                System.out.println(payoutupdate);

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    //-----------------profiledetails---------------------//
    private void profileinfo(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            profileparaid=profileresponse.getString("id");
                            profileparaname=profileresponse.getString("name");
                            profileparaemail=profileresponse.getString("email");
                            profileparaphone=profileresponse.getString("phone");
                            // fcmtoken = profileresponse.getString("fcm_token");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }




    //-------------------spinner-----------------------//
    private void loadpaypurpose() {
        try {
            Log.e(TAG,"________________________________________________________________");
            paylist.clear();
            payid.clear();
            StringRequest stringRequest1 = new StringRequest(Request.Method.GET, SERVER + "/api/Purpose",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e(TAG, response);
                            //beatslist.add("Select Beats");
                            try {

                                JSONArray jsonArray = new JSONArray(response);
                                if (jsonArray != null) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        if (i == 0) {
                                            paylist.add("Select Purpose Of Payment");
                                            payid.add("0");
                                        }
                                        JSONObject dataObject = jsonArray.optJSONObject(i);
                                        String purp = dataObject.optString("purpose");
                                        String purpid = dataObject.optString("id");
                                        //   SharedPreferences.Editor editor = spinnerpreference.edit();

                                        // editor.putString("casa_id",ids);
                                        // editor.apply();
                                        // System.out.println("This");
                                        // System.out.println(ids);
                                        paylist.add(purp);
                                        payid.add(purpid);
                                    }
                                    // Creating adapter for spinner
                                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Payment.this, R.layout.spinner_item, paylist);
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    purposepaycasa.setAdapter(dataAdapter);


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String message = "";

                            if (error instanceof NetworkError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ServerError) {
                                Log.e("stackcheck1111", "3" + error.toString());
                                NetworkResponse response = error.networkResponse;
                                if (response != null) {
                                    Log.d("qqqqqqqqqqq", String.valueOf(response.statusCode));

                                    int statusCode = response.statusCode;
                                    if (statusCode == 404) {
                                        message = "No ISP Available in this Company";
                                        Log.d("qqqqqq", " " + response.data);

                                        //ShowAlert("Server is down. Please try after some time!");
                                        //json = new String(response.data);
                                        //json = trimMessage(json, "status");
                                        // if (json != null)
                                        //    message = json;


                                    } else {
                                        message = "The server could not be found. Please try again after some time!!";

                                    }
                                } else {
                                    message = "The server could not be found. Please try again after some time!!";

                                }
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                            }
                            Log.e("eeeee", "onErrorResponse: " + error.toString());
                            if (!message.isEmpty()) {
                                Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            RequestQueue requestQueue1 = Volley.newRequestQueue(Payment.this);
            stringRequest1.setShouldCache(false);
            requestQueue1.add(stringRequest1);
        } catch (Exception e) {
            Log.d(TAG, "Exception in loadISPList():\n" + e.getMessage());
        }

    }




    @Override
    public void onResume() {
        super.onResume();

        // put your code here...

        if ((easebuzz_gateway_result.equals("crash")) && (!Typeoftransaction.transactiontype.equals("hds_jet") || !Typeoftransaction.transactiontype.equals("hds"))) {
            final PaymentParams pgPaymentParams = new PaymentParams();


            pgPaymentParams.setAPiKey(apikeyt12);

            pgPaymentParams.setAmount(strAmout);
            pgPaymentParams.setEmail(profileparaemail);
            pgPaymentParams.setName(profileparaname);
            pgPaymentParams.setPhone(profileparaphone);
            pgPaymentParams.setOrderId(getpgtran);
            System.out.println("this is the ttttttttttttttttttttttransaaaaaaaaaaaaaction id");
            System.out.println(getpgtran);

            pgPaymentParams.setCurrency(SampleAppConstants.PG_CURRENCY);
            pgPaymentParams.setDescription(conveniencefee);
            pgPaymentParams.setCity(cardtypetext);
            pgPaymentParams.setState(transactiontypestring);
            pgPaymentParams.setAddressLine1(getetaapi);
            pgPaymentParams.setAddressLine2(fixedamount);
            pgPaymentParams.setZipCode(SampleAppConstants.PG_ZIPCODE);
            pgPaymentParams.setCountry(sharedpurposename);
            pgPaymentParams.setReturnUrl(SampleAppConstants.PG_RETURN_URL);
            pgPaymentParams.setMode(SampleAppConstants.PG_MODE);
            pgPaymentParams.setUdf1(useridudf1);
            pgPaymentParams.setUdf2(sharedprefispid);
            pgPaymentParams.setUdf3(sharedprefisplist);
            pgPaymentParams.setUdf4(sharedprefvendorispid);
            pgPaymentParams.setUdf5(SampleAppConstants.PG_UDF5);
            eb_or_not = "no";
            PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, Payment.this);
            pgPaymentInitialzer.initiatePaymentProcess();
        }else if ((easebuzz_gateway_result.equals("crash")) && (Typeoftransaction.transactiontype.equals("hds_jet") || Typeoftransaction.transactiontype.equals("hds"))) {
            final PaymentParams pgPaymentParams = new PaymentParams();

            crash_message();

            pgPaymentParams.setAPiKey("6f56ca93-a2cc-48a2-b380-33e4258dc355");

            pgPaymentParams.setAmount(strAmout);
            pgPaymentParams.setEmail(profileparaemail);
            pgPaymentParams.setName(profileparaname);
            pgPaymentParams.setPhone(profileparaphone);
            pgPaymentParams.setOrderId(getpgtran);
            System.out.println("this is the ttttttttttttttttttttttransaaaaaaaaaaaaaction id");
            System.out.println(getpgtran);

            pgPaymentParams.setCurrency(SampleAppConstants.PG_CURRENCY);
            pgPaymentParams.setDescription(conveniencefee);
            pgPaymentParams.setCity(cardtypetext);
            pgPaymentParams.setState(transactiontypestring);
            pgPaymentParams.setAddressLine1(getetaapi);
            pgPaymentParams.setAddressLine2(fixedamount);
            pgPaymentParams.setZipCode(SampleAppConstants.PG_ZIPCODE);
            pgPaymentParams.setCountry(sharedpurposename);
            pgPaymentParams.setReturnUrl(SampleAppConstants.PG_RETURN_URL);
            pgPaymentParams.setMode(SampleAppConstants.PG_MODE);
            pgPaymentParams.setUdf1(useridudf1);
            pgPaymentParams.setUdf2(sharedprefispid);
            pgPaymentParams.setUdf3(sharedprefisplist);
            pgPaymentParams.setUdf4(sharedprefvendorispid);
            pgPaymentParams.setUdf5(SampleAppConstants.PG_UDF5);
            eb_or_not = "no";
            PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, Payment.this);
            pgPaymentInitialzer.initiatePaymentProcess();
        } else {

        getpgtran = "";
        gettranid();
        strAmout = "";
        sharedprefispid = "";
        spinnercasa.setSelection(0);
        purposepaycasa.setSelection(0);
        System.out.println("onresume");
        System.out.println("This + " + sharedprefispid);

      /*  if (Typeoftransaction.transactiontype == "train" || Typeoftransaction.transactiontype.equals("train")){
            System.out.println("T+1 transaction");
            cardpercentage = "2.85";
            apikeyt12 = "165ddff4-7fe4-461a-b9ee-2749e1fa99e2";
            transactiontypestring = "t1";
            getoneETA();
        }else if (Typeoftransaction.transactiontype == "ship" || Typeoftransaction.transactiontype.equals("ship")){
            System.out.println("T+2 transaction");
            cardpercentage = "2.5";
            apikeyt12 = "bcbd8fc4-261d-4484-9e44-fcf62d6219c1";
            transactiontypestring = "t2";
            getETA();
        }else if (Typeoftransaction.transactiontype == "jet" || Typeoftransaction.transactiontype.equals("jet")){
            System.out.println("T+0 transaction");
            cardpercentage = "3.9";
            apikeyt12 = "58c1a0ec-d7c1-4495-b2b2-24af75e1da38";
            transactiontypestring = "t0sds";
            sdsactivation();
        }else{
            System.out.println(Typeoftransaction.transactiontype);
        }

       */
        percentage_api();


        spinnercasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();
                    } else {

//save this value in shared pref
                        String ispid = ispids.get(position);
                        System.out.println("This ------------------ is---------------ispid------------");
                        System.out.println(ispid);
                        String isplists = isplist.get(position);
                        System.out.println("This ------------------ is---------------isplist------------");
                        System.out.println(isplist);
                        String ispvendorlist = ispvendorid.get(position);
                        System.out.println("This ------------------ is---------------ispvendorlist------------");
                        System.out.println(ispvendorlist);
                        sharedprefispid = ispid;
                        sharedprefisplist = isplists;
                        sharedprefvendorispid = ispvendorlist;
                    /*    SharedPreferences.Editor editor = spinnerpreference.edit();
                        System.out.println("Hi I am here");
                        System.out.println(ispid);
                        editor.putString("id",ispid);
                        editor.apply();

                        */

                        //-------casa name save-------//
                     /*   SharedPreferences.Editor editorcasaname = spinnernamepreference.edit();
                        System.out.println("Hi I am here");
                        System.out.println(isplists);
                        editorcasaname.putString("first_name",isplists);
                        editorcasaname.apply();

                        */

                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception in loadisp():\n" + e.getMessage());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sharedpurposename = "";
        purposepaycasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {


                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

//save this value in shared pref
                        String paynamelist = paylist.get(position);
                        sharedpurposename = paynamelist;


                    }
                } catch (Exception e) {
                    Log.d(TAG, "Exception in loadisp():\n" + e.getMessage());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

     /*   str="";
        amountfinance.setText("");
        loadpaypurpose();
        purposepaycasa.setAdapter(dataAdapter);
        toggle.setOnCheckedChangeListener();

        */
       /* SharedPreferences.Editor editor = spinnerpreference.edit();
        editor.putString("id","");
        editor.apply();

        SharedPreferences.Editor editor1 = purposepreference.edit();
        editor1.putString("purpose","");
        editor1.apply();

        */

        //  amountfinance.setText("2");

    }
    }
    public void gettranid(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/order_id",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respontran = new JSONObject(response);
                            getpgtran = respontran.getString("transactionid");
                            System.out.println("this is the trrrrrrrrrrrrrrrrrrrrrransaaaaaaaaaaaaaction id");
                            System.out.println(getpgtran);
                            easebuzz_tran_id = "NEXEB"+getpgtran;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();

                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }
    private void sendsuccessful_sds_sms(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/sds_sms",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(Payment.this,"SMS Sent",Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        })

        {
            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("amt",Amountdone);
                params.put("transid",Transactionid);
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);




    }


    private void sendsuccessfulsms(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/TSMS",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(Payment.this,"SMS Sent",Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        })

        {
            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("amt",Amountdone);
                params.put("transid",Transactionid);
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);




    }

    //---------------One day transaction sms------------------//
    private void sendsuccessfulonedaysms(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/t1_TSMS",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Toast.makeText(Payment.this,"SMS Sent",Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        })

        {
            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("amt",Amountdone);
                params.put("transid",Transactionid);
                // params.put("amt","100.00");
                // params.put("transid","RPMA23456");
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);




    }
    //---------SMS end----------------//

    //--------------Get profile details----------//
    private void getuseridforudf1(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            useridudf1=profileresponse.getString("id");
                            System.out.println(useridudf1);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);




    }
    //------------profiledetails end-----------------//



    //-------------------Get ETA-----------------//
    public void getETA(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/eta",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        try{
                            JSONObject respontran = new JSONObject(response);
                            getetaapi = respontran.getString("transaction_delivery_date");

                        }catch(JSONException e){
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    //---------------------------------------------END---------------------------//
    //-------------------Get One day ETA-----------------//
    public void getoneETA(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/t1_eta",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        try{
                            JSONObject respontran = new JSONObject(response);
                            getetaapi = respontran.getString("transaction_delivery_date");
                            System.out.println(getetaapi);

                        }catch(JSONException e){
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    //---------------------------------------------END---------------------------//

    public void getLocation(){
        try{
            locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,5000, 5,this);




        }catch(SecurityException e){
            e.printStackTrace();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        locationlatitude=location.getLatitude();
        locationlongitude=location.getLongitude();
        System.out.println("this is the latitude location");
        System.out.println(locationlatitude);
        System.out.println("this is the logngitude location");
        System.out.println(locationlongitude);
        slatitude= Double.toString(locationlatitude);
        slongitude= Double.toString(locationlongitude);
        System.out.println(" this is the converted lat location");
        System.out.println(slatitude);
        System.out.println(" this is the converted long location");
        System.out.println(slongitude);

        latlonglocation=slatitude+","+slongitude;


        System.out.println("this is the lat long location");
        System.out.println(latlonglocation);
        /*

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
        }catch(Exception e)
        {

        }
        */



    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

        // Toast.makeText(Payment.this,"Please enable Location",Toast.LENGTH_SHORT).show();

    }


    public void get_azypay_details(){


        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/easebuzzswitch",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //----Nothing----//
                        try {

                            JSONArray jsonArray = new JSONArray(response);

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject dataObject = jsonArray.optJSONObject(i);
                                azypaylimit_str = dataObject.optString("amount");
                                allow_easebuzz = dataObject.optString("number");
                                System.out.println(azypaylimit_str);
                                System.out.println(allow_easebuzz);
                                azypaylimit = Float.parseFloat(azypaylimit_str);
                                System.out.println(azypaylimit);

                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";

                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }





    //-----------------Location Update---------------------//
    public void locationupdateprofile(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/update_loc",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //----Nothing----//
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";

                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();


                if (latlonglocation == null){
                    paymentdashboardlocationupdate = "Not Provided";
                    System.out.println("this is setvaluelatlong 1");
                    System.out.println(paymentdashboardlocationupdate);
                }else{
                    paymentdashboardlocationupdate = latlonglocation;
                    System.out.println("this is setvaluelatlong 2 (actual location)");
                    System.out.println(paymentdashboardlocationupdate);
                }



                params.put("id", profileparaid);
                params.put("location", paymentdashboardlocationupdate);


                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    private void activity_log_bottom_sheet(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Payment.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","108");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

    private void sdsactivation(){

        SharedPreferences sharedPreferences = Payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/sds_activation",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject transactionresponse = new JSONObject(response);
                            System.out.println(transactionresponse);
                            sdsacti = transactionresponse.getString("status");
                            getetaapi = transactionresponse.getString("eta");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(Payment.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }




        };

        RequestQueue requestQueue = Volley.newRequestQueue(Payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }

}


