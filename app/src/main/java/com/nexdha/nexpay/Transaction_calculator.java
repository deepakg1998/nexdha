package com.nexdha.nexpay;

import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import java.text.DecimalFormat;

public class Transaction_calculator extends AppCompatActivity {
    EditText calculateamount;
    CheckBox sds;
    CheckBox oneday;
    CheckBox twoday;
    Switch amex;
    TextView textview237;
    Button Calculatebutton;
    ProgressBar progressbar10;
    String amount_one;
    double amount_one_one;
    double amount_two;
    double amount_three;
    double tran_percentage = 0;
    Switch conveniencefee;
    TextView TextViewcf;
    String tran_type;
    boolean isswitchreceiver;
    boolean amex_card;
    String beneamount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_transaction_calculator);
        progressbar10 = findViewById(R.id.progressBar10);
        progressbar10.setVisibility(View.GONE);
        calculateamount = findViewById(R.id.calculate_amount);
        calculateamount.setText("100");
        sds = findViewById(R.id.checkBox6);
        oneday = findViewById(R.id.checkBox7);
        twoday = findViewById(R.id.checkBox8);
        amex = findViewById(R.id.switch3);
        textview237=findViewById(R.id.textView237);
        TextViewcf = findViewById(R.id.textView236);
        conveniencefee = findViewById(R.id.switch1);

        Calculatebutton = findViewById(R.id.button42);
        Calculatebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressbar10.setVisibility(View.VISIBLE);
                amount_one = calculateamount.getText().toString().trim();

                if (validation() == false){
                    return;
                }
                if (sds.isChecked() == true){
                    tran_type = "Same Day";
                    tran_percentage = 3.9;
                }else if (oneday.isChecked() == true){
                    tran_type = "T+1";
                    tran_percentage = 2.85;
                }else if (twoday.isChecked() == true){
                    tran_type = "T+2";
                    tran_percentage = 2.5;
                }

                if (amex_card == true){
                    tran_percentage = 5.0;
                }
                //amount calculation
                amount_two = Double.parseDouble(amount_one);

                if (isswitchreceiver == true){
                    amount_three = amount_two+((amount_two/100)*tran_percentage);
                    beneamount.equals(amount_one);

                }else{
                    Double bene_amount = amount_two-((amount_two/100)*tran_percentage);
                    beneamount=String.valueOf(bene_amount);
                    amount_three = amount_two;
                }
                String strdecimalformat = new DecimalFormat("##.##").format(amount_three);
                System.out.println("This is the strdecimal format : "+strdecimalformat);
                amount_one_one = Double.parseDouble(strdecimalformat);


                //amount end

                new MaterialStyledDialog.Builder(Transaction_calculator.this)

                        .setTitle("Result")
                        .setDescription("Entered Amount : "+amount_one+"Amount to pay : "+amount_one_one+"\nBeneficiary will receive : "+beneamount+"\nPercentage : "+tran_percentage+" %\nType Of Transaction : "+tran_type+"\n")
                        .setHeaderColor(R.color.md_divider_white)
                        .setIcon(R.drawable.transactionfailed)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(true)
                        .setCancelable(false)
                        .withDivider(true)
                        .setPositiveText("Got it")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Transaction_calculator.super.onResume();
                            }
                        })
                        .show();
                progressbar10.setVisibility(View.GONE);

            }
        });
        sds.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (sds.isChecked()){
                    oneday.setChecked(false);
                    twoday.setChecked(false);
                }
            }
        });
        oneday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (oneday.isChecked()){
                    sds.setChecked(false);
                    twoday.setChecked(false);
                }
            }
        });
        twoday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (twoday.isChecked()){
                    oneday.setChecked(false);
                    sds.setChecked(false);
                }
            }
        });

        conveniencefee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean switchb) {

                if (switchb == true){
                    isswitchreceiver = true;
                    conveniencefee.setTextColor(Color.BLACK);
                    TextViewcf.setTextColor(Color.parseColor("#659A08"));


                }else {
                    isswitchreceiver = false;
                    TextViewcf.setTextColor(Color.BLACK);
                    conveniencefee.setTextColor(Color.parseColor("#659A08"));


                }
            }
        });
        amex.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean axc) {

                if (axc == true){
                    amex_card = true;
                    amex.setTextColor(Color.BLACK);
                    textview237.setTextColor(Color.parseColor("#659A08"));


                }else {
                    amex_card = false;
                    textview237.setTextColor(Color.BLACK);
                    amex.setTextColor(Color.parseColor("#659A08"));


                }
            }
        });

    }



    private boolean validation(){

        String validate_amount = calculateamount.getText().toString().trim();

     if (validate_amount.isEmpty() || validate_amount.length() == 0 || validate_amount.equals("") || validate_amount == null){
        Toast.makeText(Transaction_calculator.this,"Please Enter Amount",Toast.LENGTH_LONG).show();
         progressbar10.setVisibility(View.GONE);
        return false;
    }
        if (sds.isChecked() != true && oneday.isChecked() != true && twoday.isChecked() != true){
            Toast.makeText(Transaction_calculator.this,"Please select type of transaction ", Toast.LENGTH_SHORT).show();
            progressbar10.setVisibility(View.GONE);
            return false;
        }

        return true;


}
}