package com.nexdha.nexpay;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.nexdha.nexpay.constant.SERVER;

public class scanactivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private static final String TAG = " ";
    private ZXingScannerView mScannerView;
    public Spinner spinner4;
    public Button button57;
    public ImageView imageView65;
    public TextView textView266,textView265;
    private static int camId = Camera.CameraInfo.CAMERA_FACING_BACK;
    public static final int  MY_PERMISSIONS_REQUEST_CAMERA = 102;
    public EditText editTextTextPersonName2;
    public String nameonlyies;
    public ProgressBar progressBar23;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

        }

        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

        editTextTextPersonName2 = findViewById(R.id.editTextTextPersonName2);
        progressBar23=findViewById(R.id.progressBar23);
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v(TAG, rawResult.getText()); // Prints scan results
        Log.v(TAG, rawResult.getBarcodeFormat().toString());
        onBackPressed();
        // Prints the scan format (qrcode, pdf417 etc.)

        System.out.println(rawResult);
        // If you would like to resume scanning, call this method below:

        String splitt = rawResult.toString();

        String[] separated = splitt.split("\\?");
        //   upi://pay?pa=9488451373@ybl&pn=Gowthamrajp&mc=0000&mode=02&purpose=00//
        System.out.println(separated);
        System.out.println(separated[0]);//upi://pay
        System.out.println(separated[1]);//pa=9488451373@ybl&pn=Gowthamrajp&mc=0000&mode=02&purpose=00
        String[] onlyone = separated[1].split("&");
        System.out.println(onlyone);
        System.out.println(onlyone[0]);//pa=9488451373@ybl
        System.out.println(onlyone[1]);
        System.out.println(onlyone[2]);


        int i = 0;
        for (i = 0; i > onlyone.length; i++) ;
        {
            if (onlyone[i].contains("pa=")) {
                System.out.println(onlyone[i]);
                final String[] vpaonly = onlyone[i].split("=");
                System.out.println(vpaonly);
                System.out.println(vpaonly[0]);
                System.out.println(vpaonly[1]);

                SharedPreferences sharedPreferences = scanactivity.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                final String token1 = sharedPreferences.getString("token", "");
                System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
                System.out.println(token1);
                final String token200 = token1;
                System.out.println(vpaonly);

                // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
                StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/verify_upi",


                        response -> {
                            try {
                                JSONObject validvpa = new JSONObject(response);
                                System.out.println(validvpa);
                                System.out.println(vpaonly[1]);



/*
                        System.out.println(validvpa.getString("valid"));
*/

                                JSONObject onlyVPA=new JSONObject(validvpa.getString("Output"));
                                System.out.println(onlyVPA);


                                nameonlyies=onlyVPA.getString("name");
                                System.out.println(nameonlyies);
                                Intent intent = new Intent(scanactivity.this, Scan.class);
                                intent.putExtra("VPA", vpaonly[1]);
                                intent.putExtra("nameonlyies", nameonlyies);
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message = "";

                        if (error instanceof NetworkError) {
                            //   message = "cannot profile connect";
                            new MaterialStyledDialog.Builder(scanactivity.this)
                                    .setTitle("Internet Error")
                                    .setDescription("Check Your Interenet Connection")
                                    .setHeaderColor(R.color.md_divider_white)
                                    .setIcon(R.drawable.warninginternet)
                                    .withDarkerOverlay(true)
                                    .withDialogAnimation(false)
                                    .setCancelable(false)
                                    .withDivider(true)
                                    .setNegativeText("Cancel")
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                            dialog.dismiss();
                                        }
                                    })
                                    .setPositiveText("Open Settings")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                            // dialog.dismiss();
                                        }
                                    })
                                    .show();
                        } else if (error instanceof AuthFailureError) {
                            message = "There is a problem connecting to server. Please try after sometime.";
                        } else if (error instanceof NoConnectionError) {
                            message = "There is a problem connecting to server. Please try after sometime.";
                        } else if (error instanceof ParseError) {
                            message = "Connection Timedout. Please try after sometime";
                        }
                        if (!message.isEmpty()) {
                            Toast.makeText(scanactivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {

                    //-------------------------outerheader-----------------------//
                    @Override
         /*   public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();*/

                    public Map<String, String> getHeaders() throws AuthFailureError{
                        String T="" ;
                        Map<String,String> params = new HashMap<>();


                        // params.put("Content-Type", "application/json; charset=UTF-8");
                        try {
                            JSONObject response1 = new JSONObject(token200);
                            System.out.println("!!!!!!!!!!!!!!!!!!");
                            System.out.println(response1.getString("token"));
                            // do your work with response object
                            T=response1.getString("token");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("44444444444444444444444444");
                        System.out.println(token200);
                        ///check t!blank

                        String TokenS = "token "+ T;
                        System.out.println(T);

                        params.put("Authorization",TokenS);

                        return params;
                    }
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("sUpiId", vpaonly[1]);

                        return params;
                    }


                };

                RequestQueue requestQueue = Volley.newRequestQueue(scanactivity.this);
                stringRequest.setShouldCache(false);
                requestQueue.add(stringRequest);



            }


     /* String[]  fortwo= forone[1].split("&");
        System.out.println(fortwo);
        System.out.println(fortwo[0]);//9488451373@ybl
        String[]  forthree= forone[2].split("&");
        System.out.println(forthree[0]);//Gowthamrajp
        for(int i=0;i<fortwo.length;i++){
            String pa ="";
            if(pa == ""){
                System.out.println(pa);
            }
        }
*/

/*
            System.out.println(forloop[1]);//9488451373@ybl
*/


        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(Dashboard.this,"Please 123 Location",Toast.LENGTH_SHORT).show();
                    finish();
                    Intent intent = new Intent(scanactivity.this, cameradenied.class);
                    startActivity(intent);

                }
                return;
            }
        }
    }

    // other 'case' lines to check for other
    // permissions this app might reque


}
