package com.nexdha.nexpay;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface apiconfig1 {

  /*  @Multipart
    @POST("api/kyc_files")
    Call uploadFile(@Part MultipartBody.Part file, @Part("file") RequestBody name);*/
    @Multipart
    @POST("api/kyc_files")
    Call <ServerResponse> uploadMulFile(@Part("userid") int description,@Part("filevarr") RequestBody description1, @Part MultipartBody.Part file2);


}
