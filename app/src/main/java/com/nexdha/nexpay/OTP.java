package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class OTP extends AppCompatActivity {

    Button otpsubmit, otpresendbutton;
    EditText otpbox;
    String Otpverify, Otpnotverify, Otpwelcomeverify, fresponsephonenumber;
    TextView otpcountdisplay;
    public int counter;

    public static final String WELCOMEPREFERENCE = "welpref";
    SharedPreferences welcomepreference;

    public static final String FRAUDPREFERENCE = "fpref";
    SharedPreferences fraudpreference;
    public static final String MYPREFERENCES = "MyPrefs";
    SharedPreferences sharedPreferences;
    String previousActivity, phone, token, sb;
    String saltKey = MainActivity.getNativeKey1(), getHashKey;


    //I agree that by entering the OTP and my PAN, I have e-signed and agreed to the 'Terms &amp; Conditions' for using the Nexdha APP.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_otp);

        Intent mIntent = getIntent();
        previousActivity = mIntent.getStringExtra("FROM_ACTIVITY");
        phone = mIntent.getStringExtra("PHONE");

        //SpannableString text = new SpannableString("Voglio sottolineare solo questa parola");
        //text.setSpan(new UnderlineSpan(), 25, 6, 0);
        //terms_and_conditions.setText(text);

        welcomepreference = getSharedPreferences(WELCOMEPREFERENCE, Context.MODE_PRIVATE);
        sharedPreferences = getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);


        otpcoundown();
        otpbox = findViewById(R.id.otp);
        otpresendbutton = findViewById(R.id.button17);

        otpresendbutton.setOnClickListener(view -> {

            try {
                getHashKey = phone + saltKey;
                MessageDigest md = MessageDigest.getInstance("SHA-512");
                byte[] digest = md.digest(getHashKey.getBytes());
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < digest.length; i++) {
                    sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                }
                System.out.println(sb);
                resendotpmethod(sb.toString());

            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }


            otpcountdisplay.setText("OTP SENT");
            //   otpcoundown();
        });


        otpsubmit = (Button) findViewById(R.id.button13);

        otpsubmit.setOnClickListener(view -> {

            if (!ValidationOTP()) {

                return;
            }
            final String otpinput = otpbox.getText().toString().trim();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/verify_otp_2022",
                    response -> {
                        try {
                            JSONObject otpresponse = new JSONObject(response);
                            Otpverify = otpresponse.getString("Details");
                            if (Otpverify.equals("OTP Matched")) {
                                token = otpresponse.getString("token");
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("token", response);
                                editor.apply();
                                if (previousActivity.equals("Login")) {
                                    Intent intent = new Intent(OTP.this, Dashboard.class);
                                    startActivity(intent);
                                } else if (previousActivity.equals("Signup")) {
                                    Intent intent = new Intent(OTP.this, onboardin1.class);
                                    startActivity(intent);
                                    finishAffinity();
                                }


                                finish();
                            } else {
                                Toast.makeText(this, "OTP does not match", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }, error -> {
                //parseVolleyError(error);

                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(OTP.this, message, Toast.LENGTH_SHORT).show();
                }
            }) {

                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("otp", otpinput);
                    params.put("phone", phone);
                    return params;

                }
            };


            RequestQueue requestQueue = Volley.newRequestQueue(OTP.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);

            //verifytrue();


        });
    }



    public void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            String otperror = data.getString("error");

            System.out.println("OTP");
            System.out.println(responseBody);
            System.out.println(data);
            System.out.println(otperror);
            if (otperror.equals("OTP does not match")){
                Toast.makeText(OTP.this,"Invalid OTP. Please Enter valid OTP",Toast.LENGTH_LONG).show();

            }

        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }

    private void verifytrue(){

            StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Welcome",
                    response -> {
                        try {
                            JSONObject otpwelcomeresponse = new JSONObject(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }, error -> {
                String message = "";
                if (error instanceof NetworkError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ServerError) {
                    message = "";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof ParseError) {
                    message = "Cannot connect to internet......please check your internet connection";
                } else if (error instanceof TimeoutError) {
                    message = "Connection Timedout......please check your internet connection";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(OTP.this, message, Toast.LENGTH_SHORT).show();
                }
            })
        {
            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject();
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(OTP.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    private boolean ValidationOTP() {
        final String otpinput = otpbox.getText().toString().trim();

        if (otpinput.length() == 0) {
            Toast.makeText(OTP.this, "Please Enter OTP ", Toast.LENGTH_LONG).show();
            return false;
        }


        if ((otpinput.length() == 6) || (otpinput.length() == 4)) {
            System.out.println(otpinput.length());
            return true;
        } else {
            Toast.makeText(OTP.this, "Please Enter Correct OTP", Toast.LENGTH_LONG).show();
            return false;
        }


    }

    public void otpcoundown(){
        otpcountdisplay=(TextView) findViewById(R.id.textView123);
        new CountDownTimer(60000, 1000) {

            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
                otpresendbutton.setEnabled(false);
                otpcountdisplay.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                otpcountdisplay.setText("Click To send OTP");
                otpresendbutton.setTextColor(Color.parseColor("#ffffff"));
                otpresendbutton.setEnabled(true);
                otpresendbutton.setBackgroundResource(R.drawable.roundedbutton);

            }
        }.start();

    }

    private void resendotpmethod(String sb) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/login",
                response -> {

                    try {
                        JSONObject response1 = new JSONObject(response);
                        if (response1.getString("status").equals("Please try after sometime")) {
                            Toast.makeText(OTP.this, "Please try after sometime", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this, "OTP sent", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {

            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(OTP.this, message, Toast.LENGTH_SHORT).show();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", phone);
                params.put("hash_login", sb);


                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(OTP.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }



    //-----------------profiledetails---------------------//
    private <fresponsephonenumber> void profileinfo(){

        fraudpreference = getSharedPreferences(FRAUDPREFERENCE, Context.MODE_PRIVATE);


        SharedPreferences sharedPreferences = OTP.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            fresponsephonenumber = profileresponse.getString("phone");
                            //   developerverification();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(OTP.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(OTP.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(OTP.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }








}
