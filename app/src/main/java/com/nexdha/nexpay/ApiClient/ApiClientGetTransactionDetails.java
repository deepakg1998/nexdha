package com.nexdha.nexpay.ApiClient;

import com.nexdha.nexpay.dataclass.GetTransactionDetails;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiClientGetTransactionDetails {

    @GET("/api/save_server_Response?response_message=Transaction successful&state=")
    Call<ArrayList<GetTransactionDetails>> getData();
}
