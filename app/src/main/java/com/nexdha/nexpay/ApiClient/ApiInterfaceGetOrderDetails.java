package com.nexdha.nexpay.ApiClient;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterfaceGetOrderDetails {

    @POST("/api/trans_details")
    Call<ResponseBody> getData(@Body RequestBody requestBody);
}
