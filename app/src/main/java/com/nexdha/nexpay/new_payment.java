package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class new_payment extends AppCompatActivity {
    ImageView back_button;
    Button add_beneficiary, proceed_to_pay;
    Spinner spinnercasa, spinnerpurposepay, spinner_transaction_type, cardTypeSpinner;
    ArrayList<String> isplist, ispids, ispvendorid, ispaccount_number, ispifsc, ispphone;
    ArrayList<String> purpose_list, purpose_id;
    ArrayList<String> Transaction_name, Transaction_percentage, calculation_percentage, transaction_eta, gateway_list, amex_list;
    public static String percentage, final_amt, account_number, ifsc, beneficiary_name = "null", beneficiary_id, vendor_code, beneficiary_phone, purpose_of_payment = "null", transaction_type = "null", card_type, beneficiary_amount, calc_per, eta_tran, gateway_init, amex_percent, transaction_display;
    EditText amount_edit;
    String user_total = null;
    double inum;
    Switch switch_amex;
    boolean amex_or_not;
    LayoutInflater inflater;
    Dialog loadingDialog;
    ArrayList<String> cardList;
    String getCardType, cardType;
    RelativeLayout cardTypeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_new_payment);

        inflater = (LayoutInflater) new_payment.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(new_payment.this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.inflate_layout, null));
        loadingDialog.setCancelable(false);
        loadingDialog.show();

        isplist = new ArrayList<>();
        ispids = new ArrayList<>();
        ispvendorid = new ArrayList<>();
        ispaccount_number = new ArrayList<>();
        ispifsc = new ArrayList<>();
        ispphone = new ArrayList<>();
        cardList = new ArrayList<>();

        activity_new_payment();

        purpose_list = new ArrayList<>();
        purpose_id = new ArrayList<>();

        Transaction_name = new ArrayList<>();
        Transaction_percentage = new ArrayList<>();
        calculation_percentage = new ArrayList<>();
        transaction_eta = new ArrayList<>();
        gateway_list = new ArrayList<>();
        amex_list = new ArrayList<>();

        spinnercasa = findViewById(R.id.spinner5);
        spinnerpurposepay = findViewById(R.id.spinner7);
        spinner_transaction_type = findViewById(R.id.spinner6);
        cardTypeSpinner = findViewById(R.id.card_type_spinner);
        cardTypeLayout = findViewById(R.id.card_type_layout);

        amount_edit = findViewById(R.id.editTextTextPersonName3);
        switch_amex = findViewById(R.id.switch6);
        switch_amex.setOnCheckedChangeListener((compoundButton, amex) -> {
            if (amex) {
                switch_amex.setTextColor(Color.parseColor("#659A08"));
                amex_or_not = true;
            } else {
                switch_amex.setTextColor(Color.parseColor("#000000"));
                amex_or_not = false;
            }
        });

        loadcasa();
        load_purpose_payment();

        transaction_amount();
        getCardType();
        sessionCheck();
        proceed_to_pay = findViewById(R.id.button62);
        proceed_to_pay.setOnClickListener(view -> {
            if (!validation()) {
                return;
            } else {

                amountCalculation();


            }


        });

        add_beneficiary = findViewById(R.id.button61);
        add_beneficiary.setOnClickListener(view -> {
            Intent intent = new Intent(new_payment.this, AddBeneficiary.class);
            startActivity(intent);
            finish();
        });
        back_button = findViewById(R.id.imageView72);
        back_button.setOnClickListener(view -> finish());


        spinner_transaction_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {


                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

//save this value in shared pref
                        String transaction_name_list = Transaction_name.get(position);
                        transaction_display = Transaction_percentage.get(position);
                        transaction_type = transaction_name_list;
                        calc_per = calculation_percentage.get(position);
                        eta_tran = transaction_eta.get(position);
                        gateway_init = gateway_list.get(position);
                        amex_percent = amex_list.get(position);


                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerpurposepay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {


                    if (position == 0) {

                        /// Toast.makeText(getContext(), "Please select ISP", Toast.LENGTH_SHORT).show();

                    } else {

//save this value in shared pref
                        String paynamelist = purpose_list.get(position);
                        purpose_of_payment = paynamelist;


                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnercasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (position == 0) {
                    } else {

                        String ispid = ispids.get(position);
                        System.out.println("This ------------------ is---------------ispid------------");
                        System.out.println(ispid);
                        String isplists = isplist.get(position);
                        String ispvendorlist = ispvendorid.get(position);
                        String final_acc_number = ispaccount_number.get(position);
                        String final_ifsc = ispifsc.get(position);
                        String final_phone = ispphone.get(position);
                        beneficiary_id = ispid;
                        beneficiary_name = isplists;
                        vendor_code = ispvendorlist;
                        account_number = final_acc_number;
                        ifsc = final_ifsc;
                        beneficiary_phone = final_phone;

                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void amountCalculation() {
        beneficiary_amount = amount_edit.getText().toString().trim();
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/amount_calc",
                response -> {
                    try {
                        JSONObject respontran = new JSONObject(response);
                        String status = respontran.getString("status");
                        if (status.equals("success")) {
                            final_amt = respontran.getString("swipe_amount");
                            percentage = respontran.getString("percentage");
                            Intent intent = new Intent(new_payment.this, Payment_details.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(this, respontran.getString("status"), Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            String message = "";

            if (error instanceof NetworkError) {
                message = "cannot connect";
            } else if (error instanceof AuthFailureError) {
                message = "";
            } else if (error instanceof ParseError) {
                message = "Connection Timedout";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("cc_type", card_type);
                params.put("state", transaction_type);
                params.put("amount", beneficiary_amount);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("Authorization", "Token " + TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    private void getCardType() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/card_type",
                response -> {
                    try {
                        loadingDialog.dismiss();
                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                if (i == 0) {
                                    cardList.add("Select card type");
                                }
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                getCardType = jsonObject.optString("card");
                                cardList.add(getCardType);
                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                                    android.R.layout.simple_spinner_item, cardList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            cardTypeSpinner.setAdapter(adapter);


                        } else {
                            cardList.add("No card type found");
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                                    android.R.layout.simple_spinner_item, cardList);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            cardTypeSpinner.setAdapter(adapter);
                        }


                        cardTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String getCardType = parent.getItemAtPosition(position).toString();
                                if (getCardType.equals("Select card type") || (getCardType.equals("No card type found"))) {
                                    cardTypeLayout.setVisibility(View.GONE);
                                } else {
                                    cardTypeLayout.setVisibility(View.VISIBLE);
                                    load_transaction_type(cardTypeSpinner.getSelectedItem().toString());
                                    card_type = cardTypeSpinner.getSelectedItem().toString();
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error ->

        {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("Authorization", "Token " + TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private boolean validation() {

        beneficiary_amount = amount_edit.getText().toString().trim();
        String name = spinnercasa.getSelectedItem().toString();

        cardType = cardTypeSpinner.getSelectedItem().toString();
        String purpose = spinnerpurposepay.getSelectedItem().toString();


        if (name.equals("Select Beneficiary")) {
            Toast.makeText(new_payment.this, "Please Select Beneficiary ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (name.equals("No Beneficiary Available")) {
            Toast.makeText(new_payment.this, "Please Add Beneficiary", Toast.LENGTH_SHORT).show();
            return false;
        }


        if (beneficiary_amount.isEmpty()) {
            amount_edit.setError("Please Enter Amount");
            amount_edit.requestFocus();
            return false;
        } else if (Double.parseDouble(beneficiary_amount) < 100) {
            amount_edit.setError("Minimum amount is ₹ 100");
            amount_edit.requestFocus();
            return false;
        } else if (Double.parseDouble(beneficiary_amount) > 600000) {
            amount_edit.setError("Maximum amount is ₹ 6,00,000");
            amount_edit.requestFocus();
            return false;
        }


        if ((user_total.equals("null")) && Float.parseFloat(amount_edit.getText().toString()) > 100) {
            new MaterialStyledDialog.Builder(new_payment.this)
                    .setTitle("100 Rs. for first transaction")
                    .setDescription("New users are allowed to do only 100 Rs for their first transaction.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative((dialog, which) -> dialog.dismiss())
                    .show();

            return false;
        }

        if (user_total.equals("null")) {
            user_total = "0";
        }


        if (Double.parseDouble(user_total) < Double.parseDouble("110") && (Dashboard.kycverification.equals("Ret"))) {
            new MaterialStyledDialog.Builder(new_payment.this)
                    .setTitle("Resend KYC")
                    .setDescription("Please Re-send your KYC details by clicking UPLOAD KYC button below.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative((dialog, which) -> {
                        // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        Intent homeIntent = new Intent(new_payment.this, Dashboard.class);
                        startActivity(homeIntent);
                        dialog.dismiss();
                    })
                    .setPositiveText("Upload KYC")
                    .onPositive((dialog, which) -> {
                        // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        Intent homeIntent = new Intent(new_payment.this, grantreadandwritepermission.class);
                        startActivity(homeIntent);
                        dialog.dismiss();
                        finish();
                    })
                    .show();
            return false;
        }
        if (Double.parseDouble(user_total) < Double.parseDouble("110") && (Dashboard.kycverification.equals("P"))){
            new MaterialStyledDialog.Builder(new_payment.this)
                    .setTitle("KYC Pending")
                    .setDescription("Your KYC has been received and is under review. Please wait till we verify your KYC.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative((dialog, which) -> {
                        // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        Intent homeIntent = new Intent(new_payment.this, Dashboard.class);
                        startActivity(homeIntent);
                        dialog.dismiss();
                    })
                    .show();
            return false;
        }
        if (Double.parseDouble(user_total) < Double.parseDouble("110") && (Dashboard.kycverification.equals("Rej"))){
            new MaterialStyledDialog.Builder(new_payment.this)
                    .setTitle("KYC Rejected")
                    .setDescription("Your KYC has been REJECTED. Please contact support for further assistance.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative((dialog, which) -> {
                        // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        Intent homeIntent = new Intent(new_payment.this, Dashboard.class);
                        startActivity(homeIntent);
                        dialog.dismiss();
                    })

                    .show();
            return false;
        }

        if ((Float.parseFloat(amount_edit.getText().toString()) > 100) && (Dashboard.kycverification.equals("N.V"))) {
            new MaterialStyledDialog.Builder(new_payment.this)
                    .setTitle("KYC not verified")
                    .setDescription("Please send your KYC details by clicking UPLOAD KYC button below.")
                    .setHeaderColor(R.color.md_divider_white)
                    .setIcon(R.drawable.warninginternet)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(false)
                    .setCancelable(false)
                    .withDivider(true)
                    .setNegativeText("Cancel")
                    .onNegative((dialog, which) -> {
                        // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        Intent homeIntent = new Intent(new_payment.this, Dashboard.class);
                        startActivity(homeIntent);
                        dialog.dismiss();
                    })
                    .setPositiveText("Upload KYC")
                    .onPositive((dialog, which) -> {
                        // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        Intent homeIntent = new Intent(new_payment.this, grantreadandwritepermission.class);
                        startActivity(homeIntent);
                        dialog.dismiss();
                        finish();
                    })
                    .show();
            return false;
        }


        if (cardType.equals("Select card type") || cardType.equals("No card type found")) {
            Toast.makeText(new_payment.this, "Please Select card type", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            String type = spinner_transaction_type.getSelectedItem().toString();
            if (type.equals("Select Type Of Transaction")) {
                Toast.makeText(new_payment.this, "Please Select Type of Transaction ", Toast.LENGTH_SHORT).show();
                return false;
            }
        }


        if (purpose.equals("Select Purpose Of Payment")) {
            Toast.makeText(new_payment.this, "Please Select Purpose of Payment ", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    @Override
    public void onResume() {
        super.onResume();
        sessionCheck();
        spinnercasa.setSelection(0);
        beneficiary_name = "null";
        spinner_transaction_type.setSelection(0);
        transaction_type = "null";
        spinnerpurposepay.setSelection(0);
        purpose_of_payment = "null";
        amount_edit.getText().clear();
        switch_amex.setChecked(false);

    }

    private void transaction_amount(){

        SharedPreferences sharedPreferences = new_payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/total_transactions",
                response -> {
                    try {
                        JSONObject transactionresponse = new JSONObject(response);
                        System.out.println(transactionresponse);
                        if ((transactionresponse.getString("Result").equals(null)) || (transactionresponse.getString("Result") == null) || (transactionresponse.getString("Result") == "null") || (transactionresponse.getString("Result").equals("null"))) {
                            inum = 0;
                            user_total = "null";
                        } else {
                            user_total = transactionresponse.getString("Result");
                            inum = Double.parseDouble(user_total);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(new_payment.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new_payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(new_payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void sessionCheck() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/counter_check",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println(response);
                        String getResponse = jsonObject.getString("detail");

                        if (getResponse.equals("1")) {
                            String allowUser = jsonObject.getString("allow_user");
                            if (allowUser.equals("0")) {
                                final LayoutInflater layoutInflaterBlock = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog blockUserDialog = new Dialog(this);
                                blockUserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                blockUserDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                blockUserDialog.setContentView(layoutInflaterBlock.inflate(R.layout.block_user_layout, null));
                                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                blockUserDialog.getWindow().setLayout(width, height);
                                blockUserDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                blockUserDialog.setCancelable(false);
                                blockUserDialog.show();
                            } else {
                                //Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            }
                        } else if (getResponse.equals("Invalid token")) {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            });
                        } else {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("auth", TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void load_transaction_type(String cardType) {
        SharedPreferences sharedPreferences = new_payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        Transaction_percentage.clear();
        Transaction_name.clear();
        calculation_percentage.clear();
        transaction_eta.clear();
        gateway_list.clear();
        amex_list.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/trans_types_card",
                response -> {
                    System.out.println("*******************************************");
                    System.out.println(response);
                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        if (!jsonObject.getString("status").equals("error")) {
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("status"));
                            if (jsonArray != null) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    if (i == 0) {
                                        Transaction_percentage.add("Select Type Of Transaction");
                                        Transaction_name.add("0");
                                        calculation_percentage.add("0");
                                        transaction_eta.add("0");
                                        gateway_list.add("0");
                                        amex_list.add("0");
                                    }
                                    JSONObject dataObject = jsonArray.optJSONObject(i);
                                    String tranpercentage = dataObject.optString("status");
                                    String tran_name = dataObject.optString("name");
                                    String percent = dataObject.optString("percentage");
                                    String tran_eta = dataObject.optString("eta");
                                    String gateway_temp = dataObject.optString("gateway");
                                    String amex_temp = dataObject.optString("amex");

                                    Transaction_name.add(tran_name);
                                    Transaction_percentage.add(tranpercentage);
                                    calculation_percentage.add(percent);
                                    transaction_eta.add(tran_eta);
                                    gateway_list.add(gateway_temp);
                                    amex_list.add(amex_temp);
                                }
                                // Creating adapter for spinner
                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(new_payment.this, R.layout.spinner_item, Transaction_percentage);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinner_transaction_type.setAdapter(dataAdapter);


                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {
            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(new_payment.this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("card", cardType);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(new_payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }

    private void activity_new_payment(){

        SharedPreferences sharedPreferences = new_payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(new_payment.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new_payment.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        }) {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);


                params.put("AcitvityID","107");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(new_payment.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }

    private void load_purpose_payment() {
        try {
            purpose_list.clear();
            purpose_id.clear();
            StringRequest stringRequest1 = new StringRequest(Request.Method.GET, constant.SERVER + "/api/Purpose",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //beatslist.add("Select Beats");
                            try {

                                JSONArray jsonArray = new JSONArray(response);
                                if (jsonArray != null) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        if (i == 0) {
                                            purpose_list.add("Select Purpose Of Payment");
                                            purpose_id.add("0");
                                        }
                                        JSONObject dataObject = jsonArray.optJSONObject(i);
                                        String purp = dataObject.optString("purpose");
                                        String purpid = dataObject.optString("id");
                                        //   SharedPreferences.Editor editor = spinnerpreference.edit();

                                        // editor.putString("casa_id",ids);
                                        // editor.apply();
                                        // System.out.println("This");
                                        // System.out.println(ids);
                                        purpose_list.add(purp);
                                        purpose_id.add(purpid);
                                    }
                                    // Creating adapter for spinner
                                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(new_payment.this, R.layout.spinner_item, purpose_list);
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    spinnerpurposepay.setAdapter(dataAdapter);


                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String message = "";

                            if (error instanceof NetworkError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ServerError) {
                                Log.e("stackcheck1111", "3" + error.toString());
                                NetworkResponse response = error.networkResponse;
                                if (response != null) {
                                    Log.d("qqqqqqqqqqq", String.valueOf(response.statusCode));

                                    int statusCode = response.statusCode;
                                    if (statusCode == 404) {
                                        message = "No ISP Available in this Company";
                                        Log.d("qqqqqq", " " + response.data);

                                        //ShowAlert("Server is down. Please try after some time!");
                                        //json = new String(response.data);
                                        //json = trimMessage(json, "status");
                                        // if (json != null)
                                        //    message = json;


                                    } else {
                                        message = "The server could not be found. Please try again after some time!!";

                                    }
                                } else {
                                    message = "The server could not be found. Please try again after some time!!";

                                }
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                            }
                            Log.e("eeeee", "onErrorResponse: " + error.toString());
                            if (!message.isEmpty()) {
                                Toast.makeText(new_payment.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            RequestQueue requestQueue1 = Volley.newRequestQueue(new_payment.this);
            stringRequest1.setShouldCache(false);
            requestQueue1.add(stringRequest1);
        } catch (Exception e) {
        }

    }

    private void loadcasa(){
        try {

            isplist.clear();
            ispids.clear();
            ispvendorid.clear();
            StringRequest stringRequest1 = new StringRequest(Request.Method.GET, constant.SERVER + "/api/save_casa",
                    response -> {
                        //beatslist.add("Select Beats");
                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            if (jsonArray != null && jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    if (i == 0) {
                                        isplist.add("Select Beneficiary");
                                        ispids.add("0");
                                        ispvendorid.add("0");
                                        ispaccount_number.add("0");
                                        ispifsc.add("0");
                                        ispphone.add("0");
                                    }
                                    JSONObject dataObject = jsonArray.optJSONObject(i);
                                    String beats = dataObject.optString("first_name");
                                    String nick = dataObject.optString("second_name");
                                    String ids = dataObject.optString("id");
                                    String vid = dataObject.optString("notes");
                                    String vaccount_number = dataObject.optString("Account_number");
                                    String vifsc = dataObject.optString("IFSC");
                                    String vphone = dataObject.optString("phone");
                                    System.out.println("this is vid");
                                    System.out.println(vid);
                                    //   SharedPreferences.Editor editor = spinnerpreference.edit();

                                    // editor.putString("casa_id",ids);
                                    // editor.apply();
                                    // System.out.println("This");
                                    // System.out.println(ids);
                                    isplist.add(beats + "(" + nick + ")");
                                    ispids.add(ids);
                                    ispvendorid.add(vid);
                                    ispaccount_number.add(vaccount_number);
                                    ispifsc.add(vifsc);
                                    ispphone.add(vphone);
                                }
                                Log.e("Aakash", isplist + "");
                                // Creating adapter for spinner

                                ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(new_payment.this, R.layout.spinner_item, isplist);
                                dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnercasa.setAdapter(dataAdapter2);


                            } else {
                                isplist.add("No Beneficiary Available");
                                ispids.add("0");
                                ispvendorid.add("0");
                                ispaccount_number.add("0");
                                ispifsc.add("0");
                                ispphone.add("0");
                                // Creating adapter for spinner
                                ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(new_payment.this, R.layout.spinner_item, isplist);
                                dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                spinnercasa.setAdapter(dataAdapter2);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String message = "";

                            if (error instanceof NetworkError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ServerError) {
                                Log.e("stackcheck1111", "3" + error.toString());
                                NetworkResponse response = error.networkResponse;
                                if (response != null) {
                                    Log.d("qqqqqqqqqqq", String.valueOf(response.statusCode));

                                    int statusCode = response.statusCode;
                                    if (statusCode == 404) {
                                        message = "No ISP Available in this Company";
                                        Log.d("qqqqqq", " " + response.data);

                                        //ShowAlert("Server is down. Please try after some time!");
                                        //json = new String(response.data);
                                        //json = trimMessage(json, "status");
                                        // if (json != null)
                                        //    message = json;


                                    } else {
                                        message = "The server could not be found. Please try again after some time!!";

                                    }
                                } else {
                                    message = "The server could not be found. Please try again after some time!!";

                                }
                            } else if (error instanceof AuthFailureError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (error instanceof NoConnectionError) {
                                message = "Cannot connect to Internet...Please check your connection!";
                            } else if (error instanceof TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection.";
                            }
                            Log.e("eeeee", "onErrorResponse: " + error.toString());
                            if (!message.isEmpty()) {
                                Toast.makeText(new_payment.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }) {

                SharedPreferences sharedPreferences = new_payment.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                final String token1 = sharedPreferences.getString("token", "");
                final String token200 = token1;

                //-------------spinnerheader-----------------//

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String T = "";
                    Map<String, String> params = new HashMap<>();


                    // params.put("Content-Type", "application/json; charset=UTF-8");
                    try {
                        JSONObject response1 = new JSONObject(token200);
                        System.out.println("!!!!!!!!!!!!!!!!!!");
                        System.out.println(response1.getString("token"));
                        // do your work with response object
                        T = response1.getString("token");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    System.out.println("44444444444444444444444444");
                    System.out.println(token200);
                    ///check t!blank

                    String TokenS = "token "+ T;
                    System.out.println(T);

                    params.put("Authorization",TokenS);

                    return params;
                }

            };


            RequestQueue requestQueue1 = Volley.newRequestQueue(new_payment.this);
            stringRequest1.setShouldCache(false);
            requestQueue1.add(stringRequest1);
        } catch (Exception e) {

        }
    }
}