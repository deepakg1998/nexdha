package com.nexdha.nexpay;

import static android.content.ContentValues.TAG;
import static com.nexdha.nexpay.constant.SECRET_KEY;
import static com.nexdha.nexpay.constant.SITE_KEY;
import static com.nexdha.nexpay.constant.url;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class landlord extends AppCompatActivity {
    public LinearLayout rental;
    public ImageView imageView83, imageView86;
    public Button button67;
    public static final String MYCHECKPREFERENCES = "MyCheckPrefs";
    SharedPreferences sharedcheckPreferences;
    public String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public EditText Amount, propertyname, name, accountnumber, reenter_accountnumber, ifsc_code, mobilenumber, mails;
    public String amountonly;
    public String pro_name;
    public String name1;
    public String accountnumber1;
    public String reenter_accountnumber1;
    public String ifsc_code1;
    public String mobilenumber1;
    public String mails1;
    public TextView ifsclocation;
    public ProgressBar progressBar29, progressBar30;
    public String ifscgetstring;
    public String api_id;
    public String save_test;
    String previousActivity;
    public String reason;
    public String ifscstate;
    public String permissionsave;
    public String ifscbranchstore;
    public String ifscbankstore;
    RequestQueue queue;
    LayoutInflater inflater;
    Dialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landlord);
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.save_success, null));
        loadingDialog.setCancelable(false);
        imageView83 = findViewById(R.id.imageView83);
        //imageView86 = findViewById(R.id.imageView86);

        button67 = findViewById(R.id.button67);
        Amount = findViewById(R.id.amount);
        propertyname = findViewById(R.id.propertyname);
        name = findViewById(R.id.name);
        accountnumber = findViewById(R.id.accountnumber);
        reenter_accountnumber = findViewById(R.id.reenter_accountnumber);
        ifsc_code = findViewById(R.id.ifsc_code);
        progressBar29 = findViewById(R.id.progressBar29);
        mobilenumber = findViewById(R.id.mobilenumber);
        mails = findViewById(R.id.mails);
        ifsclocation = findViewById(R.id.textView331);
        queue = Volley.newRequestQueue(getApplicationContext());
        sharedcheckPreferences = getSharedPreferences(MYCHECKPREFERENCES, Context.MODE_PRIVATE);

        Intent intent = getIntent();
        previousActivity = intent.getStringExtra("FROM_ACTIVITY");

        SharedPreferences sharedPreferences = landlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        button67.setOnClickListener(view -> {
            // Intent intent = new Intent(landlord.this, addlandlord.class);
            // startActivity(intent);
      /*      amountonly = Amount.getText().toString();
            System.out.println(amountonly);

            pro_name = propertyname.getText().toString();
            System.out.println(pro_name);

            mails1 = mails.getText().toString();
            System.out.println(mails1);

            name1 = name.getText().toString();
            System.out.println(name1);*/

      /*      accountnumber1 = accountnumber.getText().toString();
            System.out.println(accountnumber1);
            reenter_accountnumber1 = reenter_accountnumber.getText().toString();
            System.out.println(reenter_accountnumber1);

            ifsc_code1 = ifsc_code.getText().toString();
            System.out.println(ifsc_code1);


            mobilenumber1 = mobilenumber.getText().toString();
            System.out.println(mobilenumber1);*/

            if (!Validationacc()) {
                return;
            } else {

                addRecaptcha();
            }

        });
        imageView83.setOnClickListener(view -> finish());
        ifsc_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {


                progressBar29.setVisibility(View.VISIBLE);


                if (s.length() >= 11) {
                    System.out.println("This is method initialisation");
                    // bankifsc = ifsccode.getText().toString().trim();
                    ifsc_code1 = ifsc_code.getText().toString().trim();
                    ;

                    ifscmethod();
                }


                // TODO Auto-generated method stub
            }
        });

    }

    private void addRecaptcha() {

        SafetyNet.getClient(landlord.this).verifyWithRecaptcha(SITE_KEY.trim())
                .addOnSuccessListener(this, response -> {
                    if (!Objects.requireNonNull(response.getTokenResult()).isEmpty()) {
                        handleSiteVerify(response.getTokenResult());
                    }
                })
                .addOnFailureListener(this, e -> {
                    if (e instanceof ApiException) {
                        ApiException apiException = (ApiException) e;
                        int status = apiException.getStatusCode();
                        System.out.println("Error code: " + status);
                        Log.d(TAG, "Error message: " +
                                CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                    } else {
                        Log.d(TAG, "Unknown type of error: " + e.getMessage());
                    }
                });

    }

    private void handleSiteVerify(String responseToken) {
        StringRequest request = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("success")) {
                            save_rent();
                            //Toast.makeText(landlord.this, "You are not a robot", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(landlord.this, "Human verification failed. Please try again later ", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "JSON exception: " + ex.getMessage());

                    }

                },
                error -> {
                    Toast.makeText(landlord.this, "Server error, Please try again later", Toast.LENGTH_SHORT).show();
                    finish();


                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("secret", SECRET_KEY.trim());
                params.put("response", responseToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);


    }


    private boolean Validationacc() {
        amountonly = Amount.getText().toString();
        System.out.println(amountonly);
        pro_name = propertyname.getText().toString();
        System.out.println(pro_name);

        name1 = name.getText().toString();
        System.out.println(name1);

        accountnumber1 = accountnumber.getText().toString();
        System.out.println(accountnumber1);

        reenter_accountnumber1 = reenter_accountnumber.getText().toString();
        System.out.println(reenter_accountnumber1);

        //ifsc_code1 = ifsc_code.getText().toString();
        //System.out.println(ifsc_code1);

  /*      ifsc_code1 = ifsc_code.getText().toString().trim();
        String ifscuppercase = ifsc_code1.toUpperCase();
        System.out.println(ifscuppercase);*/
        mobilenumber1 = mobilenumber.getText().toString();
        System.out.println(mobilenumber1);

        mails1 = mails.getText().toString();
        System.out.println(mails1);

        final String bankaccount = accountnumber.getText().toString().trim();
        final String bankreaccount = reenter_accountnumber.getText().toString().trim();
        final String userfirstname = name.getText().toString().trim();
        final String beneemail = mails.getText().toString().trim();
        final String number = mobilenumber.getText().toString().trim();

        ifsc_code1 = ifsc_code.getText().toString().trim();
        String ifscuppercase = ifsc_code1.toUpperCase();
        System.out.println(ifscuppercase);


        if (ifscuppercase.equals("PYTM0123456")) {
            ifsc_code.setError("Paytm bank not allowed", null);
            ifsc_code.requestFocus();
            // Toast.makeText(AddBeneficiary.this,"Please Enter IFSC ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            ifsc_code.setError(null, null);
            ifsc_code.clearFocus();
        }
        if (ifscuppercase.equals("PYTM0123456")) {
            ifsc_code.setError("Paytm bank not allowed", null);
            ifsc_code.requestFocus();
            // Toast.makeText(AddBeneficiary.this,"Please Enter IFSC ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            ifsc_code.setError(null, null);
            ifsc_code.clearFocus();
        }
        if (userfirstname.isEmpty() || userfirstname.length() == 0 || userfirstname.equals("") || userfirstname == null) {
            name.setError("Please Enter name", null);
            name.requestFocus();
            //  Toast.makeText(AddBeneficiary.this,"Please Enter Beneficiary ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            name.setError(null, null);
            name.clearFocus();
        }
        if (beneemail.isEmpty() || beneemail.length() == 0 || beneemail.equals("") || beneemail == null) {
            mails.setError("Please Enter mail id", null);
            mails.requestFocus();
            //  Toast.makeText(AddBeneficiary.this,"Please Enter Beneficiary ",Toast.LENGTH_LONG).show();
            return false;
        } else if (!beneemail.matches(emailPattern)) {
            mails.setError("Please Enter valid Email iD", null);
            mails.requestFocus();
            //  Toast.makeText(AddBeneficiary.this,"Please Enter Beneficiary ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            mails.setError(null, null);
            mails.clearFocus();
        }
        if (name1.length() >= 50) {
            name.setError("Please Enter a Valid Name", null);
            name.requestFocus();
            //Toast.makeText(AddBeneficiary.this,"Please Enter a Valid Name ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            name.setError(null, null);
            name.clearFocus();
        }

        if (bankaccount.isEmpty() || bankaccount.length() == 0 || bankaccount.equals("") || bankaccount == null) {
            accountnumber.setError("Please Enter Account Number", null);
            accountnumber.requestFocus();
            //Toast.makeText(AddBeneficiary.this,"Please Enter Account Number ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            accountnumber.setError(null, null);
            accountnumber.clearFocus();
        }
        if (bankreaccount.isEmpty() || bankreaccount.length() == 0 || bankreaccount.equals("") || bankreaccount == null) {
            reenter_accountnumber.setError("Please Re-enter Account Number", null);
            reenter_accountnumber.requestFocus();
            //Toast.makeText(AddBeneficiary.this,"Please Re-enter Account Number ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            reenter_accountnumber.setError(null, null);
            reenter_accountnumber.clearFocus();
        }
        if (!bankaccount.equals(bankreaccount)) {
            reenter_accountnumber.setError("Account Numbers Does not match", null);
            reenter_accountnumber.requestFocus();
            //Toast.makeText(AddBeneficiary.this,"Account Numbers Does not match",Toast.LENGTH_LONG).show();
            return false;
        } else {
            reenter_accountnumber.setError(null, null);
            reenter_accountnumber.clearFocus();
        }

        if (number.isEmpty() || number.length() == 0 || number.equals("") || number == null) {
            mobilenumber.setError("Please Enter mobile number", null);
            mobilenumber.requestFocus();
            // Toast.makeText(AddBeneficiary.this,"Please Enter Beneficiary number ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            mobilenumber.setError(null, null);
            mobilenumber.clearFocus();
        }
        if (number.length() <= 9 || number.length() >= 11) {
            //Toast.makeText(AddBeneficiary.this,"Invalid beneficiary Number",Toast.LENGTH_LONG).show();
            mobilenumber.setError("Invalid Beneficiary number", null);
            mobilenumber.requestFocus();
            return false;
        } else {
            mobilenumber.setError(null, null);
            mobilenumber.clearFocus();
        }
        if (ifsc_code1.isEmpty() || ifsc_code1.length() == 0 || ifsc_code1.equals("") || ifsc_code1 == null) {
            ifsc_code.setError("Please Enter IFSC", null);
            ifsc_code.requestFocus();
            // Toast.makeText(AddBeneficiary.this,"Please Enter IFSC ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            ifsc_code.setError(null, null);
            ifsc_code.clearFocus();
        }
        if (ifsc_code1.length() < 11 || ifsc_code1.length() > 11) {
            ifsc_code.setError("Please Enter Valid IFSC", null);
            ifsc_code.requestFocus();
            //Toast.makeText(AddBeneficiary.this,"Please Enter Valid IFSC ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            ifsc_code.setError(null, null);
            ifsc_code.clearFocus();
        }

        if (ifscgetstring.isEmpty() || ifscgetstring.length() == 0 || ifscgetstring.equals("") || ifscgetstring == null) {
            ifsc_code.setError("Click save once you see the Bank branch below entered IFSC code", null);
            ifsc_code.requestFocus();
            //Toast.makeText(AddBeneficiary.this,"Click save after IFSC gets verified ",Toast.LENGTH_LONG).show();
            return false;
        } else {
            ifsc_code.setError(null, null);
            ifsc_code.clearFocus();
        }
        if (ifscgetstring.equals("Not Found")) {
            //Toast.makeText(AddBeneficiary.this,"Please Enter a Valid IFSC ",Toast.LENGTH_LONG).show();
            ifsc_code.setError("Please Enter a Valid IFSC", null);
            ifsc_code.requestFocus();
            return false;
        } else {
            ifsc_code.setError(null, null);
            ifsc_code.clearFocus();
        }

        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void ifscmethod() {

        ifsclocation = (TextView) findViewById(R.id.textView331);
        SharedPreferences sharedPreferences = landlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/ifsc",
                response -> {
                    try {
                        JSONObject ifscresponse = new JSONObject(response);

                        System.out.println("This is ifsc response 1");
                        System.out.println(ifscresponse);
                        ifscgetstring = ifscresponse.getString("status");
                        api_id = ifscresponse.getString("id");
                        reason = ifscresponse.getString("reason");
                        System.out.println("This is ifsc array 1");
                        System.out.println(ifscgetstring);
                        if (ifscgetstring.equals("Not Found")) {
                            ifsclocation.setTextColor(Color.RED);
                            ifsclocation.setText("Not a Valid IFSC Code");
                        } else {
                            JSONObject ifscarray = ifscresponse.getJSONObject("status");
                            ifscstate = ifscarray.getString("STATE");
                            if (ifscstate.contains("TAMIL NADU") || ifscstate.contains("KERALA") || ifscstate.contains("KARNATAKA") || ifscstate.contains("ANDHRA PRADESH") || ifscstate.contains("TELANGANA") || ifscstate.contains("MAHARASHTRA")) {
                                permissionsave = "allowed";
                            } else {
                                permissionsave = "notallowed";
                            }

                            ifsclocation.setTextColor(Color.parseColor("#9B9B9B"));
                            ifsclocation.setText(ifscarray.getString("BRANCH"));
                            ifscbranchstore = ifscarray.getString("BRANCH");
                            ifscbankstore = ifscarray.getString("BANK");
                            // save.setEnabled(true);
                            // save.setBackgroundResource(R.drawable.roundedbutton);
                            // save.setTextColor(Color.WHITE);
                        }
                      /*  if (ifscarray.has("BRANCH")){
                            String nameValue = ifscarray.getString("BRANCH");
                            ifsclocation.setText(ifscarray.getString("BRANCH"));
                            System.out.println("ifsc location");
                            System.out.println(nameValue);
                        }
                        else{
                            ifsclocation.setText("Not a Valid IFSC Code");
                        }
                        */


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(landlord.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            protected Map<String, String> getParams() {
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);


                params.put("ifsc", ifsc_code1);
                return params;
            }

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(landlord.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        progressBar29.setVisibility(View.GONE);


    }

    public void save_rent() {
        SharedPreferences sharedPreferences = landlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("4444444444444444444444444444444444444444444444444444444");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.SERVER + "/api/save_casa_rental",
                response -> {
                    System.out.println("*******************************************");
                    System.out.println(response);
                    try {
                        JSONObject response4 = new JSONObject(response);
                        System.out.println("**********this is id********");
                        save_test = response4.getString("status");
                        loadingDialog.show();
                        RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_NOTIFICATION);
                        new Handler().postDelayed(this::finish, 2000);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }, error -> {
            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(landlord.this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            //This is for Headers If You Needed

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("Account_number", accountnumber1);
                params.put("first_name", name1);
                params.put("second_name", name1);
                params.put("email", mails1);
                params.put("phone", mobilenumber1);
                params.put("IFSC", ifsc_code1);
                params.put("location", "location");
                //params.put("account_type", "rental");
                // params.put("notes", "");
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };

/*    public void save_rent() {
        SharedPreferences sharedPreferences = landlord.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("4444444444444444444444444444444444444444444444444444444");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest request = new StringRequest(Request.Method.POST,SERVER+"/api/save_casa_rental", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject response4 = new JSONObject(response);
                    String parse_token = response4.getString(("status"));
                    System.out.println(parse_token);

                    if (parse_token.equals("successfully saved")){
                       // main_bg = findViewById(R.id.main_layout);
                      System.out.println(parse_token);
                        Toast.makeText(landlord.this, "Please Bill date", Toast.LENGTH_LONG).show();


                      *//*  Snackbar.make(main_bg, "Avail special offer for SME. If you are SME click here.", Snackbar.LENGTH_LONG).setDuration(15000)
                                .setAction("Request", new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View view) {

                                        Intent intent = new Intent(Dashboard.this, Moreoptions.class);
                                        startActivity(intent);
                                    }

                                }).setActionTextColor(Color.parseColor("#f94d00")).show();

*//*

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error is ", "" + error);
                System.out.println(error);
            }
        }) {


            //This is for Headers If You Needed
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T="" ;
                Map<String,String> params = new HashMap<>();
                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("Account_number", accountnumber1);
                params.put("first_name", name1);

                params.put("second_name", "name");
                params.put("email", "email@1233");
                params.put("phone", mobilenumber1);
                params.put("IFSC", ifsc_code1);

                params.put("location", "location");
                params.put("account_type", "landlord");
                params.put("notes", "otp_pass1");

                return params;
            }


         *//*   protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("pan",(panindia));


                System.out.println(panindia);
                return params;
            }*//*
        };
        RequestQueue requestQueue = Volley.newRequestQueue(landlord.this);
        request.setShouldCache(false);
        requestQueue.add(request);
    }*/

        RequestQueue requestQueue = Volley.newRequestQueue(landlord.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

}
