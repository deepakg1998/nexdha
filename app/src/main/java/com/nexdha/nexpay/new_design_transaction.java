package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.nexdha.nexpay.ApiClient.MyMediatorInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class new_design_transaction extends AppCompatActivity {
    private MyMediatorInterface myMediatorInterface;
    public Button success,failed,cancelled;
public ConstraintLayout clsuccess,clfailed,clcancelled;
public ConstraintLayout layoutforsuccess,layoutforfailed,layoutforcancelled;
    int clickcount=0;
    int clickcount1=0;
    public String tstatus= "Transaction successful";
    String data ;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public ImageView transactionhistoryclose,nodata;
    private List<transactionhistoryview> listItems;
    ProgressBar historyprogress;

    public ImageView imageView75;
    private RecyclerView recyclerView1;
    private RecyclerView.Adapter adapter1;
    public ImageView transactionhistoryclose1,nodata1;
    private List<transactionhistoryview> listitems1;
    ProgressBar historyprogress1;
    public ImageView imageView455,imageView4555;

    private RecyclerView recyclerView11;
    private RecyclerView.Adapter adapter11;
    public ImageView transactionhistoryclose11,nodata11;
    private List<transactionhistoryview> listItems11;
    ProgressBar historyprogress11;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_new_design_transaction);
        clsuccess=findViewById(R.id.clsuccess);
        layoutforsuccess=findViewById(R.id.layoutforsuccess);
        layoutforsuccess.animate().translationX(0);
        layoutforfailed=findViewById(R.id.layoutforfailed);
        layoutforfailed.animate().translationX(2000);
     //   layoutforcancelled=findViewById(R.id.layoutforcancelled);
        //layoutforcancelled.animate().translationX(-2000);
        clfailed=findViewById(R.id.clfailed);
      //  clcancelled=findViewById(R.id.clcancelled);
        success=findViewById(R.id.button);
        failed = findViewById(R.id.button3);
        //   cancelled=findViewById(R.id.button4);
        clsuccess.setVisibility(View.VISIBLE);
        clfailed.setVisibility(View.GONE);
        //  clcancelled.setVisibility(View.GONE);
        imageView75 = findViewById(R.id.imageView75);
        historyprogress = findViewById(R.id.progressBar9);
        historyprogress1 = findViewById(R.id.progressBar99);
        historyprogress11 = findViewById(R.id.progressBar999);
        transactionhistoryclose = (ImageView) findViewById(R.id.imageView15);

        myMediatorInterface = (order, abc) -> {

        };
        imageView75.setOnClickListener(v -> finish());
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);

        activity_log_transaction_history();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listItems = new ArrayList<>();
////////////////////////////////////////////////////////////////////////////////////////////
        recyclerView1 = (RecyclerView)findViewById(R.id.recyclerview1);

        // activity_log_transaction_history();

        recyclerView1.setHasFixedSize(true);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this));
        listitems1 = new ArrayList<>();


        recyclerView11 = (RecyclerView) findViewById(R.id.recyclerview11);
        // activity_log_transaction_history();

        recyclerView11.setHasFixedSize(true);
        recyclerView11.setLayoutManager(new LinearLayoutManager(this));

        listItems11 = new ArrayList<>();


        loadRecyclerview();
        loadRecyclerview1();
        sessionCheck();
        // loadRecyclerview2();

        SharedPreferences sharedPreferences = new_design_transaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAA");
        System.out.println(token1);
        Intent intent = getIntent();

        Bundle extras = intent.getExtras();

        if(extras !=null){
            String data = getIntent().getExtras().getString("STRING_I_NEED");
            System.out.println(data);
            //layoutforfailed.animate().translationX(0);
            clsuccess.setVisibility(View.GONE);
            //   clcancelled.setVisibility(View.GONE);
            clfailed.setVisibility(View.VISIBLE);
            layoutforfailed.setVisibility(View.VISIBLE);
            layoutforsuccess.animate().translationX(-2000);
            layoutforfailed.animate().translationX(0);
        }else{

        }
     /*   if(data.equals("forcancelled")){
          // String data = getIntent().getExtras().getString("STRING_I_NEED");
            System.out.println(data);
            //layoutforfailed.animate().translationX(0);
            clsuccess.setVisibility(View.GONE);
            //   clcancelled.setVisibility(View.GONE);
            clfailed.setVisibility(View.VISIBLE);
            layoutforfailed.setVisibility(View.VISIBLE);
            layoutforsuccess.animate().translationX(-2000);
            layoutforfailed.animate().translationX(0);
           // failed_cancelled();
        }else {

        }*/
        success.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount =clickcount + 1;
                if(clickcount==0){
                    loadRecyclerview();

                    //first time clicked to do this
                    clsuccess.setVisibility(View.VISIBLE);
                    clfailed.setVisibility(View.GONE);
                    layoutforfailed.animate().translationX(2000);
                    layoutforsuccess.animate().translationX(0);
                }else{
                    clsuccess.setVisibility(View.VISIBLE);
                    clfailed.setVisibility(View.GONE);
                    layoutforfailed.animate().translationX(2000);
                    layoutforsuccess.animate().translationX(0);
                    // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount, Toast.LENGTH_LONG).show();

                }



              /*  }
                else
                {*/
             /*       clsuccess.setVisibility(View.VISIBLE);
                    clfailed.setVisibility(View.GONE);
                   layoutforfailed.animate().translationX(-2000);
                    layoutforsuccess.animate().translationX(0);*/
                    //check how many times clicked and so on
                   // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount, Toast.LENGTH_LONG).show();

            }


             //   clcancelled.setVisibility(View.GONE);


                // layoutforcancelled.animate().clsuccessranslationX(2000);


        });
        failed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount1 =clickcount1 + 1;
                if(clickcount1==0){
                    loadRecyclerview1();

                    clsuccess.setVisibility(View.GONE);
                    //   clcancelled.setVisibility(View.GONE);
                    clfailed.setVisibility(View.VISIBLE);
                    layoutforfailed.setVisibility(View.VISIBLE);
                    layoutforsuccess.animate().translationX(-2000);
                    layoutforfailed.animate().translationX(0);
                }
                  else{
                    clsuccess.setVisibility(View.GONE);
                    //   clcancelled.setVisibility(View.GONE);
                    clfailed.setVisibility(View.VISIBLE);
                    layoutforfailed.setVisibility(View.VISIBLE);
                    layoutforsuccess.animate().translationX(-2000);
                    layoutforfailed.animate().translationX(0);
                   // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount1, Toast.LENGTH_LONG).show();

                }

                    //   layoutforcancelled.animate().translationX(2000);

               /* }else{*/
                   // clsuccess.setVisibility(View.GONE);
                     // clcancelled.setVisibility(View.GONE);
                    //clfailed.setVisibility(View.VISIBLE);
                    /*layoutforfailed.setVisibility(View.VISIBLE);
                    layoutforsuccess.animate().translationX(-2000);
                    layoutforfailed.animate().translationX(0);*/

                    //   layoutforcancelled.animate().translationX(2000);

                    //clsuccess.animate().translationX(-2000);
                   // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount1, Toast.LENGTH_LONG).show();

                //}


            }
        });
   /*     cancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         //      loadRecyclerview2();
                clsuccess.setVisibility(View.GONE);
                clfailed.setVisibility(View.GONE);
                clcancelled.setVisibility(View.VISIBLE);
                layoutforcancelled.setVisibility(View.VISIBLE);
                layoutforsuccess.animate().translationX(-2000);
             //   layoutforcancelled.animate().translationX(0);
                layoutforfailed.animate().translationX(2000);
            }
        });*/


    }


    @Override
    protected void onResume() {
        super.onResume();
        sessionCheck();
    }

    private void sessionCheck() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        @SuppressLint("InflateParams") StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/counter_check",
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println(response);
                        String getResponse = jsonObject.getString("detail");

                        if (getResponse.equals("1")) {
                            String allowUser = jsonObject.getString("allow_user");
                            if (allowUser.equals("0")) {
                                final LayoutInflater layoutInflaterBlock = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final Dialog blockUserDialog = new Dialog(this);
                                blockUserDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                blockUserDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                blockUserDialog.setContentView(layoutInflaterBlock.inflate(R.layout.block_user_layout, null));
                                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                blockUserDialog.getWindow().setLayout(width, height);
                                blockUserDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                                blockUserDialog.setCancelable(false);
                                blockUserDialog.show();
                            } else {
                                //Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                            }
                        } else if (getResponse.equals("Invalid token")) {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            });
                        } else {
                            final LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final Dialog sessionDialog = new Dialog(this);
                            sessionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            sessionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                            sessionDialog.setContentView(layoutInflater.inflate(R.layout.session_expired_layout, null));
                            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                            sessionDialog.getWindow().setLayout(width, height);
                            sessionDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                            sessionDialog.setCancelable(false);
                            sessionDialog.show();
                            AppCompatButton logoutSession = sessionDialog.findViewById(R.id.login_again);
                            logoutSession.setOnClickListener(view -> {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.clear();
                                editor.apply();
                                startActivity(new Intent(this, MainActivity.class));
                                finishAffinity();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {
            System.out.println(error instanceof ClientError);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                String T = "";
                Map<String, String> params = new HashMap<>();
                try {
                    JSONObject response1 = new JSONObject(token1);
                    T = response1.getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String TokenS = T;

                params.put("auth", TokenS);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);


    }


    public void failed_cancelled() {
        failed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickcount1 = clickcount1 + 1;
                if (clickcount1 == 0) {
                    loadRecyclerview1();

                    clsuccess.setVisibility(View.GONE);
                    //   clcancelled.setVisibility(View.GONE);
                    clfailed.setVisibility(View.VISIBLE);
                    layoutforfailed.setVisibility(View.VISIBLE);
                    layoutforsuccess.animate().translationX(-2000);
                    layoutforfailed.animate().translationX(0);
                } else {
                    clsuccess.setVisibility(View.GONE);
                    //   clcancelled.setVisibility(View.GONE);
                    clfailed.setVisibility(View.VISIBLE);
                    layoutforfailed.setVisibility(View.VISIBLE);
                    layoutforsuccess.animate().translationX(-2000);
                    layoutforfailed.animate().translationX(0);
                    // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount1, Toast.LENGTH_LONG).show();

                }

                //   layoutforcancelled.animate().translationX(2000);

                /* }else{*/
                // clsuccess.setVisibility(View.GONE);
                // clcancelled.setVisibility(View.GONE);
                //clfailed.setVisibility(View.VISIBLE);
                    /*layoutforfailed.setVisibility(View.VISIBLE);
                    layoutforsuccess.animate().translationX(-2000);
                    layoutforfailed.animate().translationX(0);*/

                //   layoutforcancelled.animate().translationX(2000);

                //clsuccess.animate().translationX(-2000);
                // Toast.makeText(getApplicationContext(),"Button clicked count is"+clickcount1, Toast.LENGTH_LONG).show();

                //}


            }
        });

    }
    private void activity_log_transaction_history(){

        SharedPreferences sharedPreferences = new_design_transaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        // StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://3.18.225.128/api/getuserdetails",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/Activity",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Nothing

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String message = "";

                if (error instanceof NetworkError) {
                    //   message = "cannot profile connect";
                    new MaterialStyledDialog.Builder(new_design_transaction.this)
                            .setTitle("Internet Error")
                            .setDescription("Check Your Interenet Connection")
                            .setHeaderColor(R.color.md_divider_white)
                            .setIcon(R.drawable.warninginternet)
                            .withDarkerOverlay(true)
                            .withDialogAnimation(false)
                            .setCancelable(false)
                            .withDivider(true)
                            .setNegativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    // startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveText("Open Settings")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                                    // dialog.dismiss();
                                }
                            })
                            .show();
                } else if (error instanceof AuthFailureError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof NoConnectionError) {
                    message = "There is a problem connecting to server. Please try after sometime.";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout. Please try after sometime";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(new_design_transaction.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String,String> getParams(){
                //  String updateid = "";
                // String updatetranid="";
                Map<String, String> params = new HashMap<>();

                // System.out.println("make my trippppppppppppppp");
                // System.out.println(Transactionid);

                // System.out.println("mapppppp");
                // System.out.println(Updatetranidapi);




                params.put("AcitvityID","106");
                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(new_design_transaction.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);



    }


        private void loadRecyclerview(){

        SharedPreferences sharedPreferences = new_design_transaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("-------------------------------history---------------------------------------");
        System.out.println(token1);
        final String token200 = token1;
        nodata = (ImageView) findViewById(R.id.imageView45);
        nodata.setVisibility(View.GONE);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/trans_split?response_message=Transaction successful&state=",
                response -> {
                    try {
                        // JSONObject transactionobject = new JSONObject(response);
                        // JSONArray transactionarray = transactionobject.getJSONArray(null);
                        JSONArray transactionarray = new JSONArray(response);
                        System.out.println("this is the history response");
                        System.out.println(transactionarray);

                        if (transactionarray == null || transactionarray.length() == 0) {
                            historyprogress.setVisibility(View.GONE);
                            nodata.setVisibility(View.VISIBLE);
                        } else {

                            for (int i = 0; i < transactionarray.length(); i++) {

                                JSONObject o = transactionarray.getJSONObject(i);
                                transactionhistoryview item = new transactionhistoryview(

                                        o.getString("amount"),
                                        o.getString("udf3"),
                                        o.getString("udf5"),
                                        o.getString("transaction_id"),
                                        o.getString("response_message"),
                                        o.getString("address_line_1"),
                                        o.getString("state"),
                                        o.getString("id"),
                                        o.getString("name"),
                                        o.getString("order_id")

                                );
                                System.out.println("this is the item response");
                                System.out.println(o.getString("response_message"));

                                listItems.add(item);
                                historyprogress.setVisibility(View.GONE);
                                nodata.setVisibility(View.GONE);

                           /*     if((o.getString("response_message").equals("Transaction successful"))){
                                    listItems.add(item);
                                    historyprogress.setVisibility(View.GONE);
                                  //  nodata.setVisibility(View.GONE);

                                }else{
                                  //  nodata.setVisibility(View.VISIBLE);

                                }*/


                            }

                            adapter = new MyAdapter(listItems, getApplicationContext(), myMediatorInterface);
                            recyclerView.setAdapter(adapter);

                            historyprogress.setVisibility(View.GONE);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getApplicationContext(), volleyError.getMessage(), Toast.LENGTH_LONG).show()

                    }

                })


        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("response_message",tstatus);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }

    private void loadRecyclerview1(){

        SharedPreferences sharedPreferences = new_design_transaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("-------------------------------history---------------------------------------");
        System.out.println(token1);
        final String token200 = token1;
        nodata1 = (ImageView) findViewById(R.id.imageView455);
        nodata1.setVisibility(View.GONE);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/trans_split?response_message=&state=",
                response -> {
                    try {
                        // JSONObject transactionobject = new JSONObject(response);
                        // JSONArray transactionarray = transactionobject.getJSONArray(null);
                        JSONArray transactionarray = new JSONArray(response);
                        System.out.println("this is the history response");
                        System.out.println(transactionarray);
                        if (transactionarray == null || transactionarray.length() == 0) {
                            historyprogress1.setVisibility(View.GONE);
                            nodata1.setVisibility(View.VISIBLE);
                        } else {
                            for (int i = 0; i < transactionarray.length(); i++) {
                                JSONObject o = transactionarray.getJSONObject(i);
                                transactionhistoryview item = new transactionhistoryview(

                                        o.getString("amount"),
                                        o.getString("udf3"),
                                        o.getString("udf5"),
                                        o.getString("transaction_id"),
                                        o.getString("response_message"),
                                        o.getString("address_line_1"),
                                        o.getString("state"),
                                        o.getString("id"),
                                        o.getString("name"),
                                        o.getString("order_id")

                                );
                                System.out.println("this is the item response");
                                System.out.println(item);
                                listitems1.add(item);
                                System.out.println(item);

                                historyprogress1.setVisibility(View.GONE);
                                nodata1.setVisibility(View.GONE);
                             /*   if((o.getString("response_message").equals("Transaction successful"))){

                                //  nodata1.setVisibility(View.VISIBLE);

                                }else{
                                    listitems1.add(item);
                                    historyprogress1.setVisibility(View.GONE);
                                 ///   nodata1.setVisibility(View.GONE);
                                }*/
                            }
                            adapter1 = new listAdapter(listitems1, getApplicationContext(), myMediatorInterface);
                            recyclerView1.setAdapter(adapter1);
                            historyprogress1.setVisibility(View.GONE);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getApplicationContext(), volleyError.getMessage(), Toast.LENGTH_LONG).show()

                    }

                })


        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("response_message","c");
                System.out.println("response_message");
                return params;
            }

        };



        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }
    private void loadRecyclerview2(){

        SharedPreferences sharedPreferences = new_design_transaction.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("-------------------------------history---------------------------------------");
        System.out.println(token1);
        final String token200 = token1;
        nodata11 = (ImageView) findViewById(R.id.imageView4555);



        StringRequest stringRequest = new StringRequest(Request.Method.GET, SERVER + "/api/save_PG_Tran_Response",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            // JSONObject transactionobject = new JSONObject(response);
                            // JSONArray transactionarray = transactionobject.getJSONArray(null);
                            JSONArray transactionarray = new JSONArray(response);
                            System.out.println("this is the history response");
                            System.out.println(transactionarray);

                            if (transactionarray==null || transactionarray.length()==0){
                                historyprogress11.setVisibility(View.GONE);
                                nodata11.setVisibility(View.VISIBLE);
                            }else{
                                for (int i=0; i<transactionarray.length();i++){
                                    JSONObject o = transactionarray.getJSONObject(i);
                                    transactionhistoryview item = new transactionhistoryview(

                                            o.getString("amount"),
                                            o.getString("udf3"),
                                            o.getString("created_at"),
                                            o.getString("transaction_id"),
                                            o.getString("response_message"),
                                            o.getString("address_line_1"),
                                            o.getString("state"),
                                            o.getString("id"),
                                            o.getString("name"),
                                            o.getString("order_id")

                                    );
                                    System.out.println("this is the item response");
                                    System.out.println(item);

                                    if((o.getString("response_message").equals("Transaction cancelled"))){
                                        listItems11.add(item);
                                        historyprogress11.setVisibility(View.GONE);
                                    }else{
                                        nodata11.setVisibility(View.VISIBLE);
                                    }

                                }
                                adapter11 = new MyAdapter(listItems11, getApplicationContext(), myMediatorInterface);
                                recyclerView11.setAdapter(adapter11);
                                historyprogress11.setVisibility(View.GONE);
                            }


                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getApplicationContext(), volleyError.getMessage(), Toast.LENGTH_LONG).show()

                    }

                })


        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }
        };



        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }
}