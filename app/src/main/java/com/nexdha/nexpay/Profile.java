package com.nexdha.nexpay;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nexdha.nexpay.constant.SERVER;

public class Profile extends AppCompatActivity {

    TextView profilename,profileemail,profilephone;
    public ImageView imageview12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
      //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_pagefive);

        profileinfo();
        profilename = (TextView) findViewById(R.id.textView23);
        profileemail = (TextView) findViewById(R.id.textView25);
        profilephone = (TextView) findViewById(R.id.textView26);
        imageview12 = (ImageView) findViewById(R.id.imageView12);
        imageview12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeCurrentpage();
            }
        });

    }

    public void closeCurrentpage() {
        finish();
    }

    private void profileinfo(){

        SharedPreferences sharedPreferences = Profile.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

       StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserdetails",
               new Response.Listener<String>() {
                   @Override
                   public void onResponse(String response) {
                       try {
                           JSONObject profileresponse = new JSONObject(response);
                           profilename.setText(profileresponse.getString("name"));
                           profileemail.setText(profileresponse.getString("email"));
                           profilephone.setText(profileresponse.getString("phone"));

                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
                   }
               }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
               String message = "";

               if (error instanceof NetworkError) {
                   message = "cannot connect";
               } else if (error instanceof AuthFailureError) {
                   message = "";
               } else if (error instanceof NoConnectionError) {
                   message = "cannot connect";
               } else if (error instanceof ParseError) {
                   message = "Connection Timedout";
               }
               if (!message.isEmpty()) {
                   Toast.makeText(Profile.this, "", Toast.LENGTH_SHORT).show();
               }
          }
    })

       {

           //-------------------------outerheader-----------------------//
           @Override
           public Map<String, String> getHeaders() throws AuthFailureError{
               String T="" ;
               Map<String,String> params = new HashMap<>();


               // params.put("Content-Type", "application/json; charset=UTF-8");
               try {
                   JSONObject response1 = new JSONObject(token200);
                   System.out.println("!!!!!!!!!!!!!!!!!!");
                   System.out.println(response1.getString("token"));
                   // do your work with response object
                   T=response1.getString("token");


               } catch (JSONException e) {
                   e.printStackTrace();
               }

               System.out.println("44444444444444444444444444");
               System.out.println(token200);
               ///check t!blank

               String TokenS = "token "+ T;
               System.out.println(T);

               params.put("Authorization",TokenS);

               return params;
           }


       };

        RequestQueue requestQueue = Volley.newRequestQueue(Profile.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
}
