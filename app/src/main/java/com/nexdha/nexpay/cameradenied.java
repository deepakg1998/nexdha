package com.nexdha.nexpay;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class cameradenied extends AppCompatActivity {

public Button permission1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_cameradenied);
        permission1=findViewById(R.id.permission1);
        permission1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(cameradenied.this, Upitransaction.class);
                startActivity(intent);

            }
        });

    }
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(cameradenied.this, Upitransaction.class);
        startActivity(intent);

    }
}