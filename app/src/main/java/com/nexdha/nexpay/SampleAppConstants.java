package com.nexdha.nexpay;

import static com.nexdha.nexpay.constant.SERVER;

public class SampleAppConstants {

    //API_KEY is given by the Payment Gateway. Please Copy Paste Here.
    public static final String PG_API_KEY = "bcbd8fc4-261d-4484-9e44-fcf62d6219c1";

    //URL to Accept Payment Response After Payment. This needs to be done at the client's web server.
    public static final String PG_RETURN_URL = SERVER + "/api/save_server_Response";

    //Enter the Mode of Payment Here . Allowed Values are "LIVE" or "TEST".
    public static final String PG_MODE = "LIVE";

    //PG_CURRENCY is given by the Payment Gateway. Only "INR" Allowed.
    public static final String PG_CURRENCY = "INR";

    //PG_COUNTRY is given by the Payment Gateway. Only "IND" Allowed.
    public static final String PG_COUNTRY = "IND";


  //  public static final String PG_AMOUNT = "5000.00";
  //  public static final String PG_EMAIL = "testguru72@gmail.com";
  // public static final String PG_NAME = "test";
  // public static final String PG_PHONE = "9080617193";
  // public static       String PG_ORDER_ID = "";
  //  public static final String PG_DESCRIPTION = "test";
    public static final String PG_CITY = "Bangalore";
    public static final String PG_STATE = "Karnataka";
  //  public static final String PG_ADD_1 = "abc";
 //   public static final String PG_ADD_2 = "abc";
    public static final String PG_ZIPCODE = "123456";
    public static final String PG_BANK_CODE = "UPIU";
    public static final String PG_UDF1 = "udf1";
   // public static final String PG_UDF2 = "udf2";
   //public static final String PG_UDF3 = "udf3";
   // public static final String PG_UDF4 = "udf4";
    public static final String PG_UDF5 = "udf5";


}
