package com.nexdha.nexpay;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nexdha.nexpay.constant.SERVER;

public class Emailotp extends AppCompatActivity {
    EditText emailotpedittext;
    Button verifybutton;
    String eotp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_emailotp);
        sendingemailotp();
        emailotpedittext = (EditText) findViewById(R.id.editText16);





        verifybutton = (Button) findViewById(R.id.button32);
        verifybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = Emailotp.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                final String token1 = sharedPreferences.getString("token", "");
                System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
                System.out.println(token1);
                final String token200 = token1;

                 eotp = emailotpedittext.getText().toString();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/verify_eotp",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject profileresponse = new JSONObject(response);
                                    String verifyotp = profileresponse.getString("Status");
                                    if (verifyotp .equals("Email OTP Matched")){
                                        Toast.makeText(Emailotp.this, "OTP Matched", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(Emailotp.this, "Enter Correct OTP", Toast.LENGTH_SHORT).show();
                                    }
                                   // Toast.makeText(Emailotp.this, "OTP sent to registered email", Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                /*
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
                */

                    }
                })

                {

                    //-------------------------outerheader-----------------------//
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError{
                        String T="" ;
                        Map<String,String> params = new HashMap<>();


                        // params.put("Content-Type", "application/json; charset=UTF-8");
                        try {
                            JSONObject response1 = new JSONObject(token200);
                            System.out.println("!!!!!!!!!!!!!!!!!!");
                            System.out.println(response1.getString("token"));
                            // do your work with response object
                            T=response1.getString("token");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        System.out.println("44444444444444444444444444");
                        System.out.println(token200);
                        ///check t!blank

                        String TokenS = "token "+ T;
                        System.out.println(T);

                        params.put("Authorization",TokenS);

                        return params;
                    }


                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();




                        params.put("eotp", eotp);


                        return params;
                    }


                };

                RequestQueue requestQueue = Volley.newRequestQueue(Emailotp.this);
                stringRequest.setShouldCache(false);
                requestQueue.add(stringRequest);
            }
        });
    }


    //--------------Send Email OTP-------------//
    private void sendingemailotp(){
        SharedPreferences sharedPreferences = Emailotp.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/emailotp",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject profileresponse = new JSONObject(response);
                            Toast.makeText(Emailotp.this, "OTP sent to registered email", Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                /*
                String message = "";

                if (error instanceof NetworkError) {
                    message = "cannot connect";
                } else if (error instanceof AuthFailureError) {
                    message = "";
                } else if (error instanceof NoConnectionError) {
                    message = "cannot connect";
                } else if (error instanceof ParseError) {
                    message = "Connection Timedout";
                }
                if (!message.isEmpty()) {
                    Toast.makeText(Dashboard.this, message, Toast.LENGTH_SHORT).show();
                }
                */

            }
        })

        {

            //-------------------------outerheader-----------------------//
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                String T="" ;
                Map<String,String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T=response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token "+ T;
                System.out.println(T);

                params.put("Authorization",TokenS);

                return params;
            }


        };

        RequestQueue requestQueue = Volley.newRequestQueue(Emailotp.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }
}
