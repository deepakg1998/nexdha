package com.nexdha.nexpay;

import static android.content.ContentValues.TAG;
import static com.nexdha.nexpay.constant.SECRET_KEY;
import static com.nexdha.nexpay.constant.SERVER;
import static com.nexdha.nexpay.constant.url;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.ClientError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class LoginPage extends AppCompatActivity {

    EditText phonenumber;
    Button button2;
    TextView button;
    ProgressBar progresslogin;


    SharedPreferences sharedPreferences;
    RequestQueue queue;
    Dialog loadingDialog;
    LayoutInflater inflater;
    String saltKey = MainActivity.getNativeKey1(), getHashKey;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_login_page);
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingDialog = new Dialog(this);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(inflater.inflate(R.layout.loading_layout, null));
        loadingDialog.setCancelable(false);


        progresslogin = findViewById(R.id.progressBar4);
        progresslogin.setVisibility(View.GONE);
        phonenumber = findViewById(R.id.editText5);
        button = findViewById(R.id.button);
        queue = Volley.newRequestQueue(getApplicationContext());

        button.setOnClickListener(v -> openSignup());
        button2 = (Button) findViewById(R.id.button2);

        button2.setOnClickListener(v -> {
            if (ValidationLogin()) {
                loadingDialog.show();
                try {
                    final String phone = phonenumber.getText().toString().trim();
                    final String result = phone.substring(Math.max(phone.length() - 10, 0)).replaceAll("-", "");
                    getHashKey = result + saltKey;
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    byte[] digest = md.digest(getHashKey.getBytes());
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < digest.length; i++) {
                        sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                    }
                    System.out.println(sb);
                    openLogApi(sb.toString(), result);

                } catch (NoSuchAlgorithmException e) {
                    throw new RuntimeException(e);
                }
             /*   SafetyNet.getClient(this).verifyWithRecaptcha(SITE_KEY)
                        .addOnSuccessListener(this, response -> {
                            if (!Objects.requireNonNull(response.getTokenResult()).isEmpty()) {
                                handleSiteVerify(response.getTokenResult());
                            }
                        })
                        .addOnFailureListener(this, e -> {
                            if (e instanceof ApiException) {
                                Toast.makeText(this, "recaptcha not worked", Toast.LENGTH_SHORT).show();
                                ApiException apiException = (ApiException) e;
                                int status = apiException.getStatusCode();
                                System.out.println("Error code: " + status);
                                Log.d(TAG, "Error message: " +
                                        CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                            } else {
                                Log.d(TAG, "Unknown type of error: " + e.getMessage());
                            }
                        });  */
            }


        });


    }

    private void handleSiteVerify(String responseToken) {
        StringRequest request = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getBoolean("success")) {
                            loadingDialog.show();
                            try {
                                final String phone = phonenumber.getText().toString().trim();
                                final String result = phone.substring(Math.max(phone.length() - 10, 0)).replaceAll("-", "");
                                getHashKey = result + saltKey;
                                MessageDigest md = MessageDigest.getInstance("SHA-512");
                                byte[] digest = md.digest(getHashKey.getBytes());
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < digest.length; i++) {
                                    sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
                                }
                                System.out.println(sb);
                                openLogApi(sb.toString(), result);

                            } catch (NoSuchAlgorithmException e) {
                                throw new RuntimeException(e);
                            }
                            //Toast.makeText(LoginPage.this, "You are not a robot", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginPage.this, "Human verification failed. Please try again later ", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "JSON exception: " + ex.getMessage());

                    }

                },
                error -> {

                    Toast.makeText(LoginPage.this, "Server error, Please try again later", Toast.LENGTH_SHORT).show();
                    finish();


                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("secret", SECRET_KEY.trim());
                params.put("response", responseToken);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);


    }

    private void openLogApi(String sb, String phone) {
        progresslogin.setVisibility(View.GONE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/login",
                response -> {

                    try {
                        JSONObject response1 = new JSONObject(response);
                        // do your work with response object
                        if (response1.getString("status").equals("Number not available")) {
                            loadingDialog.dismiss();
                            Toast.makeText(LoginPage.this, "Number Does Not Exists Please Signup", Toast.LENGTH_LONG).show();
                            progresslogin.setVisibility(View.GONE);
                        } else if (response1.getString("status").equals("Please try after sometime")) {
                            loadingDialog.dismiss();
                            Toast.makeText(LoginPage.this, "Please try after sometime", Toast.LENGTH_LONG).show();
                            progresslogin.setVisibility(View.GONE);
                        } else {
                            loadingDialog.dismiss();
                            Intent intent = new Intent(LoginPage.this, OTP.class);
                            intent.putExtra("FROM_ACTIVITY", "Login");
                            intent.putExtra("PHONE", phone);
                            startActivity(intent);
                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }, error -> {

            System.out.println("!!!!!!!!!!!!!!Error 2!!!!!!!");
            System.out.println(error instanceof ClientError);
            progresslogin.setVisibility(View.GONE);


            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ClientError) {
                message = "Unknown Error occurred";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(LoginPage.this, message, Toast.LENGTH_SHORT).show();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", phone);
                params.put("hash_login", sb);


                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(LoginPage.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }


  /*  public void parseVolleyError(VolleyError error) {
        try {
            String responseBody = new String(error.networkResponse.data, "utf-8");
            JSONObject data = new JSONObject(responseBody);
            System.out.println("This is response Body" + responseBody);

            if (responseBody.contains("phone")){
                System.out.println("Phone");
                String storedata = data.getString("phone");
                if (storedata.contains("user with this phone already exists.")){
                    Toast.makeText(LoginPage.this,"Mobile Number Exists, Please Login",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(LoginPage.this, SignUp.class);
                    startActivity(intent);
                    progresslogin.setVisibility(View.GONE);
                }
            }




        } catch (JSONException e) {
        } catch (UnsupportedEncodingException errorr) {
        }
    }

    */


    public void openSignup() {
        Intent intent = new Intent(this, SignUp.class);
        startActivity(intent);
        finish();
    }
    private void openotppage(){
        SharedPreferences sharedPreferences = LoginPage.this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String token1 = sharedPreferences.getString("token", "");
        System.out.println("----------------------------------------------------------------------");
        System.out.println(token1);
        final String token200 = token1;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SERVER + "/api/getuserid",
                response -> {
                    System.out.println("***************openotppage*******************");
                    System.out.println(response);

                }, error -> {
            String message = "";
            if (error instanceof NetworkError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ServerError) {
                message = "";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof ParseError) {
                message = "Cannot connect to internet......please check your internet connection";
            } else if (error instanceof TimeoutError) {
                message = "Connection Timedout......please check your internet connection";
            }
            if (!message.isEmpty()) {
                Toast.makeText(LoginPage.this, message, Toast.LENGTH_SHORT).show();
            }
        })

        {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String T = "";
                Map<String, String> params = new HashMap<>();


                // params.put("Content-Type", "application/json; charset=UTF-8");
                try {
                    JSONObject response1 = new JSONObject(token200);
                    System.out.println("!!!!!!!!!!!!!!!!!!");
                    System.out.println(response1.getString("token"));
                    // do your work with response object
                    T = response1.getString("token");


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("44444444444444444444444444");
                System.out.println(token200);
                ///check t!blank

                String TokenS = "token " + T;
                System.out.println(T);

                params.put("Authorization", TokenS);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(LoginPage.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }



    private boolean ValidationLogin(){
        final String phone = phonenumber.getText().toString().trim();

        if (phone.isEmpty() || phone.length() == 0 || phone.equals("") || phone == null){
            Toast.makeText(LoginPage.this,"Please Enter Mobile Number ",Toast.LENGTH_LONG).show();
            return false;
        }


      /*  if (phone.isEmpty() || phone.length() != 10){
            Toast.makeText(LoginPage.this,"Please Enter Correct Phone Number ",Toast.LENGTH_LONG).show();
            return false;
        }

       */


        return true;

    }

}
